################################################
# Level: trunk/CMakeLists.txt
################################################

cmake_minimum_required(VERSION 2.8)
project(prime-phylo CXX)

SET(CMAKE_BUILD_TYPE Debug)

set(CPACK_PACKAGE_VERSION_MAJOR "1")
set(CPACK_PACKAGE_VERSION_MINOR "0")
set(CPACK_PACKAGE_VERSION_PATCH "11")

set(PACKAGE_VERSION ${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH})
set(CPACK_SOURCE_PACKAGE_FILE_NAME ${PROJECT_NAME}-${PACKAGE_VERSION})
set(CPACK_GENERATOR RPM;DEB)

set(tmp_man_pages_dir  ${CMAKE_BINARY_DIR}/tmp_manpages)

set(TESTDATA_DIR "${CMAKE_SOURCE_DIR}/example_data/testdata_for_automatic_tests")
set(date_for_man_pages "13 Mar, 2012")

include(${CMAKE_SOURCE_DIR}/cmake/macro.cmake)

# is these packages dependencies on Ubuntu 11.10?
# libplot2c2
# libboost-mpi1.46.1 
# libboost-serialization1.46.1

set(CPACK_DEBIAN_PACKAGE_DEPENDS "libxml2 (>= 2.6.16 ), liblapack3gf (>= 3.0.0),  libatlas3gf-base, libgfortran3, libopenmpi1.3, libquadmath0, libstdc++6, zlib1g")

# how can we control the filename of the produced DEBIAN package?
# CPACK_DEBIAN_PACKAGE_VERSION CPACK_DEBIAN_PACKAGE_NAME don't seem to change the filename.
#




# Some of the dependencies in CPACK_DEBIAN_PACKAGE_DEPENDS were found by running this command:
# for i in `ls /tmp/primeinstall/bin/*`; do ldd $i; done | awk '{print $3;}' | xargs -i dpkg -S {}

set(CPACK_SOURCE_GENERATOR TGZ;ZIP)
# set(CPACK_RPM_PACKAGE_ARCHITECTURE "i386")
set(CPACK_PACKAGE_CONTACT "erik.sjolund@sbc.su.se") 
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "prime-phylo is a software package for probabilistic integrated models of evolution. The homepage is located at http://prime.sbc.su.se/. It is written in the programming language C++.")
set(CPACK_PACKAGE_VENDOR "")


#configure_file(debian/control.cmake @CMAKE_BINARY_DIR@/debian/control @ONLY)

# A tip that could be useful if we want to include generated files into the target "package_source":
# How to include a generated file into the target "package_source": A tip from
# http://www.cmake.org/pipermail/cmake/2011-January/041850.html

set(CPACK_SOURCE_IGNORE_FILES "\\\\.svn/" "/deprecated_files/" "/docbook/" "/Scripts/" "/primeOrthology/" "/.cdtproject" "/excluded_from_release/" "/example_data/" "/.cproject" "/.project") 
include(CPack)

#add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/${PROJECT_NAME}-${PACKAGE_VERSION}.tar.gz)
#  COMMAND make package_source
#  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
#)

#add_custom_target(ports 
#  COMMAND  
#  DEPEND ${CMAKE_BINARY_DIR}/${PROJECT_NAME}-${PACKAGE_VERSION}.tar.gz
#  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
#)

#file(<MD5|SHA1|SHA224|SHA256|SHA384|SHA512> filename variable)

macro(my_find_program progname)
  find_program(${progname}_EXECUTABLE ${progname})
  if(NOT ${progname}_EXECUTABLE)
    message(FATAL_ERROR  "${progname} not found" )
  endif()
endmacro(my_find_program)

#my_find_program(xsltproc)
my_find_program(gengetopt)

find_program(svnversion_EXECUTABLE svnversion)
if(svnversion_EXECUTABLE)
  execute_process( COMMAND ${svnversion_EXECUTABLE} -n ${CMAKE_SOURCE_DIR} OUTPUT_VARIABLE SVN_VERSION_OUTPUT)
  if(SVN_VERSION_OUTPUT MATCHES "exported")
    set(SVN_REVISION UNKNOWN)
  else()
    set(SVN_REVISION "${SVN_VERSION_OUTPUT}")
  endif()
else()
    set(SVN_REVISION UNKNOWN)
endif()

if(SVN_REVISION)
  set(SVN_REVISION_STRING "( subversion revision: ${SVN_REVISION} )")
endif()

set(VERSION_FOR_HELP_OUTPUT "${PACKAGE_VERSION} ${SVN_REVISION_STRING}")

option(BUILD_DOCUMENTATION "build Docbook and Doxygen html documentation" OFF )

if(EXISTS ${CMAKE_SOURCE_DIR}/example_data)
  enable_testing()
endif()

add_subdirectory(src/cxx)
#add_subdirectory(src/docbook)
add_subdirectory(src/perl)

#configure_file(packaging/macports/Portfile.cmake  @CMAKE_BINARY_DIR@/packaging/macports/Portfile.without_checksums @ONLY)

#file(MAKE_DIRECTORY @CMAKE_BINARY_DIR@/packaging/macports/ports/science/${PROJECT_NAME})
#configure_file(packaging/macports/fix_checksums_and_publish_on_web2.sh.cmake @CMAKE_BINARY_DIR@/packaging/macports/fix_checksums_and_publish_on_web2.sh @ONLY)

file(GLOB manpages ${tmp_man_pages_dir}/[^.]*[^~])
foreach(i ${manpages})
  install(FILES ${i} DESTINATION share/man/man1)
endforeach()


if(EXISTS @CMAKE_SOURCE_DIR@/excluded_from_release)
  set(BUILD_FOR_DEBIAN_OR_UBUNTU "ubuntu" CACHE STRING "The command ${CMAKE_BINARY_DIR}/build_debian_package.sh will build for distribution type (debian or ubuntu)")
  set(BUILD_FOR_DEBIAN_OR_UBUNTU_RELEASE "precise" CACHE STRING "The command ${CMAKE_BINARY_DIR}/build_debian_package.sh will build for this debian/ubuntu release")
  set(BUILD_FOR_DEBIAN_OR_UBUNTU_ARCH "amd64" CACHE STRING "The command ${CMAKE_BINARY_DIR}/build_debian_package.sh will build for this debian/ubuntu arch")
  set(CPU_COUNT_FOR_BUILD_IN_VAGRANT "1" CACHE STRING "The command ${CMAKE_BINARY_DIR}/build_debian_package.sh will use this number of CPUs when building in Vagrant")
  if(@BUILD_FOR_DEBIAN_OR_UBUNTU_ARCH@ STREQUAL "amd64")
    set(vagrant_box "precise64")
  else()
    set(vagrant_box "precise32")
  endif()
  set(vagrant_dir "${CMAKE_BINARY_DIR}/vagrant_debian_build")
  configure_file(excluded_from_release/packaging/debian/build_debian_package.sh.cmake ${CMAKE_BINARY_DIR}/build_debian_package.sh @ONLY)
  file(MAKE_DIRECTORY "${vagrantdir}")
  configure_file(excluded_from_release/packaging/debian/Vagrantfile.cmake "${vagrant_dir}/Vagrantfile" @ONLY)
  configure_file(excluded_from_release/packaging/debian/provision.sh.cmake "${vagrant_dir}/provision.sh" @ONLY)
endif()