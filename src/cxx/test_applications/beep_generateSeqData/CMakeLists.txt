include(${CMAKE_SOURCE_DIR}/src/cxx/cmake-include.txt)

include_directories( ${MPI_INCLUDE_PATH} )
include_directories( ${MPI_INCLUDE_PATH}/openmpi/ompi/mpi/cxx )

add_executable(${programname_of_this_subdir} beep_generateSeqData.cc
${CMAKE_CURRENT_BINARY_DIR}/cmdline.c   ${CMAKE_CURRENT_BINARY_DIR}/cmdline.h
)


target_link_libraries(${programname_of_this_subdir} prime-phylo prime-phylo-sfile )
install(TARGETS ${programname_of_this_subdir} DESTINATION bin)

