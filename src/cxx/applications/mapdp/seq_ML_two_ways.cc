#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>
#include <cmath>

#include "Beep.hh" // Used ?
#include "BirthDeathMCMC.hh"
#include "Density2P.hh"
#include "DP_ML.hh"
#include "DPLengthFactorizer.hh"
#include "DummyMCMC.hh"
#include "EdgeRateMCMC_common.hh"
#include "EdgeWeightHandler.hh"
#include "GammaDensity.hh"
#include "GammaMap.hh"
#include "InvGaussDensity.hh"
#include "LambdaMap.hh"
#include "LogNormDensity.hh"
#include "MatrixTransitionHandler.hh"


#include "MCMCModel.hh"
#include "PRNG.hh"
#include "Probability.hh"

#include "PRNG.hh"
#include "ReconciliationTimeMCMC.hh"
#include "ReconciliationTimeSampler.hh"
#include "SeqIO.hh"
#include "SequenceData.hh"
#include "SimpleMCMC.hh"
#include "SimpleML.hh"
#include "SiteRateHandler.hh"
#include "StrStrMap.hh"
#include "SubstitutionMCMC.hh"
#include "Tree.hh"
#include "RandomTreeGenerator.hh"
#include "TreeIO.hh"
#include "TreeMCMC.hh"
#include "UniformDensity.hh"

//#include <boost/numeric/ublas/matrix.hpp>
//#include <boost/multi_array.hpp>
//#include <boost/numeric/ublas/io.hpp>

// Global options with default settings
//------------------------------------
int nParams = 2;

char* outfile=NULL;
unsigned MaxIter = 50001;
unsigned Thinning = 1000;
unsigned printFactor = 1;
unsigned RSeed = 0;
bool quiet = false;
bool show_debug_info = false;
bool do_likelihood = false;

std::string seqModel = "jc69";
std::string seqType;
std::vector<double> Pi;
std::vector<double> R;
unsigned n_cats = 1;
double alpha = 1.0;
bool fixedAlpha = false;

char* tree_file          = 0; //NULL
bool fixed_tree          = false;
std::vector<std::string> outgroup;

std::string rateModel = "Const";
std::string density = "Uniform";
std::map<unsigned, beep::Real> start_rates;
double mean = -1.0;
double variance = -1.0;
bool fixed_rateparams = false;
bool fixed_mean = false;
double maxP = -1.0;

bool fixed_times = false;
std::map<unsigned, beep::Real> start_times;
bool fixed_bdrates = false;
double birthRate = -1.0;
double deathRate = -1.0;

// allowed models
std::vector<std::string> subst_models;
std::vector<std::string> rate_models;
std::vector<std::string> rate_densities;

// osv stuff
double uniformIntervalMax = 3.0;
bool do_r_t_partition = false;
bool do_interupt = false;
unsigned noOfDiscrIntervals = 200;
double DPratio = 0.001;
 
using namespace beep;
using namespace std;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);

int
main(int argc, char **argv) 
{  
  // Set up allowed models
  //rate model
  subst_models.push_back("UNIFORMAA");
  subst_models.push_back("JTT");
  subst_models.push_back("UNIFORMCODON");
  subst_models.push_back("ARVECODON");
  subst_models.push_back("JC69");
  
  //rate model
  rate_models.push_back("iid");
  rate_models.push_back("gbm");
  rate_models.push_back("const");
  
  //rate model
  rate_densities.push_back("Uniform");
  rate_densities.push_back("Gamma");
  rate_densities.push_back("LogN");
  rate_densities.push_back("InvG");

  if (argc < nParams) 
    {
      usage(argv[0]);
    }
  try
    { 
      int opt = readOptions(argc, argv);
      //       if(opt + 2 > argc)
      // 	{
      // 	  cerr << "Too few arguments\n";
      // 	  usage(argv[0]);
      // 	  exit(1);
      // 	}
      

      // Set up random generator
      //---------------------------------------------------------
      PRNG rand;
      if (RSeed != 0)
	{
	  rand.setSeed(RSeed);
	}

      // Set up the SubstitutionMatrix
      //---------------------------------------------------------
      MatrixTransitionHandler* Q;
      if(seqModel == "USR")
	{
	  Q = new MatrixTransitionHandler(MatrixTransitionHandler::userDefined(seqType, Pi, R)); //So Stupid way of creating an instance!!!
	}
      else
	{
	  Q = new MatrixTransitionHandler(MatrixTransitionHandler::create(seqModel));
	}

      //Get tree and Data
      //---------------------------------------------

      cout << "Get tree and Data\n";
      string datafile(argv[opt++]);
      SequenceData D = SeqIO::readSequences(datafile, Q->getType());

      Tree* G;
      if(tree_file)
	{
	  TreeIO io = TreeIO::fromFile(tree_file);
	  bool readEdgeTimes = fixed_times && start_times.empty();
	  G = new Tree(io.readBeepTree(readEdgeTimes, false, false, readEdgeTimes, 0, 0));
	}
      else
	{
	  G = new Tree(RandomTreeGenerator::generateRandomTree(D.getAllSequenceNames()));
	}

      G->setName("G");
      

      Tree S = Tree::EmptyTree();
      S.setName("S");

      BranchSwapping bs;

      // Set any user defined times
      if(start_times.empty() == false)
	{
	  Node* n = 0; 
	  if(start_times.size() != G->getNumberOfLeaves() - 2)
	    {
	      cerr << "\nError: The number of times given in -Gt, "
		   << start_times.size()
		   << " does not comply with number of internal nodes in tree, "
		   << G->getNumberOfLeaves() - 2
		   << "\n\n";
	      exit(23);
	    }
	  for(map<unsigned, Real>::const_iterator i = start_times.begin();
	      i != start_times.end(); i++)
	    {
	      if(i->first >= G->getNumberOfNodes())
		{
		  cerr << "\nError: Trying to set time for vertex "
		       << i->first
		       << ", which do not exist in tree\n\n";
		  exit(23);
		}
	      n = G->getNode(i->first);
	      if(n->isLeaf())
		{
		  cerr << "\nWarning: The time submitted for leaf "
		       << n->getNumber()
		       << " will be ignored! Leavf's time = 0\n\n";
		}
	      if(n->isRoot())
		{
		  cerr << "\nWarning: The time submitted for root "
		       << n->getNumber()
		       << " will be ignored! Root's time = 1.0\n\n";
		}
	      else
		{
		  cerr << "i: " << n->getNumber() << " i_time: " << i->second << "\n";
		  n->setNodeTime(i->second);
		  cerr << "i2: " << n->getNumber() << "\n";
		}
	    }
	  G->getRootNode()->setNodeTime(1.0);
	}

      cout << G->print(true,true,true,true);

      DummyMCMC dm;

      if (do_r_t_partition)
	{
	  // Set up a gamma and gs
	  StrStrMap gs;
	  // Map all leaves of G to the single leaf in S
	  for(unsigned i = 0; i < G->getNumberOfNodes(); i++)
	    {
	      gs.insert(G->getNode(i)->getName(), S.getRootNode()->getName());
	    }
	  
	  LambdaMap lambda(*G, S, gs);
	  GammaMap gamma = GammaMap::MostParsimonious(*G, S, lambda);
	  
	  if(birthRate <= 0 || deathRate <= 0)
	    {
	      Real max_rate = MAX_INTENSITY / S.getRootNode()->getTime();
	      birthRate = max_rate * rand.genrand_real3();
	      deathRate = max_rate * rand.genrand_real3();
	    }
	  
	  BirthDeathProbs bdp(S,birthRate,deathRate);
	  //GuestTreeMCMC gtm(dm, *G, gs, bdp, 1.0);
	  UniformTreeMCMC gtm(dm, *G, "G", 3.0);
	  if(fixed_tree)
	    {
	      gtm.fixTree();
	      gtm.fixRoot();
	    }

	  Real topTime = 1.0;

	  BirthDeathMCMC bdm(gtm, S, birthRate, deathRate, &topTime);
	  ReconciliationTimeMCMC rtm(bdm, *G, bdm, gamma, "EdgeTimes", 0.001);
	  //ReconciliationTimeMCMC rtm(bdm, *G, bdm, gamma, "EdgeTimes", 1.0);
	  
	  //Create a temporary object to initiate times of G 
	  if(fixed_times)
	    {
	      rtm.fixTimes();
	    }
	  else if(start_times.empty())
	    {
	      //	  ReconciliationTimeSampler sampler(*G, gs, bdm, gamma, dm.getPRNG());
	      ReconciliationTimeSampler sampler(*G, bdm, gamma);
	      cerr << G->print(true,true,true,true);
	      sampler.sampleTimes(false);
	    }
	  
	  //Set up mean and variance of substitution rates
	  //---------------------------------------------------------
	  
	  if(maxP <= 0) 
	    {
	      maxP = 5.0 / G->rootToLeafTime();
	    }
	  if(mean <=0)
	    {
	      mean = maxP * rand.genrand_real3(); 
	    }
	  if(variance <=0)
	    {
	      variance = mean * rand.genrand_real3()/10; 
	    }
	  
	  // Set up density function for rates
	  //---------------------------------------------------------
	  Density2P* df;
	  capitalize(density);
	  if(density == "INVG")
	    {
	      df = new InvGaussDensity(mean, variance);
	    }
	  else if(density == "LOGN")
	    { 
	      df = new LogNormDensity(mean, variance);
	    }
	  else if(density == "GAMMA")
	    {
	      cerr << "Do Gamma\n";
	      df = new GammaDensity(mean, variance);
	    }
	  else if(density == "UNIFORM")
	    {
	      df = new UniformDensity(0, 10, true);
	      cerr << "*******************************************************\n"
		   << "Note! mean and variance will always be fixed when using\n"
		   << "UniformDensity, the default interval will be (0,10)\n"
		   << "You might want to use the -Ef option\n"
		   << "*******************************************************\n";
	      fixed_rateparams = true;
	    }
	  else
	    {
	      cerr << "Expected 'InvG', 'LogN', 'Gamma' or 'Const' "
		   << "for option -d\n";
	      usage(argv[0]);
	      exit(1);
	    }

	  EdgeRateMCMC* erm;
	  //erm = new iidRateMCMC(rtm, *df, *G, "EdgeRates");
	  erm = new iidRateMCMC(rtm, *df, *G, "EdgeRates", 0.001);
// 	  gtm.setRateMCMC(erm);

	  // Start with generating rates, because if user only defines rates for 
	  // some nodes, we want random values for the other!
	  erm->generateRates();  // Get random start values for rates 

	  // Set any user defined rates
	  if(start_rates.empty() == false)
	    {
	      if(rateModel == "const")
		{
		  if(start_rates.size() > 1)
		    {
		      cerr << "\nWarning: Only the first rate, "
			   << start_rates.begin()->second
			   << " will be used, since rate\n is constant "
			   <<"over tree. Remaining rates given are ignored\n\n";
		    }
		  erm->setRate(start_rates.begin()->second, 0);
		}
	      else
		{
		  Node* n = 0; 
		  Node* rc = 0;
		  for(map<unsigned, Real>::const_iterator i = start_rates.begin();
		      i != start_rates.end(); i++)
		    {
		      if(i->first >= G->getNumberOfNodes())
			{
			  cerr << "\nError: Trying to set rate for vertex "
			       << i->first
			       << ", which do not exist in tree\n\n";
			  exit(23);
			}
		      n = G->getNode(i->first);
		      if(n->isRoot())
			{
			  cerr << "\nWarning: The rate submitted for root's incoming "
			       << "edge will be ignored!\n\n";
			}
		      else
			{
			  if(n->getParent()->isRoot())
			    {
			      if(rc)
				{
				  cerr << "\nWarning: The rate given for root's child node "
				       << n->getNumber()
				       << "\nwill overwrite the rate of root's child node"
				       << rc->getNumber() << "!\n\n";
				}
			      else
				{
				  rc = n;
				}
			    }
			  erm->setRate(i->second, n);
			}
		    }
		}
	    }

	  if(fixed_rateparams)
	    {
	      erm->fixMean();
	      erm->fixVariance();
	    }

	  if(fixed_mean)
	    {
	      erm->fixMean();
	    }
		  
	  EdgeTimeRateHandler etrh(*erm);

	  UniformDensity uf(0, uniformIntervalMax, true); // U[0, 3]

	  cerr << G->print(true,true,true,true);
		  
	  if (do_interupt)
	    {
	      cout << "run green combined variant !\n";
	      DPLengthFactorizer dplf(bdm, *G, S, rtm, *erm, *df, birthRate, deathRate, noOfDiscrIntervals, DPratio);
	      ConstRateMCMC alphaC(dplf, uf, *G, "Alpha"); 
	      //ConstRateMCMC alphaC(dplf, uf, *G, "Alpha");  // Cheating -- must be fixed!!!

	      if(n_cats == 1 || fixedAlpha)
		{
		  alphaC.fixRates();
		  alphaC.setRate(alpha, 0);
		}

	      SiteRateHandler srm(n_cats, alphaC);

	      vector<string> partitionList ; 
	      partitionList.push_back(string("all"));

	      SubstitutionMCMC sm(alphaC, D, *G, srm, *Q, etrh, partitionList);
	      //NotSoSimpleML ML_iterator(sm, Thinning);
	      SimpleML ML_iterator(sm, Thinning);
	      //SimpleMCMC ML_iterator(sm, Thinning);

	      if (do_likelihood)
		{
		  cout << "sm.currentStateProb(): " << sm.currentStateProb() << endl;
		  exit(0);
		}      

	      if (outfile != NULL)
		{
		  try 
		    {
		      ML_iterator.setOutputFile(outfile);
		    }
		  catch(AnError e)
		    {
		      e.action();
		    }
		  catch (int e)
		    {
		      cerr << "Problems opening output file! ('"
			   << outfile
			   << "') Using stdout instead.\n";
		    }
		}  

	      if (quiet)
		{
		  ML_iterator.setShowDiagnostics(false);
		}
      
	      if (!quiet) 
		{
		  cout << "#Running: ";
		  for(int i = 0; i < argc; i++)
		    {
		      cout << argv[i] << " ";
		    }
		  cout << " in directory"
		       << getenv("PWD")
		       << "\n#\n";
		  cout << "#Start MCMC (Seed = " << RSeed << ")\n";
#ifdef FIX_ROOT_EDGES_RATE
		  cout << "NOTE! In this version of beep_rates, the edge rates of\n"
		       << "root's both children are fixed during analysis (note that\n"
		       << "also for the gbm model this relates to the 'edge rate').\n";
#endif

		}

	      ML_iterator.iterate(MaxIter, printFactor);

	      // Specializations for parameter retrieval test
	      cerr << G->print(true,true,true,true);
	      
	      for(unsigned nIndex = 0; nIndex <= (G->getNumberOfNodes()-1); nIndex++) 
		{
		  Node* n = G->getNode(nIndex);
		  unsigned nParentIndex = 0;
		  Real nSubstRate = 0.0;
		  Real nNodeTime = 0.0;
		  Real nEdgeTime = 0.0;
		  if (!n->isRoot())
		    {
		      nSubstRate = G->getRate(*n);
		      nNodeTime = G->getTime(*n);
		      nEdgeTime = G->getEdgeTime(*n);
		      Node* nParent = n->getParent();
		      nParentIndex = nParent->getNumber();
		    }
		  Real nLength = nSubstRate * nEdgeTime;
		  cout << "nIndex: " << nIndex << " nParentIndex: " << nParentIndex << " nSubstRate: " << nSubstRate << " nNodeTime: " << nNodeTime << " nEdgeTime: " << nEdgeTime << " nLength: " << nLength << "\n";
		}
		 
	    }
	  else
	    {
	      cout << "run red r*t variant !\n";
	      ConstRateMCMC alphaC(*erm, uf, *G, "Alpha"); 
	      if(n_cats == 1 || fixedAlpha)
		{
		  alphaC.fixRates();
		  alphaC.setRate(alpha, 0);
		}

	      SiteRateHandler srm(n_cats, alphaC);

	      vector<string> partitionList ; 
	      partitionList.push_back(string("all"));

	      SubstitutionMCMC sm(alphaC, D, *G, srm, *Q, etrh, partitionList);
		  
	      //NotSoSimpleML ML_iterator(sm, Thinning);
	      SimpleML ML_iterator(sm, Thinning);
	      //SimpleMCMC ML_iterator(sm, Thinning);

	      if (do_likelihood)
		{
		  cout << "sm.currentStateProb(): " << sm.currentStateProb() << endl;
		  exit(0);
		}      

	      if (outfile != NULL)
		{
		  try 
		    {
		      ML_iterator.setOutputFile(outfile);
		    }
		  catch(AnError e)
		    {
		      e.action();
		    }
		  catch (int e)
		    {
		      cerr << "Problems opening output file! ('"
			   << outfile
			   << "') Using stdout instead.\n";
		    }
		}  

	      if (quiet)
		{
		  ML_iterator.setShowDiagnostics(false);
		}
      
	      if (!quiet) 
		{
		  cout << "#Running: ";
		  for(int i = 0; i < argc; i++)
		    {
		      cout << argv[i] << " ";
		    }
		  cout << " in directory"
		       << getenv("PWD")
		       << "\n#\n";
		  cout << "#Start MCMC (Seed = " << RSeed << ")\n";
#ifdef FIX_ROOT_EDGES_RATE
		  cout << "NOTE! In this version of beep_rates, the edge rates of\n"
		       << "root's both children are fixed during analysis (note that\n"
		       << "also for the gbm model this relates to the 'edge rate').\n";
#endif

		}

	      ML_iterator.iterate(MaxIter, printFactor);

	      	  
	      // Specializations for parameter retrieval test
	      cerr << G->print(true,true,true,true);
	      
	      for(unsigned nIndex = 0; nIndex <= (G->getNumberOfNodes()-1); nIndex++) 
		{
		  Node* n = G->getNode(nIndex);
		  unsigned nParentIndex = 0;
		  Real nSubstRate = 0.0;
		  Real nNodeTime = 0.0;
		  Real nEdgeTime = 0.0;
		  if (!n->isRoot())
		    {
		      nSubstRate = G->getRate(*n);
		      nNodeTime = G->getTime(*n);
		      nEdgeTime = G->getEdgeTime(*n);
		      Node* nParent = n->getParent();
		      nParentIndex = nParent->getNumber();
		    }
		  Real nLength = nSubstRate * nEdgeTime;
		  cout << "nIndex: " << nIndex << " nParentIndex: " << nParentIndex << " nSubstRate: " << nSubstRate << " nNodeTime: " << nNodeTime << " nEdgeTime: " << nEdgeTime << " nLength: " << nLength << "\n";
		}

	    }	  
	  delete df;
	  delete erm;
	}
      else
	{
	  cout << "run blue l variant !\n";
	  UniformTreeMCMC utm(dm, *G, "G", 1.0);
	  utm.fixRoot(); // This means that we in practice work on unrooted trees
	  utm.fixTree();

	  // Set up density function for rates
	  UniformDensity uf(0, uniformIntervalMax, true); // U[0, 2] // Use this both for EdgeRateMCMC and ConstRateMCMC...

	  // Set up density function for rates
	  GammaDensity df(mean, variance); // ... and this only for DP 

	  EdgeRateMCMC* erm;
	  erm = new iidRateMCMC(utm, uf, *G, "EdgeRates");
	  erm->fixMean();
	  erm->fixVariance();

	  //	      EdgeTimeRateHandler ewh(*erm);
	  EdgeWeightHandler ewh(*erm);

	  ConstRateMCMC alphaC(*erm, uf, *G, "Alpha"); 
	  alphaC.fixRates();
	  alphaC.setRate(alpha, 0);
	  SiteRateHandler srm(1, alphaC);

	  vector<string> partitionList ; 
	  partitionList.push_back(string("all"));

	  SubstitutionMCMC sm(alphaC, D, *G, srm, *Q, ewh, partitionList);

	  // NotSoSimpleML ML_iterator(sm, Thinning);
	  SimpleML ML_iterator(sm, Thinning);
	  ML_iterator.iterate(MaxIter, printFactor);
	  Probability lengthsProb = ML_iterator.getLocalOptimum();
	  cout << "lengthsProb: " << lengthsProb << "\n";

	  vector<double> lengths;
	  for (unsigned nIndex = 0; nIndex <= (G->getNumberOfNodes()-1); nIndex++)
	    {
	      Node* n = G->getNode(nIndex);
	      if (!n->isRoot())
		{
		  lengths.push_back(erm->getRate(n));
		  //		      cout << "nIndex: " << nIndex << " nLength: " << erm->getRate(n) << "\n";
		}
	      else
		{
		  lengths.push_back(0.0);
		  //		      cout << "nIndex: " << nIndex << " isRoot!\n";
		}
	    }
		  
	  DP_ML optimizer(*G,S,&df,birthRate,deathRate, noOfDiscrIntervals);
	  Probability DPProb = optimizer.DP(G->getRootNode()->getNumber(),true,false,lengths);
	  Probability totalLengthsProb = lengthsProb * DPProb;
	  optimizer.backTrace((G->getRootNode()->getNumber()),noOfDiscrIntervals);
	  cout << "DPProb: " << DPProb << " totalLengthsProb: " << totalLengthsProb << "\n";

	  //	      cerr << "writeG():\n"
	  //	   << G->print(true, true, true)
	  //	   << endl;
	}
      delete Q;
    }
  catch(AnError e)
    {
      cerr <<" error\n";
      e.action(); 
    }
}


void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <datafile> \n"
    << "\n"
    << "Parameters:\n"
    << "   <datafile>         a string\n"

    << "Options:\n"
    << "   -u, -h                This text.\n"
    << "   -o <filename>         output file\n"
    << "   -i <float>            number of iterations\n"
    << "   -t <float>            thinning\n"  
    << "   -w <float>            Write to cerr <float> times less often than\n"
    << "                         to cout\n"  
    << "   -s <int>              Seed for pseudo-random number generator. If\n"
    << "   -q                    Do not output diagnostics to stderr.\n"
    << "   -d                    Debug info.\n"
    << "   -l                    Output likelihood. No MCMC.\n"
    << "   -r                    Rates and times. The alternative is lengths\n"
    << "   -m                    ML-type factorization\n"
    << "   -S<option>            Options related to Substitution model\n"
    << "     -Sm <'JC69'/'UniformAA'/'JTT'/'UniformCodon'/'ArveCodon'>\n"
    << "                         the substitution model to use, (JC69 is the\n"
    << "                         default). Must fit data type \n"
    << "     -Su <datatype='DNA'/'AminoAcid'/'Codon'> <Pi=float1 float2 ... floatn>\n"
    << "                         <R=float1 float2 ...float(n*(n-1)/2)>\n"
    << "                         the user-defined substitution model to use.\n"
    << "                         The size of pi and R must fit data type (DNA: n=4\n"
    << "                         AminoAcid: n=20, Codon: n = 62), respectively. \n"
    << "                         If both -Su and -Sm is given (don't do this!),\n"
    << "                         only the last given is used\n"
    << "     -Sn <float>         nCats, the number of discrete rate\n"
    << "                         categories (default is one category)\n"
    << "     -Sa <float>         alpha, the shape parameter of the Gamma\n"
    << "                         distribution for site rates (default: 1)\n"
    //     << "     -p <float>         probability of a site being invarant\n"
    << "   -E<option>            Options relating to edge rate model\n"
    << "     -Em <'iid'/'gbm'/'const'> \n"
    << "                         the edge rate model to use (default: const)\n"
    << "     -Ed <'Gamma'/'InvG'/'LogN'/'Uniform'> \n"
    << "                         the density function to use for edge rates,\n"
    << "                         (Uniform is the default) \n"
    << "     -Ep <float> <float> start mean and variance of edge rate model\n"
    << "     -Ef <float> <float> fixed mean and variance of edge rate model\n"
    << "     -Ea <float>         fixed mean of edge rate model\n"
    << "     -Er { <node#=edgerate> <node#=edgerate> ... }\n"
    << "                         User defined edge rates for tree in tree\n"
    << "                         file. Root's rate is ignored. If rates are \n"
    << "                         given for both root'children, one is ignored.\n" 
    << "                         (note spaces after and before '}'\n"
    << "     -El <float>         User-defined (fixed) max value of rate params\n"
    << "   -G<option>            Options related to the tree\n"
    << "     -Gg                 fix tree. If not set, NNI branch-swapping\n"
    << "                         will be performed on the tree\n"
    << "     -Gi <tree_file>      use tree in file <tree_file> as start tree\n"
    << "     -Go { <outgroup1> <outgroup2> ... }\n"
    << "                         Root tree at smallest subtree containing\n"
    << "                         leaves given by Go's argument (note spaces\n"
    << "                         after '{'and before '}'\n"
    << "     -Gf                 fix edge times. If not set, a birth-death\n"
    << "                         model is used as a prior of the edge rates\n"
    << "     -Gt { <node#=nodetime> <vertex#=nodetime> ... }\n"
    << "                         User defined nodetimes for tree in tree_file\n"
    << "                         Times for all internal vertices except root/n"
    << "                         need to be set to avoid inconsistencies\n"
    << "                         (note spaces after and before '}'\n"
    << "   -B<option>            Options related to the birth death process\n"
    << "     -Bf <float> <float> fix the birth and death rates of the birth-\n"
    << "                         death model to these values \n"
    << "     -Bp <float> <float> start value of the birth and death rate parameters\n"
    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while(opt < argc && argv[opt][0] == '-') 
    {	    
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'i':
	  {
	    if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	      {
		cerr << "Expected integer after option '-i'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 't':
	  {
	    if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	      {
		cerr << "Expected integer after option '-t'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'w':
	  {
	    if (sscanf(argv[++opt], "%d", &printFactor) == 0)
	      {
		cerr << "Expected integer after option '-p'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 's':
	  if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	    {
	      cerr << "Expected integer after option '-s'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	case 'q':
	  {
	    quiet = true;
	    break;
	  }
	case 'd':
	  {
	    show_debug_info = true;
	    break;
	  }
	case 'l':
	  {
	    do_likelihood = true;
	    break;
	  }
	case 'r':
	  {
	    do_r_t_partition = true;
	    break;
	  }
	case 'm':
	  {
	    do_interupt = true;
	    break;
	  }
	case 'S':
	  {
	    switch(argv[opt][2])
	      {
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      seqModel = argv[++opt];
		      capitalize(seqModel);
		      if(find(subst_models.begin(), subst_models.end(), seqModel) 
			 == subst_models.end())
			{
			  cerr << "Expected 'UniformAA', 'JTT', 'UniformCodon' "
			       << "or 'JC69' for option -Sm\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		    }
		  else
		    {
		      cerr << "Expected seqModel after option '-m'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'u':
		{
		  if (opt + 1 < argc)
		    {
		      seqModel = "USR";
		      seqType = argv[++opt];
		      capitalize(seqType);
		      int dim = 0;
		      if(seqType == "DNA")
			dim = 4;
		      else if(seqType == "AMINOACID")
			dim = 20;
		      else if(seqType == "CODON")
			dim = 61;
		      else
			{
			  cerr << seqType
			       << " is not a valid data type for option -Su\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		      if(opt + (dim * (dim + 1) / 2) < argc)
			{
			  for(int i = 0; i < dim; i++)
			    {
			      if(argv[++opt][0] == '-')
				{
				  cerr << "Inappropriate state for Pi: "
				       << argv[opt]
				       << " in option -Su\n";
				  usage(argv[0]);
				  exit(1); // check which error code to use!
				}
				    
			      Pi.push_back(atof(argv[opt]));
			    }
			  for(int i= 0; i < dim * (dim - 1) /2; i++)
			    {
			      if(argv[++opt][0] == '-')
				{
				  cerr << "Inappropriate state for R: "
				       << argv[opt]
				       << " in option -Su\n";
				  usage(argv[0]);
				  exit(1); // check which error code to use!
				}

			      R.push_back(atof(argv[opt]));
			    }
			}
		      else
			{
			  cerr << "Too few parameters to -Su " 
			       << seqType
			       << "\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		    }
		  else
		    {
		      cerr << "Expected seqModel after option '-m'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'n':
		{
		  if (++opt < argc) 
		    {
		      n_cats = atoi(argv[opt]);
		    }
		  else
		    {
		      std::cerr << "Expected int (number of site rate\n"
				<< "classes) for option 'n'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'a':
		{
		  if (++opt < argc) 
		    {
		      alpha = atof(argv[opt]);
		      fixedAlpha = true;
		    }
		  else
		    {
		      cerr << "Expected float (shape parameter for site rate\n"
			   << "variation) for option '-a'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		  usage(argv[0]);
		  exit(1); //Check for correct error code
		}
		break;
	      }
	    break;
	  }	
	case 'E':
	  {
	    switch(argv[opt][2])
	      {
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      rateModel = argv[++opt];
		      if(find(rate_models.begin(), rate_models.end(), rateModel) 
			 == rate_models.end())
			{
			  cerr << "Model "
			       << argv[opt] 
			       << "does not exist\n" 
			       << "Expected 'iid', ''gbm' or 'const' "
			       << "after option '-Em'\n";
			  usage(argv[0]);
			  exit(1); //Check what error code should be used!
			}
		      // else we use default = const
		    }
		  else
		    {
		      cerr << "Expected 'iid', ''gbm' or 'const' after "
			   << "option '-Em'\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}
	      case 'd':
		{
		  if (opt + 1 < argc)
		    {
		      density = argv[++opt];
		    }
		  else
		    {
		      cerr << "Expected density after option '-d'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'f':
		{
		  fixed_rateparams = true;
		  // Don't break here, because we want to fall through to 'r'
		  // set the rates that are arguments both to '-r' and '-f'!
		}
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      mean = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  variance = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Expected float (variance for "
			       << "edge rates)\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected pair of floats (mean and variance\n"
			   << "for edge rates) for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'a':
		{
		  if (++opt < argc) 
		    {
		      mean = atof(argv[opt]);
		      fixed_mean = true;
		    }
		  else
		    {
		      cerr << "Expected pair of floats (mean and variance\n"
			   << "for edge rates) for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'r':
		{
		  if (string(argv[++opt])  == "{")
		    {
		      string s = argv[++opt];
		      while(s != "}")
			{
			  unsigned key = atoi(s.substr(0, s.find("=")).c_str());
			  Real data = atof(s.substr(s.find("=")+1).c_str());
			  start_rates[key] = data;
			  s = argv[++opt];
			}
		      if(start_rates.empty())
			{
			  cerr << "Set of edge rates expected after "
			       << "option -Gu\n";
			  usage(argv[0]);
			  exit(1);
			}
		    }
		  else
		    {
		      cerr << "Expected edge times as strings after -Gu\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}
	      case 'l':
		{
		  if (++opt < argc) 
		    {
		      maxP = atof(argv[opt]);
		      if(maxP <= 0)
			{
			  cerr << "Warning: the value given for -El was\n"
			       << "<= 0, and will be ignored\n";
			}
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	case 'G':
	  {
	    switch(argv[opt][2])
	      {
	      case 'g':
		{
		  fixed_tree = true;
		  break;
		}
	      case 'i':
		{
		  if (opt + 1 < argc)
		    {		  
		      tree_file = argv[++opt];
		      break;
		    }
		  else
		    {
		      cerr << "Expected file name (string) after -Gi\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		}
	      case 'o':
		{
		  if (string(argv[++opt])  == "{")
		    {
		      opt++;
		      while(string(argv[opt]) != "}")
			{
			  outgroup.push_back(argv[opt++]);
			}
		      if(outgroup.empty())
			{
			  cerr << "Set of outgroup names expected after "
			       << "option -Go\n";
			  usage(argv[0]);
			  exit(1);
			}
		    }
		  else
		    {
		      cerr << "Expected outgroup names as strings after -Go\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}		
	      case 'f':
		{
		  fixed_times = true;
		  fixed_bdrates = true;
		  break;
		}
	      case 't':
		{
		  if (string(argv[++opt])  == "{")
		    {
		      string s = argv[++opt];
		      while(s != "}")
			{
			  unsigned key = atoi(s.substr(0, s.find("=")).c_str());
			  Real data = atof(s.substr(s.find("=")+1).c_str());
			  start_times[key] = data;
			  s = argv[++opt];
			}
		      if(start_times.empty())
			{
			  cerr << "Set of edge times expected after "
			       << "option -Gu\n";
			  usage(argv[0]);
			  exit(1);
			}
		    }
		  else
		    {
		      cerr << "Expected edge times as strings after -Gu\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	case 'B':
	  {
	    switch(argv[opt][2])
	      {
	      case 'f':
		{
		  fixed_bdrates = true;
		  // Don't break here, because we want to fall through to 'r'
		  // set the rates that are arguments both to '-r' and '-f'!
		}
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      birthRate = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  deathRate = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Error: Expected a gene loss (death) rate\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected birth rate for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};
