/* 
 * File:   PrimeSample.hh
 * Author: peter9
 *
 * Created on November 30, 2009, 12:29 PM
 */

#ifndef _PRIMESAMPLE_HH
#define	_PRIMESAMPLE_HH

#include "Tree.hh"
#include "GammaDensity.hh"
#include "EdgeDiscBDProbs.hh"

namespace beep{

    class PrimeSample {
    public:
        PrimeSample(const Tree &G, const GammaDensity &gamma, const EdgeDiscBDProbs &bd_probs):
        m_G(G), m_gamma(gamma), m_bd_probs(bd_probs)
        {}

        Tree & getGeneTree(){return m_G;}

        GammaDensity & getGamma(){return m_gamma;}

        EdgeDiscBDProbs & getBDProbs(){return m_bd_probs;}

        virtual ~PrimeSample(){}
    private:
        Tree m_G;                   //Gene tree
        GammaDensity m_gamma;       //Density function
        EdgeDiscBDProbs m_bd_probs; //Birth-death rates holder
    };

};

#endif	/* _PRIMESAMPLE_HH */

