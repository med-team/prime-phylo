/*
 * PrIME-CT
 *
 * Creates a consensus tree with speciation probabilities from
 * gene trees created in an MCMC run using, for instance, PrIME-GSR.
 *
 * Author: Peter Andersson (peter9@kth.se) November/December 2009
 *
 *
 * Note: This program is written somewhat complicated. It should be refactored
 * into using the class GSROrthologyEstimator instead for simplicity and
 * maintainability.
 * /Peter
 */




/*Includes*/

//Boost
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>


//Standard libraries
#include <stdlib.h>
#include <ctime>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <map>

//Gengetopt
#include "cmdline.h"

//From PrIME library
#include "Tree.hh"
#include "TreeIO.hh"
#include "SeqIO.hh"
#include "GammaDensity.hh"
#include "SequenceData.hh"
#include "MatrixTransitionHandler.hh"
#include "StrStrMap.hh"
#include "EdgeDiscGSR.hh"
#include "Tokenizer.hh"
#include "Hacks.hh"
#include "TimeEstimator.hh"
#include "CT/EdgeDiscGSRObjects.hh"
#include "mcmc/PosteriorSampler.hh"

//From primeCT
#include "ConsensusTree.hh"
#include "ConsensusNode.hh"
#include "VirtualVertex.hh"
#include "GeneSet.hh"


/*Namespaces used*/
using namespace beep;
using namespace std;



/*Definitions and constants*/
//To be able to use foreach() constructs
#define foreach BOOST_FOREACH
//Flag desciding if diagnostics about the flow of the program is written to stdout
static bool DIAGNOSTICS = true;
//Identities for the input arguments
static const int INPUT_SEQUENCES_SAMPLES_FILE = 0;
static const int INPUT_SPECIES_TREE_FILE = 1;
static const int INPUT_GSMAP_FILE = 2;
//Threshold for majority-rule consensus
static const float posterior_threshold = 0.5f;
//Default MCMC parameters
static const float DEFAULT_BIRTH_RATE = 1.0f;
static const float DEFAULT_DEATH_RATE = 1.0f;
static const float DEFAULT_MEAN = 1.0f;
static const float DEFAULT_VARIANCE = 1.0f;
//Convenience typedef
typedef map< VirtualVertex, pair<Probability, Probability> > vvmap;
//NOTE:
//For some reason I cannot find a way to make a pointer to an element
//in a vvmap, this is a workaround. The iterator for vvmap is seen as a
//regular pointer. /Peter
typedef vvmap::iterator vvmapel_pt;





/*Pre-declarations*/
//See each function definition for descriptions.

static void read_cmd_opt(   struct gengetopt_args_info  &args_info,
                            int                         argc,
                            char                        *argv[]);

static Tree read_species_tree(struct gengetopt_args_info &args_info);

static void create_disc_tree(   Tree &species_tree,
                                EdgeDiscTree **DS,
                                struct gengetopt_args_info &args_info);

static void read_samples(   vector<EdgeDiscGSRObjects*> &gsr_objects,
                            EdgeDiscTree &DS,
                            struct gengetopt_args_info &args_info);

static void create_states(  vector<EdgeDiscGSR*> &states,
                            vector<EdgeDiscGSRObjects*> &gsr_objects,
                            EdgeDiscTree        &DS,
                            StrStrMap           &gs_map);

static void create_lookup_tables(   map<int,string> &D,
                                    map<string,int> &inv_D,
                                    StrStrMap &gs_map);

static void find_vertices(  vvmap &vertices,
                            vector<EdgeDiscGSR*> &states,
                            map<int,string> &D,
                            map<string,int> &inv_D);

static void find_vertices_rec(  vvmap &vertices,
                                Node *node,
                                EdgeDiscGSR *state,
                                map<int,string> &D,
                                map<string,int> &inv_D,
                                GeneSet &part);

static void clean(  vector<EdgeDiscGSRObjects*> &gsr_objects,
                    vector<EdgeDiscGSR*> &states);

static void find_high_post_vertices(vvmap &vertices,
                                    vector<vvmapel_pt> &hp_vertices,
                                    float threshold_count);

static void calc_speciation_probs(  vector<vvmapel_pt> &hp_vertices,
                                    vector<EdgeDiscGSR*> &states,
                                    map<int,string> &D);

static bool independent(Node *lca_a, Node *lca_b);

static Node* find_lca(SetOfNodes &nodes, Tree &T);

static void build_consensus_tree(   ConsensusTree &ctree,
                                    vector<vvmapel_pt> &hp_vertices,
                                    map<int,string> &D);

static void build_consensus_tree_rec(   GeneSet &D,
                                        ConsensusTree &ctree,
                                        vector<vvmapel_pt> &hp_vertices,
                                        ConsensusNode *c,
                                        map<int, string> &ID);

static void find_mvvs( vector<vvmapel_pt> &mvvs,  vector<vvmapel_pt> &V);

static void mcmc_run(   vector<EdgeDiscGSRObjects*>    &gsr_objects,
                        vector<EdgeDiscGSR*>    &states,
                        Tree                    &species_tree,
                        EdgeDiscTree            &DS,
                        StrStrMap               &gs_map,
                        struct gengetopt_args_info &args_info);

/**
 * main
 */
int main(int argc, char *argv[]){

    /*
     * Program variables
     */
    struct gengetopt_args_info  args_info;      //Used for option parsing
    vector<EdgeDiscGSRObjects*> gsr_objects;    //Samples read from file
    vector<EdgeDiscGSR*>        states;         //GSR states
    TreeIO                      tree_reader;    //Used for reading from files
    Tree                        species_tree;   //The species tree
    EdgeDiscTree                *DS;            //Discretized species tree
    StrStrMap                   gs_map;         //The gene-species map used
    map<int, string>            ID;             //Lookup table for id-gene mapping
    map<string, int>            inv_ID;         //Lookup table for gene-id mapping
    vvmap                       vertices;       //Table of virtual vertices
    vector<vvmapel_pt>          hp_vertices;    //High posterior vertices
    ConsensusTree               ctree;          //Consensus tree

    /*
     * Read command-line options
     */
    read_cmd_opt(args_info, argc, argv);


    /*
     * Read species tree
     */
    if(DIAGNOSTICS){cout << "Reading species tree.. ";}
    species_tree = read_species_tree(args_info);
    if(DIAGNOSTICS){cout << "done!" << endl;}



    /*
     * Create discretized tree
     */
    if(DIAGNOSTICS){cout << "Creating discretized tree.. ";}
    create_disc_tree(species_tree, &DS, args_info);
    if(DIAGNOSTICS){cout << "done!" << endl;}



    /*
     * Read gene-species map
     */
    if(DIAGNOSTICS){cout << "Reading gene-species map.. ";}
    gs_map = tree_reader.readGeneSpeciesInfo(args_info.inputs[INPUT_GSMAP_FILE]);
    if(DIAGNOSTICS){cout << "done!" << endl;}
    


    /*
     * Create/Recreate states
     */
    if(args_info.mcmc_flag){
        /*
         * Perform MCMC calculations
         */
        if(DIAGNOSTICS) {cout << "Creating states from MCMC calculations.." << endl;}
        mcmc_run(gsr_objects, states, species_tree, *DS, gs_map, args_info);
        if(DIAGNOSTICS) {cout << "done creating states from MCMC calculations!" << endl;}
    }
    else{
        /*
         * Read in samples from file
         */
        if(DIAGNOSTICS){cout << "Reading samples.. ";}
        read_samples(gsr_objects, *DS, args_info);
        if(DIAGNOSTICS){cout << "done!" << endl;}

        /*
         * Recreate states
         */
        if(DIAGNOSTICS){cout << "Recreating states.. ";}
        create_states(states, gsr_objects, *DS, gs_map);
        if(DIAGNOSTICS){cout << "done!" << endl;}
    }


    /*
     * Find vertices in trees and gather data for posterior and
     * speciation probability calculations
     */

    //Give each gene an id in the range 0, 1, ..., gs_map.size()-1
    //and create lookup-tables for gene-id and id-gene.
    if(DIAGNOSTICS){cout << "Creating lookup tables.. ";}
    create_lookup_tables(ID, inv_ID, gs_map);
    if(DIAGNOSTICS){cout << "done!" << endl;}


    //Find vertices in states and store data for probability calculations
    if(DIAGNOSTICS){cout << "Finding virtual vertices.. " << endl;}
    find_vertices(  vertices, states, ID, inv_ID);
    if(DIAGNOSTICS){cout << "done finding virtual vertices!" << endl;}


    /*
     * Gather vertices with high posterior probability
     */
    if(DIAGNOSTICS){cout << "Selecting virtual vertices with high posterior.. " << endl;}
    find_high_post_vertices(    vertices,
                                hp_vertices,
                                floor(states.size() * posterior_threshold));
    if(DIAGNOSTICS){cout << "done selecting virtual vertices!" << endl;}



    /*
     * Find speciation probabilities for vertices with high posterior
     */
    if(DIAGNOSTICS){cout << "Calculating speciation probabilities.. " << endl;}
    if(!args_info.no_speciation_probabilities_flag){
        calc_speciation_probs(hp_vertices, states, ID);
    }
    //Normalize the probabilities by dividing with the number of states
    foreach(vvmapel_pt it, hp_vertices){

        it->second.first /= static_cast<Real>( states.size() );
        if( it->second.first.val() > 1.0 ){
            cout << "ERROR: Posterior: " << it->second.first.val() << endl;
            cout << "nr_states: " << states.size() << endl;
            cout << "Vertex: " << it->first << endl;
        }
        it->second.second /= static_cast<Real>( states.size() );
        if( it->second.second.val() > 1.0 ){
            cout << "ERROR: Orthology: " << it->second.second.val() << endl;
            cout << "nr_states: " << states.size() << endl;
            cout << "Vertex: " << it->first << endl;
        }
    }
    if(DIAGNOSTICS){cout << "done calculating speciation probs!" << endl;}


    /*
     * Create consensus tree
     */
    if(DIAGNOSTICS) {cout << "Building consensus tree..";}
    build_consensus_tree(ctree, hp_vertices, ID);
    if(DIAGNOSTICS){cout << "done!" << endl;}

    /*
     * Print consensus tree
     */
    if(DIAGNOSTICS) {cout << "Printing consensus tree.. " << endl;}
    if(args_info.output_file_given){
        ofstream file(args_info.output_file_arg);
        file << ctree;
        file.close();
    }
    else{
        cout << ctree << endl;
    }
    if(DIAGNOSTICS) {cout << "done printing consensus tree!" << endl;}

    //Draw tree to file
    cout << "Drawing tree and saving to file test.svg" << endl;
    ofstream file("test.svg");
    ctree.drawTreeSVG(file);
    file.close();
    cout << "Done drawing tree" << endl;

    /*
     * Clean up
     */
    //Delete samples and states
    clean(gsr_objects, states);

    //Delete consensus tree nodes
    ctree.deleteAllNodes();


    //Delete discretized tree
    delete DS;

    /*
     * Exit program
     */
    exit(EXIT_SUCCESS);
}



















/**
 * find_mvvs
 *
 * Finds all MVVs in a set of virtual vertices.
 *
 * @param mvvs Storage for found MVVs. Assumed to be intitially empty.
 * @param V A set of virtual vertices.
 * @author peter9
 */
void
find_mvvs( vector<vvmapel_pt> &mvvs,  vector<vvmapel_pt> &V)
{
    /*Error check*/
    if(V.size() <= 0){
        throw AnError("Error in find_mvvs: Set of virtual vertices empty.");
    }

    /*If there is only one element in V, it must be the only MVV.*/
    if(V.size() == 1){
        mvvs.push_back(V[0]);
        return;
    }

    /*
     * Here V.size >= 2.
     * We maintain a bitvector stating which virtual vertices we have found a
     * "larger" element for, i.e. which vertices that are dominated by another
     * vertex in V. The analogy is that we "kill off" smaller vertices until we
     * only have a conquering vertex left. If we no longer can find any new dominators
     * but there are more than 1 element left alive, there are several conquerers,
     * which are mutually incomparable.
     */
    vector<bool> dead(V.size(), false);
    vvmapel_pt max;
    vvmapel_pt cur;
    unsigned int max_index, cur_index;
    bool end = false;
    /*
     * Perform several runs of max-finding until we have considered all possibilities.
     * Since some elements are incomparable, we may have more that one maximum.
     */
    while( !end ){
        //Find starting point, i.e. first vertex that is still alive.
        end = true;
        for(max_index = 0; max_index < V.size(); max_index++){
            if( dead[max_index] ){
                continue;
            }
            end = false;
            max = V[max_index];
            dead[max_index] = true;
            break;
        }
        if(end){
            break;
        }

        //Find maximum
        if( max_index < V.size() ){
            for(cur_index = 0; cur_index < V.size(); cur_index++){
                //Skip dead vertices
                if(dead[cur_index]){
                    continue;
                }
                cur = V[cur_index];
                if( max->first.dominates( cur->first ) ){
                    /*
                     * If current maximum dominates the current vertex, then
                     * we "kill" the current vertex.
                     */
                    dead[cur_index] = true;
                }
                else if( cur->first.dominates( max->first ) ){
                    /*
                     * Similarly, if the current vertex dominates the maximum vertex
                     * Then the maximum is no longer needed in any run, so we kill
                     * it.
                     * In this case we must also restart the current run since
                     * the new maximum may dominate a vertex that the previous
                     * maximum was not comparable with, and thus may still
                     * be alive.
                     */
                    max = cur;
                    max_index = cur_index;
                    dead[cur_index] = true;
                    cur_index = 0; //Restart
                }
                /*
                 * Else, the vertices are incomparable. We simply continue in this
                 * case.
                 */
            }
            /*Maximum found, add it to set of MVVs.*/
            mvvs.push_back( max );
        }
    }
}








/**
 * build_consensus_tree
 *
 * Constructs a majority rule consensus tree.
 *
 * @param ctree Storage for the final consensus tree.
 * @param hp_vertices A consistent set of virtual vertices with probabilities.
 * @param ID id to gene name map.
 *
 */
void
build_consensus_tree(   ConsensusTree &ctree,
                        vector<vvmapel_pt> &hp_vertices,
                        map<int, string> &ID)
{
    /*Create root node*/
    ConsensusNode* root = new ConsensusNode();
    ctree.setRoot(root);

    /*Create gene set with all genes*/
    GeneSet D = GeneSet(ID.size());
    for(unsigned int i = 0; i < ID.size(); i++){
        D.addGene(i);
    }

    /*Create tree recursively*/
    build_consensus_tree_rec(D, ctree, hp_vertices, root, ID);
}



/**
 * printV
 *
 * Prints a vector of virtual vertices.
 *
 * @param V Vector of virtual vertices.
 * @param name String describing the name of the set of vertices.
 * @author peter9
 */
void
printV(vector<vvmapel_pt> &V, string name)
{
    cout << name << ": " << endl;
    foreach(vvmapel_pt it, V){
        cout << it->first << endl;
    }
}









/**
 * build_consensus_tree_rec
 *
 * Constructs a consensus tree recursively. Used by build_consensus_tree.
 *
 * @param D Gene set for this level.
 * @param ctree Storage of final consensus tree.
 * @param V Current set of virtual vertices.
 * @param c Current node.
 * @param ID id to gene name map.
 * @author peter9
 *
 */
void
build_consensus_tree_rec(   GeneSet &D,
                            ConsensusTree &ctree,
                            vector<vvmapel_pt> &V,
                            ConsensusNode *c,
                            map<int, string> &ID)
{
    /*
     * Error check
     */
    assert(D.size() > 0);
    if(D.size() < 1){
        throw AnError("Programming error in build_consensus_tree_rec: D is empty.");
    }

    /* 
     * Base cases
     */
    if(D.size() == 1){
        //If c is the only node in the tree, stop and return.
        if(c->isRoot()){
            return;
        }
        //Otherwise, we have encountered a leaf that must be added to the tree.
        
        //Change c to be a leaf
        vector<unsigned int> ids = D.getGeneIDs();
        c->setName( ID[ ids[0] ] );
        return;
    }
    if(V.size() == 0){
        //Create new nodes for all remaining genes in D and set them as
        //children to the current node
        vector<unsigned int> ids = D.getGeneIDs();
        foreach(unsigned int id, ids){
            //Create node for gene
            ConsensusNode *leaf = new ConsensusNode();
            leaf->setName( ID[id] );
            //Create edge from c
            leaf->setParent(c);
            c->addChild(leaf);
        }
        return;
    }


    /*
     * Recursion step
     */

    //Find MVVs.
    vector<vvmapel_pt> mvvs;
    find_mvvs(mvvs, V);

    //Create combined gene set of the MVVs to check for left-out genes in D
    GeneSet Dprime(ID.size());
    foreach(vvmapel_pt pt, mvvs){
        Dprime.addGeneSet( pt->first.getLeftDescendantPart() );
        Dprime.addGeneSet( pt->first.getRightDescendantPart() );
    }
    /*Error check*/
    if(D.size() < Dprime.size()){
        throw AnError("Programming error in build_consensus_tree_rec. |D|<|Dprime|.");
    }
    //Check for left-out genes
    if( Dprime.size() < D.size() ){
        /*
         * Not every gene is covered by a virtual vertex.
         * We create an edge to every left-out gene from c then make a recursive
         * call with the other genes.
         */

        //Get left-out genes and create leaves for them.
        vector<unsigned int> ids = D.getSetDifference(Dprime).getGeneIDs();
        foreach(unsigned int id, ids){
            //Create node for gene
            ConsensusNode *leaf = new ConsensusNode();
            leaf->setName( ID[id] );
            //Create edge
            leaf->setParent(c);
            c->addChild( leaf );
        }
        
        /*
         * Create a new node for all genes in Dprime and make a recursive call
         * with Dprime as the gene set.
         */
        //Create node for gene
        ConsensusNode *node = new ConsensusNode();
        //Create edge from c
        node->setParent(c);
        c->addChild( node );
        //Recursive call
        build_consensus_tree_rec(Dprime, ctree, V, node, ID);
        return;
    }


    /*
     * Now we now that |Dprime| == |D| and we do not have to consider
     * Dprime anymore.
     */

    //Create new virtual vertex sets for recursive calls. One pair for each MVV.
    foreach(vvmapel_pt mvv, mvvs){
        //Create parent node for MVV, i.e. the parent of the MVVs children sets
        ConsensusNode *pnode;
        if(mvvs.size() == 1){
            pnode = c;
        }
        else{
            pnode = new ConsensusNode();
            pnode->setParent(c);
            c->addChild(pnode);
        }

        //An MVV is always a high posterior vertex so add probabilities
        pnode->addProbabilities(mvv->second.first, mvv->second.second);

        //Create new children for each part of the MVV
        ConsensusNode *left = new ConsensusNode();
        ConsensusNode *right = new ConsensusNode();
        left->setParent(pnode);
        right->setParent(pnode);
        pnode->addChild(left);
        pnode->addChild(right);

        //Create new vertex sets for each part
        vector<vvmapel_pt> V_L, V_R;
        GeneSet mvv_left = mvv->first.getLeftDescendantPart();
        GeneSet mvv_right = mvv->first.getRightDescendantPart();
        //Place all virtual vertices in V that are dominated by the MVV
        //in the correct subset
        foreach(vvmapel_pt vv, V){
            GeneSet uni = vv->first.getLeftDescendantPart().getSetUnion(
                            vv->first.getRightDescendantPart() );
            if( uni.subsetOf( mvv_left )){
                V_L.push_back(vv);
            }
            else if(uni.subsetOf( mvv_right )){
                V_R.push_back(vv);
            }
        }
        //Make recursive calls
        build_consensus_tree_rec(mvv_left, ctree, V_L, left, ID);
        build_consensus_tree_rec(mvv_right, ctree, V_R, right, ID);
    }

}



















/**
 * calc_speciation_probs
 *
 * Finds the CUMULATIVE speciation probabilities for a set of virtual vertices.
 *
 * @param hp_vertices A set of virtual vertices with probabilities,
 *          the cumulative speciation probabilities will be stored here upon return.
 * @param states The set of states used to find the probabilities over.
 * @param ID id to gene name map.
 */
void
calc_speciation_probs(  vector<vvmapel_pt> &hp_vertices,
                        vector<EdgeDiscGSR*> &states,
                        map<int,string> &ID)
{
    //Get "dummy-state"
    EdgeDiscGSR *first_state = states[0];

    //Get species tree for all states
    Tree S = states[0]->getDiscretizedHostTree().getTree();

    //Create gene_id to species-leaf-node map
    map<int, Node*> gs_node_map;
    for(unsigned int i = 0 ; i < ID.size(); i++){
        gs_node_map[i] = S.findLeaf( first_state->getGSMap().find( ID[i] ) );
    }

    //Walk though each state
    foreach(EdgeDiscGSR *state, states){
        //Update each vertex speciation probability
        foreach(vvmapel_pt it, hp_vertices){
            //Find lcas of descendant parts
            Tree G = state->getTree();
            vector<unsigned int> left_gene_ids = it->first.getLeftDescendantPart().getGeneIDs();
            SetOfNodes left_nodes, right_nodes;
            vector<unsigned int> right_gene_ids = it->first.getRightDescendantPart().getGeneIDs();
            foreach(int i, left_gene_ids){
                    left_nodes.insert( G.findLeaf( ID[i] ) );
            }
            Node *lca_left = find_lca(left_nodes, G );
            foreach(int i, right_gene_ids){
                    right_nodes.insert( G.findLeaf( ID[i] ) );
            }
            Node *lca_right = find_lca(right_nodes, G );

            //Check for independence
            if( independent(lca_left, lca_right) ){
                /**
                 * We find speciation probability for current vertex by finding the
                 * probability of placing the current node at the LCA in the species tree
                 * of the genes in the descendant parts of the current virtual vertex.
                 *
                 */
                
                //Find LCA in species tree between all species having genes
                //in one of the descendant parts.
                //This can be optimized by only calculating this once during the first state.
                SetOfNodes species_leaves;
                //Left part
                foreach(int i, left_gene_ids){
                    species_leaves.insert( gs_node_map[i] );
                }
                //Right part
                foreach(int i, right_gene_ids){
                    species_leaves.insert( gs_node_map[i] );
                }

                Node *lca_s = find_lca(species_leaves, S );
                
                //Create "discretized" speciation node
                EdgeDiscretizer::Point point(lca_s, 0);

                //Calculate speciation probability
                Node *lca_g = G.mostRecentCommonAncestor(lca_left, lca_right);
                Probability speciation_prob = state->getPlacementProbability(lca_g, &point);
//                cout << speciation_prob.val() << endl;

                if(speciation_prob.val() > 1.0){
                    cout << "PrIME-CT: WARNING, Speciation prob > 1.0." << endl
                                << "speciation_prob: " << speciation_prob.val() << endl
                                << "state->getPlacementProbability(lca_g, &point): " << state->getPlacementProbability(lca_g, &point).val() << endl
                                << "state->calculateDataProbability(): " << state->calculateDataProbability().val() << endl;
                    cout << state->getDebugInfo(false,false,true,true);
                    cout << "Total placement probability of node " << lca_g->getNumber() << ": " << state->getTotalPlacementProbability(lca_g).val() << endl;
                    cout << "Tree: " << state->getTree() << endl;
                    cout << "Species tree: " << S << endl;
                }
                //Store probability
                it->second.second += speciation_prob;
            }
        }
    }
}









/**
 * independent
 *
 * Decide if a pair of nodes are independent.
 *
 * @param lca_a Node 1
 * @param lca_b Node 2
 */
bool
independent(Node * lca_a, Node *lca_b)
{
    return !( lca_a->dominates(*lca_b) || lca_b->dominates(*lca_a) );
}

/**
 * find_lca
 *
 * Finds and returns the lca node of a set of nodes.
 *
 * @param nodes A set of nodes
 * @param T Tree in which the nodes are found.
 *
 */
Node*
find_lca(SetOfNodes &nodes, Tree &T){
    Node *lca = nodes[0];
    for(unsigned int i = 1; i < nodes.size(); i++){
        lca = T.mostRecentCommonAncestor(lca, nodes[i]);
    }
    return lca;
}







/**
 * find_high_posterior_vertices
 *
 * Finds all virtual vertices with a posterior probability over some threshold.
 *
 * @param vertices A set of virtual vertices to sift. Assumes count has already been calculated.
 * @param hp_vertices Storage for the found vertices upon return.
 * @param threshold_count Posterior threshold. Note that this is a number reperesenting a count
 *                          of states a vertex must have occured in to qualify as a high posterior
 *                          vertex. It is NOT a probability.
 */
void
find_high_post_vertices(vvmap &vertices,
                        vector<vvmap::iterator> &hp_vertices,
                        float threshold_count)
{
    for(vvmap::iterator it = vertices.begin(); it != vertices.end(); it++){
        if(it->second.first.val() > threshold_count){
            hp_vertices.push_back(it);
        }
    }
}















/**
 * find_vertices
 *
 * Finds all virtual vertices in a set of GSR states.
 *
 * @param vertices Storage for the set of virtual vertices upon return, including the count, for each
 *                  vertex, of how many states it was found in.
 * @param states The GSR states to consider.
 * @param ID id to gene name map.
 * @param inv_D gene name to id map.
 *
 */
void
find_vertices(  vvmap &vertices,
                vector<EdgeDiscGSR*> &states,
                map<int,string> &ID,
                map<string,int> &inv_ID)
{
    /*
     * Loop through all states and find all virtual vertices together with
     * their counts.
     */
    foreach(EdgeDiscGSR *state, states){
        /*
         * Find all virtual vertices in the state and update counts.
         * The procedure is recursive and starts at the
         * root of the current gene tree.
         */
        GeneSet g(0);
        find_vertices_rec(  vertices, state->getTree().getRootNode(), state, ID, inv_ID, g);
    }
}









/**
 * create_lookup_tables
 *
 * Create an id for each gene in the range 0,..., gs_map.size()-1.
 * Then created lookup tables for gene name to id mapping and vice versa.
 *
 * @param ID Storage of id to gene map upon return.
 * @param inv_ID Storage of gene to id map upon return.
 * @param gs_map Gene to species map
 */
void
create_lookup_tables(   map<int,string> &ID,
                        map<string,int> &inv_ID,
                        StrStrMap &gs_map)
{
    int     nr_genes;   //Total number of genes
    string  gene_name;  //Gene name

    /*
     * Go through all genes in the gene-species map. Create an
     * id and a record in both lookup-tables for each gene found
     */
    nr_genes = gs_map.size();
    for(int i = 0; i < nr_genes; i++){
        gene_name = gs_map.getNthItem(i);
        ID.insert( pair<int, string>(i, gene_name) );
        inv_ID.insert( pair<string, int>(gene_name, i) );
    }
}








/**
 * find_vertices_rec
 *
 * Finds all virtual vertices in one GSR state recursively.
 *
 * @param vertices Storage for the set of virtual vertices upon return, including the count, for each
 *                  vertex, of how many states it was found in.
 * @param node Current node.
 * @param state The GSR state to consider.
 * @param ID id to gene name map.
 * @param inv_D gene name to id map.
 * @param gs_map gene to species map
 * @param part Gene set to add found leaves to.
 */
void
find_vertices_rec(  vvmap &vertices,
                    Node *node,
                    EdgeDiscGSR *state,
                    map<int,string> &ID,
                    map<string,int> &inv_ID,
                    GeneSet &part)
{
    /*Error check*/
    if(node == NULL){
        throw AnError("Programming error in find_vertices_rec: node == NULL");
    }

    /*
     * If we have reached a leaf, add gene to part of parent vertex.
     */
    if(node->isLeaf()){
        part.addGene(inv_ID[node->getName()]);
        return;
    }

    /*
     * Non-leaf node.
     * The general idea is to create a new virtual vertex and construct its
     * descendant parts recursively in each subtree of the current node.
     * Then we check if the virtual vertex has been found before. If there is an
     * old record of the virtual vertex, then we update it by:
     * - increasing the count of the virtual vertex.
     *
     * Otherwise we enter the newly found virtual vertex with count 1 to 
     * the set of virtual vertices.
     *
     * Also we add all genes found recursively to the descendant part of the
     * parent of the current node.
     */

    //Create new virtual vertex.
    VirtualVertex vv(ID.size());

    //Create descendant parts and construct them recursively.
    GeneSet left(ID.size()), right(ID.size());
    find_vertices_rec(vertices, node->getLeftChild(), state, ID, inv_ID, left);
    find_vertices_rec(vertices, node->getRightChild(), state, ID, inv_ID, right);

    //Add descendant parts to the virtual vertex.
    vv.setLeftDescendantPart(left);
    vv.setRightDescendantPart(right);

    //Add genes found to the descendant part of the parent to the current node.
    if( !(node->isRoot()) ){
        part.addGeneSet(left);
        part.addGeneSet(right);
    }

    //Update virtual vertex set
    /*
     * IMPLEMENTATION NOTE:
     * If vv does not exist in vertices, then the statement vertices[vv]
     * adds vv and the count and cumulative speciation probability
     * both defaults to 0. See documentation for std::map for more info.
     */
    vertices[vv].first += 1.0;
}













/**
 * read_species_tree
 *
 * Read a species tree from file.
 *
 * @param args_info Program arguments
 */
Tree
read_species_tree(struct gengetopt_args_info &args_info)
{
    TreeIO tree_reader = TreeIO::fromFile(args_info.inputs[INPUT_SPECIES_TREE_FILE]);
    return tree_reader.readHostTree();
}












/**
 * read_cmd_opt
 *
 * Deal with typical command-line option parsing using gengetopt.
 *
 * @param args_info On return, contains all option and input info.
 * @param argc NO input parameters (same as for main)
 * @param argv Input arguments (same as for main)
 */
void
read_cmd_opt(   struct gengetopt_args_info    &args_info,
                int                           argc,
                char                          *argv[])
{
    /*Parse options and input*/
    if(cmdline_parser(argc, argv, &args_info) != 0){
        exit(EXIT_FAILURE);
    }

    /*Check for correct number of inputs*/
    if(args_info.inputs_num != 3) {
        cerr    << gengetopt_args_info_usage << endl
                << "Type '" << argv[0] << " -h' for help" << endl;
        exit(EXIT_FAILURE);
    }

    /*Set diagnostics printout*/
    DIAGNOSTICS = !(args_info.silent_flag);
   
}












/**
 * create_disc_tree
 *
 * Creates a discretized species tree from a species tree.
 *
 * @param species_tree The species tree
 * @param DS At return: Pointer to the discretized species tree
 * @param args_info Program arguments
 *
 */
void
create_disc_tree(   Tree &species_tree,
                    EdgeDiscTree **DS,
                    struct gengetopt_args_info &args_info)
{
    float           timestep;       //Timestep for discretized tree
    int             min_intervals;  //Minimum number of intervals in DS
    EdgeDiscretizer *disc;          //Used for creating discretized trees

    //Set timestep and min_interval parameters
    timestep = args_info.timestep_arg;
    min_intervals = args_info.min_intervals_arg;
    
    //Create discretized tree
    if (timestep == 0)
    {
        disc = new EquiSplitEdgeDiscretizer(min_intervals);
    }
    else
    {
        disc = new StepSizeEdgeDiscretizer(timestep, min_intervals);
    }
    *DS = disc->getDiscretization(species_tree);
    
    delete disc;
}















/**
 * mcmc_run
 *
 * Creates an MCMC run, which sample states and store them.
 * This function does not care about burn-in periods.
 * Every state sampled is stored.
 *
 * @param samples Storage for state variables upon return
 * @param states Storage for states upon return
 * @param species_tree The species tree
 * @param DS The discretized species tree
 * @param args_info Gengetopt parameter
 */
void
mcmc_run(   vector<EdgeDiscGSRObjects*>    &gsr_objects,
            vector<EdgeDiscGSR*>    &states,
            Tree                    &species_tree,
            EdgeDiscTree            &DS,
            StrStrMap               &gs_map,
            struct gengetopt_args_info &args_info)
{
    /*Time estimation variable.*/
    TimeEstimator time(args_info.iterations_arg);

    /*Variables used for ease of notation.*/
    int j = args_info.thinning_arg;
    int it = args_info.iterations_arg;

    /*Create substitution model.*/
    MatrixTransitionHandler Q = MatrixTransitionHandler(MatrixTransitionHandler::create(args_info.substitution_model_arg));

    /*Get sequence data.*/
    SequenceData D = SequenceData(
                        SeqIO::readSequences(args_info.inputs[INPUT_SEQUENCES_SAMPLES_FILE],
                                                Q.getType()));

    /*Dummy sample variables.*/
    GammaDensity gd = GammaDensity(DEFAULT_MEAN, DEFAULT_VARIANCE);
    EdgeDiscBDProbs bd(DS, DEFAULT_MEAN, DEFAULT_VARIANCE);

    /*Create MCMC layer.*/
    PosteriorSampler mcmc(D, species_tree, gs_map, Q, gd, DS);
    mcmc.getGSRSampler()->setBDParameters(DEFAULT_BIRTH_RATE, DEFAULT_DEATH_RATE);

    
    /*Start mcmc iterations.*/
    for(int i = j; i <= it; i += j){
        /*Get next sample*/
        EdgeDiscGSR *gsr = mcmc.getGSRSampler()->nextState(args_info.thinning_arg);

        /*Calculate estimated time left*/
        if(DIAGNOSTICS){
            time.update(j);
            time.printEstimatedTimeLeft();
        }

        /*
         * Copy state variables into a PrimeSample, then create a new state from these parameters.
         * This is because EdgeDiscGSR works with references internally so copying a state directly
         * will not work. To save a state we actually have to construct a new state,
         * otherwise the information about the state will be overwritten by MCMCGSRLayer.
         */
        EdgeDiscGSRObjects *ps = new EdgeDiscGSRObjects(gsr->getTree(), gd, bd);
        gsr_objects.push_back(ps);
        
        states.push_back( new EdgeDiscGSR( ps->getGeneTree(), DS, gs_map, ps->getGamma(), ps->getBDProbs()) );
    }
}













/**
 * read_samples
 *
 * Read all samples from a prime output file.
 *
 * @param samples Storage for sample objects
 * @param DS Discretized species tree needed to create sample objects
 * @param args_info Input parameter including the filenames for gene/species
 *                  tree files.
 */
void
read_samples(vector<EdgeDiscGSRObjects*>       &gsr_objects,
            EdgeDiscTree                &DS,
            struct gengetopt_args_info  &args_info)
{
    TreeIO tree_reader;                 //Used for parsing prime trees
    ifstream sample_file;               //File object for samples
    string curr_line;                   //Current line when reading samples
    Tokenizer tokenizer(";");           //String tokenizer
    string token;                       //Holder for current token

    float mean;                         //Mean for a density function
    float variance;                     //Variance for a density function
    float birth_rate;                   //Birth rate for a state
    float death_rate;                   //Death rate for a state
    
    GammaDensity *gamma;                 //Density function
    EdgeDiscBDProbs *bd_probs;           //Birth-death rates holder
    Tree G;                             //Gene tree

 
    int a = 0;
    /*Read samples and create states*/
    sample_file.open(args_info.inputs[INPUT_SEQUENCES_SAMPLES_FILE]);
    if ( sample_file.is_open() ){
        while (! sample_file.eof() )
        {
            cout << "read: " << a++ << endl;
            //Skip lines of comments
            if(sample_file.peek() == '#'){
                sample_file.ignore(1024, '\n');
                continue;
            }

            //Get next line from file
            curr_line = "";
            getline(sample_file, curr_line);

            //Skip empty lines
            if(curr_line == ""){
                continue;
            }

            /*
             * Information in each line is separated by ';'. The tokenizer
             * splits the line at each ';'
             */
            //Set tokenizer
            tokenizer.setString(curr_line);

            //Skip info about likelihoods and number of iterations
            tokenizer.getNextToken();
            tokenizer.getNextToken();

            //Read gene tree
            tree_reader.setSourceString(tokenizer.getNextToken());
            G = tree_reader.readNewickTree();

            //Read mean
            token = tokenizer.getNextToken();
            boost::trim<string>(token);
            mean = boost::lexical_cast<float> (token);

            //Read variance
            token = tokenizer.getNextToken();
            boost::trim<string>(token);
            variance = boost::lexical_cast<float> (token);

            //Skip unnecessary gene tree topology
            tokenizer.getNextToken();

            //Read birth rate
            token = tokenizer.getNextToken();
            boost::trim<string>(token);
            birth_rate = boost::lexical_cast<float> (token);

            //Read death rate
            token = tokenizer.getNextToken();
            boost::trim<string>(token);
            death_rate = boost::lexical_cast<float> (token);

            //Create density function
            gamma = new GammaDensity(mean,variance);
            bd_probs = new EdgeDiscBDProbs(DS, birth_rate, death_rate);

            //Create sample
            gsr_objects.push_back( new EdgeDiscGSRObjects(G, *gamma, *bd_probs) );

            delete gamma;
            delete bd_probs;
        }
        sample_file.close();
    }
    else{
        cerr << "Could not open file: " << sample_file << endl;
        exit(EXIT_FAILURE);
    }
}



















/**
 * create_states
 *
 * Read all gene trees and the species tree into appropriate data structures.
 *
 * @param states Storage for states upon return.
 * @param samples State variables.
 * @param DS Discretized species tree.
 * @param gs_map Gene to species map.
 */
void
create_states(vector<EdgeDiscGSR*>      &states,
            vector<EdgeDiscGSRObjects*>        &gsr_objects,
            EdgeDiscTree                &DS,
            StrStrMap                   &gs_map)
{
    foreach ( EdgeDiscGSRObjects *sample, gsr_objects ){
        states.push_back( new EdgeDiscGSR(  sample->getGeneTree(),
                                            DS,
                                            gs_map,
                                            sample->getGamma(),
                                            sample->getBDProbs())
                                            );
    }
}














/**
 * clean
 *
 * Free memory used by each state.
 *
 * @param samples State variables.
 * @param states States.
 */
void
clean( vector<EdgeDiscGSRObjects*> &gsr_objects, vector<EdgeDiscGSR*> &states)
{
    //Delete states
    foreach(EdgeDiscGSR *e, states){
        delete e;
    }
    //Delete samples
    foreach(EdgeDiscGSRObjects *s, gsr_objects){
        delete s;
    }
}








//    //==================START DEBUG PRINT==================
//    cout << "High posterior vertices: " << endl;
//    cout << "Number of nodes: " << hp_vertices.size() << endl;
//    cout << "Vertices: " << endl;
//    //vvmap::iterator it;
//    foreach(vvmap::iterator it, hp_vertices){
//        cout << it->first;
//        cout    << "\t Count: "
//                << it->second.first.val()
//                << "\t  Cumulative spec prob: "
//                << it->second.second.val()
//                << endl;
//        cout << endl;
//    }
//    //==================END DEBUG PRINT==================

