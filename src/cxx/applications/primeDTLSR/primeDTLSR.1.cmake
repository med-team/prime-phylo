.\" Comment: Man page for primeDTLSR 
.pc
.TH primeDTLSR 1 "@date_for_man_pages@" "@PROJECT_NAME@ @PACKAGE_VERSION@" 
.SH NAME
primeDTLSR \- Guest-in-host tree inference tool
.SH SYNOPSIS
.B primeDTLSR
[\fIOPTIONS\fR]\fI seqfile hostfile [\fIgsfile\fR]\fR
.SH DESCRIPTION

Guest-in-host tree inference enabling reconciliation analysis
using the underlying DTLRS model. Model properties:
  
.IP 1)
The guest tree topology evolves inside the host tree by means of duplication, loss and horizontal transfer events guided by homogeneous rates, much like a birth-death process. Lineages branch deterministically at host tree nodes.
.IP 2)
Relaxed molecular clock; sequence evolution rate variation over guest tree edges. Rates are drawn iid from specified distribution. Also, rate variation over sites may be added, using discretized gamma distribution with mean 1. 
.IP 3)
Substitution model of choice; standard or user-defined. The implementation uses a discretization of the host tree to approximate the probability of all possible reconciliation realizations for the current parameter state.
.PP
The implementation uses a discretization of the host tree to approximate the
probability of all possible reconciliation realizations for the current parameter
state.

.I seqfile
is a file with aligned sequences for guest tree leaves. 

.I hostfile
is a PrIME Newick file with host tree incl. divergence times. Leaves must have time 0 and root have time > 0.

.I gsfile
is a tab-delimited file relating guest tree leaves to host tree leaves if info not included in
.I hostfile.

.SH OPTIONS
.TP
.BR \-h ", " \-u ", " \-?
Display help (this text).
.TP
.BR \-o " " \fIFILE\fR
Output filename. Defaults to stderr.
.TP
.BR \-s " " \fIUNSIGNED_INT\fR
Seed for pseudo-random number generator. Defaults to random seed.
.TP
.BR \-i " " \fIUNSIGNED_INT\fR
Number of iterations. Defaults to @conf_value_from_cmake_Iterations@.
.TP
.BR \-t " " \fIUNSIGNED_INT\fR
Thinning, i.e. sample every <value>-th iteration. Defaults to @conf_value_from_cmake_Thinning@.
.TP
.BR \-w " " \fIUNSIGNED_INT\fR
Output diagnostics to stderr every <value>-th sample. Defaults to @conf_value_from_cmake_PrintFactor@.
.TP
.BR \-q 
Do not output diagnostics. Non-quiet by default.
.TP
.BR \-m " " \fRMCMC|PDHC|PD
Execution type (MCMC, posterior density hill-climbing from initial values, or just initial posterior density). Defaults to @conf_value_from_cmake_RunType@.
.TP
.BR \-Sm " " \fRUniformAA|JC69|JTT|UniformCodon|ArveCodon
Substitution model. @conf_value_from_cmake_SubstModel@ by default.
.TP
.BR \-Su " " \fRDNA|AminoAcid|Codon " <Pi=float1 float2 ... floatn> <R=float1 float2 ...float(n*(n\-1)/2)>"
User-defined substitution model. The size of Pi and R must fit data type (DNA: n=4, AminoAcid: n=20, Codon: n=62). R is given as a flattened upper triangular matrix. Don't use both option \-Su and \-Sm.
.TP
.BR \-Sn " " \fIUNSIGNED_INT\fR
Number of steps of discretized Gamma-distribution for sequence evolution rate variation over sites. Defaults to @conf_value_from_cmake_SiteRateCats@ (no variation).
.TP
.BR \-Ed " " \fRGamma|InvG|LogN|Uniform
Distribution for IID rate variation among edges. Defaults to @conf_value_from_cmake_EdgeRateDistr@.
.TP
.BR \-Ep " " \fIFLOAT\fR " " \fIFLOAT\fR
 Initial mean and variance of edge rate distribution. Defaults to @conf_value_from_cmake_EdgeRateParams_first@ and @conf_value_from_cmake_EdgeRateParams_second@.
.TP
.BR \-Ef
Fix mean and variance of edge rate distribution. Non-fixed by default.
.TP
.BR \-Gi " " \fIFILE\fR
Filename with initial guest tree topology.
.TP
.BR \-Gg
Fix initial guest tree topology, i.e. perform no branch-swapping. Non-fixed by default.
.TP
.BR \-Bp " " \fIFLOAT\fR " " \fIFLOAT\fR " " \fIFLOAT\fR
Initial duplication, loss and transfer rates. Defaults to @conf_value_from_cmake_BirthDeathTransfer_first@, @conf_value_from_cmake_BirthDeathTransfer_second@ and @conf_value_from_cmake_BirthDeathTransfer_third@.
.TP
.BR \-Bf
Fix duplication, loss and transfer rates to initial values. Non-fixed by default.
.TP
.BR \-Bt " " \fIFLOAT\fR
Override time span of edge above root in host tree. Must be greater than 0. Defaults to file-contained value.
.TP
.BR \-Dt " " \fIFLOAT\fR
Approximate discretization timestep. Set to 0 to divide edge generations into the same amount of parts (see \-Di). Defaults to @conf_value_from_cmake_DiscTimestep@.
.TP
.BR \-Di " " \fIUNSIGNED_INT\fR
Minimum number of discretization subintervals per edge generation. If \-Dt is set 0, this becomes the exact number of subintervals. Minimum 2. @Defaults to conf_value_from_cmake_DiscMinIvs@.
.TP
.BR \-C " " \fIUNSIGNED_INT\fR
Number of transfer counts during sampling. Defaults to @conf_value_from_cmake_TransferCounts@.
.TP
.BR \-r
Rescale the host tree so that the root-to-leaf time equals 1.0. All inferred parameters will refer to the new scale. Off by default.
.TP
.BR \-Z
Do not print elapsed wall time and CPU time
.TP
.BR \-W
Do not print the command line
.TP
.BR \-debuginfo
Show misc. info to stderr before iterating. Not shown by default.

.SH "EXIT STATUS"
.TP
.B 0
Successful program execution.
.TP
.B 1
Some error occurred

.SH URL
.TP
The prime\-phylo home page: http://prime.sbc.su.se

.SH "SEE ALSO"
.BR primeDLRS (1),
.BR primeGEM (1),
.BR showtree (1),
.BR chainsaw (1),
.BR reconcile (1),
.BR reroot (1),
.BR tree2leafnames (1),
.BR treesize (1)

