#include <map>

#include "AnError.hh"
#include "Beep.hh"
#include "BeepOption.hh"
#include "Density2PMCMC.hh"
#include "DummyMCMC.hh"
#include "EdgeRateMCMC_common.hh"
#include "EdgeWeightHandler.hh"
#include "EdgeWeightMCMC.hh"
#include "EpochDLTRS.hh"
#include "EpochTree.hh"
#include "EpochBDTMCMC.hh"
#include "EpochBDTProbs.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "MatrixTransitionHandler.hh"
#include "RandomTreeGenerator.hh"
#include "SeqIO.hh"
#include "SimpleMCMCPostSample.hh"
#include "SimpleML.hh"
#include "SiteRateHandler.hh"
#include "StrStrMap.hh"
#include "SubstitutionMCMC.hh"
#include "TreeIO.hh"
#include "TreeMCMC.hh"
#include "UniformDensity.hh"

#include "conf_value_from_cmake_primeDTLSR.hh"

// Number of input parameters (not related to options).
static const int NO_OF_PARAMS = 2;
static const int NO_OF_OPTIONAL_PARAMS = 1;

// Suggestion ratios for the various MCMCs.
static const double SUGG_RATIO_BDT_RATES    =   1.0;
static const double SUGG_RATIO_G_TOPO_START =   3.0; // Initial value.
static const double SUGG_RATIO_G_TOPO_FINAL =   3.0; // Final value.
static const unsigned SUGG_RATIO_G_TOPO_STEPS = 0;   // Steps between initial and final.
static const double SUGG_RATIO_EDGE_LENGTH  =   1.0;

// All program options in map.
beep::option::BeepOptionMap Options;

// All program params in map.
std::map<std::string, std::string> Params;

// Forward declaration of helpers.
void createDefaultOptions();
void readParameters(int& argIndex, int argc, char **argv);
std::string getUsageStr();
void showHelpMsg();
void iterate(beep::StdMCMCModel* mcmcEnd, std::string runType, std::string runInfoStr, unsigned transCnt);
std::string getRunInfoStr(int argc, char **argv, int seed);
beep::Density2P* createDensity2P(std::string id, double mean, double variance);

/************************************************************************
 * Main program.
 ************************************************************************/ 
int main(int argc, char **argv) 
{
    using namespace beep;
    using namespace beep::option;
    using namespace std;

	try
	{
		// Create options with default values, then read user input.
		createDefaultOptions();
		int argIndex = 1;
		if (!Options.parseOptions(argIndex, argc, argv))
		{
			showHelpMsg();
			exit(0);
		}
		readParameters(argIndex, argc, argv);
		
		string runType = Options.getStringAlt("RunType");
		
		// Set up random number generator.
		PRNG rand;
		if (Options.hasBeenParsed("Seed"))
		{
			rand.setSeed(Options.getUnsigned("Seed"));
		}
		
		// For practical reasons, set up the substitution matrix first.
		string sm = Options.getStringAlt("SubstModel");
		MatrixTransitionHandler Q(MatrixTransitionHandler::create(sm));
		if (Options.hasBeenParsed("UserSubstModel"))
		{
			if (Options.hasBeenParsed("SubstModel"))
				throw AnError("Cannot use both predefined and user-defined subst. model.");
			UserSubstModelOption* smUsr = Options.getUserSubstModelOption("UserSubstModel");
			Q = MatrixTransitionHandler::userDefined(smUsr->type, smUsr->pi, smUsr->r);
		}
	
		// Get seq. data from file. This is why Q must already exist.
		SequenceData D(SeqIO::readSequences(Params["SeqData"], Q.getType()));
		
		// Create S from file.
		Tree S(TreeIO::fromFile(Params["HostTree"]).readHostTree());
		if (Options.getBool("Rescale"))
		{
			Real sc = S.rootToLeafTime();
			RealVector* tms = new RealVector(S.getTimes());
			for (RealVector::iterator it = tms->begin(); it != tms->end(); ++it)
			{
				(*it) /= sc;
			}
			S.setTopTime(S.getTopTime() / sc);
			S.setTimes(*tms, true);
			cout << "# Host tree root-to-leaf span was rescaled from " << sc << " to 1.0.\n";
		}

		// Create discretized tree ES.
		if (Options.hasBeenParsed("TopTime"))
			S.setTopTime(Options.getDouble("TopTime"));
		unsigned ivs = Options.getUnsigned("Intervals");
		Real timestep = Options.getDouble("Timestep");
		EpochTree ES(S, ivs, timestep);
		
		// Create G-S map (empty to begin with).
		StrStrMap gsMap = StrStrMap();
			
		// Create G randomly, unless user specified file.
		TreeIO tio = TreeIO::fromFile(Options.getString("GuestTree"));
		Tree G = Options.hasBeenParsed("GuestTree") ?
			Tree(tio.readBeepTree(NULL, &gsMap)) :
			Tree(RandomTreeGenerator::generateRandomTree(D.getAllSequenceNames()));
			
		// Fill G-S map. If empty (random G, or no info in file G) we
		// must use file passed as parameter with that info.
		if (gsMap.size() == 0)
		{
			if (Params["GSMap"] == "")
				throw AnError("Missing guest-to-host leaf mapping.");
			gsMap = StrStrMap(TreeIO::readGeneSpeciesInfo(Params["GSMap"]));	
		}
		G.doDeleteTimes();
		G.doDeleteRates();
		
		// MCMC: First-of-chain dummy.
		DummyMCMC mcmcDummy = DummyMCMC();
		
		// Retrieve number of transfers to count. Turn off in case of hill-climbing,
		// since this is pointless and will significantly mar performance
		// (it suffices to look at last iteration).
		//TODO: Implement counts support for hill-climbing.
		unsigned trCounts = Options.getUnsigned("TransferCounts");
		if (runType == "PDHC") { trCounts = 0; }
		
		// MCMC: Birth, death and transfer rates.
		vector<double> bdt = Options.getDoubleX3("BirthDeathTransfer");
		EpochBDTProbs bdtProbs(ES, bdt[0], bdt[1], bdt[2], trCounts);
		EpochBDTMCMC mcmcBDTRates(mcmcDummy, bdtProbs, SUGG_RATIO_BDT_RATES);
		if (Options.getBool("FixedBDTRates")) { mcmcBDTRates.fixRates(); };
		
		// MCMC: Guest tree topology.
		UniformTreeMCMC mcmcGTopo(mcmcBDTRates, G, SUGG_RATIO_G_TOPO_START);
		//mcmcGTopo.setChangingSuggestRatio(SUGG_RATIO_G_TOPO_FINAL, SUGG_RATIO_G_TOPO_STEPS);
		if (Options.getBool("FixedG"))
		{
			mcmcGTopo.fixTree();
			mcmcGTopo.fixRoot();
		}
		mcmcGTopo.setDetailedNotification(true);
		
		// MCMC: Sequence evolution rate variation over edges (IID).
		string erdfname = Options.getStringAlt("EdgeRateDistr");
		pair<double,double> edgeRateMeanVar = Options.getDoubleX2("EdgeRateParams");
		Density2P* edgeRateDF = createDensity2P(erdfname, edgeRateMeanVar.first, edgeRateMeanVar.second);
		Density2PMCMC mcmcEdgeRate(mcmcGTopo, *edgeRateDF, true);
		if (Options.hasBeenParsed("EdgeRateFixed"))
		{
			mcmcEdgeRate.fixMean();
			mcmcEdgeRate.fixVariance();
		}
		
		// Model DLTRS. It integrates over all possible reconciliations of G
		// given the discretization in ES. Rate is continuously handled as t/l
		// where t refers to time span of edge in S and l length of edge in G.
		EpochDLTRS dltrs = EpochDLTRS(G, ES, gsMap, *edgeRateDF, bdtProbs);
		
		// MCMC: Lengths (weights) of edges in G. These are stored in 'dltrs'.
		EdgeWeightMCMC mcmcEdgeWeight(mcmcEdgeRate, dltrs, SUGG_RATIO_EDGE_LENGTH, true);
		mcmcEdgeWeight.generateWeights(true);
		mcmcEdgeWeight.setDetailedNotification(true);
		
		// MCMC: Site rate.
		UniformDensity uniformDF(0, 3, true);	// Uniform prior for alpha=beta.
		ConstRateMCMC mcmcSiteRate(mcmcEdgeWeight, uniformDF, G, "Alpha"); 
		
		// Set up site rate handler.
		unsigned siteRateCats = Options.getUnsigned("SiteRateCats");
		SiteRateHandler siteRateHandler(siteRateCats, mcmcSiteRate);
		if (siteRateCats == 1) { mcmcSiteRate.fixRates(); }

		// Proxy for edge lengths (weights).
		EdgeWeightHandler edgeWeightHandler(dltrs);
		
		// MCMC: Substitution.
		vector<string> partList = vector<string>(1, "all");
		SubstitutionMCMC mcmcSubst(mcmcSiteRate, D, G, siteRateHandler, Q,
				edgeWeightHandler, partList);
		
		if (Options.getBool("DebugInfo"))
		{
			cout << "# PRE-ITERATE DEBUG INFO:" << endl
				<< ES.getDebugInfo(true, false)
				<< bdtProbs.getDebugInfo(false, false, false)
				<< dltrs.getDebugInfo(false);
		}
		
		// Finally, iterate.
		string runInfoStr = getRunInfoStr(argc, argv, rand.getSeed());
		iterate(&mcmcSubst, runType, runInfoStr, trCounts);
		
		// START DEBUG PRINTOUTS
//		for (unsigned i=0; i<G.getNumberOfNodes(); ++i)
//		{
//			const Node* u = G.getNode(i);
//			if (!u->isLeaf())
//			{
//				cout << "ats[" << u->getNumber() << "]:" << endl << dltrs.m_ats[u];
//				//cout << "ats0[" << u->getNumber() << "]:" << endl << dltrs.m_ats0[u];
//			}
//			cout << "lins[" << u->getNumber() << "]:" << endl << dltrs.m_lins[u];
//			//cout << "lins0[" << u->getNumber() << "]:" << endl << dltrs.m_lins0[u];
//			
//			if (u->isLeaf())
//			{
//				cout << "Rates for leaf " << u->getNumber() << " " << u->getName() << ":\n";
//				Real l = u->getLength();
//				for (unsigned j=0; j<ES.getNoOfEpochs(); ++j)
//				{
//					const EpochPtSet& ep = ES.getEpoch(j);
//					for (unsigned k=1; k<ep.getNoOfTimes(); ++k)
//					{
//						Real t = ep.getTime(k);
//						Real r = l / t;
//						cout << l << " / " << t << " = " << r 
//							<< " => " << edgeRateDF(r).val() << "   (log " << edgeRateDF(r) << ")"
//							<< endl;
//					}
//				}
//			}
//		}
//		//cout << "Qef:" << dltrs.m_Qef << endl;
//		if (Options.getBool("DebugInfo"))
//		{
//			cout << ES.getDebugInfo(false, false)
//				<< bdtProbs.getDebugInfo(true, true, false)
//				<< dltrs.getDebugInfo(true);
//		}
//		// END DEBUG PRINTOUTS
		
		// Clean up in (somewhat) reverse order of creation.
		delete edgeRateDF;
	}
	catch (AnError& e)
	{
		e.action();
		cout << getUsageStr();
		cout << "Display help with option -h.";
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
		cout << getUsageStr();
		cout << "Display help with option -h.";
	}
	cout << endl;
};


/************************************************************************
 * Starts MCMC or hill-climbing.
 ************************************************************************/
void iterate(beep::StdMCMCModel* mcmcEnd, std::string runType, std::string runInfoStr, unsigned transCnt)
{
	using namespace beep;
	using namespace beep::option;
	using namespace std;
	
	// Iterate according to choice of execution type.
	if (runType == "PD")
	{
		// First-iteration posterior density.
		cout << mcmcEnd->currentStateProb() << endl;
	}
	else
	{
		// MCMC: Iterator.
		unsigned thinning = Options.getUnsigned("Thinning");
		SimpleMCMC* iter;
		if (runType == "PDHC")
		{
			// Posterior density hill-climbing.
			iter = new SimpleML(*mcmcEnd, thinning);
		}
		else
		{
			// True MCMC.
			if (transCnt == 0)
			{
				iter = new SimpleMCMC(*mcmcEnd, thinning);
			}
			else
			{
				// Post-sample for performance reasons.
				iter = new SimpleMCMCPostSample(*mcmcEnd, thinning);
			}
		}

		// Try redirecting output to specified file.
		string out = Options.getString("Output");
		if (out != "") 
		{
			try
			{
				iter->setOutputFile(out.c_str());
			}
			catch(AnError& e)
			{
				e.action();
			}
			catch (...)
			{
				cerr << "Problems opening output file '" << out << "'! "
					<< "Reverting to stdout." << endl;
			}
		}  

		// If quiet run, don't show diagnostics.
		if (Options.getBool("Quiet")) { iter->setShowDiagnostics(false); }
		else                          {
                  if (!Options.getBool("DoNotPrintRunInfo")) {
                   cerr << runInfoStr;             
                  }
                }
		
		// Print run information.
                  if (!Options.getBool("DoNotPrintRunInfo")) {
                   cout << runInfoStr;             
                  }
		
		// Get time and clock before iterations.
		time_t t0 = time(0);
		clock_t ct0 = clock();
		
		// Iterate!!!!
		iter->iterate(Options.getUnsigned("Iterations"), Options.getUnsigned("PrintFactor"));
				
		// Get time and clock after iterations. Print them.
		time_t t1 = time(0);    
		clock_t ct1 = clock();
   	        if (!Options.getBool("DoNotPrintElapsedComputeTime")) {
		  cout << "# Wall time: " << difftime(t1, t0) << " s." << endl;
		  cout << "# CPU time: " << (Real(ct1 - ct0) / CLOCKS_PER_SEC) << " s."	<< endl;
                }
		cout << "# Total acceptance ratio: "
			<< mcmcEnd->getAcceptanceRatio() << endl
			<< mcmcEnd->getAcceptanceInfo() << '#';

		delete iter;
	}
}

/************************************************************************
 * Return info string on run.
 ************************************************************************/
std::string getRunInfoStr(int argc, char **argv, int seed)
{
	using namespace std;
	ostringstream oss;
	oss << "# Running: " << endl;
	oss << "#"; for (int i = 0; i < argc; i++) { oss << " " << argv[i]; } oss << endl;
	oss << "# of type " << Options.getStringAlt("RunType") << " with seed " << seed
		<< " in directory " << getenv("PWD") << endl;
	return oss.str();
}


/************************************************************************
 * Helper. Creates a 2-parameter density function from its string ID.
 ************************************************************************/
beep::Density2P* createDensity2P(std::string id, double mean, double variance)
{
	using namespace beep;
	Density2P* df = NULL;
	if      (id == "INVG")    { df = new InvGaussDensity(mean, variance);      }
	else if (id == "LOGN")    { df = new LogNormDensity(mean, variance);       }
	else if (id == "GAMMA")   { df = new GammaDensity(mean, variance);         }
	else if (id == "UNIFORM") { df = new UniformDensity(mean, variance, true); }
	else { throw AnError("Unknown Density2P identifier: " + id +'.'); }
	return df;
};


/************************************************************************
 * Creates default option values.
 ************************************************************************/
void createDefaultOptions()
{
	using namespace beep::option;
	
	Options.addStringOption("Output", "o", "",
			"  -o <string>\n"
			"    Output filename. Defaults to stderr.\n");
	Options.addUnsignedOption("Seed", "s", 0,
			"  -s <unsigned int>\n"
			"    Seed for pseudo-random number generator. Defaults to random seed.\n");
        // The conf_value_from_cmake_* values are set in CMakeLists.txt
	Options.addUnsignedOption("Iterations", "i", conf_value_from_cmake_Iterations,
			"  -i <unsigned int>\n"
			"    Number of iterations. Defaults to " conf_value_from_cmake_Iterations_string ".\n");
	Options.addUnsignedOption("Thinning", "t", conf_value_from_cmake_Thinning,
			"  -t <uint>\n"
			"    Thinning, i.e. sample every <value>-th iteration. Defaults to " conf_value_from_cmake_Thinning_string ".\n");
	Options.addUnsignedOption("PrintFactor", "w", conf_value_from_cmake_PrintFactor,
			"  -w <unsigned int>\n"
			"    Output diagnostics to stderr every <value>-th sample. Defaults to " conf_value_from_cmake_PrintFactor_string ".\n");
	Options.addBoolOption("Quiet", "q", false,
			"  -q \n"
			"    Do not output diagnostics. Non-quiet by default.\n");
	Options.addStringAltOption("RunType", "m", conf_value_from_cmake_RunType_string, "MCMC,PDHC,PD",
			"  -m <'MCMC'/'PDHC'/'PD'>\n"
			"    Execution type (MCMC, posterior density hill-climbing from initial,\n"
			"    values, or just initial posterior density). Defaults to "  conf_value_from_cmake_RunType_string " .\n",
			UPPER);
	Options.addStringAltOption("SubstModel", "Sm", conf_value_from_cmake_SubstModel_string, "UniformAA,JC69,JTT,UniformCodon,ArveCodon",
			"  -Sm <'UniformAA'/'JC69'/'JTT'/'UniformCodon'/'ArveCodon'>\n"
			"    Substitution model. " conf_value_from_cmake_SubstModel_string " by default.\n",
			UPPER);
	Options.addUserSubstModelOption("UserSubstModel", "Su",
			"  -Su <'DNA'/'AminoAcid'/'Codon'> <Pi=float1 ... floatn> <R=float1 ... float(n*(n-1)/2)>\n"
			"    User-defined substitution model. The size of Pi and R must fit data type \n"
			"    (DNA: n=4, AminoAcid: n=20, Codon: n=62). R is given as a flattened\n"
			"    upper triangular matrix. Don't use both option -Su and option -Sm.\n");
	Options.addUnsignedOption("SiteRateCats", "Sn", conf_value_from_cmake_SiteRateCats,
			"  -Sn <unsigned int>\n"
			"    Number of steps of discretized Gamma-distribution for rate\n"
			"    variation among sites. Defaults to "  conf_value_from_cmake_SiteRateCats_string " (no variation).\n");
	Options.addStringAltOption("EdgeRateDistr", "Ed", conf_value_from_cmake_EdgeRateDistr_string, "Gamma,InvG,LogN,Uniform",
			"  -Ed <'Gamma'/'InvG'/'LogN'/'Uniform'>\n"
			"    Distribution for IID rate variation among edges. Defaults to "  conf_value_from_cmake_EdgeRateDistr_string ".\n",
			UPPER);
	Options.addDoubleX2Option("EdgeRateParams", "Ep", pair<double,double>(conf_value_from_cmake_EdgeRateParams_first, conf_value_from_cmake_EdgeRateParams_second),
			"  -Ep <float> <float>\n"
			"    Initial mean and variance of edge rate distribution.\n"
			"    Defaults to " conf_value_from_cmake_EdgeRateParams_first_string " and " conf_value_from_cmake_EdgeRateParams_second_string ".\n");
	Options.addBoolOption("EdgeRateFixed", "Ef", false,
			"  -Ef \n"
			"    Fix mean and variance of edge rate distribution. Non-fixed by default.\n");
	Options.addStringOption("GuestTree", "Gi", "",
			"  -Gi <string>\n"
			"    Filename with initial guest tree topology.\n");
	Options.addBoolOption("FixedG", "Gg", false,
			"  -Gg \n"
			"    Fix initial guest tree topology, i.e. perform no branch-swapping.\n"
			"    Non-fixed by default.\n");
	Options.addDoubleX3Option("BirthDeathTransfer", "Bp", conf_value_from_cmake_BirthDeathTransfer_first, conf_value_from_cmake_BirthDeathTransfer_second, conf_value_from_cmake_BirthDeathTransfer_third,
			"  -Bp <float> <float> <float>\n"
			"    Initial duplication, loss and transfer rates. Defaults to\n"
			"    "  conf_value_from_cmake_BirthDeathTransfer_first_string ", " conf_value_from_cmake_BirthDeathTransfer_second_string ", "  conf_value_from_cmake_BirthDeathTransfer_third_string ". A rate initialized to 0 will not be perturbed.\n");
	Options.addBoolOption("FixedBDTRates", "Bf", false,
			"  -Bf \n"
			"    Fix duplication, loss and transfer rates to initial values.\n"
			"    Non-fixed by default.\n");
	Options.addDoubleOption("TopTime", "Bt", 1.0,
			"  -Bt <float>\n"
			"    Override time span of edge above root in host tree. Must be greater than 0.\n"
			"    Defaults to file-contained value.\n");
	Options.addDoubleOption("Timestep", "Dt", conf_value_from_cmake_DiscTimestep,
			"  -Dt <float>\n"
			"    Approximate discretization timestep. Set to 0 to divide edge \n"
			"    generations into the same amount of parts (see -Di). Defaults to " conf_value_from_cmake_DiscTimestep_string ".\n");
	Options.addUnsignedOption("Intervals", "Di", conf_value_from_cmake_DiscMinIvs,
			"  -Di <unsigned int>\n"
			"    Minimum number of discretization subintervals per edge generation.\n"
			"    If -Dt is set 0, this becomes the exact number of subintervals.\n"
			"    Minimum 2. Defaults to " conf_value_from_cmake_DiscMinIvs_string ".\n");
	Options.addUnsignedOption("TransferCounts", "C", conf_value_from_cmake_TransferCounts,
			"  -C <unsigned int>\n"
			"    Number of transfer counts during sampling. Defaults to " conf_value_from_cmake_TransferCounts_string " .\n");
	Options.addBoolOption("Rescale", "r", false,
			"  -r \n"
			"    Rescale the host tree so that the root-to-leaf time equals 1.0. All\n"
			"    inferred parameters will refer to the new scale. Off by default.\n");
	Options.addBoolOption("DoNotPrintElapsedComputeTime", "Z", false,
			"  -Z \n"
                              "       Do not print elapsed wall time and CPU time\n");
	Options.addBoolOption("DoNotPrintRunInfo", "W", false,
			"  -W \n"
                              "      Do not print the command line\n");
	Options.addBoolOption("DebugInfo", "debuginfo", false,
			"  -debuginfo \n"
			"    Show misc. info to stderr before iterating. Not shown by default.\n");

};


/************************************************************************
 * Helper. Reads program params passed as arguments.
 ************************************************************************/
void readParameters(int& argIndex, int argc, char **argv)
{
	if (argc - argIndex < NO_OF_PARAMS)
	{
		throw beep::AnError("Too few input parameters!");
	}
	Params["SeqData"] = argv[argIndex++];  // 1: Sequence data.
	Params["HostTree"] = argv[argIndex++]; // 2: Host tree.
	Params["GSMap"] = (argIndex < argc) ?  // 3: G-S map, optional.
			argv[argIndex++] : "";	
};


/************************************************************************
 * Returns info string for program usage.
 ************************************************************************/
std::string getUsageStr()
{
	return "Usage:   [<options>] <seqfile> <hostfile> [<gsfile>]\n";
}


/************************************************************************
 * Prints help message to stderr.
 ************************************************************************/
void showHelpMsg()
{
	using std::cerr;
	using std::endl;
	cerr
	<< "================================================================================" << endl
	<< getUsageStr()
	<< endl
	<< "Parameters:" << endl
	<< "  <seqfile>     (string), file with aligned sequences for guest tree leaves." << endl
	<< "  <hostfile>    (string), PrIME Newick file with host tree incl. divergence times." << endl
	<< "                          Leaves must have time 0 and root have time > 0." << endl
	<< "  <gsfile>      (string), tab-delimited file relating guest tree leaves to host" << endl
	<< "                          tree leaves if info not included in <hostfile>." << endl
	<< endl
	<< "Description: Guest-in-host tree inference enabling reconciliation analysis" << endl
	<< "using the underlying DTLRS model. Model properties:" << endl
	<< "  1) The guest tree topology evolves inside the host tree by means of duplication," << endl
	<< "  loss and horizontal transfer events guided by homogeneous rates, much" << endl
	<< "  like a birth-death process. Lineages branch deterministically at host tree nodes." << endl
	<< "  2) Relaxed molecular clock; sequence evolution rate variation over guest tree edges." << endl
	<< "  Rates are drawn iid from specified distribution. Also, rate variation over sites" << endl
	<< "  may be added, using discretized gamma distribution with mean 1." << endl
	<< "  3) Substitution model of choice; standard or user-defined." << endl
	<< "The implementation uses a discretization of the host tree to approximate the" << endl
	<< "probability of all possible reconciliation realizations for the current parameter" << endl
	<< "state." << endl
	<< endl
	<< "Options:" << endl
	<< "  -h/-u/-?" << endl
	<< "    Display help (this text)." << endl
	<< Options
	<< "================================================================================" << endl;
}
