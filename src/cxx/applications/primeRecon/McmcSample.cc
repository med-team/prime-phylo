/* 
 * File:   McmcSample.cpp
 * Author: naurang
 * 
 * Created on November 29, 2011, 6:38 PM
 */

#include "McmcSample.hh"
#include <boost/foreach.hpp>
#include <string>
#include <sstream>

namespace beep {

    McmcSample::McmcSample() {
    }

    McmcSample::McmcSample(const McmcSample& orig) {
    }

    McmcSample::~McmcSample() {
    }

    void McmcSample::setTreeStrRep(string _tree) {
        this->treeStrRep = _tree;
    }

    string McmcSample::getTreeStrRep() {
        return this->treeStrRep;
    }

    void McmcSample::setVvProbs(map<string, double> _probs) {
        this->vvProbs = _probs;
    }

    map<string, double>  McmcSample::getVvProbs() {
        return this->vvProbs;
    }

    void McmcSample::setBirthRate(Real _birth){
        this->birth = _birth;
    }

    void McmcSample::setDeathRate(Real _death){
        this->death = _death;
    }

    void McmcSample::setMeanRate(Real _mean){
        this->mean = _mean;
    }

    void McmcSample::setVarianceRate(Real _variance){
        this->variance = _variance;
    }

    string McmcSample::getStringRepresentation() {
        ostringstream ostr;

        ostr << this->getTreeStrRep() << ";\t";
        
        map<string, double> tVvProbs = this->vvProbs;
        map<string, double>::const_iterator iter;
        for (iter = tVvProbs.begin(); iter != tVvProbs.end(); ++iter) {
            //str.append(iter->second);
            ostr << iter->second << ";\t";
            //cout << iter->second << "  ";
            //str.append(";\t");
        }
        //cout << tVvProbs.size() << endl;
        ostr << this->mean << ";\t";
        ostr << this->variance << ";\t";
        ostr << this->birth << ";\t";
        ostr << this->death << ";\t";
        //cout << ostr.str() << endl;

        return ostr.str();
    }
};
