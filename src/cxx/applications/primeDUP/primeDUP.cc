// JOELGS: Commenting this until Peter's and Mattias' work has been revived.

///*
// * File:   prime_test.cpp
// * Author: fmattias
// *
// * Created on November 25, 2009, 2:59 PM
// */
//
//#include <stdlib.h>
//
//#include "LogNormDensity.hh"
//
//#include <MatrixTransitionHandler.hh>
//#include <SequenceData.hh>
//#include <StrStrMap.hh>
//#include <mcmc/PosteriorSampler.hh>
//#include <Tree.hh>
//#include <TreeIO.hh>
//#include <SeqIO.hh>
//#include <GammaDensity.hh>
//
//#include "Density2P_common.hh"
//#include "TreeDiscretizers.hh"
//#include "EdgeDiscTree.hh"
//#include "EdgeDiscPtKeyIterator.hh"
//#include "lsd/ApproximateEstimator.hh"
//#include "lsd/LSDGeneTreeGenerator.hh"
//#include "SequenceGenerator.hh"
//#include "lsd/LSDProbs.hh"
////#include "lsd/LSDModelSelector.hh"
//#include "DiscretizedDensity.hh"
//
//#include "cmdline.h"
//#include "InvGaussDensity.hh"
//#include "io/LSDFileFormat.hxx"
//#include "TimeEstimator.hh"
////#include "mcmc/ModelSampler.hh"
//#include "LogFilePrinter.hh"
//
//static const unsigned int PRIME_DUP_ARG_MIN = 3;
//static const unsigned int PRIME_DUP_ARG_MAX = 4;
//static const int MCMC_SKIP = 100;
//
//#define SPECIES_TREE_ARG 0
//#define SEQUENCE_ARG 1
//#define GENE_SPECIES_MAP_ARG 2
//#define LSD_ARG 3
//
//using namespace beep;
//using namespace std;
//
//void readLSDProbabilities(const char *path,
//                          EdgeDiscTree &discretizedSpeciesTree,
//                          LSDProbs &lsd);
//
//Density2P *createDensity(string density, float mean, float variance);
//
///*
// *
// */
//int main(int argc, char** argv)
//{
//    /* Initialize command options parser */
//    struct gengetopt_args_info args_info;
//
//    if(cmdline_parser(argc, argv, &args_info) != 0){
//        exit(EXIT_FAILURE);
//    }
//
//    if(args_info.inputs_num < PRIME_DUP_ARG_MIN) {
//        cerr << "primeDUP: Error: Too few arguments" << endl;
//        exit(EXIT_FAILURE);
//    }
//    if(args_info.inputs_num > PRIME_DUP_ARG_MAX) {
//        cerr << "primeDUP: Error: Too many arguments" << endl;
//    }
//
//    string logFilePath(args_info.log_file_arg);
//    LogFilePrinter::setOutputFile(logFilePath);
//
//    /* Read sequence data */
//    SequenceData sequences(SeqIO::readSequences(args_info.inputs[SEQUENCE_ARG]));
//
//    /* Read species tree */
//    TreeIO speciesReader(TreeIO::fromFile(args_info.inputs[SPECIES_TREE_ARG]));
//    Tree speciesTree(speciesReader.readHostTree());
//    cout << speciesTree << endl;
//    EquiSplitEdgeDiscretizer discretizer(args_info.discretization_intervals_arg);
//    EdgeDiscTree *discSpeciesTree = discretizer.getDiscretization(speciesTree);
//
//    /* Real gene species map */
//    StrStrMap geneSpeciesMap(
//            TreeIO::readGeneSpeciesInfo(args_info.inputs[GENE_SPECIES_MAP_ARG])
//            );
//
//    /* Create density and substitution model */
//    MatrixTransitionHandler substitutionModel =
//            MatrixTransitionHandler::create(args_info.substitution_model_arg);
//
//    Density2P *rateDensity = createDensity(args_info.density_function_arg,
//                                           args_info.mean_arg,
//                                           args_info.variance_arg);
//    Real min = 1e-10;
//    Real max = 10;
//    rateDensity->setRange(min, max);
//
//    /* Read LSD estimates if available */
//    LSDProbs lsdEstimates;
//    if(args_info.inputs_num == 4) {
//        /* Read LSD probabilities */
//        readLSDProbabilities(args_info.inputs[LSD_ARG],
//                             *discSpeciesTree,
//                             lsdEstimates);
//    }
//    else {
//        /* Estimate LSDs */
//        Density2P *rateDensity = createDensity(args_info.density_function_arg,
//                                           args_info.mean_arg,
//                                           args_info.variance_arg);
//
//        cout << "Running LSD estimator" << endl;
//        TimeEstimator *timer = TimeEstimator::instance();
//        timer->reset(args_info.estimation_burn_in_iterations_arg +
//                     args_info.estimation_fixed_iterations_arg +
//                     args_info.estimation_map_iterations_arg +
//                     args_info.estimation_iterations_arg*100);
//        timer->start();
//        PosteriorSampler mcmc(sequences, speciesTree, geneSpeciesMap, substitutionModel, *rateDensity, args_info.discretization_intervals_arg);
//        ApproximateEstimator lsdEstimator(*mcmc.getGSRSampler()->getDiscretizedTree(),
//                                          mcmc,
//                                          args_info.estimation_burn_in_iterations_arg,
//                                          args_info.estimation_fixed_iterations_arg,
//                                          args_info.estimation_map_iterations_arg);
//        lsdEstimator.getLSDs(lsdEstimates, args_info.estimation_iterations_arg);
//
//        /* Output estimations to file if the user wanted it */
//        if(args_info.estimation_output_given){
//            ofstream estimationOutput(args_info.estimation_output_arg);
//            lsdEstimator.writeToXML(estimationOutput);
//            estimationOutput.close();
//        }
//
//        if(args_info.estimation_only_flag) {
//            exit(EXIT_SUCCESS);
//        }
//
//        delete rateDensity;
//    }
//
//#if 0
//    /* Create the probability values on which will perform model selection  */
//    vector<Probability> test;
//
//    if(!args_info.single_test_given) {
//        int numTests = args_info.multiple_tests_arg;
//        for(int i = 0; i <= numTests; i++) {
//            test.push_back((1.0/numTests)*i);
//        }
//    }
//    else {
//        test.push_back(args_info.single_test_arg);
//    }
//
//    /* Try alternative LSD models */
//    vector<PosteriorSampler *> models;
//    for(unsigned int i = 0; i < test.size(); i++) {
//
//        cout << "burning in chain for probability " << test[i].val() << endl;
//
//        lsdEstimates.setAll(test[i]);
//
//        Density2P *rateDensity = createDensity(args_info.density_function_arg,
//                                               args_info.mean_arg,
//                                               args_info.variance_arg);
//        PosteriorSampler *model = new PosteriorSampler(sequences,
//                                                       speciesTree,
//                                                       geneSpeciesMap,
//                                                       substitutionModel,
//                                                       *rateDensity,
//                                                       *discSpeciesTree,
//                                                       lsdEstimates
//                                                       );
//        model->nextState(args_info.burn_in_iterations_arg);
//        models.push_back(model);
//    }
//
//    DummyMCMC dummy;
//    LSDModelMCMC modelMCMC(dummy, models);
//    ModelSampler modelSampler(&modelMCMC);
//
//    modelSampler.nextState(args_info.iterations_arg);
//
//    std::vector<unsigned int> modelVisits = modelMCMC.getModelVisits();
//
//    /* Print bayes factors */
//    for(unsigned int i = 0; i < modelVisits.size(); i++) {
//        cout << "Pr[M = " << test[i].val() << " | D] = " <<  (float)modelVisits[i] / args_info.iterations_arg << endl;
//    }
//#endif
//    return (EXIT_SUCCESS);
//}
//
//void readLSDProbabilities(const char *path,
//                          EdgeDiscTree &discretizedSpeciesTree,
//                          LSDProbs &lsd)
//{
//    ifstream lsdFile(path);
//    LSDProbs::readFromFile(lsdFile, discretizedSpeciesTree, lsd);
//    lsdFile.close();
//}
//
//Density2P *
//createDensity(string density, float mean, float variance)
//{
//
//    /* Set embedded parameters if density is uniform */
//    bool embedded = density == "UNIFORM";
//    Density2P *densityFunction = Density2P_common::createDensity(mean, variance, embedded, density);
//
//    /* Check that density function does not include negative rates */
//    Real min, max;
//    densityFunction->getRange(min, max);
//    if(min <= 0){
//        cerr << "genGSR: Error: Parameters of density function allows "
//                "negative rates." << endl;
//        exit(EXIT_FAILURE);
//    }
//
//    return densityFunction;
//}
