/* 
 * File:   OrthoUtils.cc
 * Author: ikramu
 * 
 * Created on October 22, 2012, 1:19 PM
 */

#include "OrthoUtils.hh"

OrthoUtils::OrthoUtils() {
}

OrthoUtils::OrthoUtils(const OrthoUtils& orig) {
}

OrthoUtils::~OrthoUtils() {
}

void OrthoUtils::estimate_orthologies(string posterior_file, string true_tree, string specie_tree,
        vector<string> leaves, vector<map<string, double> > &ortho_list) {
    //vector<map<string, double> > ortho_list;
    string gene_tree; //Gene tree
    ifstream sample_file; //File object for samples
    string curr_line; //Current line when reading samples
    int burn_in = 1;
    string header = "";
    bool endOfHeader = false;
    int hl;
    double mean, var;
    double birth, death;

    /*Read samples and create states*/
    sample_file.open(posterior_file.c_str());
    if (sample_file.is_open()) {
        try {
            while (!sample_file.eof()) {
                //Skip lines of comments
                if ((sample_file.peek() == '#') && !endOfHeader) {
                    sample_file.ignore(SAMPLE_LENGTH, '\n');
                    getline(sample_file, header);

                    Tokenizer tok(";");
                    tok.setString(header);
                    hl = 0;
                    while (tok.hasMoreTokens()) {
                        tok.getNextToken();
                        hl++;
                    }

                    continue;
                } else if ((sample_file.peek() == '#') && endOfHeader) {
                    // don't do anything, this is the summary at 
                    // the end of prime output.
                    sample_file.ignore(SAMPLE_LENGTH, '\n');
                    continue;
                }
                if (burn_in <= BURN_IN) {
                    sample_file.ignore(SAMPLE_LENGTH, '\n');
                    burn_in++;
                    continue;
                }

                // set the endOfHeader to true, if not done already
                if (!endOfHeader)
                    endOfHeader = true;

                //Get next line from file
                curr_line = "";
                getline(sample_file, curr_line);

                //Skip empty lines
                if (curr_line == "") {
                    cout << "line is empty..." << endl;
                    continue;
                }

                get_data_from_sample(curr_line, gene_tree, mean, var, birth, death);

                DLRSOrthoCalculator *dos = new DLRSOrthoCalculator(gene_tree, specie_tree, mean, var, birth, death, true);
                map<string, double> ortho_prob = dos->getOrthoEstimates();
                ortho_list.push_back(ortho_prob);
                // only take the samples after burn-in                
                //cout << "sample number: " << burn_in << endl;
                cout << "read gene tree from sample no: " << burn_in << endl;
                //cout << G << endl;
                burn_in++;

                //find_vertices_single(vertices, gsr, ID, inv_ID);
                //find_vertices_single(vertices, G, ID, inv_ID);
                delete(dos);
            }
        } catch (exception &ex) {
            cout << ex.what() << endl;
        }
        sample_file.close();
        //num_states = (burn_in - 1) - BURN_IN;
        //cout << "Total samples in hp_vertices are " << num_states << endl;
        //find_high_post_vertices(vertices, hp_vertices, numStates * posterior_threshold);
    } else {
        cerr << "Could not open file: " << sample_file << endl;
        exit(EXIT_FAILURE);
    }
}

void OrthoUtils::get_data_from_sample(string curr_line, string &gene_tree, double &mean, double &var, double &birth, double &death) {
    //TreeIO tree_reader; // used for parsing prime trees
    Tokenizer tokenizer(";"); //String tokenizer    

    /*
     * Information in each line is separated by ';'. The tokenizer
     * splits the line at each ';'
     */
    //Set tokenizer
    tokenizer.setString(curr_line);

    //Skip info about likelihoods and number of iterations
    //cout << tokenizer.getNextToken() << endl;
    //cout << tokenizer.getNextToken() << endl;
    tokenizer.getNextToken();
    tokenizer.getNextToken();

    //Read gene tree
    //tree_reader.setSourceString(tokenizer.getNextToken());

    gene_tree = tokenizer.getNextToken();
    mean = atof(tokenizer.getNextToken().c_str());
    var = atof(tokenizer.getNextToken().c_str());
    tokenizer.getNextToken();
    birth = atof(tokenizer.getNextToken().c_str());
    death = atof(tokenizer.getNextToken().c_str());
}

map<string, double> OrthoUtils::compute_ortho_single(string gene_tree, string specie_tree, double mean,
        double var, double birth, double death, vector<string> leaves) {
    //double mean = 0.0545746;
    //double var = 0.000356028;
    //double birth = 3.54539;
    //double death = 3.6247;

    //string gene_tree = "./data/gene.tree";
    //string specie_tree = "/home/ikramu/PrIME/trunk/src/cxx/tools/ortho_true_vertices/data/mhcLasse.stree";
    //string true_tree = "/home/ikramu/PrIME/trunk/src/cxx/tools/ortho_true_vertices/data/g95.true";

    //vector<string> leaves = get_leaves_from_true_file(true_tree);

    DLRSOrthoCalculator dos(gene_tree, specie_tree, mean, var, birth, death);
    map<string, double> ortho_estimates = dos.getOrthoEstimates();

    return ortho_estimates;
}

vector<string> OrthoUtils::get_leaves_from_true_file(string true_file) {
    vector<string> leaves;
    string tree;
    //ifstream tf(true_file);
    std::fstream ifs(true_file.c_str());
    if (ifs) {
        std::stringstream oss;
        oss << ifs.rdbuf();
        Tokenizer tk(";");
        tree = oss.str();
        tk.setString(tree);
        //string tree;
        while (tk.hasMoreTokens()) {
            string s = tk.getNextToken();
            if (s != "\n") {
                trim(s);
                tree = s;
            }

        }
    }

    TreeIO tio = TreeIO::fromString(tree);
    Tree g = tio.readBeepTree(NULL, NULL);

    vector<Node*> all_nodes = g.getAllNodes();

    //cout << g << endl;

    for (vector<Node*>::iterator i = all_nodes.begin(); i != all_nodes.end(); i++) {
        //cout << (*i)->getNumber() << endl;
        if ((*i) != NULL && !((*i)->isLeaf())) {
            vector<Node*> dnodes = g.getDescendentNodes(*i);
            stringstream strstream;
            for (unsigned int i = 0; i < dnodes.size(); i++)
                strstream << dnodes[i]->getName() << " ";

            string list = strstream.str();
            trim(list);
            leaves.push_back(list);
            //cout << "For " << (*i)->getNumber() << " the leaves are: [" << list << "]" << endl;
        }
    }
    return leaves;
}

vector<string> OrthoUtils::get_gene_pair_from_true_tree(string true_file) {
    vector<string> leaves;
    string tree;

    std::fstream ifs(true_file.c_str());
    if (ifs) {
        std::stringstream oss;
        oss << ifs.rdbuf();
        Tokenizer tk(";");
        tree = oss.str();
        tk.setString(tree);
        //string tree;
        while (tk.hasMoreTokens()) {
            string s = tk.getNextToken();
            if (s != "\n") {
                trim(s);
                tree = s;
            }

        }
    }

    TreeIO tio = TreeIO::fromString(tree);
    Tree g = tio.readBeepTree(NULL, NULL);
    cout << g << endl;
    vector<Node*> all_nodes = g.getAllNodes();

    for (vector<Node*>::iterator iter = all_nodes.begin(); iter != all_nodes.end(); iter++) {
        if ((*iter != NULL) && !(*iter)->isLeaf()) {
            vector<Node*> lNodes = g.getDescendentNodes((*iter)->getLeftChild());
            vector<Node*> rNodes = g.getDescendentNodes((*iter)->getRightChild());
            for (unsigned int i = 0; i < lNodes.size(); i++) {
                for (unsigned int j = 0; j < rNodes.size(); j++) {
                    if (not_same_specie(lNodes[i]->getName(), rNodes[j]->getName())) {
                        vector<string> vp;
                        vp.push_back(lNodes[i]->getName());
                        vp.push_back(rNodes[j]->getName());
                        sort(vp.begin(), vp.end());
                        
                        string cpair = vp[0] + (string)" " + vp[1];
                        leaves.push_back(cpair);
                    }
                }
            }
        }
    }

    return leaves;
}

void OrthoUtils::print_map(map<string, double> tmap) {

    for (map<string, double>::iterator iter = tmap.begin(); iter != tmap.end(); iter++) {
        cout << iter->first << ":" << iter->second << endl;
    }
}

bool OrthoUtils::not_same_specie(string left_gene, string right_gene) {
    if(get_specie_from_gene_name(left_gene) == get_specie_from_gene_name(right_gene))
        return false;
    else 
        return true;
}

string OrthoUtils::get_specie_from_gene_name(string gene) {
    Tokenizer tk("_");
    tk.setString(gene);
    
    // example would be like gene_mus_1 or gene_homo_2
    // first is the word "gene" so skip
    tk.getNextToken();
    
    // return the next one which is specie name
    return tk.getNextToken();
}

map<string, double> OrthoUtils::normalize_orthologies(vector<map<string, double> > ortho_list) {
    map<string, double> norm_ortho = ortho_list[0];

    for (map<string, double>::iterator iter = norm_ortho.begin(); iter != norm_ortho.end(); iter++) {
        iter->second = 0.0;
    }

    for (unsigned int i = 0; i < ortho_list.size(); i++) {
        map<string, double> sample = ortho_list[i];
        for (map<string, double>::iterator j = norm_ortho.begin(); j != norm_ortho.end(); j++) {
            //if(sample)
            if (sample.find(j->first) != sample.end())
                j->second += sample.at(j->first);
        }
    }

    for (map<string, double>::iterator iter = norm_ortho.begin(); iter != norm_ortho.end(); iter++) {
        iter->second /= ortho_list.size();
        cout << iter->first << ":" << iter->second << endl;
    }

    return norm_ortho;
}

void OrthoUtils::write_orthologies(map<string, double> orthologies, string posterior_file) {
    string outfile = posterior_file + std::string(".computed");
    ofstream out;
    out.open(outfile.c_str());

    if (out.is_open()) {
        for (map<string, double>::iterator iter = orthologies.begin(); iter != orthologies.end(); iter++) {
            //if(iter != NULL)
            out << iter->first << "\t" << iter->second << endl;
            //cout << iter->first << ":" << iter->second << endl;
        }
    } else {
        cerr << "error: can't open " << outfile << " for writing" << endl;
    }
    out.close();
}

void OrthoUtils::print_vector(vector<string> vec) {
    for (vector<string>::iterator i = vec.begin(); i != vec.end(); i++)
        cout << *i << endl;
}

void OrthoUtils::set_orthology_map(vector<string> pairs){
    orthology_map.clear();
    for(vector<string>::iterator iter = pairs.begin(); iter != pairs.end(); iter++){
        orthology_map.insert(pair<string, double> (*iter, 0.0));
    }
}
