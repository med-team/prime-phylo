#include <iostream>
#include <Beep.hh>
#include <DLRSOrthoCalculator.hh>

#include "OrthoUtils.hh"

/* Namespaces used */
using namespace std;
using namespace beep;
using namespace boost::algorithm;

int main(int argc, char *argv[]) {

    if (argc != 4) {
        cerr << "\nerror: wrong number of arguments\n" << endl;
        cerr << "usage: ortho_true_trees posterior_file specie_file true_tree" << endl;
        cerr << "posterior_file: DLRS posterior file containing samples from Pr[G,l,\\theta|D,S]" << endl;
        cerr << "specie_file: specie tree (a.k.a S in above formula)" << endl;
        cerr << "true_tree: true reconciliation for synthetic gene tree made of leaves represented by synthetic sequences (a.k.a D in above)" << endl;
        cerr << endl;
        exit(1);
    }

    //string specie_tree = "/home/ikramu/PrIME/trunk/src/cxx/tools/ortho_true_vertices/data/mhcLasse.stree";
    //string true_tree = "/home/ikramu/PrIME/trunk/src/cxx/tools/ortho_true_vertices/data/g95.true";
    //string posterior_file = "/home/ikramu/PrIME/trunk/src/cxx/tools/ortho_true_vertices/data/g95.all.test";

    string posterior_file = argv[1];
    string specie_tree = argv[2];
    string true_tree = argv[3];
    
    OrthoUtils outils;

    /*
    vector<string> lst;
    lst.push_back("gene_homo_1");
    lst.push_back("gene_mus_1");
    lst.push_back("gene_homo_2");
    lst.push_back("gene_mus_2");
    lst.push_back("gene_mus_3");

    outils.print_vector(lst);
    cout << endl;
    std::sort(lst.begin(), lst.end());
    outils.print_vector(lst);
    */
    
    vector<map<string, double> > ortho_list;

    TreeIO tio = TreeIO::fromFile(specie_tree);
    Tree sptree = tio.readHostTree();

    //vector<string> leaves = outils.get_leaves_from_true_file(true_tree);
    vector<string> leaves = outils.get_gene_pair_from_true_tree(true_tree);
    outils.set_orthology_map(leaves);
    outils.print_vector(leaves);

    outils.estimate_orthologies(posterior_file, true_tree, specie_tree, leaves, ortho_list);

    cout << "total samples are " << ortho_list.size() << endl;

    map<string, double> norm_ortho = outils.normalize_orthologies(ortho_list);
    outils.write_orthologies(norm_ortho, posterior_file);
    return 0;
}



