/* Reads a set of files containing the expected number of duplications at each
 * discretization point. The files are combined either to one file where the
 * average is taken over the expected values, or an SVG graph is plotted.
 *
 *
 * File:   lsdcombine.cc
 * Author: fmattias
 *
 * Created on November 16, 2009, 11:53 AM
 */

#include <cmath>
#include <fstream>
#include <iostream>
#include <plotter.h>

#include "Beep.hh"
#include "io/LSDEstimatesFormat.hxx"

#include "cmdline.h"
#include "DuplicationStatistics.hh"
#include "GraphPlotter.hh"

/**
 * In this file exDup is a acronym for the expected number of duplications.
 */

// Height and width of the graphs
static const double GRAPH_WIDTH = 400.0;
static const double GRAPH_HEIGHT = 200.0;

// The spacing between graphs
static const double GRAPH_SPACING = 50.0;

// The total plot area
static const double PLOT_WIDTH = 702.0*2;
static const double PLOT_HEIGHT = 702.0*2;

using namespace beep;
using namespace std;

// An type used for functions passed to the processXML function
typedef void (*XMLProcesser)(DuplicationStatistics &, edgeType &, exDupType &);

/**
 * Applies the process function to each of the elements in the XML document.
 *
 * expectedDuplications - Contains expected number of duplications for each
 *                        discretization point.
 * averageOutputXML - Output XML document.
 * XMLProcessor - A function that will be applied to each of the elements in the
 *                given XML document.
 */
auto_ptr<LSDEstimatesType> processXML(DuplicationStatistics &expectedDuplications, 
                                      auto_ptr<LSDEstimatesType> averageOutputXML,
                                      XMLProcesser process);

/**
 * updateAverage
 *
 * Adds data points to expectedDuplications from the given XML elements.
 *
 * NOTE: This function is intended to be used as an XMLProcesser.
 *
 * expectedDuplications - Contains expected number of duplications for each
 *                        discretization point.
 * edge - The edge of the data point being added.
 * exDup - The structure holding the data point and information about the
 *         discretization point.
 */
void updateAverage(DuplicationStatistics &expectedDuplications, edgeType &edge, exDupType &exDup);

/**
 * saveAveragedData
 *
 * Updates the value of exDup with the updated average given in
 * expectedDuplications.
 *
 * NOTE: This function is intended to be used as an XMLProcesser.
 *
 * expectedDuplications - Contains expected number of duplications for each
 *                        discretization point.
 * edge - The edge of the data point being added.
 * exDup - The structure holding the data point and information about the
 *         discretization point.
 */
void saveAverage(DuplicationStatistics &expectedDuplications, edgeType &edge, exDupType &exDup);

/**
 * printAverageData
 *
 * Prints the averaged expected number of duplications in a user friendly
 * format to the specified output stream.
 *
 * expectedDuplications - Contains expected number of duplications for each
 *                        discretization point.
 * output - The stream where the averaged data will be written.
 */
void printAverageData(DuplicationStatistics &expectedDuplications,
                      ostream &output);

/**
 * plotSVG
 *
 * Prints the averaged expected number of duplications graphically in a SVG
 * format to the specifed output stream.
 *
 * expectedDuplications - Contains expected number of duplications for each
 *                        discretization point.
 * svgOutputFile - The stream that the SVG will be written to.
 */
void plotSVG(DuplicationStatistics &expectedDuplications,
             ostream &svgOutputFile);

/**
 * plotGraphPerEdge
 *
 * Plots a graph over each edge in the specified plotter.
 * 
 * plotter - The plotter that will draw the graphs.
 * expectedDuplications - Contains expected number of duplications for each
 *                        discretization point.
 * graphSpacing - The space between each graph and the edges of the canvas.
 */
void plotGraphPerEdge(Plotter &plotter, 
                      DuplicationStatistics &expectedDuplications,
                      double graphSpacing);

int main(int argc, char** argv)
{
    /* Initialize command options parser */
    struct gengetopt_args_info args_info;

    if(cmdline_parser(argc, argv, &args_info) != 0){
        exit(EXIT_FAILURE);
    }

    if(args_info.inputs_num < 1){
        cerr << "lsdcombine: Error: No lsd files specified." << endl;
        exit(EXIT_FAILURE);
    }

    /* The number of families is equal to the number of input files */
    int numberOfFamilies = args_info.inputs_num;

    /* Setup structure that holds the average */
    DuplicationStatistics expectedDuplications;
    
    /* Parse each file and update statistics */
    for(int family = 0; family < numberOfFamilies; family++) {
        /* Process each file and update score */
        auto_ptr<LSDEstimatesType> familyFile( LSDEstimates(args_info.inputs[family]) );
        processXML(expectedDuplications, familyFile, updateAverage);
    }

    /* Save averaged data in new file */
    if(args_info.xml_output_given) {
        /* Take data from the first file */
        auto_ptr<LSDEstimatesType> averageXML( LSDEstimates(args_info.inputs[0]) );

        /* Update it with average */
        averageXML = processXML(expectedDuplications, averageXML, saveAverage);

        /* Save it in a new xml file */
        xml_schema::namespace_infomap map;
        map[""].name = "";
        map[""].schema = "http://www.nada.kth.se/~fmattias/LSDEstiamtesFormat.xsd";

        ofstream averageOutputFile(args_info.xml_output_arg);
        LSDEstimates(averageOutputFile, *averageXML, map);
        averageOutputFile.close();
    }
    else {
        printAverageData(expectedDuplications, cout);
    }

    /* Plot to svg if user wanted it */
    if(args_info.svg_output_given) {
        ofstream svgOutputFile(args_info.svg_output_arg);
        plotSVG(expectedDuplications, svgOutputFile);
        svgOutputFile.close();
    }

    return (EXIT_SUCCESS);
}

auto_ptr<LSDEstimatesType> processXML(DuplicationStatistics &expectedDuplications, 
                                      auto_ptr<LSDEstimatesType> xmlFile,
                                      XMLProcesser process)
{
    LSDEstimatesType::edge_iterator edge_it, edge_itEnd;
    edge_it = xmlFile->edge().begin();
    edge_itEnd = xmlFile->edge().end();

    /* Iterate over each edges */
    for(; edge_it != edge_itEnd; ++edge_it) {
        /* Object for current edge */
        edgeType &edge = *edge_it;

        /* Set up iterator for expected number of duplication elements */
        edgeType::exDup_iterator exDup_it, exDup_itEnd;
        exDup_it = edge.exDup().begin();
        exDup_itEnd = edge.exDup().end();

        /* Iterate over exDup elements and process them */
        for(; exDup_it != exDup_itEnd; ++exDup_it) {
            exDupType &exDup = *exDup_it;
            process(expectedDuplications, edge, exDup);
        }
    }

    return xmlFile;
}

void updateAverage(DuplicationStatistics &expectedDuplications,
                   edgeType &edge,
                   exDupType &exDup)
{
    expectedDuplications.addDataPoint(edge.id(), exDup.id(), exDup.relTime(), exDup);
}

void saveAverage(DuplicationStatistics &expectedDuplications,
                                        edgeType &edge,
                                        exDupType &exDup)
{
    // Convert to double and update
    double &exDupValue = exDup;
    exDupValue = expectedDuplications.getAverage(edge.id(), exDup.id());
}

void printAverageData(DuplicationStatistics &expectedDuplications, ostream &output)
{
    /* Setup iterator for discretization point statistics */
    DuplicationStatistics::iterator expectedDuplications_it, expectedDuplications_itEnd;

    expectedDuplications_it = expectedDuplications.begin();
    expectedDuplications_itEnd = expectedDuplications.end();

    /* For each discretization vertex ordered first by the edges print
     * data to stdout */
    for(; expectedDuplications_it != expectedDuplications_itEnd; ++expectedDuplications_it) {
        int edgeId = (*expectedDuplications_it).first;
        int discPointId = (*expectedDuplications_it).second;
        output << "edge: " << edgeId << " id: " << discPointId << " -> " << expectedDuplications.getAverage(edgeId, discPointId) << endl;
    }
}
void plotSVG(DuplicationStatistics &expectedDuplications, ostream &svgOutputFile)
{
    PlotterParams plotParameters;
    plotParameters.setplparam("PAGESIZE", (char *)"a4");

    SVGPlotter plotter(cin, svgOutputFile, cerr, plotParameters);

    if(plotter.openpl() < 0) {
        cerr << "lsdcombine: error: Could not open plotter.\n";
        exit(EXIT_FAILURE);
    }

    plotter.fspace (0.0, 0.0, PLOT_WIDTH, PLOT_HEIGHT); // specify user coor system
    plotter.erase();

    plotGraphPerEdge(plotter, expectedDuplications, GRAPH_SPACING);

    if (plotter.closepl () < 0) {
        cerr << "lsdcombine: error: Could not close plotter\n";
        exit(EXIT_FAILURE);
    }
}

void plotGraphPerEdge(Plotter &plotter, 
                     DuplicationStatistics &expectedDuplications,
                     double graphSpacing)
{
    /* A container of graphs */
    map<int, GraphPlotter *> graphs;

    /* Setup iterator over statistics data */
    DuplicationStatistics::iterator expectedDuplications_it, expectedDuplications_itEnd;
    expectedDuplications_it = expectedDuplications.begin();
    expectedDuplications_itEnd = expectedDuplications.end();

    /* For each discretization point add a data point */
    for(; expectedDuplications_it != expectedDuplications_itEnd; ++expectedDuplications_it) {
        int edgeId = (*expectedDuplications_it).first;
        int discPoint = (*expectedDuplications_it).second;

        /* If graph does not exist, create one */
        if(graphs[edgeId] == 0) {
            GraphPlotter *newGraph = new GraphPlotter(GRAPH_HEIGHT, GRAPH_WIDTH);
            ostringstream graphName;
            graphName << "Edge: " << edgeId;
            newGraph->setName(graphName.str().c_str());
            graphs[edgeId] = newGraph;
        }

        /* Add data to graph */
        GraphPlotter *graphPlotter = graphs[edgeId];
        double average = expectedDuplications.getAverage(edgeId, discPoint);
        double std = expectedDuplications.getStd(edgeId, discPoint);
        graphPlotter->addDataPoint(expectedDuplications_it.getTime(), average, std);
    }

    map<int, GraphPlotter *>::iterator graphs_it, graphs_itEnd;
    graphs_it = graphs.begin();
    graphs_itEnd = graphs.end();

    /* Iterate over each edge and print the data for the points on that edge */
    int graphNumber = 0;
    for(; graphs_it != graphs_itEnd; ++graphs_it) {
        /* Position for graphs */
        double graphX = graphSpacing + (GRAPH_WIDTH + graphSpacing)*(graphNumber % 2);
        double graphY = PLOT_HEIGHT - (GRAPH_HEIGHT + graphSpacing)*(graphNumber / 2 + 1);

        GraphPlotter *graphPlotter = (*graphs_it).second;
        graphPlotter->plotGraph(plotter, graphX, graphY);

        graphNumber++;

        delete graphPlotter;
    }
}
