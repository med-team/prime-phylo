#include <iostream>
#include <cstdlib>
using namespace std;

#include "AnError.hh"
#include "LambdaMap.hh"
#include "TreeIO.hh"
using namespace beep;

void
usage(char *s)
{
  cerr << "Usage: "
       << s
       << " <gene tree> <species tree> <vertex> [<gene-species map>]\n"
       << "\nThis program outputs the gene vertex which is a potential"
       << "\nwhole genome duplication above the given species vertex."
       << "\nIf this is ambigous, we give up...\n"
       << endl;
}

int
main (int argc, char **argv) 
{
  

  if (argc < 4 || argc > 5)
    {
      usage(argv[0]);
      exit (1);
    }

  try {
    vector<SetOfNodes> AC(1000); // Unnecessary here
    StrStrMap          gs;

    TreeIO io = TreeIO::fromFile(argv[1]);
    Tree G = io.readGuestTree(&AC, &gs);

    io.setSourceFile(argv[2]);
    Tree S = io.readNewickTree();
    //    Tree S = io.readHostTree();

    unsigned vnum = atoi(argv[3]);

    if (argc == 5) {
      gs = TreeIO::readGeneSpeciesInfo(argv[4]);
    }

    LambdaMap lambda(G, S, gs);
    GammaMap gamma = GammaMap::MostParsimonious(G, S, lambda);

    Node *x = S.getNode(vnum);
    if (x == NULL) 
      {
	cerr << "No node with that ID!\n";
	exit(1);
      }
    SetOfNodes V = gamma.getGamma(x);
    int size = V.size();
    if (size != 2) {
      if (size < 2) {
	cout << "# Missing WGD! There are " << V.size() << " vertices in that gamma\n";
      } else {
	cout << "# Ambigous! There are " << V.size() << " vertices in that gamma\n";
      }
      exit(0);
    } else {
      cout << G.mostRecentCommonAncestor(V[0], V[1])->getNumber()
	   << endl;
    }

  }
  catch (AnError e) {
    e.action();
  }
  return EXIT_SUCCESS;
}
