
include(${CMAKE_SOURCE_DIR}/src/cxx/cmake-include.txt)

add_executable(${programname_of_this_subdir}
showtree.cc
${CMAKE_CURRENT_BINARY_DIR}/cmdline.c   ${CMAKE_CURRENT_BINARY_DIR}/cmdline.h 
)
configure_file(showtree.1.cmake ${tmp_man_pages_dir}/showtree.1 @ONLY)

target_link_libraries(${programname_of_this_subdir} prime-phylo)

prime_add_tests(${CMAKE_CURRENT_SOURCE_DIR}/tests)

install(TARGETS ${programname_of_this_subdir} DESTINATION bin)
