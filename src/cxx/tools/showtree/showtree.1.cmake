.\" Comment: Man page for showtree 
.pc
.TH showtree 1 "@date_for_man_pages@" "@PROJECT_NAME@ @PACKAGE_VERSION@" 
.SH NAME
showtree \- View and/or convert phylogenetic trees in the terminal.
.SH SYNOPSIS
.B showtree
[\fIOPTIONS\fR]\fI [\fItreefile\fR]\fR

.SH DESCRIPTION

View and/or convert phylogenetic trees in the terminal.

.I treefile
is the filename of the for the file containing trees in PRIME format. 

.PP

.SH OPTIONS
.TP
.BR \-\-help
Display help (this text).
.TP
.BR \-V ", " \-\-version
Print version and exit
.TP
.BR \-e ", " \-\-show\-edgetimes
Show edge times
.TP
.BR \-n ", " \-show\-nodetimes
Show node times
.TP
.BR \-b ", " \-\-show\-edgeweights
Show edge weights (i.e. branch lengths)
.TP
.BR \-x ", " \-\-show\-tot\-edgetime
Show total edge time instead of tree
.TP
.BR \-y ", " \-\-show\-tot\-edgeweight
Show total edge weight instead of tree
.TP
.BR \-c ", " \-\-read\-edge\-times\-after\-colon
Read edge times after colon
.TP
.BR \-p ", " \-\-convert\-and\-output\-tree\-in\-prime\-format
Convert and output tree in PRIME format
.TP
.BR \-i ", " \-\-show\-id
Show ID (only valid together with \-p)
.TP
.BR \-S ", " \-\-read\-tree\-from\-stdin
Read tree from STDIN (no <treefile> nor <tree> needed)
.TP
.BR \-f ", " \-\-format " " \fRasciidrawing|prime|newick|simplenewick
The output format. Default is asciidrawing.
Output formats:

newick - convert and output tree in NEWICK format

simplenewick - Simple newick format, no branch lengths. This also enables reading of unbalanced trees. 

prime - prime format

asciidrawing - an ascii drawing

.PP
.TP
.BR \-t ", " \-\-tree\-number
Use the tree with this number. If not set all trees will be printed out.
.TP
.BR \-t ", " \-\-tree\-number
Use the tree with this number. If not set all trees will be printed out.


.SH "EXIT STATUS"
.TP
.B 0
Successful program execution.
.TP
.B 1
Some error occurred

.SH URL
.TP
The prime\-phylo home page: http://prime.sbc.su.se

.SH "SEE ALSO"
.BR primeDTLSR (1),
.BR primeDLRS (1),
.BR primeGEM (1),
.BR chainsaw (1),
.BR reconcile (1),
.BR reroot (1),
.BR tree2leafnames (1),
.BR treesize (1)
