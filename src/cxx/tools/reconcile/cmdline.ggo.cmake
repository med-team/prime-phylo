package "@programname_of_this_subdir@"
version "@VERSION_FOR_HELP_OUTPUT@"
purpose "What this program does"

args "--unamed-opts"

option "reroot" r  "Reroot the tree to minimize the number of implied duplications/losses if the most parsimonious reconciliation is chosen"  values="any","random","no"  enum default="no" optional 
option "weights" w  "Determine the relative importance of duplications vs losses. First weight is for duplications and the second for losses. The default is 1.0,0.0 which counts duplications only. Weights are required to be positive." typestr="FLOAT,FLOAT" float  optional multiple(2)

usage "reconcile <gene tree> <species tree> [<gene-species map>] [outfile]"


description "This program outputs the reconciliation that minimizes the number of duplications"
        
