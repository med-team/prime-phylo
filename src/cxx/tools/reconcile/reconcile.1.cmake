.\" Comment: Man page for reconcile 
.pc
.TH reconcile 1 "@date_for_man_pages@" "@PROJECT_NAME@ @PACKAGE_VERSION@" 
.SH NAME
reconcile \- This program outputs the reconciliation that minimizes the number of 
duplications

.SH SYNOPSIS
.B reconcile
[\fIOPTIONS\fR]\fI \fIgene-tree\fR \fIspecies-tree\fR [\fIgene-species-map\fR]\fR

.SH DESCRIPTION

This program outputs the reconciliation that minimizes the number of duplications

.PP

.SH OPTIONS
.TP
.BR \-\-help
Display help (this text).
.TP
.BR \-V ", " \-\-version
Print version and exit
.TP
.BR \-r ", " \-\-reroot " " \fRany|random|no
Reroot the tree to minimize the number of implied duplications/losses if the most parsimonious reconciliation is chosen. Default is "no".
.TP
.BR \-w " " \-\-weights " " \fIFLOAT\fR " " \fIFLOAT\fR
Determine the relative importance of duplications vs losses. First weight is for duplications and the second for losses. The default is 1.0,0.0 which counts duplications only. Weights are required to be positive.

.SH "EXIT STATUS"
.TP
.B 0
Successful program execution.
.TP
.B 1
Some error occurred

.SH URL
.TP
The prime\-phylo home page: http://prime.sbc.su.se

.SH "SEE ALSO"
.BR primeDTLSR (1),
.BR primeDLRS (1),
.BR primeGEM (1),
.BR chainsaw (1),
.BR reconcile (1),
.BR reroot (1),
.BR tree2leafnames (1),
.BR treesize (1)
