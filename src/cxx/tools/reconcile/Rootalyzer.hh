#ifndef Rootalyzer_HH
#define Rootalyzer_HH

#include "LambdaMap.hh"
#include "Node.hh"
#include "NodeMap.hh"
#include "StrStrMap.hh"

#include <vector>

namespace beep
{
  class Rootalyzer
  {
  public:
    Rootalyzer(Tree &G, 	// Tree needing a root analysis
	       Tree &S,		// Species tree that G "lives" in
	       StrStrMap gs,	// Map from genes to species
	       Real dup_weight,	// 
	       Real loss_weight	// 
	       );

    Node* getCheapestRoot();	//! Return a node with minimal dup/loss cost, chosen arbitrary.
    Node* getRandomCheapRoot();	//! Choose uniformly at random from nodes with minimal dup/loss cost.


  protected:
    void TryRootBelow(Node *v, Node *T1_lambda, unsigned dups_to_bring);
    void TryRootOnEdge(Node *v, Node *parent, Node *v_sibling, Node *upperLambda, unsigned dups_to_bring);

    unsigned countDuplications(Node *v);
    unsigned impliesDuplication(Node *ulambda, Node *vlambda, Node *wlambda);
    unsigned int impliedLosses(Node*);
    void print(Node *v);
    
  public:
    //! Get a debug output of duplications for various root choices.
    void print();


  private:
    Tree &G;
    Tree &S;
    LambdaMap lambda;
    NodeMap<Real> cost;		// For return value(s)
    NodeMap<unsigned> n_dups_below; // For computations, keep
    // track of the number of duplications in this subtree, if root is somewhere "up"
    NodeMap<unsigned> n_losses_below;

    Real dup_weight;		// Cost of postulating a duplication
    Real loss_weight;		// Cost of inferred losses
    std::vector<Node*> minimal_cost_nodes;
    Real minimal_cost;
  };
}

#endif
