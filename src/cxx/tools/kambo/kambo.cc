#include "AnError.hh"
#include "BDHybridTreeGenerator.hh"
#include "BirthDeathMCMC.hh"
#include "BirthDeathInHybridMCMC.hh"
#include "DummyMCMC.hh"
#include "Hacks.hh"
#include "TopTimeMCMC.hh"
#include "TreeMCMC.hh"
#include "HybridTreeIO.hh"
#include "SimpleML.hh"
#include "SimpleMCMC.hh"
#include "StrStrMap.hh"
#include "Node.hh"
#include "HybridHostTreeMCMC.hh"
#include "HybridGuestTreeMCMC.hh"

// Global options
//-------------------------------------------------------------
int nParams = 2;

char* outfile=NULL;
unsigned MaxIter = 10000;
unsigned Thinning = 10;
unsigned printFactor = 1;
unsigned RSeed = 0;
bool quiet = false;
bool show_debug_info = false;
bool do_likelihood = false;
bool do_ML = false;

//hybridHost related
char* sfile = 0; //NULL
bool treeFixed = false;
bool fixSERates = false;
double specRate = 3.0;
double extiRate = 3.0;
double hybrRate = 1.0;
unsigned useTree = 0;
unsigned nGtrees = 1000000000;
int maxGhosts = 10;

// Birth-death process related
bool fixBDRates = false;
double duplRate = 1.0;
double lossRate = 1.0;
double topTime = -1.0;
bool fixTopTime = false;
double Beta = -1.0;
bool mustChooseRates = true;

// reconciliation related
bool estimate_orthology = false;
bool specprob = false;
bool useDivTimes = false;

bool fixTree = true;
bool fixRoot = true;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);

int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if (argc < nParams +1) 
    {
      usage(argv[0]);
      exit(1);
    }
  try
    {
      // tell the user we've started
      //---------------------------------------------------------
      cerr << "Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cerr << argv[i] << " ";
	}
      cerr << " in directory ";
      cerr << getenv("PWD");
      cerr << "\n\n";
      
      //---------------------------------------------------------
      // Read input and options
      //---------------------------------------------------------

      // Check for options
      //-----------------------------------
      int opt = readOptions(argc, argv);

      //Get the trees
      //---------------------------------------------
      sfile = argv[opt++];
      cerr << "host = " << sfile << endl;
      HybridTreeIO io2 = HybridTreeIO::fromFile(sfile);
      HybridTree H;
      if(sfile)
	{
	  cerr << "Reading Host tree file\n"; 
	  vector<HybridTree> Hvec = 
	    io2.readAllHybridTrees();  
	  H = Hvec[useTree];
	  cerr << H;
	  if(H.hasTimes() == false)
	    {
	      throw AnError("Host tree must have times given\n", 1);
	    }
	}
      else
	{
	  throw AnError("Must have host tree infile, currently", 1);
	  BDHybridTreeGenerator treeMaker(specRate,extiRate,hybrRate);
	  treeMaker.generateHybridTree(H);
	}
      if(H.getName() == "Tree")
	{
	  H.setName("S");
	}

//       // Speed improvement -- remove if branchswapping on host tree
//       H.perturbedTree(false);
      cerr << "Host tree " << H.getName() << H  << endl;

      string guest(argv[opt++]);
      cerr << "guest = " << guest << endl;
      TreeIO io = TreeIO::fromFile(guest);
      vector<StrStrMap> gs;
      TreeIOTraits traits;
      traits.enforceStandardSanity();
      if(useDivTimes && traits.containsTimeInformation() == false)
	{
	  throw AnError("If -Gt given then guest tree must have times\n", 1);
	}
      vector<Tree> G = io.readAllBeepTrees(traits, 0, &gs); 
      if(gs.size() == 0)
	{
// 	  throw AnError("Sorry, can't handle external gs yet!",1);
	  if(opt + 1 > argc)
	    {
	      cerr << "gs was not present in guest tree, "
		   << "therefore I expected a <gs> argument\n";
	      usage(argv[0]);
	    }
	  gs = TreeIO::readGeneSpeciesInfoVector(argv[opt]);
	}
      
      
      //--------------------------------------------------------
      // Set up all classes
      //--------------------------------------------------------

      //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      PRNG rand;
      if (RSeed != 0)
	{
	  rand.setSeed(RSeed);
	}
      
      // Set up priors
      //-------------------------------------------------------
      DummyMCMC dm;
      HybridHostTreeMCMC hhtm(dm, H, maxGhosts);
      hhtm.fixBDHparameters(specRate,extiRate,hybrRate);
      if(Beta < 0)
	{
	  Beta = H.getRootNode()->getNodeTime();
	}
      if(treeFixed)
	{
	  hhtm.fixTree();
	}
      
      StdMCMCModel* prior = &hhtm;
      
      unsigned k = 0;
      vector<StrStrMap>::iterator j = gs.begin();
      for(vector<Tree>::iterator i = G.begin(); 
	  i != G.end() && k < nGtrees; i++, j++, k++)
	{
	  assert(j != gs.end());

	  TopTimeMCMC* ttm = new TopTimeMCMC(*prior, H, Beta); 
	  if(fixTopTime)
	    {
	      ttm->fixTopTime();
	    }
	  BirthDeathInHybridMCMC* bdm = new BirthDeathInHybridMCMC(*ttm, H, 
						   duplRate, lossRate, 
						   &ttm->getTopTime());
	  if(fixBDRates)
	    {
	      bdm->fixRates();
	    }
	  
	  HybridGuestTreeMCMC* hgtml = new HybridGuestTreeMCMC(*bdm, *i, H, 
							       *j, *bdm);  
	  if(fixRoot)
	    {
	      hgtml->fixRoot();
	    }
	  
	  hgtml->fixTree(); 

// 	  if(useDivTimes)
// 	    {
// 	      hgtml->useDivergenceTimes();
// 	    }
	  // Seed improvement remove if branchswapping on guest tree ???
	  i->perturbedTree(false);
	  
	  prior = hgtml;
	}
      
      // Set up MCMC handler
      //-------------------------------------------------------
      SimpleMCMC* iterator;
      if(do_ML)
	{
	  iterator = new SimpleML(*prior, Thinning);
	}
      else
	{
	  iterator = new SimpleMCMC(*prior, Thinning);
	}
      
      if (outfile != NULL)
	{
	  try 
	    {
	      iterator->setOutputFile(outfile);
	    }
	  catch(AnError e)
	    {
	      e.action();
	    }
	  catch (int e)
	    {
	      cerr << "Problems opening output file! ('"
		   << outfile
		   << "') Using stdout instead.\n";
	    }
	}  
      
      if (quiet)
	{
	  iterator->setShowDiagnostics(false);
	}
      
      if (!quiet) 
	{
	  cerr << "Start MCMC (Seed = " << rand.getSeed() << ")\n";
	}
      
      
      // Copy startup info to outfile (can be used to restart analysis)
      cout << "# Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cout << argv[i] << " ";
	}
      cout << " in directory"
	   << getenv("PWD")
	   << "\n"
	   << "Start MCMC (Seed = " << rand.getSeed() << ")\n";
      
      //--------------------------------------------------------
      // Now we're set to do stuff
      //---------------------------------------------------------
      
      time_t t0 = time(0);
      clock_t ct0 = clock();
      
      iterator->iterate(MaxIter, printFactor);
      
      time_t t1 = time(0);    
      
      clock_t ct1 = clock();
      cerr << "Wall time: " << readableTime(t1 - t0) 
	   << endl
	   << "CPU time: " << readableTime((ct1 - ct0)/CLOCKS_PER_SEC)
	   << endl;
      
      if (!quiet)
	{
	  cerr << prior->getAcceptanceRatio()
		   << " = acceptance ratio   Wall time = "
	       << readableTime(t1-t0)
	       << "\n";
	}
      
      if (prior->getAcceptanceRatio() == 0) 
	{
	  cerr << "Warning! MCMC acceptance ratio was 0.\n";
	}
      
    }      
  catch(AnError& e)
    {
      cerr << "Error:\n";
      e.action();      
    }
  catch(exception& e)
    {
      cerr << "Exception:\n"
	   << e.what()
	   << endl;
      exit(1);
    }

  return(0);
};


void 
usage(char* cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <host tree> <guest tree> [<gene-species map<]\n"
    << "\n"
    << "Parameters:\n"
    << "   <datafile>         Name of file containing sequences in FastA or\n"
    << "                      Genbank format.\n"
    << "   <species tree>     Species tree in Newick format. Branchlengths\n"
    << "                      are important and represent time.\n"
    << "   <gene-species map> Optional. This file contains lines with a\n"
    << "                      gene name in the first column and species\n"
    << "                      name as found in the species tree in the\n"
    << "                      second. You can also choose to associate the \n"
    << "                      genes with species in the gene tree. Please\n"
    << "                      see documentation.\n"


    << "Options:\n"
    << "   -u, -h                This text.\n"
    << "   -o <filename>         Output file\n"
    << "   -i <float>            number of iterations\n"
    << "   -t <float>            thinning\n"  
    << "   -m                    Do maximum likelihood. No MCMC.\n"
    << "   -w <float>            Write to cerr <float> times less often than\n"
    << "                         to cout\n"  
    << "   -s <int>              Seed for pseudo-random number generator.\n"
    << "                         If set to 0 (default), the process id is\n"
    << "                         used as seed.\n"
    << "   -q                    Do not output diagnostics to stderr.\n"
    << "   -g                    Debug info.\n"
    << "   -l                    Output likelihood. No MCMC.\n"
    << "   -G<option>            Options related to the gene tree\n"
    << "     -Gr                 reroot gene tree. Default is a fixed root\n"
    << "     -Go                 Estimate orthology. Default is discard \n"
    << "                         orthology data.\n"
    << "     -Gs                 record speciation probabilities (orthology\n" 
    << "                         probabilities will not be recorded)\n"
    << "     -Gn <int>           use the <int> of the guest trees\n"
    << "     -Gt                 use divergence times in guest tree\n"
    << "   -B<option>            Options related to the birth death process\n"
    << "     -Bf <float> <float> fix the birth/death rates to these values \n"
    << "     -Bp <float> <float> start values of birth/death rate parameters\n"
    << "     -Bt <float>         Start value for 'top time', the time\n"
    << "                         between first duplication and root of S.\n"
    << "     -Bi                 Fix 'top time'\n"
    << "     -Bb <float>         The beta parameter for a prior distribution\n"
    << "                         on species root distance\n"
    << "    -H<option>           Hybridization\n"
    << "     -Hi <file>          User defined host tree\n"
    << "     -Hf <float> <float> fix speciation/extinction rates to these values\n"
    << "     -Hp <float> <float> start values of speciation/extinction rates\n"
    << "     -Hx <float>         fixed values of hybridization rate\n"
    << "     -Hg <int>           precision in ghost computation\n"
    << "     -Hn <int>           use host tree n:o <int>\n"

    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'i':
	  {
	    if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	      {
		cerr << "Expected integer after option '-i'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 't':
	  {
	    if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	      {
		cerr << "Expected integer after option '-t'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'w':
	  {
	    if (sscanf(argv[++opt], "%d", &printFactor) == 0)
	      {
		cerr << "Expected integer after option '-w'\n";
		usage(argv[0]);
	      }
	    break;
	  }

	case 's':
	  {
	    if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	      {
		cerr << "Expected integer after option '-s'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'q':
	  {
	    quiet = true;
	    break;
	  }	
	case 'd':
	  {
	    show_debug_info = true;
	    break;
	  }
	case 'l':
	  {
	    do_likelihood = true;
	    break;
	  }	   
	case 'm':
	  {
	    do_ML = true;
	    break;
	  }	   
	case 'G':
	  {
	    switch(argv[opt][2])
	      {
	      case 'r':
		{
		  fixRoot = false;
		  break;
		}	      
	      case 's':
		{
		  specprob = true;
		  // no break here 
		}
	      case 'o':
		{
		  estimate_orthology = true;
		  break;
		}
	      case 'n':
		{
		  if (++opt < argc)
		    {
		      nGtrees = atoi(argv[opt]);
		    }
		  else
		    {
		      cerr << "Error: Expected an int after -Gn!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 't':
		{
		  useDivTimes = true;
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	case 'B':
	  {
	    switch(argv[opt][2])
	      {
	      case 'f':
		{
		  fixBDRates = true;
		  // Don't break here, because we want to fall through to 'r'
		  // set the rates that are arguments both to '-r' and '-f'!
		}
	      case 'p':
		{
		  mustChooseRates = false;
		  if (++opt < argc) 
		    {
		      duplRate = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  lossRate = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Error: Expected a gene loss (death) rate\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected birth rate for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 't':
		{
		  if (++opt < argc && atof(argv[opt]) > 0)
		    {
		      topTime = atof(argv[opt]);
		    }
		  else
		    {
		      cerr << "Error: Expected a 'top time' > 0 after -Bt!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'i':
		{
		  fixTopTime = true;
		  break;
		}
	      case 'b':
		{
		  if (++opt < argc) 
		    {
		      if (sscanf(argv[opt], "%lf", &Beta) == 0)
			{
			  cerr << "Expected number after option '-b'\n";
			  usage(argv[0]);
			}
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	case 'H':
	  {
	    switch(argv[opt][2])
	      {
	      case 'h':
		{
		  treeFixed = true;
		  break;
		}
	      case 'i':
		{
		  if (opt + 1 < argc)
		    {
		      sfile = argv[++opt];
		    }
		  else
		    {
		      cerr << "Expected filename after option '-Hi'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'f':
		{
		  fixSERates = true;
		  // Don't break here, because we want to fall through to 'r'
		  // set the rates that are arguments both to '-r' and '-f'!
		}
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      specRate = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  extiRate = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Error: Expected an extinction rate also "
			       << "after '-Hp' or '-Hf'\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected speciation rate for option "
			   << "'-Hp' or '-Hf'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'x':
		{
		  if (++opt < argc) 
		    {
		      hybrRate = atof(argv[opt]);
		    }
		  else
		    {
		      cerr << "Expected hybridization rate for option "
			   << "'-Hx' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'n':
		{
		  if (++opt < argc)
		    {
		      useTree = atoi(argv[opt]);
		    }
		  else
		    {
		      cerr << "Error: Expected an int after -Hn!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'g':
		{
		  if (++opt < argc)
		    {
		      maxGhosts = atoi(argv[opt]);
		    }
		  else
		    {
		      cerr << "Error: Expected an int after -Hp!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

	  
  

	

