#include <iostream>

using namespace std;

#include "AnError.hh"
#include "TreeIO.hh"
using namespace beep;

bool id = false;
bool et = false;
bool nt = false;
bool bl = false;
bool nwiset = false;
bool primeformat = false;
bool newickformat = false;
bool simple = false;

void
usage(char *s)
{
  std::cerr << "Usage: "
	    << s
	    << " <treefile>\n"
    	    << "Argument:\n"
	    << "  treefile  (string) file containing a host tree in PRIME format\n" 
	    << std::endl;
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  if (argc != 2) 
    {
      usage(argv[0]);
      exit(1);
    }
  try
    {
      int opt = 1;

      TreeIO io = TreeIO::fromFile(argv[opt]);

      TreeIOTraits traits;
      io.checkTagsForTree(traits);

      Tree T = io.readHostTree();
      cout << T << endl;
      
    }
  catch (AnError e) 
    {
      e.action();
    }
  return EXIT_SUCCESS;
};
