package "@programname_of_this_subdir@"
version "@VERSION_FOR_HELP_OUTPUT@"
purpose "converts from the beep file format to other formats ( right now only to XML )"
  
# right now there are no unamed-opts  
# args "--unamed-opts"

option "output-format" f "Output format" enum  default="xml" values="xml" optional
option "beep-filename" b "Beep file name" string
option "no-format-xml" w "do not add formatting spaces to xml output" flag off

description " "

text "some text here
"


