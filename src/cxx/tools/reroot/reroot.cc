#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

#include <boost/algorithm/string.hpp>

#include "AnError.hh"
#include "Beep.hh"
#include "BranchSwapping.hh"
#include "Node.hh"
#include "Tree.hh"
#include "HybridTreeInputOutput.hh"
#include "HybridTree.hh"
#include "TreeIOTraits.hh"
#include "cmdline.h"


// Global options with default settings
//------------------------------------
int nParams = 1;

int number = -1;
char* prefix = 0;
std::vector<std::string> outgroup;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  struct gengetopt_args_info args_info;

  try
    {
      if (cmdline_parser(argc, argv, &args_info) != 0)
	exit(1);

      if (args_info.read_tree_from_stdin_flag)
	{
	  if (args_info.inputs_num != 0 ) 
	    {
	    throw AnError("If you specify the flag -S (--read-tree-from-stdin) there should not be any unnamed parameters", 1);
	    }
	}
      else
	{
	  if (args_info.inputs_num != 1)
	    {
	      throw AnError("If you don't specify the flag -S (--read-tree-from-stdin), you have to provide a filename for a tree file", 1);
	    }
	}

      TreeInputOutput io;
      FILE * f;
      if( args_info.read_tree_from_stdin_flag )
	{ 
	  f = stdin;
	} 
      else
	{ 
	  f=  fopen(args_info.inputs[0],  "r");
	  if (!f) {
	    throw AnError("File not found", args_info.inputs[0], 1);
	  }
	}
      switch(args_info.input_format_arg)
	{
	case input_format_arg_prime:
	  {
	    io.fromFileStream(f, inputFormatBeepOrHybrid); 
	    break;
	  }
	case input_format_arg_inputxml: 
	  {
	    io.fromFileStream(f, inputFormatXml);
	    break;
	  }
	}

      BranchSwapping bs;
      TreeIOTraits traits;

      vector<Tree> Tvec = io.readAllBeepTrees(traits, 0,0);
      if (Tvec.size() == 0) 
	{
	  throw AnError("No tree found!", 2);
	} 
      else if (Tvec.size() > 1) 
	{
	  WARNING3("Found ", Tvec.size(), " trees in input, only the first tree is used.");
	}

      //
      // For option -n (node-index)
      //
      if (args_info.node_index_given)
	{
	  unsigned idx = static_cast<unsigned>(args_info.node_index_arg);
	      
	  if (idx < 0) {
	    throw AnError("Negative node number given", 1);
	  } else if (idx >= Tvec[0].getNumberOfNodes()) {
	    throw AnError("Illegal node number for rooting on tree " + Tvec[0].getName());
	  }
	  
	  bs.setRootOn(Tvec[0].getNode(idx));
	  cout << io.writeGuestTree(Tvec[0]) << endl;
	}
      //
      // For option -g (outgroup)
      //
      else if (args_info.outgroup_given) 
	{
	  boost::split(outgroup, args_info.outgroup_arg, boost::is_any_of(","));
	  if (outgroup.size() > Tvec[0].getNumberOfLeaves() - 2){
	    throw AnError("Too many outgroup leaves given!", args_info.outgroup_arg, 1);
	  }
	  bs.rootAtOutgroup(Tvec[0], outgroup);
	  cout << io.writeGuestTree(Tvec[0]) << endl;
	}
      //
      // For -e (exhaustive)
      //
      else if (args_info.exhaustive_given)
	{
	  for(unsigned j = 0; j < Tvec[0].getNumberOfNodes(); j++)
	    {	
	      Tree T1 = Tvec[0];
	      Node* node = T1.getNode(j);
	      if(node->isRoot() == false 
		 && node != T1.getRootNode()->getRightChild())		 
		{
		  bs.setRootOn(node);
		  if (prefix != 0) 
		    {		  
		      ostringstream oss;
		      oss << prefix << "_" << j << ".tree";
		      ofstream fout(oss.str().c_str());
		      
		      fout << io.writeGuestTree(T1) << endl;
		    }
		  else 
		    {
		      cout << io.writeGuestTree(T1) << endl;
		    }
		}
	    }
	}
      //
      // Default: random rooting
      //
      else
	{
	  int size = Tvec[0].getNumberOfNodes();
	  int n_leaves = Tvec[0].getNumberOfLeaves();
	  if (n_leaves < 3 ) {
	    WARNING3("Only ", n_leaves, " leaves in tree, rerooting nonsensical.");
	    cout << io.writeGuestTree(Tvec[0]) << endl;
	  } else {
	    PRNG r;
	    Node *v;
	    do {
	      unsigned idx = r.genrand_modulo(size);
	      v = Tvec[0].getNode(idx);
	    } while (v->isRoot());
	    bs.setRootOn(v);
	    cout << io.writeGuestTree(Tvec[0]) << endl;
	  }
	}
    }
  catch (AnError e) {
    e.action();
  }
  catch(exception& e)       // AnError or an exception has been thrown
    {
      cout <<"Error: "     // tell user about it
	   << e.what();     
    }
}


	  
  
