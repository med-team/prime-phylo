
#ifndef PRINTGB
#define PRINTGB

#include "entry.h"
/* #include <ffprint.h>  */

/*  For spacing when making a flatfile   */
#define BEGIN       1
#define TITLE       3
#define JOURNAL     3
#define STANDARD    3
#define ORGANISM    3
#define AUTHORS     3
#define FEATKEY     6
#define FFLEFT      13
#define FEATURES    22

/*  Type of JOURNAL line formats  */
#define UNPUBLISHED 1
#define THESIS      2
#define BOOK        3
#define PUBLICATION 4
#define SUBMISSION  5

/* int print_simple_gb_entry (struct entry * ent, struct print *file); */

#endif

