/* 
 * Operations on sequence files. Based on GenBank flat file format
 */

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <limits.h>
#include <stdlib.h>//malloc.h>
#include "entry.h"
#include "sfile.h"
//#include "printgb.h"
#include "gb.tab.h"

sfile * 
seq_open(char *filename, char *type) {
    FILE *f;

    f = fopen(filename, type);
    if (f == NULL) {
	return NULL;
    } else {
	return seq_file2sfile(f);
    }
}


seq *   
seq_read_all(sfile *sf, int *listsize) {
    return seq_read(sf, INT_MAX, listsize);
}


seq *
seq_new_sequence(locus, type, definition, length)
    char *locus;
    char *type;
    char *definition;
    unsigned length;
{
   struct entry   *ent;
   
   ent = (struct entry *) malloc (sizeof(struct entry));
   if (ent) {
       ent->locus = strdup(locus);
       ent->seqlen = length;
       strncpy(ent->moltype, type, 20);
       ent->topology[0] = '\0';
       ent->endiv[0] = '\0';
       ent->distdate[0] = '\0';
       strncpy(ent->definition, definition, 250);
       ent->accession[0] = '\0';
       ent->segment = -1;
       ent->segs = -1;
       ent->source[0] = '\0';
       ent->genusspec[0] = '\0';
       ent->origin[0] = '\0';
       ent->sequence = (char *) malloc(length*sizeof(char));
       if (ent->sequence == NULL) {
	   fprintf(stderr, "Out of memory when creating new sequence.\n");
	   abort(); // was before exit(1);
       }
       ent->secaccs = NULL;
       ent->keywords = NULL;
       ent->taxes = NULL;
       ent->references = NULL;
       ent->comments = NULL;
       ent->featoccs = NULL;
       ent->next = NULL;
   }
   else {
       fprintf (stderr, "Out of memory when creating new sequence.\n");
       abort(); // was before exit(1);
   }
   
   return (ent);
}


seq * 
seq_append(seq *head, seq *tail)
{
    seq *tmp;

    if (!head) {
	return tail;
    } else {
	tmp = head;
	while (tmp->next) {
	    tmp = tmp->next;
	}
	tmp->next = tail;
	return head;
    }
}

seq *
seq_copy_sequence(seq *e)
{
    seq *s;

    s = seq_new_sequence(e->locus, e->moltype, e->definition, e->seqlen);
    bcopy(e->sequence, s->sequence, e->seqlen);

    return s;
}


/* int */
/* begin_parse_entry(struct entry *ent) { */
/*   return 0; */
/* } */

/* int */
/* end_parse_entry(struct entry *ent) { */
/*   ent->next = entry_list; */
/*   entry_list = ent; */
/*   return 0;      */
/* } */

/* 
 * Return a pointer to the locus name. 
 * Don't mess with the character field!
 */

char*
seq_locus(struct entry *ent) {
  if (ent) {
    return ent->locus;
  } else { 
    return NULL;
  }
}

struct entry *
seq_find_locus(struct entry *ent, char *locus) {
  struct entry *i; 

  for (i = ent; i != NULL; i = i->next) {
    if (!strcmp(locus, i->locus)) {
      return i;
    }
  }
  return NULL;
}


/*
 * Returns length of the sequence, -1 if argument == NULL.
 */

int
seq_seq_length(struct entry *e) {
  if (e) {
    return e->seqlen;
  } else { 
    return -1;
  }
}
  

/*
 * Returns the character at site i in sequence e
 */

char
seq_seq_site(struct entry *e, int i) {

  if (e) {
    if ((i >= 0) && (i < seq_seq_length(e))) {
      return e->sequence[i];
    } else {
      fprintf(stderr, "Out of bounds when trying to read site %d of sequence %s.\n", i, seq_locus(e));
      abort(); // was before exit(1);
    } 
  } else {
    fprintf(stderr, "Tried using an empty locus entry. Aborting.\n");
abort(); // was before exit(1);
  }
}
     

struct entry*
seq_next_entry(struct entry *e) {
  return e->next;
}


unsigned
seq_entry_list_length(struct entry *list) {
  unsigned length = 0;
    
  while (list) {
    list = seq_next_entry(list);
    length++;
  }

  return length;
}


/* 
 * This function will print the sequence pointed to by e. Note that if there
 * are many sequences in a list, only the first one will be printed.
 * The function will abort if a NULL pointer is submitted.
 */

void
seq_print(struct entry *e) {
  struct liststr *l;
  struct comment *commentp;
  char *top, *date;
  long chars_in_line, chars_in_group, char_counter, length;
  
  if (e) {
    if (e->sequence) {
      if (strlen(e->sequence) != e->seqlen) {
	fprintf(stderr, "In sequence %s, the actual sequence length differs from what is stated in locus line.\n", e->locus);
      } 
    } else {
      fprintf(stderr, "An empty sequence is found.\n");
    }
    top = !strcmp(e->topology, "Circular") ? e->topology : "";
    date = "";
    printf("LOCUS       %-10s%7d bp %-7s  %-10s%-3s       %-11s\n",
	   e->locus, e->seqlen, e->moltype, top, e->endiv, e->distdate);
    printf("DEFINITION  ");
    if (strlen(e->definition) > 0) {
      printf("%s\n", e->definition);
    } else {
      printf("%s\n", "<no definition accessible>");
    } 
    if (strlen(e->accession) > 0) {
      printf("ACCESSION   %s", e->accession);
      if (e->secaccs) {
	l = e->secaccs;
	while (l) {
	  printf("  %s", l->name);
	  l = l->next;
	}
      }
      printf("\n");		/* We have a bug here. If there are too many */
				/* secondary accession, the line will be too */
				/* long. */
    }

    if (e->comments != NULL) {
	commentp = e->comments;
	printf("COMMENT     %s\n", commentp->text);
	commentp = commentp->next;
	while (commentp) {
	    printf("            %s\n", commentp->text);
	    commentp = commentp->next;
	}
    }

    printf("ORIGIN      %s\n", e->origin);
    
    chars_in_line = 60;		/* To know when to break a line */
    chars_in_group = 10;	/* To know when to insert spaces */
    char_counter = 0;		/* For the numbers left of the sequence groups */
    length = strlen(e->sequence);

    while (char_counter < length) { /* Loop over characters */
      printf("%9d", char_counter + 1);
      while (char_counter < length && chars_in_line > 0) { /* Over lines */
	putchar(' ');		/* Initally and between groups */
	while (char_counter < length && chars_in_group > 0) { /* Over groups */
	  putchar(e->sequence[char_counter++]);
	  chars_in_group--;
	  chars_in_line--;
	}
	chars_in_group = 10;	/* Re-start a group */
      }
      putchar('\n');
      chars_in_line = 60;		/* Re-start a line */
    }
    
    printf("//\n");		/* End of sequence entry */
  } else {
    fprintf(stderr, "NULL pointer supplied to print function.\n");
  }
}



void
seq_add_comment(e, s)
    seq *e;
    char *s;
{
    struct comment *tmp;
    struct comment **comm_pointer;

    if (e) {
	if (s) {
	    comm_pointer = &e->comments;
	    while (*comm_pointer != NULL) {
		comm_pointer = &((*comm_pointer)->next);
	    }
	    tmp = (struct comment *) malloc(sizeof(struct comment *));
	    if (tmp) {
		tmp->text = strdup(s);
		tmp->next = NULL;
	    } else {
		fprintf(stderr, "Out of memory! (%s:%d)\n", __FILE__, __LINE__);
		abort(); // was before exit(1);
	    }
	    *comm_pointer = tmp;
	}
    } else {
	fprintf(stderr, "Bad programming! No sequence supplied. (%s:%d)\n", __FILE__, __LINE__);
	abort(); // was before exit(1);
    }
}



/*
 * seq_set_definition
 * Sets the definition string in a sequence entry to the given string.
 * That is, as much as possible of the given string is copied into the
 * definition field in the entry structure.
 */

void
seq_set_definition(e, s)
    seq *e;
    char *s;
{
    if (e) {
	if (s) {
	    strncpy(e->definition, s, ENTRY_DEF_LEN);
	    e->definition[ENTRY_DEF_LEN] = '\0';
	}
    } else {
	fprintf(stderr, "Bad programming! No sequence supplied. (%s:%d)\n", __FILE__, __LINE__);
	abort(); // was before exit(1);
    }
}



/*
 * seq_set_accession
 * Sets the accession string in a sequence entry to the given string.
 * That is, as much as possible of the given string is copied into the
 * accesssion field in the entry structure.
 */

void
seq_set_accession(e, s)
    seq *e;
    char *s;
{
    if (e) {
	if (s) {
	    strncpy(e->accession, s, ENTRY_ACC_LEN);
	    e->accession[ENTRY_ACC_LEN] = '\0';
	}
    } else {
	fprintf(stderr, "Bad programming! No sequence supplied. (%s:%d)\n", __FILE__, __LINE__);
	abort(); // was before exit(1);
    }
}


void
seq_free_comments(c)
    struct comment *c;
{
    struct comment *tmp;

    while (c) {
	tmp = c->next;
	free(c->text);
	free(c);
	c = tmp;
    }
}

void
seq_free(entries)
    seq *entries;
{
    seq *tmp;

    while (entries) {
	tmp = entries->next;

	free(entries->sequence);
	free(entries->locus);
	seq_free_comments(entries->comments);
	free(entries);
	entries = tmp;
    }
}
