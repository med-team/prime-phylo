#include "EdgeRateMCMC_common.hh"


namespace beep
{
//----------------------------------------------------------------------
//
// Specializations
//
//----------------------------------------------------------------------

//! Helper function of EdgeRateMCMC_common<gbmRatemodel>::generateRates()
//----------------------------------------------------------------------
template<>
void gbmRateMCMC::recursiveGenerateRates(const Node* n,
		Real parentRate)
{
	Real beta = variance * n->getTime();
	// root's left child
	// set up rateProb
	Real rate = 0;
	rateProb->setEmbeddedParameters(std::log(parentRate) - 0.5 * beta, beta);
	do //! \todo{This should be handled in a better way/bens}
	{
		rate = rateProb->sampleValue(R.genrand_real3());
	}
	while(rate < min || rate > max);
	setRate(rate, n);
	if(!n->isLeaf())
	{
		// pass recursion on
		recursiveGenerateRates(n->getLeftChild(), rate);
		recursiveGenerateRates(n->getRightChild(), rate);
	}
	return;
}



//! Generates a random sample of edgeRates. iidRateModel version.
//----------------------------------------------------------------------
template<>
void EdgeRateMCMC_common<iidRateModel>::generateRates()
{
	for(unsigned i = 0; i < edgeRates.size(); i++)
	{
		Node* n = T->getNode(i);
		Node* p = n->getParent();

		bool doSkip = false;
		switch (getRootWeightPerturbation())
		{
		case EdgeWeightModel::BOTH:
			doSkip = n->isRoot();
			break;
		case EdgeWeightModel::RIGHT_ONLY:
			doSkip = (n->isRoot() || (p->isRoot() && p->getLeftChild() == n));
			break;
		case EdgeWeightModel::NONE:
			doSkip = (n->isRoot() || p->isRoot());
			break;
		}

		if (doSkip)
		{
			continue;
		}

		Real newRate = rateProb->sampleValue(R.genrand_real3());
		setRate(newRate, n);
	}

	if (getRootWeightPerturbation() == EdgeWeightModel::NONE)
	{
		setRate(getMean(), T->getRootNode()->getLeftChild());
	}
}





//! Generates a random sample of edgeRates. gbmRateModel version.
//----------------------------------------------------------------------
template<>
void gbmRateMCMC::generateRates()
{
	Node& r = *T->getRootNode();
	if(!r.isLeaf())
	{
		Node& lc = *r.getLeftChild();
		Node& rc = *r.getRightChild();
		// Get start rate and store it as lc's rate
		Real rate = rateProb->getMean();
		setRate(rate, lc);
		if (perturbedRootEdges == RIGHT_ONLY || perturbedRootEdges == NONE)
		{
			// To avoid over-parametrization, the root's rate is not modeled
			// As an approximation the process is modeled to start at root's
			// left child, lc, and continuing 1) down T_{lc} and 2) up to root
			// and then down planted subtree at root's right child.
			Real ltime = lc.getTime();
			Real rtime = rc.getTime();
                        Real fparam = std::log(rate) - 	0.5 * variance * (rtime - 3.0 * ltime);
                        Real rparam = variance * (rtime + ltime);
			rateProb->setEmbeddedParameters(std::log(rate) -
					0.5 * variance * (rtime - 3.0 * ltime),
					variance * (rtime + ltime));
			setRate(rateProb->sampleValue(R.genrand_real3()), rc);
		}
		else
		{
			setRate(rate, rc);
		}

		// Generate remaining rates
		// Right subtree
		if(!rc.isLeaf())
		{
			recursiveGenerateRates(rc.getLeftChild(), rate);
			recursiveGenerateRates(rc.getRightChild(), rate);
		}
		// Left subtree
		if(!lc.isLeaf())
		{
			recursiveGenerateRates(lc.getLeftChild(), rate);
			recursiveGenerateRates(lc.getRightChild(), rate);
		}
	}
	return;
}









//! Perturbs a randomly chosen edgeRate.
// Rate of root's left child (rlc) is assumed to be treated
// separately (e.g., iidRate model: rlc = rrc (rate of root's right
// child), gbmRatemodel: rlc is the mean).
//----------------------------------------------------------------------
template<>
Probability EdgeRateMCMC_common<ConstRateModel>::perturbRate()
{
	assert(edgeRates.size() > 0); // Check precondition. TODO: check earlier

	// ConstRateModel disregards what root is sent, but we need
	// idxNode = root to be able to set T->perturbedNode properly
	// both here and in discardState()
	idx_node = T->getRootNode();
	oldValue = getRate(idx_node);                  // Save previous value

	Probability propRatio(1.0);                    // Create return object
	setRate(perturbNormal(oldValue, suggestion_variance,
			min, max, propRatio), idx_node);
#ifdef PERTURBED_NODE
	// We always want to recalculate all nodes, so it is always
	// the root node that should be sent.
	T->perturbedNode(idx_node);
#endif
	return propRatio;
};

template<>
Probability EdgeRateMCMC_common<iidRateModel>::perturbRate()
{
	return this->perturbRate_notRoot();
}

template<>
Probability gbmRateMCMC::perturbRate()
{
	return this->perturbRate_notRoot();
}

//! Perturbs a randomly chosen edgeRate.
// Rate of root's left child (rlc) is assumed to be treated
// separately (e.g., iidRate model: rlc = rrc (rate of root's right
// child), gbmRatemodel: rlc is the mean).
//----------------------------------------------------------------------
template<>
Probability EdgeRateMCMC_common<ConstRateModel>::perturbRate(unsigned x)
{
	assert(edgeRates.size() > 0); // Check precondition. TODO: check earlier

	// ConstRateModel disregards what root is sent, but we need
	// idxNode = root to be able to set T->perturbedNode properly
	// both here and in discardState()
	idx_node = T->getRootNode();
	oldValue = getRate(idx_node);                  // Save previous value

	Probability propRatio(1.0);                    // Create return object
	setRate(perturbNormal(oldValue, suggestion_variance,
			min, max, propRatio), idx_node);
#ifdef PERTURBED_NODE
	// We always want to recalculate all nodes, so it is always
	// the root node that should be sent.
	T->perturbedNode(idx_node);
#endif
	return propRatio;
}










template<>
Probability EdgeRateMCMC_common<iidRateModel>::perturbRate(unsigned x)
{
	return this->perturbRate_notRoot(x);
}


















template<>
Probability gbmRateMCMC::perturbRate(unsigned x)
{
	return this->perturbRate_notRoot(x);
}

//   // Specialization for iid - multiplies all rates with ratio.
//   template<>
//   void EdgeRateMCMC_common<iidRateModel>::adjustRates(const Real& ratio)
//   {
//     for(unsigned i = 0; i < edgeRates.size(); i++)
//       {
// 	Node* n = T->getNode(i);
// 	Node* p = n->getParent();
// #ifdef INCLUDE_ROOT_RATE // joelgs: forbidden to use!
//   	if(n->isRoot())
// #else
//   	if(n->isRoot() || (p->isRoot() && p->getLeftChild() == n))
// #endif
// 	  {
// 	    continue;
// 	  }
// 	Real newRate = getRate(n) * ratio;
// 	setRate(newRate, n);
//       }
// #ifdef PERTURBED_NODE
//     // We will always need to recalculate whole tree
//     T->perturbedNode(T->getRootNode());
// #endif

//     return;
//   }

// Specialization for gbm - multiplies all rates with ratio.
//   template<>
//   void EdgeRateMCMC_common<gbmRateModel>::adjustRates(const Real& ratio)
//   {
//     for(unsigned i = 0; i < edgeRates.size(); i++)
//       {
// 	Node* n = T->getNode(i);
// 	Node* p = n->getParent();
// #ifdef INCLUDE_ROOT_RATE	// joelgs: forbidden to use!
//   	if(n->isRoot())
// #elif defined FIX_ROOT_EDGES_RATE // joelgs: forbidden to use!
// 	if(n->isRoot() || p->isRoot())
// #else
//   	if(n->isRoot() || (p->isRoot() && p->getLeftChild() == n))
// #endif
// 	  {
// 	    continue;
// 	  }
// 	Real newRate = getRate(n) * ratio;
// 	setRate(newRate, n);
//       }
// #ifdef PERTURBED_NODE
//     // We will always need to recalculate whole tree
//     T->perturbedNode(T->getRootNode());
// #endif

//     return;
//   }

// Specialization for iid
template<>
std::string EdgeRateMCMC_common<iidRateModel>::ratesStr() const
{
	std::ostringstream oss;
	Node* p;
	Node* n;
	for(unsigned i = 0; i < edgeRates.size(); i++)
	{
		n = T->getNode(i);
		p = n->getParent();
		bool doSkip = false;
		switch (getRootWeightPerturbation())
		{
		case EdgeWeightModel::BOTH:
			doSkip = n->isRoot();
			break;
		case EdgeWeightModel::RIGHT_ONLY:
			doSkip = (n->isRoot() || (p->isRoot() && p->getLeftChild() == n));
			break;
		case EdgeWeightModel::NONE:
			throw AnError("Fixed root edges are set -- you should not use iid.");
			break;
		}
		if (doSkip)
		{
			continue;
		}
		oss << edgeRates[i] << ";\t";
	}
	return oss.str();
}


// Specialization for iid
template<>
std::string EdgeRateMCMC_common<iidRateModel>::ratesHeader() const
{
	std::ostringstream oss;
	Node* p;
	Node* n;
	for(unsigned i = 0; i < edgeRates.size(); i++)
	{
		n = T->getNode(i);
		p = n->getParent();
		bool doSkip = false;
		switch (getRootWeightPerturbation())
		{
		case EdgeWeightModel::BOTH:
			doSkip = n->isRoot();
			break;
		case EdgeWeightModel::RIGHT_ONLY:
			doSkip = (n->isRoot() || (p->isRoot() && p->getLeftChild() == n));
			break;
		case EdgeWeightModel::NONE:
			throw AnError("Fixed root edges are set -- you should not use iid.");
			break;
		}
		if (doSkip)
		{
			continue;
		}
		oss << "edgeRate[" << i << "](float);\t";
	}
	return oss.str();
}


// Specialization for gbm
template<>
std::string gbmRateMCMC::ratesStr() const
{
	std::ostringstream oss;
	Node* p;
	Node* n;
	for(unsigned i = 0; i < edgeRates.size(); i++)
	{
		n = T->getNode(i);
		p = n->getParent();
		bool doSkip = false;
		switch (getRootWeightPerturbation())
		{
		case EdgeWeightModel::BOTH:
			doSkip = n->isRoot();
			break;
		case EdgeWeightModel::RIGHT_ONLY:
			doSkip = (n->isRoot() || (p->isRoot() && p->getLeftChild() == n));
			break;
		case EdgeWeightModel::NONE:
			doSkip = (n->isRoot() || p->isRoot());
			break;
		}
		if (doSkip)
		{
			continue;
		}
		oss << edgeRates[i] << ";\t";
	}
	return oss.str();
}



// Specialization for gbm
template<>
std::string gbmRateMCMC::ratesHeader() const
{
	std::ostringstream oss;
	Node* p;
	Node* n;
	for(unsigned i = 0; i < edgeRates.size(); i++)
	{
		n = T->getNode(i);
		p = n->getParent();
		bool doSkip = false;
		switch (getRootWeightPerturbation())
		{
		case EdgeWeightModel::BOTH:
			doSkip = n->isRoot();
			break;
		case EdgeWeightModel::RIGHT_ONLY:
			doSkip = (n->isRoot() || (p->isRoot() && p->getLeftChild() == n));
			break;
		case EdgeWeightModel::NONE:
			doSkip = (n->isRoot() || p->isRoot());
			break;
		}
		if (doSkip)
		{
			continue;
		}
		oss << "edgeRate[" << i << "](float);\t";
	}
	return oss.str();
}



}//end namespace beep





