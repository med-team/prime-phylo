#ifndef LENGTHRATEMCMC_HH
#define LENGTHRATEMCMC_HH

#include "EdgeWeightMCMC.hh"
#include "EdgeRateModel.hh"

#include <string>
#include <sstream>

namespace beep
{

  class LengthRateMCMC : public EdgeWeightMCMC
  {
  public:
    LengthRateMCMC(MCMCModel& prior, EdgeRateModel& rateModel_in,
		   const std::string& name_in, const Real& suggestRatio = 1.0)
      : EdgeWeightMCMC(prior, rateModel_in, name_in, suggestRatio),
	rateModel(&rateModel_in)
    {
    };

    ~LengthRateMCMC()
    {};
    

    Probability
    updateDataProbability()
    {
      model->update();
      const Tree& T = model->getTree();
      for(unsigned i = 0; i < T.getNumberOfNodes(); i++)
	{
	  Node& n = *T.getNode(i);
	  if(n.isRoot())
	    {
	      continue;
	    }
	  else if(n.getParent()->isRoot())
	    {
	      Node& s = *n.getSibling();
	      rateModel->setRate((T.getLength(n) + T.getLength(s)) /
		      ( T.getTime(n) + T.getTime(s)), n);
	    }	 
	  else  
	    {   
	      Real new_rate = T.getLength(n) / T.getTime(n);
	      rateModel->setRate(new_rate, n);
	    }
	}
      return rateModel->calculateDataProbability();
    }
  private:
    // Attributes
    //------------------------------------------------------
    EdgeRateModel* rateModel;
  };

}//end namespace beep

#endif 
