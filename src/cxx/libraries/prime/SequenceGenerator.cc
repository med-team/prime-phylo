#include <cassert>
#include <sstream>

#include "SequenceGenerator.hh"
#include "LA_DiagonalMatrix.hh"
#include "LA_Vector.hh"
#include "LA_Matrix.hh"

using std::vector; 
using std::string;
using std::cout;
using std::cerr;
using std::ostringstream;

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Construct/Destruct
  //
  //----------------------------------------------------------------------

  // Construct 
  //----------------------------------------------------------------------
  SequenceGenerator::SequenceGenerator(Tree& T_in, 
				       MatrixTransitionHandler& Q_in,
				       SiteRateHandler& siteRates_in,
				       EdgeWeightHandler& ewh_in,
				       PRNG& R_in)
    :  seqType(Q_in.getType()),
       T(T_in),
       Q(siteRates_in.nCat(), Q_in),
       siteRates(siteRates_in),
       ewh(&ewh_in),
       R(R_in)
  {
    assert(seqType.alphabetSize() == Q_in.getAlphabetSize());
  }

  // Destructor
  //----------------------------------------------------------------------
  SequenceGenerator::~SequenceGenerator()
  {}

  // Copy Constructor
  //----------------------------------------------------------------------
  SequenceGenerator::SequenceGenerator(const SequenceGenerator& sg)
    :  seqType(sg.seqType),
       T(sg.T),
       Q(sg.Q),
       siteRates(sg.siteRates),
       ewh(sg.ewh),
       R(sg.R)
  {}  

  // Assigment operator
  //----------------------------------------------------------------------
  SequenceGenerator&
  SequenceGenerator::operator=(const SequenceGenerator& sg)
  {  
    if(this == &sg)
      {
	seqType = sg.seqType;
	T = sg.T;
	Q = sg.Q;
	siteRates = sg.siteRates;
	ewh = sg.ewh;
	R = sg.R;
      }
    return *this;
  }
  
  //----------------------------------------------------------------------
  //
  // Public interface
  //
  //----------------------------------------------------------------------

  // For simulations
  //----------------------------------------------------------------------
  SequenceData
  SequenceGenerator::generateData(const unsigned& nchar, 
				  const bool ancestral)
  {
    SequenceData D(seqType);
    Node& root = *T.getRootNode();
    vector<unsigned> posRate;  // Hold position specific rate class
    beep::LA_DiagonalMatrix pi = Q[0].getPi();
  
    // Construct root sequence
    vector<unsigned> u_seq;
    ostringstream char_seq;
    for(unsigned i = 0; i < nchar; i++)
      {
	Real rand = R.genrand_real1();
	unsigned j = 0;
	for(Real limit = pi(j,j); limit < rand && j < pi.getDim(); 
	    j++, limit += pi(j,j));
	u_seq.push_back(j);
	char_seq << seqType.uint2char(j);
	posRate.push_back(R.genrand_modulo(siteRates.nCat()));
      }
    if (root.isLeaf())
      {
	D.addData(root.getName(), char_seq.str());
	cout <<"done=n";
	return D;
      }
    else
      {
	if (ancestral == true)
	  {
	    cerr //<< T
	      << root.getNumber()
	      << "\t"
	      << char_seq.str();
	    ostringstream name;
	    name << "root_" << root.getNumber();
	    D.addData(name.str(), char_seq.str());
	  }
	
	// Create rest of node sequences
	recursivelyGenerate(*root.getLeftChild(), u_seq, posRate, D, ancestral);
	recursivelyGenerate(*root.getRightChild(), u_seq, posRate, D, ancestral);
	return D;
      }
  }


  //----------------------------------------------------------------------
  //
  // I/O
  //
  //----------------------------------------------------------------------
  std::ostream& operator<<(std::ostream& os, 
				 const SequenceGenerator& sg)
  {
    return os << sg.print()
      ;
  }
  
  std::string SequenceGenerator::print() const
  {
    ostringstream oss;
    oss 
      << "Sequence data of "
      << seqType.print()
      << " is generated on the following tree:\n"
      << indentString(T.print())
      << "with the following site rate settings:\n"
      << indentString(siteRates.print())
      << "the following edge weights settings:\n"
      << indentString(ewh->print())
      << "and the following substitution model:\n"
      << indentString(Q[0].print())
      ;
    return oss.str();
  }


    //----------------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------------


    // Helper function for simulateSequence
    //----------------------------------------------------------------------
    void 
      SequenceGenerator::recursivelyGenerate(const Node& n, 
					     const vector<unsigned>& start, 
					     const vector<unsigned>& posRate,
					     SequenceData& D, 
					     const bool& ancestral)
      {
	Real bl = ewh->getWeight(n);
	for(unsigned i = 0; i < siteRates.nCat(); i++)
	  {
	    Q[i].resetP(bl * siteRates.getRate(i));
	  }
	vector<unsigned> u_seq;
	ostringstream char_seq;
	LA_Vector curr(seqType.alphabetSize());
	for(unsigned i = 0; i < start.size(); i++)
	  {
	    Q[posRate[i]].col_mult(curr, start[i]);
	    Real rand = R.genrand_real1();

	    unsigned j = 0;
	    for(Real limit = curr[j]; 
		(limit < rand) && (j < curr.getDim() - 1);
		j++, limit += curr[j]);
	    u_seq.push_back(j);
	    if(seqType == myCodon)
	      {
		char_seq << myCodon.uint2str(j);
	      }
	    else
	      {
		char_seq << seqType.uint2char(j);
	      }
	  }
	if(n.isLeaf())
	  {
	    D.addData(n.getName(), char_seq.str());
	  }
	else
	  {
	    if (ancestral == true)
	      {
		cerr << n.getNumber()
		     << "\t"
		     << char_seq.str();
		ostringstream name;
		name << "node_" << n.getNumber();
		D.addData(name.str(), char_seq.str());
	      }
	    recursivelyGenerate(*n.getLeftChild(), u_seq, posRate, D, ancestral);
	    recursivelyGenerate(*n.getRightChild(), u_seq, posRate, D, ancestral);
	  }
	return;
      }

    // Helper function for operator<<
    //----------------------------------------------------------------------
    string 
      SequenceGenerator::printSequence(const vector<unsigned>& V) const
      {
	ostringstream os;
	for (vector<unsigned>::const_iterator i = V.begin(); i != V.end(); i++)
	  {
	    os << *i;
	  }
	os << "\n";
	return os.str();
      }


  }//end namespace beep
