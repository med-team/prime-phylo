#ifndef EDGEWEIGHTMCMC_HH
#define EDGEWEIGHTMCMC_HH

#include <iostream>
#include <string>

#include "StdMCMCModel.hh"
#include "EdgeRateModel.hh"
#include "Node.hh"

namespace beep
{
  //----------------------------------------------------------------------
  //
  // Class EdgeWeightMCMC
  // Interface base class for MCMC-shells of EdgeRateModel subclasses.
  //
  //----------------------------------------------------------------------
  class EdgeWeightMCMC : public StdMCMCModel
  {
  public:

    //----------------------------------------------------------------------
    //
    // Construct/destruct/assign
    //
    //----------------------------------------------------------------------
    EdgeWeightMCMC(MCMCModel& prior, EdgeWeightModel& ewm, 
		   Real suggestRatio = 1.0, bool useTruncNormalSugg = false);
    //With name
    EdgeWeightMCMC(MCMCModel& prior, EdgeWeightModel& ewm, 
		   const std::string& name, Real suggestRatio = 1.0,
		   bool useTruncNormalSugg = false);
    EdgeWeightMCMC(const EdgeWeightMCMC& ewm);
    EdgeWeightMCMC& operator=(const EdgeWeightMCMC& ewm);
    ~EdgeWeightMCMC();

    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------

    // fix parameters
    //----------------------------------------------------------------------
    void fixWeights();

    //! Generates a random sample of edgeRates. Base version
    // Works for, e.g., ConstRateModel
    //----------------------------------------------------------------------
    void generateWeights(bool setRootWeight=false, Real newWeight=0.01);

    //! Turns on detailed notification of tree perturbation events to listeners.
    //----------------------------------------------------------------------
    void setDetailedNotification(bool doSendDetails);
    
    //! Get current tree
    void showCurrentTree();
	
    //----------------------------------------------------------------------
    // Inherited from StdMCMCModel
    //----------------------------------------------------------------------

    // Perturbs with equal probability either the mean or the variance of 
    // underlying density or one of the edge rates.
    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();
    std::string ownStrRep() const;
    std::string ownHeader() const;
    std::string getAcceptanceInfo() const;

    Probability  updateDataProbability();

    void updateToExternalPerturb(RealVector& newWeights);

    //----------------------------------------------------------------------
    // I/O
    //----------------------------------------------------------------------
      std::string print() const;

    //----------------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------------
  protected:
    //! perturbRate() Document!
    //! Returns propRatio!
    //----------------------------------------------------------------------
    Probability perturbWeight();
    std::string weightsStr() const;
    std::string weightsHeader() const;

    //----------------------------------------------------------------------
    //
    // Attibutes
    //
    //----------------------------------------------------------------------
    EdgeWeightModel* model; //!< the model used to model weights
    Real oldWeight;
    Node* idxWeight;
    Real suggestion_variance;
    bool detailedNotifInfo;          //!< Flag for notifying tree listeners with detailed info.
    std::pair<unsigned,unsigned> accPropCnt; //!< Acc. and total prop. counts.
    bool useTruncatedNormalSugg;    //! True to use truncated Normal proposal instead of Lognormal.

  };
}//end namespace beep

#endif
