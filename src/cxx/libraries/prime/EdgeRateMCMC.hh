#ifndef EDGERATEMCMC_HH
#define EDGERATEMCMC_HH

#include "EdgeRateModel.hh"
#include "StdMCMCModel.hh"


namespace beep
{
  //----------------------------------------------------------------------
  //
  // Class EdgeRateMCMC
  //! Interface base class for MCMC-shells of EdgeRateModel subclasses.
  //! The actual MCMC-shell is templatized, and to provide dynamic binding
  //! to functions specific to MCMC-shell, this base class i provided.
  //! Note virtual inheritance!
  //
  //----------------------------------------------------------------------
  class EdgeRateMCMC : public StdMCMCModel, public virtual EdgeRateModel
  {
  protected:
    EdgeRateMCMC(MCMCModel& prior, unsigned nParams,
		 const std::string& name, const Real& suggestRatio = 1.0);
    EdgeRateMCMC(const EdgeRateMCMC& erm);
    EdgeRateMCMC& operator=(const EdgeRateMCMC& erm);

  public:
    virtual ~EdgeRateMCMC();

  public:
    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------

    // fix parameters
    //----------------------------------------------------------------------
    //! Fix parameter mean not be pertubed in MCMC
    virtual void fixMean();
    //! Fix parameter variance not be pertubed in MCMC
    virtual void fixVariance();
    //! Fix parameter rates not be pertubed in MCMC
    virtual void fixRates() = 0;
    
    //! Allows user to change the allowed max value for parameters
    //! \todo{might need to be handled in a better way/bens}
    virtual void setMax(const Real& newMax);

    //! Provides means to generate a random sample of edge rates.
    virtual void generateRates() = 0;

    // Inherited from StdMCMCModel
    //----------------------------------------------------------------------

    //! Perturbs with equal probability either the mean or the variance of 
    //! underlying density or one of the edge rates.
    MCMCObject suggestOwnState();
    MCMCObject suggestOwnState(unsigned x);
    void commitOwnState();
    void commitOwnState(unsigned x);
    void discardOwnState();
    void discardOwnState(unsigned x);
    std::string ownStrRep() const;
    std::string ownHeader() const;
    std::string getAcceptanceInfo() const;
    //! \todo {I think maybe we should include a hyperprior on the mean and
    //!        variance parameters - currently they seem to prone to
    //!        take very high values/bens}
    Probability updateDataProbability();

    virtual void updateRatesUsingTree() = 0;
    virtual void writeLengthsToTree()= 0;

    // I/O
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const EdgeRateMCMC& e)
    {
      return o << e.print();
    };


    std::string print() const;


    //----------------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------------
  protected:
    //! Updates what parameters that will be perturbed in MCMC.
    //! Each parameter i such that idx_limits[i] != 0, is assigned a
    //! probability to be perturbed p[i] = 1/n_params. Afterwars,
    //! idx_limits[i] holds the cumulative perturb. prob of i.
    //! Precondition: \f$ n_params = |\{i:idx_limits \neq 0\}|\f$
    //! Postcondition: \f$ idx_limits[i] = \sum_{j=0}^i p[i]\f$
    virtual void update_idx_limits();

    //! Helper function for generateDates.
    virtual void recursiveGenerateRates(const Node* n, Real parentRate) = 0;

    //! Perturbs a randomly chosen edgeRate.
    //! Precondition: T.getNumberOfNodes() > 1 
    //! Precondition: min < edgeRates[i] < max, for any Node i in T. 
    //! Allows dynamic binding in suggestOwnState()
    // TODO: first precondition should be guaranteed earlier /bens
    virtual Probability perturbRate(unsigned x) = 0;
    virtual Probability perturbRate() = 0;
    virtual void resetPerturbedNode() = 0;

    virtual void adjustRates(const Real& ratio) = 0;
    //! returns a string of values of modeled edgeRates.
    //! Format: "<value of  edgeRate[0]>\t<value of edgeRate[1]\t...".
    //! Allows dynamic overloading in ownStrRep
    virtual std::string ratesStr() const = 0;

    //! returns a header string for modeled edgeRates.
    //! Format: "edgeRate[0];\tedgeRate[1];\t...".
    //! Allows dynamic overloading in ownHeader
    virtual std::string ratesHeader() const = 0;

    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------
  protected:
    Real oldValue;                   //!< stores value of previous iteration
    std::vector<Real> idx_limits;    //!< holds local perturb-probs 
    Node* idx_node;  //!< keeps track of what edgeRate has been perturbed 
    Real min;     //!< lower bound for mean and rates (variance > 0, always).
    Real max;     //!< upper bound for mean, variance and rates.

    //! The suggestion function uses a normal distribution around the current
    //! value. For the proposal-ratios to function right, we need to keep the 
    //! variance to the distribution constant. This is a problem since we do not
    //! know the location/scale of the distribution ahead of time. As a fix, we
    //! store a variance initiated from the start values of dupl/loss rates.
    Real suggestion_variance;
    std::pair<unsigned,unsigned> accPropCnt;  //!< Acc. and total prop. counts.
  };
}//end namespace beep

#endif
