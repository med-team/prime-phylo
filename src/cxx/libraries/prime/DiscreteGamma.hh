#ifndef DISCRETEGAMMA_HH
#define DISCRETEGAMMA_HH

#include <vector>

#include "Beep.hh"

//---------------------------------------------------------------------
//
//! This file includes a number of functions that ultimately returns
//! the mean values of k classes of a discretized gamma-function
//! The Key functions is getDiscreteGammaClasses(...)
//! Author: Bengt Sennblad, SBC, � the MCMC-club, SBC
//
//---------------------------------------------------------------------

namespace beep
{

  //---------------------------------------------------------------------
  //
  // Discretizes the continuous gamma distribution into k "categories",
  // each holding an equal part of the probability mass, 1/k,
  // then returns the mean value r_i for each category.
  //
  // Input:  k: The number of categories that the gamma dist. will be cut in.
  //         alpha: The shape value for the gamma distribution.
  //         beta:  The inverse scale parameter for the gamma distribution, i.e.
  //                1/theta in some other notations. E.g. we have mean=alpha/beta.
  //
  // Output: A vector with the mean value r_i for each discrete gamma category c_i.
  //
  // Method: Described in Yang (1994).
  //         First we calculate the k-1 internal percentage points for the chi-2
  //         distribution using the function ppchi2(). Then, utilizing
  //         the fact that chi2(2*alpha) equals Gamma(alpha, 2), and that
  //         X~Gamma(alpha,beta) <=> cX~Gamma(alpha,beta/c) we can
  //         calculate the corresponding percentage point of the gamma dist.
  //         We can then obtain the mean value in each category through the
  //         following formula:
  //         r_i = alpha/beta*[ I(b*beta,alpha+1) - I(a*beta,alpha+1)]*k 
  //         where a and b are the percentage points that cut category
  //         i, and I(z,alpha) is the incomplete gamma ratio which is
  //         calculated with the function GammaInc in "Incomplete_Gamma.drw".
  //
  // Auxilliary functions: ppchi2() gets the percentage point of the chi2
  //                       distribution.
  //                       gammain() gets the mean of the gamma distribution
  //                       category.
  //
  //-----------------------------------------------------------------------
  std::vector<Real> getDiscreteGammaClasses(const unsigned& k, 
					    const Real& alpha, 
					    const Real& beta);


  //-----------------------------------------------------------------------
  //
  // Returns percentage points for the gamma distribution, i.e.
  // the inverse of the cdf function. Note that notation of beta is such
  // that mean=alpha/beta. Sometimes theta=1/beta is used instead.
  //
  //-----------------------------------------------------------------------
  Real ppGamma(const Real& p, const Real& alpha, const Real& beta);

  //------------------------------------------------------------------
  //
  //! Algorithm As 91, 
  //! Reference: Best & Roberts, 1975, Appl. Statist 24(3):385-388
  //!
  //! To evaluate the percentage points of the chi-squared
  //! probability distribution function, i.e. it is the inverse of
  //! chi-squared's cdf function.
  //! P must lie in the range 0.000002 to 0.999998, V must be positive,
  //! G must be supplied and should be equal to ln(gamma(V/2.0))
  //!
  //! INPUT;   P and V   
  //!          P; Is the cutoff probability
  //!	    V; Degree of freedom   V = 2*alpha
  //!
  //!  Method; Evaluates the Percentage point of the 
  //!          chi2 square distribution
  //!          with the probability P and V degree of freedom
  //!          to obtain the percentage point to the gamma distribution
  //!	
  //!  Output; The Percentage point of chi2 square distribution with 
  //!         probability P and V degree of freedom.
  //
  //------------------------------------------------------------------
  Real ppchi2(const Real& P, const Real& V);


  //------------------------------------------------------------------
  //
  // Computes incomplete gamma ratio for positive values of arguments X 
  // and alpha. Uses series expansion if alpha > X or X < 1.0, otherwise a 
  // continued fraction expansion 
  //
  // INPUT: X and alpha  
  //	  X:     is the percentage point for the function
  //	  alpha: the alpha value of gammafunction  
  //	  X and alpha must be non-negative real value, alpha must be nonzero   
  //
  //  Reference: Bhattacharjee, 1970, Algorithm As 32, J.R.Stat.Soc.C. 19(3)
  //
  //  Output: the value of the incomplete gamma function for X and alpha.
  //
  //-------------------------------------------------------------------------
  Real gamma_in(const Real& X, const Real& alpha);


  //------------------------------------------------------------------
  //
  // This procedure evaluates the natural logarithm of 
  // gamma(x) for all x > 0, accurate to the 10 decimal 
  // places. Stirling's formula is used for the central 
  // polynomial part of the procedure
  //THIS NEEDS TO BE DOUBLE CHECKED!!!
  //-------------------------------------------------------
  Real loggamma_fn(Real x);


  //------------------------------------------------------------------
  //
  // Gauinv finds percentage points of the normal distribution,
  // i.e. it is the inverse of the cdf function.
  //-----------------------------------------------------------
  Real gauinv(const Real& P);

  // Algorithm AS 66 - The normal integral
  // 
  // by I.D. HIll, 1973, Applied Statistics 22(3):424-427
  // Calculates the upper or lower tail area of the standard normal 
  // distribution curve corresponding to any given argument.
  //
  // Parameters: Real x - the argument value of the normal distr function
  //             bool upper - true = calculate area from x to infinity
  //                          false = calclat area from 0 to x
  //
  // Data constants: LTONE should be set to the value at which the lower
  //                 tail area becomes 1.0 to the accuracy of the machine.
  //                 LTONE=(n+9)/3 gives the rquired value accurately 
  //                 enough for a machine that produces n nedical digits 
  //                 in its real number.
  //                 UTZERO should be set to the value at which the upper
  //                 tail area becomes 0.0 to the accuracy of the machine.
  //                 This may be taken as the value such that
  //                 exp(-0.5*UTZERO�)/(UTZERO*scrt(2*pi)) is just greater
  //                 than the smallest allowable real number.
  //--------------------------------------------------------------------
  Real alnorm(Real x, bool upper);

}// end namespace beep
#endif
