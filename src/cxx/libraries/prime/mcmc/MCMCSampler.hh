/* The purpose of this class is to act as a convenient base class for
 * classes that need to sample from an MCMC chain that has a complex setup.
 * This class takes a template parameter called "ProbabilityModel_T" which
 * will represent the "samples", it does not have to be a ProbabilityModel only
 * something that is updated when the provided MCMC chain is advanced.
 *
 *
 *
 * File:   MCMCSampler.hh
 * Author: fmattias
 *
 * Created on January 25, 2010, 3:20 PM
 */

#ifndef _MCMCSAMPLER_HH
#define	_MCMCSAMPLER_HH

#include "AnError.hh"
#include "Probability.hh"
#include "TimeEstimator.hh"
#include "SimpleMCMC.hh"

namespace beep {


template <class ProbabilityModel_T>
class MCMCSampler {
public:
    /**
     * A StateProcesser is a callback function that does additional calculations
     * on a state of the MCMC chain at a certain frequency.
     * 
     * @param state A state of the MCMC-chain.
     * @param iterationsPerformed The number of MCMC-iterations to wait
     *          before processing a state.
     */
    typedef void (*StateProcesser)(ProbabilityModel_T *state, unsigned int iterationsPerformed);

    /**
     * Initializes the sampler with an empty MCMC, which means that
     * no samples can be drawn until the object has been initialized with
     * MCMCSampler::initialize(), which is only intended to be called from
     * derived classes.
     */
    MCMCSampler()
    {
        m_mcmc = 0;
        m_model = 0;
        m_printInterval = DEFAULT_PRINT_INTERVAL;
        m_isInitialized = false;
    }

    /**
     * Initializes the samples with the provided MCMC object and a pointer to
     * a sample. It is assumed that as the MCMC object changes so does the
     * sample, i.e. the sample is a pointer to something that is perturbed.
     *
     * model - A pointer to a sample that changes with the MCMC object.
     * mcmc - The MCMC object.
     */
    MCMCSampler(ProbabilityModel_T *model, SimpleMCMC *mcmc) :
                                                        m_model(model),
                                                        m_mcmc(mcmc),
                                                        m_thinning(DEFAULT_THINNING),
                                                        m_isInitialized(true),
                                                        m_printInterval(DEFAULT_PRINT_INTERVAL)

    {
    }

  virtual ~MCMCSampler()
  {}


    /**
     * currentState
     *
     * Returns the current state.
     */
    virtual ProbabilityModel_T *currentState()
    {
        if(!m_isInitialized) {
            throw AnError("MCMCSampler::currentState(): Base class not initialized.");
        }

        return m_model;
    }

    /**
     * nextState
     *
     * Performs jumps iterations in the underlying MCMC chain and returns
     * the last state.
     */
    virtual ProbabilityModel_T *nextState(int jumps = 1)
    {
        if(!m_isInitialized) {
            throw AnError("MCMCSampler::nextState(): Base class not initialized.");
        }

        // Advance chain if we should do at least one jump
        if(jumps > 0) {
            m_mcmc->advance(jumps);
            return m_model;
        }
        else {
            return m_model;
        }
    }

    /**
     * nextState
     *
     * Does exactly like nextState but also calls the process function when
     * callbackInterval jumps has been performed.
     *
     * jumps - The total number of iterations to be performed.
     * callbackInterval - The interval in which the callback should be called,
     *                    i.e. a value of 100 indicates that the callback should
     *                    be called each 100 iteration.
     * callback - A method that is called on each callbackInterval sample.
     */
    virtual ProbabilityModel_T *nextState(int jumps, 
                                          int callbackInterval,
                                          StateProcesser callback)
    {
        // The number of blocks of callbackInterval to iterate
        unsigned int blockIterations = jumps / callbackInterval;

        // The number of iterations left
        unsigned int rest = jumps % callbackInterval;

        // Perform the block iterations
        for(unsigned int i = 0; i < blockIterations; i++) {
            nextState(callbackInterval);
            callback(m_model, callbackInterval);
        }

        // Some iterations are left perform them
        if(rest > 0) {
            nextState(rest);
            callback(m_model, rest);

        }
        return m_model;
    }

    /**
     * nextStateWithTimeLeft
     *
     * Performs jumps iterations and prints the time left each printInterval
     * iterations.
     *
     * jumps - Number of iterations to perform.
     * printInterval - Determines how often we should print the time left.
     */
    virtual ProbabilityModel_T *nextStateWithTimeLeft(int jumps, int printInterval)
    {
        TimeEstimator::instance()->reset(jumps);
        return nextState(jumps, printInterval, printTimeLeft);
    }

    /**
     * nextStateWithTimeLeft
     *
     * Performs jumps iterations and prints the time left each printInterval
     * iterations that has been set by setPrintInterval().
     *
     * jumps - Number of iterations to perform.
     */
    virtual ProbabilityModel_T *nextStateWithTimeLeft(int jumps)
    {
        return nextStateWithTimeLeft(jumps, m_printInterval);
    }

    /**
     * nextSample
     *
     * Performs the number of iterations set by setThinning and returns the
     * last state.
     */
    virtual ProbabilityModel_T *nextSample()
    {
        return nextState(m_thinning);
    }

    /**
     * setMCMC
     *
     * Sets the mcmc chain that provides new samples.
     */
    virtual void setMCMC(SimpleMCMC *mcmc)
    {
        m_mcmc = mcmc;
    }

    /**
     * getMCMC
     *
     * Get the mcmc chain that provides new samples.
     */
    virtual SimpleMCMC *getMCMC()
    {
          return m_mcmc;
    }

    /**
     * setThinning
     *
     * Sets the number of iterations between each sample.
     *
     * thinning - The number of iterations to perform in nextSample().
     */
    void setThinning(int thinning);

    /**
     * setPrintInterval
     *
     * Sets the number of iterations between each printing of time left used
     * in nextSampleWithTimeLeft().
     *
     * printInterval - Determines how often we should print the time left.
     */
    void setPrintInterval(int printInterval);

protected:

    /**
     * Initializes the class with an mcmc chain and a structure containing
     * a sample.
     *
     * model - A pointer to the sample.
     * mcmc - The mcmc chain.
     */
    void initialize(ProbabilityModel_T *model, SimpleMCMC *mcmc)
    {
        m_model = model;
        m_mcmc = mcmc;
        m_isInitialized = true;
    }

    /**
     * printTimeLeft
     *
     * A function that updates the singleton time estimator and prints the
     * estimated time left.
     */
    static void printTimeLeft(ProbabilityModel_T *, unsigned int iterationsPerformed)
    {
        TimeEstimator::instance()->update(iterationsPerformed);
        TimeEstimator::instance()->printEstimatedTimeLeft();
    }

private:
    // The current model, it is assumed that it keeps references or pointers
    // to the objects changed in the mcmc.
    ProbabilityModel_T *m_model;

    // The mcmc chain
    SimpleMCMC *m_mcmc;

    // The number of iterations between each sample
    int m_thinning;

    // The default used for printing the time left
    int m_printInterval;

    // Is set to true when m_model and m_mcmc is initialized
    bool m_isInitialized;

    // Default number of iterations for thinning
    static const int DEFAULT_THINNING = 100;

    // The default interval between each print of the estimated time left
    static const int DEFAULT_PRINT_INTERVAL = 1000;
};

}

#endif	/* _MCMCSAMPLER_HH */


