//**********************************************************************************************
// FOR REFERENCE, THIS CLASS CONTAINS A SNAPSHOT OF PETER/MATTIAS' EXTENDED GSR IMPLEMENTATION
// AS OF 2010-06-10.
// NO COMPATILIBILITY TESTS HAVE BEEN MADE, I.E. EXPECT COMPILATION ERRORS TO START WITH.
// HOWEVER, IT SHOULD EASE THE TRANSITION TO A MORE COHERENT STRUCTURE. NOTE THAT PETER/MATTIAS'
// EXTENDED VERSION MAY EXIST IN A MORE RECENT STATE IN A SEPARATE SVN-BRANCH.
//**********************************************************************************************

///*
// * File:   MCMCGSRSampler.cc
// * Author: fmattias
// *
// * Created on January 25, 2010, 6:25 PM
// */
//
//#include "GSRSampler.hh"
//#include "EdgeWeightMCMC.hh"
//#include "PerturbationObservable.hh"
//#include "RandomTreeGenerator.hh"
//
//using namespace beep;
//
//const double   GSRSampler::SUGG_RATIO_BD_RATES     = 1.0;
//const double   GSRSampler::SUGG_RATIO_G_TOPO_START = 3.0; // Initial value.
//const double   GSRSampler::SUGG_RATIO_G_TOPO_FINAL = 3.0; // Final value.
//const unsigned GSRSampler::SUGG_RATIO_G_TOPO_STEPS = 1;   // Steps between start and final.
//const double   GSRSampler::SUGG_RATIO_G_LENGTHS    = 1.0;
//
//GSRSampler::GSRSampler(Tree                    &speciesTree,
//                               StrStrMap               &geneSpeciesMap,
//                               Density2P               &edgeRateDensity,
//                               int                     minIntervals,
//                               float                   timeStep) :
//                                                     m_deleteSpeciesTree(true),
//                                                     m_speciesTree(speciesTree),
//                                                     m_geneSpeciesMap(geneSpeciesMap),
//                                                     m_edgeRateDensity(&edgeRateDensity)
//
//{
//    createDiscretizedTree(minIntervals, timeStep);
//    setup();
//}
//
//GSRSampler::GSRSampler(Tree                    &speciesTree,
//                               StrStrMap               &geneSpeciesMap,
//                               Density2P               &edgeRateDensity,
//                               EdgeDiscTree            &discretizedTree,
//                               LSDProbs                &lsd) :
//                                                     m_deleteSpeciesTree(false),
//                                                     m_speciesTree(speciesTree),
//                                                     m_geneSpeciesMap(geneSpeciesMap),
//                                                     m_edgeRateDensity(&edgeRateDensity),
//                                                     m_lsd(lsd),
//                                                     m_discretizedTree(&discretizedTree)
//
//
//{
//    setup();
//}
//
//GSRSampler::~GSRSampler()
//{
//    delete m_geneTree;
//    delete m_gsrModel;
//    if(m_deleteSpeciesTree){
//        delete m_discretizedTree;
//        delete m_edgeDiscretizer;
//    }
//    delete m_mcmcDummy;
//    delete m_birthDeathProbs;
//    delete m_mcmc_birthDeathRates;
//    delete m_mcmc_geneTreeTopology;
//    delete m_mcmc_edgeRates;
//    delete m_mcmc_geneTreeLengths;
//    delete m_gsrMCMC;
//}
//
//void GSRSampler::setup()
//{
//    m_birthRate = 1.0;
//    m_deathRate = 1.0;
//
//    createGeneNamesList(m_geneSpeciesMap);
//
//    // Remove old observers, when creating multiple chains this can lead
//    // to a segmentation fault if the old observers are deleted.
//    m_edgeRateDensity->clearPertObservers();
//
//    //Initiate gene tree
//    m_geneTree = new Tree(RandomTreeGenerator::generateRandomTree(m_geneNames));
//
//    // MCMC: End-of-chain.
//    m_mcmcDummy = new DummyMCMC();
//
//    // MCMC: Birth-death rates (duplication-loss rates).
//    m_birthDeathProbs = new EdgeDiscBDProbs(*m_discretizedTree, m_birthRate, m_deathRate, m_lsd);
//    m_mcmc_birthDeathRates = new EdgeDiscBDMCMC(*m_mcmcDummy, *m_birthDeathProbs, SUGG_RATIO_BD_RATES);
//
//
//    // MCMC: Guest tree topology.
//    m_mcmc_geneTreeTopology = new UniformTreeMCMC(*m_mcmc_birthDeathRates, *m_geneTree, SUGG_RATIO_G_TOPO_START);
//    m_mcmc_geneTreeTopology->setChangingSuggestRatio(SUGG_RATIO_G_TOPO_FINAL, SUGG_RATIO_G_TOPO_STEPS);
//    // I removed detailed notification here because partial update fails otherwise
////    m_mcmc_geneTreeTopology->setDetailedNotification(true);
//
//    // MCMC: Substitution rate variation over edges (IID).
//    m_mcmc_edgeRates = new Density2PMCMC(*m_mcmc_geneTreeTopology, *m_edgeRateDensity, true);
//    // TODO: fix density parameters if density is uniform
//
//
//    // Set up GSR integrator.
//    m_gsrModel = new EdgeDiscGSR(*m_geneTree,
//                                 *m_discretizedTree,
//                                  m_geneSpeciesMap,
//                                 *m_edgeRateDensity,
//                                 *m_birthDeathProbs);
//    m_gsrModel->setLSDProbabilities(m_lsd);
//
//
//    // MCMC: Edge lengths. The GSR class holds these, and may therefore be model.
//    m_mcmc_geneTreeLengths = new EdgeWeightMCMC(*m_mcmc_edgeRates, *m_gsrModel, SUGG_RATIO_G_LENGTHS);
//    m_mcmc_geneTreeLengths->generateWeights();
//    m_mcmc_geneTreeLengths->setDetailedNotification(true);
//
//    m_gsrMCMC = new SimpleMCMC(*m_mcmc_geneTreeLengths, 1);
//
//    MCMCSampler<EdgeDiscGSR>::initialize(m_gsrModel, m_gsrMCMC);
//}
//
//void
//GSRSampler::createDiscretizedTree(int min_intervals, float time_step)
//{
//    // Create discretized species tree
//    if (time_step == 0)
//    {
//        m_edgeDiscretizer = new EquiSplitEdgeDiscretizer(min_intervals);
//    }
//    else
//    {
//        m_edgeDiscretizer = new StepSizeEdgeDiscretizer(time_step, min_intervals);
//    }
//    m_discretizedTree = m_edgeDiscretizer->getDiscretization(m_speciesTree);
//    m_deleteSpeciesTree = true;
//}
//
//void
//GSRSampler::createGeneNamesList(StrStrMap &geneSpeciesMap)
//{
//    for(unsigned int i = 0; i < geneSpeciesMap.size(); i++) {
//        m_geneNames.push_back(geneSpeciesMap.getNthItem(i));
//    }
//}
//
//Density2P *
//GSRSampler::getDensity()
//{
//    return m_edgeRateDensity;
//}
//
//EdgeDiscTree *
//GSRSampler::getDiscretizedTree()
//{
//    return m_discretizedTree;
//}
//
//
//void
//GSRSampler::setBDParameters(float birthRate, float deathRate)
//{
//    //Set new member values
//    m_birthRate = birthRate;
//    m_deathRate = deathRate;
//
//    //Set new parameters and update state perturbation listeners
//    m_birthDeathProbs->setRates(birthRate, deathRate, true);
//    PerturbationEvent updateEvent(PerturbationEvent::PERTURBATION);
//    m_birthDeathProbs->notifyPertObservers(&updateEvent);
//}
//
//void
//GSRSampler::fixGeneTree()
//{
//    m_mcmc_geneTreeTopology->fixTree();
//    m_mcmc_geneTreeTopology->fixRoot();
//}
//
//void
//GSRSampler::fixGeneTree(const Tree &geneTree)
//{
//    /* Set new gene tree and notify observers */
//    m_mcmc_geneTreeTopology->setTree(geneTree);
//    PerturbationEvent perturbation(PerturbationEvent::PERTURBATION);
//    m_geneTree->notifyPertObservers(&perturbation);
//
//    /* Update overall probability */
//    m_mcmc_geneTreeLengths->updateDataProbability();
//    fixGeneTree();
//}
//
//void
//GSRSampler::fixLengths()
//{
//    m_mcmc_geneTreeLengths->fixWeights();
//}
//
//void
//GSRSampler::fixBDParameters()
//{
//    m_mcmc_birthDeathRates->fixRates();
//}
//
//void
//GSRSampler::fixDensityParameters()
//{
//    m_mcmc_edgeRates->fixMean();
//    m_mcmc_edgeRates->fixVariance();
//}
//
//Tree *GSRSampler::getGeneTree()
//{
//    return m_geneTree;
//}
//
//EdgeWeightMCMC *GSRSampler::getWeightMCMC()
//{
//    return m_mcmc_geneTreeLengths;
//}
