//**********************************************************************************************
// FOR REFERENCE, THIS CLASS CONTAINS A SNAPSHOT OF PETER/MATTIAS' EXTENDED GSR IMPLEMENTATION
// AS OF 2010-06-10.
// NO COMPATILIBILITY TESTS HAVE BEEN MADE, I.E. EXPECT COMPILATION ERRORS TO START WITH.
// HOWEVER, IT SHOULD EASE THE TRANSITION TO A MORE COHERENT STRUCTURE. NOTE THAT PETER/MATTIAS'
// EXTENDED VERSION MAY EXIST IN A MORE RECENT STATE IN A SEPARATE SVN-BRANCH.
//**********************************************************************************************

///* The idea of this class is to provide an easy interface to the GSR MCMC
// * prior. This class can be used in other samplers to further nest the MCMC.
// *
// * File:   MCMCGSRSampler.hh
// * Author: fmattias
// *
// * Created on January 25, 2010, 6:25 PM
// */
//
//#ifndef _MCMCGSRSAMPLER_HH
//#define	_MCMCGSRSAMPLER_HH
//
//#include <vector>
//
//#include "Density2P.hh"
//#include "Density2PMCMC.hh"
//#include "DummyMCMC.hh"
//#include "EdgeDiscBDMCMC.hh"
//#include "EdgeDiscBDProbs.hh"
//#include "EdgeDiscGSR.hh"
//#include "EdgeDiscTree.hh"
//#include "EdgeWeightMCMC.hh"
//#include "lsd/LSDProbs.hh"
//#include "PRNG.hh"
//#include "SimpleMCMC.hh"
//#include "StrStrMap.hh"
//#include "Tree.hh"
//#include "TreeDiscretizers.hh"
//#include "TreeMCMC.hh"
//
//#include "MCMCSampler.hh"
//
//
//namespace beep {
//
//class GSRSampler : public MCMCSampler<EdgeDiscGSR> {
//public:
//
//    /**
//     * Creates a sampler that discretizes the species tree by the minIntervals
//     * and timeStep parameters.
//     *
//     * speciesTree - The species tree, it is assumed not to change between
//     *               iterations.
//     * geneSpeciesMap - A map between genes and species.
//     * edgeRateDensity - The density function that models the rates over the
//     *                   edges.
//     * minIntervals -  The minimum number of intervals that an edge in the
//     *                 species tree will be discretized in.
//     * timeStep - The distance between two discretization points. If 0
//     *            minIntervals will be used instead.
//     */
//    GSRSampler(Tree &speciesTree,
//               StrStrMap &geneSpeciesMap,
//               Density2P &edgeRateDensity,
//               int minIntervals,
//               float timeStep);
//
//
//    /**
//     * Creates a sampler that uses a specified discretization of the species
//     * tree.
//     *
//     * speciesTree - The species tree, it is assumed not to change between
//     *               iterations.
//     * geneSpeciesMap - A map between genes and species.
//     * edgeRateDensity - The density function that models the rates over the
//     *                   edges.
//     * discretizedTree - A discretization of the species tree.
//     * lsd - Probabilities for large scale duplications in the species tree.
//     */
//    GSRSampler(Tree &speciesTree,
//               StrStrMap &geneSpeciesMap,
//               Density2P &edgeRateDensity,
//               EdgeDiscTree &discretizedTree,
//               LSDProbs &lsd);
//
//    /**
//     * Destructor.
//     */
//    virtual ~GSRSampler();
//
//
//    /**
//     * Returns the rate density.
//     */
//    Density2P *getDensity();
//
//    /**
//     * getDiscretizedTree
//     *
//     * Returns the discretized species tree.
//     */
//    EdgeDiscTree *getDiscretizedTree();
//
//    /**
//     * setBDParameters
//     *
//     * Set the birth death parameters to a new value.
//     *
//     * birthRate - New birth rate.
//     * deathRate - New death rate.
//     */
//    void setBDParameters(float birthRate, float deathRate);
//
//    /**
//     * getGeneTree
//     *
//     * Returns the current gene tree.
//     */
//    Tree *getGeneTree();
//
//    /**
//     * getWeightMCMC
//     *
//     * Returns the mcmc corresponding to the lengths in the GSR mcmc.
//     */
//    EdgeWeightMCMC *getWeightMCMC();
//
//    void fixGeneTree();
//    /**
//     * fixGeneTree
//     *
//     * Fixes the gene tree topology in the MCMC to the given gene tree, when
//     * the gene tree has been fixed you cannot unfix them again.
//     *
//     * geneTree - The fixed topology.
//     */
//    void fixGeneTree(const Tree &geneTree);
//
//    /**
//     * fixLengths
//     *
//     * Fixate the lengths of the gene tree, when the lengths are fixed you
//     * cannot unfix them again.
//     */
//    void fixLengths();
//
//    /**
//     * fixBDParameters
//     *
//     * Fixate the birth and death parameters, when the parameters are fixed you
//     * cannot unfix them again.
//     */
//    void fixBDParameters();
//
//    /**
//     * fixDensityParameters
//     *
//     * Fixate the mean and variance of the edge rate density, when the
//     * parameters are fixed you cannot unfix them again.
//     */
//    void fixDensityParameters();
//
//private:
//    // Do not allow copy constructor to be called.
//    GSRSampler(const GSRSampler &);
//
//    /**
//     * setup()
//     *
//     * Sets up the nested GSR MCMC chain.
//     */
//    void setup();
//
//    /**
//     * Creates a discretized species tree given the discretization step.
//     *
//     * minIntervals - The minimum number of intervals that each edge discretized
//     *                into.
//     * timeStep     - The time step between each discretization point, if = 0,
//     *                minIntervals is used.
//     */
//    void createDiscretizedTree(int minIntervals, float timeStep);
//
//    /**
//     * Creates a list of the gene names from a StrStrMap.
//     *
//     * geneSpeciesMap - A map between genes and species.
//     */
//    void createGeneNamesList(StrStrMap &geneSpeciesMap);
//
//    // Determine whether the discretized species tree should be deleted or not
//    bool                        m_deleteSpeciesTree;
//
//    // The birth rate in the birth-death process
//    Real                        m_birthRate;
//    // The death rate in the birth-death process
//    Real                        m_deathRate;
//
//    // The species tree
//    Tree                        m_speciesTree;
//    // A map from genes to species
//    StrStrMap                   m_geneSpeciesMap;
//    // A random generator
//    PRNG                        m_rand;
//    // The edge rate density function
//    Density2P                   *m_edgeRateDensity;
//    // Large scale duplication probabilities
//    LSDProbs                    m_lsd;
//    // A list of names of each gene
//    std::vector<std::string>    m_geneNames;
//
//
//    EdgeDiscGSR                 *m_gsrModel;
//    EdgeDiscTree                *m_discretizedTree;
//    Tree                        *m_geneTree;
//    EdgeDiscretizer             *m_edgeDiscretizer;
//    DummyMCMC                   *m_mcmcDummy;
//    EdgeDiscBDProbs             *m_birthDeathProbs;
//    EdgeDiscBDMCMC              *m_mcmc_birthDeathRates;
//    UniformTreeMCMC             *m_mcmc_geneTreeTopology;
//    Density2PMCMC               *m_mcmc_edgeRates;
//    EdgeWeightMCMC              *m_mcmc_geneTreeLengths;
//
//    SimpleMCMC                  *m_gsrMCMC;
//
//private:
//    // Suggestion rates. Affects perturbation frequency of the various parameters.
//    static const double   SUGG_RATIO_BD_RATES;
//    static const double   SUGG_RATIO_G_TOPO_START; // Initial value.
//    static const double   SUGG_RATIO_G_TOPO_FINAL; // Final value.
//    static const unsigned SUGG_RATIO_G_TOPO_STEPS;   // Steps between start and final.
//    static const double   SUGG_RATIO_G_LENGTHS;
//
//};
//
//}
//
//#endif	/* _MCMCGSRSAMPLER_HH */

