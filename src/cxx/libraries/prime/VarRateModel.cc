#include "VarRateModel.hh"

#include "AnError.hh"
#include "Density2P.hh"
#include "Node.hh"
#include "Tree.hh"

#include <iostream>
#include <sstream>
#include <cmath>

//----------------------------------------------------------------------
//
// class VarRateModel, inherits from EdgeRateModel
// Pure virtual base class for model of rates that vary over a tree T. 
//
// Note that subclasses of this class is also declared in this file:
//
//----------------------------------------------------------------------

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Constructor/Destructor
  //
  //----------------------------------------------------------------------
  VarRateModel::VarRateModel(Density2P& rateProb, const Tree& T_in,
			     EdgeWeightModel::RootWeightPerturbation rwp)
    : EdgeRateModel(),
      EdgeRateModel_common(rateProb, T_in, rwp)
  { 
    assert(T->getNumberOfNodes() > 1); // check precondition
    edgeRates = RealVector(T->getNumberOfNodes(), rateProb.getMean());
  };

  // with specified values for edgeRates
  VarRateModel::VarRateModel(Density2P& rateProb,
			     const Tree& T_in, const RealVector& edgeRates_in,
			     EdgeWeightModel::RootWeightPerturbation rwp)
    : EdgeRateModel(),
      EdgeRateModel_common(rateProb, T_in, rwp)
  {
    assert(T->getNumberOfNodes() > 1); // check precondition
    assert(edgeRates_in.size() == T->getNumberOfNodes()); // check precondition

    edgeRates = edgeRates_in;
    std::cerr << "done " << endl;
  };

  VarRateModel::VarRateModel(const VarRateModel& vrm)
    : EdgeRateModel(vrm),
      EdgeRateModel_common(vrm)
  {
  };

  VarRateModel::~VarRateModel() {};

  VarRateModel&
  VarRateModel::operator=(const VarRateModel& vrm)
  {
    if(this != &vrm)
      {
	EdgeRateModel::operator=(vrm);
	EdgeRateModel_common::operator=(vrm);
      }
    return *this;
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------

  // Interface inherited from ProbabilityModel
  //----------------------------------------------------------------------
  void
  VarRateModel::update()
  {
    // TODO: If the tree changes we must change the rates accordingly, but how?
    // /bens
  };

  std::string
  VarRateModel::type() const
  {
    return "a undefined model ";
  }

  void
  VarRateModel::setRate(const Real& newRate, const Node& n)
  {
    assert(!n.isRoot());
    // Note! BeepVector asserts saneness of n
    if(rateProb->isInRange(newRate)) //! \todo{ This will be double checks use assert! - No! we need the check!}
      {
	edgeRates[n] = newRate;
	return;
      }
    else
      {
	ostringstream oss;
	oss << "VarRateModel::setRate(r): r = " << newRate << " is out of range for node " << n.getNumber() << "!";
	throw AnError(oss.str(), 1);
	// 	throw AnError("VarRateModel::setRate(r): r out of range!", 1);
      }
  }

  void
  VarRateModel::setRate(const Real& newRate, const Node* n)
  {
    assert(n != 0); // pointer points to a Node
    return setRate(newRate, *n);
  }

  //------------------------------------------------------------------
  //
  // I/O
  //
  //------------------------------------------------------------------

  // Always define an ostream operator!
  // This should provide a neat output describing the model and its
  // current settings for output to the user. It should list current
  // parameter values by linking to their respective ostream operator
  //------------------------------------------------------------------
  std::ostream&
  operator<<(std::ostream &o, const VarRateModel& erm)
  {
    return o << erm.print();
  }

  std::string
  VarRateModel::print() const
  {
    return EdgeRateModel_common::print();
  };


  unsigned
  VarRateModel::nRates() const
  {
    unsigned nr = 0;
    switch (getRootWeightPerturbation())
      {
      case EdgeWeightModel::BOTH:
	nr = T->getNumberOfNodes()-1;
	break;
      case EdgeWeightModel::RIGHT_ONLY:
	nr = T->getNumberOfNodes()-3;
	break;
      case EdgeWeightModel::NONE:
	nr = T->getNumberOfNodes()-2;
	break;
      }
    return nr;
  }



  //----------------------------------------------------------------------
  //
  // subclass iidRateModel
  //
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  //
  // Constructor/Destructor
  //
  //----------------------------------------------------------------------
  iidRateModel::iidRateModel(Density2P& rateProb, const Tree& T,
			     EdgeWeightModel::RootWeightPerturbation rwp)
    : EdgeRateModel(),
      VarRateModel(rateProb, T, rwp)
  {
  }

  // With Specified values for edgeRates
  iidRateModel::iidRateModel(Density2P& rateProb, const Tree& T, 
			     const RealVector& edgeRates_in,
			     EdgeWeightModel::RootWeightPerturbation rwp)
    : EdgeRateModel(),
      VarRateModel(rateProb, T, edgeRates_in, rwp)
  {
  }

  iidRateModel::iidRateModel(const iidRateModel& irm)
    : EdgeRateModel(irm),
      VarRateModel(irm)
  {
  };

  iidRateModel::~iidRateModel() {};

  iidRateModel&
  iidRateModel::operator=(const iidRateModel& irm)
  {
    if(this != &irm)
      {
	EdgeRateModel::operator=(irm);
	VarRateModel::operator=(irm);
      }
    return *this;
  }


  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------
  Probability
  iidRateModel::calculateDataProbability()
  {
    Node& r = *T->getRootNode();
    Probability ret(1.0);
    if(!r.isLeaf())
      {
	Node* lc = r.getLeftChild();
	Node* rc = r.getRightChild();

	// To avoid over-parametrization, the rate of edges adjacent
	// to the root is not modeled. Instead these edges adjacent,
	// are modeled as a single edge, i.e., root edge to right child
	// have the same rate as left child with probability 1.0
	ret *= recursiveDataProb(lc);
	if(!rc->isLeaf())
	  {
	    ret *= recursiveDataProb(rc->getLeftChild()) *
	      recursiveDataProb(rc->getRightChild());
	  }
      }
    like=ret;
    return ret;
  };


  std::string
  iidRateModel::type() const
  {
    return "an iid model ";
  }

  // returns value of rate for node
  //----------------------------------------------------------------------
  Real
  iidRateModel::getRate(const Node& n) const
  {
    // Note! Beep_Vecotr asserts saneness of n
    assert(!n.isRoot());
    return  edgeRates[n];
  };

  Real
  iidRateModel::getRate(const Node* n) const
  {
    assert(n != 0);
    return getRate(*n);
  }

  void
  iidRateModel::setRate(const Real& newRate, const Node& n)
  {
    assert(!n.isRoot());
    VarRateModel::setRate(newRate, n);
    if (n.getParent()->isRoot() && getRootWeightPerturbation() != EdgeWeightModel::BOTH)
      {
	edgeRates[n.getSibling()] = newRate;
      }
  }

  void
  iidRateModel::setRate(const Real& newRate, const Node* n)
  {
    assert(n != 0); // pointer must point to a Node
    return setRate(newRate, *n);
  }


  //------------------------------------------------------------------
  // I/O
  //------------------------------------------------------------------

  // Always define an ostream operator!
  // This should provide a neat output describing the model and its
  // current settings for output to the user. It should list current
  // parameter values by li nking to their respective ostream operator
  //------------------------------------------------------------------
  std::ostream&
  operator<<(std::ostream &o, const iidRateModel& erm)
  {
    return o << erm.print();
  }

  std::string
  iidRateModel::print() const
  {
    std::ostringstream oss;
    oss << indentString(VarRateModel::print());
    //     if(estimateRates)
    if(true)
      oss << "using a iid rate Model.\n";
    return oss.str();
  };


  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------
  // Calculates the product of f(r(e)), for all e\in E(T), recursively
  Probability
  iidRateModel::recursiveDataProb(Node* n)
  {
    Probability ret(1.0);
    if(!n->isLeaf())
      {
	//first pass recursion on
	ret = recursiveDataProb(n->getLeftChild()) *
	  recursiveDataProb(n->getRightChild());
      }
    return ret * rateProb->operator()(edgeRates[n]);
  };





  //----------------------------------------------------------------------
  //
  // subclass gbmRateModel -
  //
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  //
  // Constructor/Destructor
  //
  //----------------------------------------------------------------------
  gbmRateModel::gbmRateModel(Density2P& rateProb, const Tree& T,
			     EdgeWeightModel::RootWeightPerturbation rwp)
    : EdgeRateModel(),
      VarRateModel(rateProb, T, rwp),
      variance(rateProb.getVariance())
  {
  };

  // With specified values for edgeRates
  gbmRateModel::gbmRateModel(Density2P& rateProb, const Tree& T, const RealVector& edgeRates,
			     EdgeWeightModel::RootWeightPerturbation rwp)
    : EdgeRateModel(),
      VarRateModel(rateProb, T, edgeRates, rwp),
      variance(rateProb.getVariance())
  {
  };

  gbmRateModel::gbmRateModel(const gbmRateModel& grm)
    : EdgeRateModel(grm),
      VarRateModel(grm),
      variance(grm.variance)
  {
  };

  gbmRateModel::~gbmRateModel() {};

  gbmRateModel&
  gbmRateModel::operator=(const gbmRateModel& grm)
  {
    if(this != &grm)
      {
	EdgeRateModel::operator=(grm);
	VarRateModel::operator=(grm);
	variance = grm.variance;
      }
    return *this;
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------
  Probability
  gbmRateModel::calculateDataProbability()
  {
    Node& r = *T->getRootNode();
    Probability ret(1.0);
    if(!r.isLeaf())
      {
	Node& lc = *r.getLeftChild();
	Node& rc = *r.getRightChild();
	Real lrate = edgeRates[lc];
	// 	Real rrate = edgeRates[rc];
	// To avoid over-parametrization, the root's rate is not modeled
	// As an approximation the process is modeled to start at root's
	// left child, lc, and continuing 1) down T_{lc} and 2) up to root
	// and then down planted subtree at root's right child.
	if(!lc.isLeaf())
	  {                                    // left subtree
	    ret *= recursiveDataProb(lc.getLeftChild(), lrate) *
	      recursiveDataProb(lc.getRightChild(), lrate);
	  }
	ret *= recursiveDataProb(&rc, lrate);// i.e., Pr[lrate = rootrate]=1
	// 	if(!rc.isLeaf())
	// 	  {                                    // right subtree
	// 	    ret *= recursiveDataProb(rc.getLeftChild(), rrate) *
	// 	      recursiveDataProb(rc.getRightChild(), rrate);
	// 	  }

	// #ifndef FIX_ROOT_EDGES_RATE  // joelgs: Forbidden to use!
	// 	// log(rrate) ~ N(log(lrate)-variance(rtime-3ltime)/2,
	// 	// variance(ltime+rtime))
	// 	// See Linder et al. (unpublished)
	// 	Real ltime = lc.getTime();
	// 	Real rtime = rc.getTime();
	// 	rateProb->setEmbeddedParameters(std::log(lrate) -
	// 					0.5 * variance * (rtime - 3.0 * ltime),
	// 					variance * (rtime + ltime));
	//  	ret *=(*rateProb)(rrate);
	// #endif
      }
    like = ret;
    return ret;
  };


  std::string
  gbmRateModel::type() const
  {
    return "a gbm model ";
  }

  // Returns mean of underlying density.
  Real
  gbmRateModel::getMean() const
  {
    return edgeRates[T->getRootNode()->getLeftChild()];
  }

  // Returns variance of underlying density.
  Real
  gbmRateModel::getVariance() const
  {
    return variance;
  }

  // Sets mean of underlying density.
  void
  gbmRateModel::setMean(const Real& newValue)
  {
    edgeRates[T->getRootNode()->getLeftChild()] = newValue;
    //!\todo{ This should bne handled by the MCMCModel in some way!}
    // Since setMean actuallt changes a rate we need to indicate this!
    T->perturbedNode(T->getRootNode()->getLeftChild());
    if (getRootWeightPerturbation() == EdgeWeightModel::NONE)
      {
	edgeRates[T->getRootNode()->getRightChild()] = newValue;
	//!\todo{ This should bne handled by the MCMCModel in some way!}
	// Since setMean actuallt changes a rate we need to indicate this!
	T->perturbedNode(T->getRootNode());
      }
  }

  // Returns variance of underlying density.
  void
  gbmRateModel::setVariance(const Real& newValue)
  {
    variance = newValue;
    return;
  }

  Real
  gbmRateModel::getRate(const Node& n) const
  {
    // note! BeepVector asserts for saneness of n
    assert(!n.isRoot());
    // Edge rate is average of adjacent nodes' rates
    Real r;
    Node& p = *n.getParent();
    if(p.isRoot())
      {
	// We model the two edges adjacent to root as one edge, thus:
	r =  (edgeRates[n] + edgeRates[n.getSibling()]) / 2.0;
      }
    else
      {
	r =  (edgeRates[n] + edgeRates[p]) / 2.0;
      }
    return r;
  };

  Real
  gbmRateModel::getRate(const Node* n) const
  {
    assert(n != 0);
    return getRate(*n);
  }

  //------------------------------------------------------------------
  //
  // I/O
  //
  //------------------------------------------------------------------
  std::ostream&
  operator<<(std::ostream &o, const gbmRateModel& erm)
  {
    return o << erm.print();
  }

  std::string
  gbmRateModel::print()const
  {
    std::ostringstream oss;
    Node& lc = *T->getRootNode()->getLeftChild();
    // Set rateProbs mean and variance to 'start values' of gbm process
    rateProb->setParameters(edgeRates[lc], variance);
    oss << indentString(VarRateModel::print());
    if(true)
      //     if(estimateRates)
      oss << "using a gbm rate Model.\n";
    return oss.str();
  };

  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------
  // Denote the density of a rate f(rate, mean, variance).
  // We return product of f(rate(n), parentRate, time * variance)
  Probability
  gbmRateModel::recursiveDataProb(Node* n, Real parentRate)
  {
    Real& rate = edgeRates[n];
    Real beta = n->getTime() * variance;
    Probability ret(1.0);
    if(!n->isLeaf())
      {
	// pass recursion on
	ret = recursiveDataProb(n->getLeftChild(), rate) *
	  recursiveDataProb(n->getRightChild(), rate);
      }
    // set up rateProb
    // The following is the orginal Thorne et al. for LogN
    rateProb->setEmbeddedParameters(std::log(parentRate) - 0.5 * beta, beta);
    //     // The following is the Yang variant for Gamma
    //     rateProb->setParameters(parentRate, variance * time);
    return ret * (*rateProb)(rate);
  };

}//end namespace beep


