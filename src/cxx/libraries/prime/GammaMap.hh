#ifndef GAMMAMAP_HH
#define GAMMAMAP_HH

#include "LambdaMap.hh"
#include "SetOfNodes.hh"

#include <deque>
#include <set>
#include <list>
#include <vector>
#include <cstdio>

namespace beep
{
  // forward declarations
  class Tree;

  class GammaMap;
  bool operator==(const GammaMap &gm1, const GammaMap &gm2);
  bool operator<(const GammaMap &gm1, const GammaMap &gm2);

  //-----------------------------------------------------------------
  //
  //! Implement the map, \f$ \check\gamma \f$, species node --> gene nodes.
  //! Consider a guest tree, G, as evolving 'inside' a host tree, S.
  //! A (full) reconciliation, intuitively, is a function
  //! \f[ \gamma:V(S)\rightarrow 2^{V(G)} \f]
  //! such that \f$ \gamma(x) \f$ comprise all vertices in G whose
  //! incoming edges 'passes' the edge <parent(x), x> in S.
  //! In the 'reduced' reconciliation, \f$ \check\gamma(x) \f$ comprise 
  //! the vertices in \f$ \gamma(x) \f$ that are minimal in the partial 
  //! order of G. This makes \f$ \check\gamma \f$ an antichain on the
  //! subtree \f$ S_u \f$. 
  //!
  //! There are two main user patterns:
  //! -# For gene node, u, get gamma-path induced by \f$ x: u\in\gamma(x) \f$
  //! -# for species node, x, get list of gene nodes \f$ \gamma(x) \f$
  //
  //! Author: Lars Arvestad, SBC, � the MCMC-club, SBC, all rights reserved
  //-----------------------------------------------------------------
  class GammaMap
  {
  // quick hack
  friend class ReconciledTreeMCMC;
  public:
    //-----------------------------------------------------------------
    //
    //! @name Construct/Destruct/Assign
    //
    //-----------------------------------------------------------------
    //@{
    GammaMap(Tree &G, Tree &S, const LambdaMap& L, 
	     std::vector<SetOfNodes> &AC_info);
    GammaMap(Tree &G, Tree &S, const LambdaMap& L);

    GammaMap(Tree &G, Tree &S, const StrStrMap& gs, 
	     std::vector<SetOfNodes> &AC_info);
    GammaMap(Tree &G, Tree &S, const StrStrMap& gs);

    GammaMap(const GammaMap& original);	// Copy constructor

    //! Here is a so called 'named constructor' for initing gamma_star:
    static GammaMap MostParsimonious(Tree &G, Tree &S, LambdaMap &L); 
    static GammaMap MostParsimonious(Tree &G, Tree &S, StrStrMap& gs); 

    ~GammaMap();
    GammaMap& operator=(const GammaMap &gm);
    //@}

    //---------------------------------------------------------------
    //
    // Interface
    //
    //---------------------------------------------------------------

    //---------------------------------------------------------------
    // Access
    //---------------------------------------------------------------

    //! @name Access to trees
    //! Get references to the trees
    //---------------------------------------------------------------
    //@{
    Tree& getS();
    Tree& getG();
    //@}

    //! Check validity of gamma
    bool valid() const;

    //! Check if there is a gamma to work with
    bool empty() const;

     //---------------------------------------------------------------
    // gamma-paths are placed on gene tree nodes, which means that
    // either the gene node is a speciation, or the speciation occurs on
    // the edge to the node's parent. 
    //-----------------------------------------------------------------

    //! @name Size of \f$ \gamma(x) \f$
    //! Get the size of gamma(x), i.e., the number of GeneNodes mapped to x.
    //-----------------------------------------------------------------
    //@{
    unsigned getSize(Node& x) const;   //version with reference
    unsigned getSize(Node* x) const;   //version with pointer
    //@}

    //! Used in primetv \todo{Document better!}
    // Uses helper findWidest below
    unsigned sizeOfWidestSpeciesLeaf() const; 

    //! Count the gamma-paths, i.e., \f$ \{x: u\in\gamma(x)\} \f$, 
    //! on a gene node
    //-----------------------------------------------------------------
    unsigned numberOfGammaPaths(Node &u) const;

    //! @name Access to gamma-paths
    //! The following methods retrieves the max-element, according
    //! to porder, of the gamma-path on u, i.e., \f$ \{x: u\in\gamma(x)\} \f$,
    //! Null is returned if there is no anti-chain on u.
    //-----------------------------------------------------------------
    //@{
    Node* getHighestGammaPath(Node &u) const; //! Dominates all others on u
    Node* getLowestGammaPath(Node &u) const;  //! Dominated by others
    //@}

    //! Test to see if a given gene tree node u, or a "virtual" unary node on 
    //! the parental edge to u, is in gamma(x) for some species tree node x.
    //-----------------------------------------------------------------
    bool isInGamma(Node *u, Node* x) const;

    //! Test to see if a given gene tree node u, or a "virtual" unary node on 
    //! the parental edge to u, is in gamma(x) for some species tree node x.
    //-----------------------------------------------------------------
    bool isSpeciationInGamma(Node *u, Node* x) const;

    //! We determine whether a node u is a speciation or not by looking if
    //! there is an anti-chain for x on u, and if lambda(u) == x.
    //-----------------------------------------------------------------
    bool isSpeciation(Node &u) const;

    /// Return the set of guest nodes, \f$ \check\gamma(x) \f$, on host node x
    SetOfNodes getGamma(Node *x) const;
    //! Orthology analysis: Return a list of id pairs for orthologous nodes.
    //-----------------------------------------------------------------
    std::multimap<int, int> getOrthology() const;


    //! Given that x is on u, what lineage is u in? 
    //! (1) u could be a speciation corresponding to u. In this case, x
    //!     is returned.
    //! (2) u is a duplication, and is found in found in a lineage y,
    //!     where y is a child of x. Return y!
    //!  This is computed by looking at z = lambda(u) and tracing up until
    //!  z is a child of x. One could probably consider storing the
    //!  lineage information for u, but I don't want to add that code
    //!  right now. This is a more immediate solution.
    //-----------------------------------------------------------------
    Node* getLineage(Node* x, Node &u) const;

    //! Find which child of speciesnode x dominates genenode u
    //-----------------------------------------------------------------
    Node& getDominatingChild(Node& x, Node& u) const 
    {
      return *x.getDominatingChild(lambda[&u]);
    };
    Node* getDominatingChild(Node* x, Node* u) const 
    {
      return x->getDominatingChild(lambda[u]);
    };

    //! \todo{please document twistAndTurn}
    void twistAndTurn();
    void twistAndTurn(Node *v, Node *x);

    //-----------------------------------------------------------------
    // Manipulation
    //-----------------------------------------------------------------

    //! Resets gamma to and empty gamma
    //-----------------------------------------------------------------
    void reset();


    //! Make a change in the gamma chain
    //! \todo{This should probably be moved to a separate class! /bens}
    //-----------------------------------------------------------------
    void perturbation(GammaMap &UpperBound); 

    //! \name
    //! Puts u in gamma(x), and registers x as having an anti-chain for x.
    //! Would like to make x argument const below, but then I am not allowed 
    //! to put x in a dequeue!
    //-----------------------------------------------------------------
    //@{
    void addToSet(Node *x, Node *u);
    void addToSet(Node *x, Node &u); 
    //@}

    //! helper function for getorthology()
    std::list<Node*> getOrthology(Node* v, 
				  std::multimap<int, int> &orthology) const;
    //-----------------------------------------------------------------
    //! \name I/O
    //-----------------------------------------------------------------
    //@{
  public:
    friend std::ostream& operator<< (std::ostream& o, const GammaMap &gamma);
    std::string print(const bool& full=false) const;
    //@}

  private:
    //-----------------------------------------------------------------
    //
    // Implementation
    //
    //-----------------------------------------------------------------
    //! Recursive function for reading a user-set gamma in tabular format
    //! todo{If chainsOnNode is changed to a set, this function is
    //! unnecessary / bens}
    void readGamma(Node* sn, std::vector<SetOfNodes>& AC_info);

    //! Check that current gamma is valid, throws AnError if not!
    //! Precondition: ChainsOnNode is correctly sorted!
    Node* checkGamma(Node *gn);
    Node *checkGammaForDuplication(Node *gn, Node *sn, Node *sl, Node *sr);
    Node *checkGammaForSpeciation(Node *gn, Node *sn, Node *sl, Node *sr);
    Node *checkGammaMembership(Node *gn, Node *sn);


    //! \todo{This might now be redundant because of chckGamma above
    //! Used in primetv - change code there and remove this}
    bool valid(Node *x) const;

    //! Used in primetv
    unsigned sizeOfWidestSpeciesLeaf(Node *x, unsigned cur_max) const;


    //! Utility function
    void removeFromSet(Node *x, Node *v);


    // Using R, get root of subtree containing a random anti-chain.
    //! \todo{This should probably be moved to a separate class! /bens}
    //-----------------------------------------------------------------
    void getRandomSubtree(GammaMap &gamma_star, Node **u, Node **x);

    //! \name Get counts of antichains - more details? /bens
    //! \todo{This should probably be moved to a separate class?? /bens}
    //-----------------------------------------------------------------
    //@{
    unsigned  countAntiChainsUpper(Node &u, Node *x, std::vector<int> &N);
    unsigned  countAntiChainsLower(Node &u, Node *x, std::vector<int> &N);
    //@}

    //! \name
    //! Make the actual changes: \todo{improve documentation}
    //! \todo{This should perhaps(?) be moved to a separate class?? /bens}
    //-----------------------------------------------------------------
    //@{
    void makeGammaChangeAbove(Node &u, Node *x,   // The entry point
			      std::vector<int> &N, unsigned new_position);           
    void makeGammaChangeBelow(Node &u, Node *x,   // Used by the "entry point"
			      std::vector<int> &N, unsigned new_position); 
    void removeOldAntiChain(Node *u, Node *x);	  // Also used by entry point
    //@}

    //-----------------------------------------------------------------
    //! \name Comparison
    //-----------------------------------------------------------------
    //@{
    friend bool operator==(const GammaMap &gm1, const GammaMap &gm2)
    {
      return (gm1.gamma == gm2.gamma);
    }

    friend bool operator<(const GammaMap &gm1, const GammaMap &gm2)
    {
      return (gm1.gamma < gm2.gamma);
    }
    //@}

    //! GammaMap::gamma stores the 'reduced reconciliation', 
    //! \f$ \check\gamma \f$. This function converts returns the full
    //! reconciliation \f$ \gamma(x) \f$

    SetOfNodes getFullGamma(const Node& x) const;

    //! \name Methods documented in doc/gamma_alg.txt. used by 
    //! GammaMap::MostParsimonious(),
    //! \todo{documnet better}
    //-----------------------------------------------------------------
    //@{
    void computeGammaBound(Node *g);
    void computeGammaBoundBelow(Node *g);
    void assignGammaBound(Node *u, Node *x);
    //@}

  private:			
    //---------------------------------------------------------------
    //
    // ATTRIBUTES
    //
    //---------------------------------------------------------------

    //---------------------------------------------------------------
    // Resources available outside this class
    //---------------------------------------------------------------
    Tree* Gtree; //! \todo{This should probably better be a pointer}
    Tree* Stree; //! \todo{This should probably better be a pointer}
    LambdaMap lambda;
  
    //---------------------------------------------------------------
    // Actual gamma map attributes
    //---------------------------------------------------------------


    // The actual gamma map, species node --> anti-chain:
    //  vector<set<Node*> > gamma;	
    //---------------------------------------------------------------
    std::vector<SetOfNodes> gamma;
    //This could possibly be a BeepVector, e.g., GammaMap could inherit
    //from SetOfNodesVector = BeepVector<SetOfNodes>

    // For mapping gene nodes to anti-chains / species-nodes:
    // The front of the deque is the lowest anti-chain on that node,
    // while the back is the highest.
    // This datastructure is more or less a substitute for explicitly represent
    // speciation events in the gene tree using unary nodes.
    //! \todo{This could definitely be a set instead /bens}
    //---------------------------------------------------------------
    std::vector<std::deque<Node*> > chainsOnNode; 

  };

}// end namespace beep


#endif
