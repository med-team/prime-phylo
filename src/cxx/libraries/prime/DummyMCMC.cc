#include "DummyMCMC.hh"

#include "AnError.hh"
#include "MCMCObject.hh"

#include <sstream>

//----------------------------------------------------------------------
//
// class DummyMCMC
// subclass of MCMCModel that implements a MCMCModel interface
// that works as an endpoint in a chain of priors/MCMCModel
//
// Since numberOfStates returns 0, suggestState should never be called
//
//  Author Bengt Sennblad, 
//  copyright the MCMC club, SBC
//
//----------------------------------------------------------------------


namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Constructor/Destructor
  //
  //----------------------------------------------------------------------
  DummyMCMC::DummyMCMC()
    : MCMCModel()
  {};
  
  DummyMCMC::DummyMCMC(const DummyMCMC& crm)
    : MCMCModel(crm)
  {};
  
  DummyMCMC::~DummyMCMC()
  {};
  
  DummyMCMC& 
  DummyMCMC::operator=(const DummyMCMC& crm)
  {
    return *this;
  };

    //
    // Named constructor for easy instantiation
    //
  DummyMCMC DummyMCMC::TheEmptyMCMCModel()
    {
      DummyMCMC d;
      return d;
    }

  // Deprecated! Do not use! Will be phased out! Only for back-compatibility!
  //----------------------------------------------------------------------
  DummyMCMC::DummyMCMC(PRNG& R)
    :MCMCModel()
  {};

  // Deprecated! Do not use! Will be phased out! Only for back-compatibility!
  //----------------------------------------------------------------------
DummyMCMC DummyMCMC::TheEmptyMCMCModel(PRNG& R)
  {
    return DummyMCMC();
  };

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------
  
  // Inherited from MCMCModel
  // Should never be called
  //----------------------------------------------------------------------
  MCMCObject  
  DummyMCMC::suggestNewState()
  {
    throw AnError("DummyMCMC::suggestState():\n"
		  "We should never go here!\n", 1);
    return MCMCObject(1.0, 1.0);
  }; 
 
  MCMCObject
  DummyMCMC::suggestNewState(unsigned x)
  {
    throw AnError("DummyMCMC::suggestState():\n"
		  "We should never go here!\n", 1);
    return MCMCObject(1.0, 1.0);
  };  
  
  Probability  
  DummyMCMC::initStateProb()
  {
    return 1.0;
  };  

  Probability  
  DummyMCMC::currentStateProb()
  {
    return 1.0;
  };  

  void  
  DummyMCMC::commitNewState()
  {
    throw AnError("DummyMCMC::commitState():\n"
		  "We should never go here!\n", 1);
    return;
  }  

  void  
  DummyMCMC::commitNewState(unsigned x)
  {
    throw AnError("DummyMCMC::commitState():\n"
		  "We should never go here!\n", 1);
    return;
  }  

  void  
  DummyMCMC::discardNewState()
  {
    throw AnError("DummyMCMC::discardState():\n"
		  "We should never go here!\n", 1);
    return;
  }  

  void  
  DummyMCMC::discardNewState(unsigned x)
  {
    throw AnError("DummyMCMC::discardState():\n"
		  "We should never go here!\n", 1);
    return;
  }  

  std::string  
  DummyMCMC::strRepresentation() const
  {
    std::string s = "";
    return s;
  }

  std::string  
  DummyMCMC::strHeader() const
  {
    std::string s = "";
    return s;
  }

  std::string
  DummyMCMC::getAcceptanceInfo() const
  {
	  return "";
  }

  // Return number of states in prior (probably only used first time!)
  // This will always be 0 
  //----------------------------------------------------------------------
  unsigned  
  DummyMCMC::nParams() const
  {
    return 0;
  }
  ;
  //----------------------------------------------------------------------
  //
  // I/O
  //
  //----------------------------------------------------------------------
    
  // Always define an ostream operator!
  // This should provide a neat output describing the model and its 
  // current settings for output to the user. It should list current 
  // parameter values by linking to their respective ostream operator
  //------------------------------------------------------------------
  ostream& 
  operator<<(ostream &o, const DummyMCMC& dm) 
  {
    return o << dm.print()
      ;
  };
    

  string  
  DummyMCMC::print() const
  {
    return std::string("No prior\n");
  }
}//end namespace beep

