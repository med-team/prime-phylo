#ifndef SPECIESTREEMCMC_HH
#define SPECIESTREEMCMC_HH

#include <string>
#include <ostream>

#include "StdMCMCModel.hh"
#include "Tree.hh"

namespace beep
{
  //! \todo{Rename this class!!!!
  //! This actually represent a rather abstract variable representing a
  //! time preceeding than the first branching point in the guest tree
  //! It is definitely not related to the host tree (except that it must
  //! precede - or equal  - its root). Thus the class should be renamed
  //! and the variable should not be stored in S's root time. /bens}

  class SpeciesTreeMCMC : public StdMCMCModel
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //-------------------------------------------------------------
    SpeciesTreeMCMC(MCMCModel& prior, Tree& S, const Real beta);
    ~SpeciesTreeMCMC();

    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();
    std::string ownStrRep() const;
    std::string ownHeader() const;
    Probability updateDataProbability();

    //-------------------------------------------------------------
    // Access
    //-------------------------------------------------------------
    Tree& getTree() const;

    //-------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const SpeciesTreeMCMC& A);
    std::string print() const;

  private:
//     //-------------------------------------------------------------
//     //
//     // Implementation
//     //
//     //-------------------------------------------------------------
//     // Utility function for changing birth/death rates.
//     // Returns a number in the interval [0.8, 1.25].
//     //----------------------------------------------------------------
//     Real rateChange(); //! \todo{not used?}
//     Probability calcRootTimeProb();

    //-------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------
    Tree& S;
    Real beta;
    Real oldRootTime;

    const Real suggestion_variance;
  };}
  //end namespace beep

#endif
