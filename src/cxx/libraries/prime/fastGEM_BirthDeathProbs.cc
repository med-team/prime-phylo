#include <cassert>
#include <cmath>
#include <sstream>

#include "fastGEM_BirthDeathProbs.hh"

using namespace std;

namespace beep
{
  class Tree;

  //---------------------------------------------------------------
  // 
  // Construct/Destruct
  //
  //---------------------------------------------------------------
  fastGEM_BirthDeathProbs::fastGEM_BirthDeathProbs(Tree& S, unsigned noOfDiscrPoints, std::vector<double>* discrPoints, const Real& birthRate, const Real& deathRate = 0.0)
    : BirthDeathProbs(S,birthRate,deathRate),
      noOfDiscrPoints(noOfDiscrPoints),
      discrPoints(discrPoints),
      P11dup(S.getNumberOfNodes()+1,noOfDiscrPoints),
      P11spec(S.getNumberOfNodes()+1),
      loss(S.getNumberOfNodes()+1),
      timeStep(2.0/noOfDiscrPoints),
      pxTimes(S.getNumberOfNodes()+1,noOfDiscrPoints)
  { 
    for (unsigned discrIndex = 0; discrIndex <=  noOfDiscrPoints; discrIndex++)
      {
	discrPoints->push_back(discrIndex * timeStep);
      }
    
    fillPxTimeTable();
   
    for (unsigned Sindex = 0; Sindex <= S.getNumberOfNodes()-1; Sindex++)
      {
	for (unsigned xIndex = 0; xIndex <= (noOfDiscrPoints-1); xIndex++)
	  {
	    setP11dupValue(Sindex,xIndex,0.0);
	  }
	setP11specValue(Sindex,0.0);

	Probability lossValue = BD_zero[Sindex];
	setLossValue(Sindex,lossValue);
      }

  }

  //virtual 
  //---------------------------------------------------------------
  fastGEM_BirthDeathProbs::~fastGEM_BirthDeathProbs()
  {}

  //---------------------------------------------------------------------
  //
  // Interface
  //
  //---------------------------------------------------------------------
  void
  fastGEM_BirthDeathProbs::update()
  {
    BirthDeathProbs::update();

    fillPxTimeTable();
    
    for (unsigned Sindex = 0; Sindex <= S.getNumberOfNodes()-1; Sindex++)
      {
	for (unsigned xIndex = 0; xIndex <= (noOfDiscrPoints-1); xIndex++)
	  {
	    setP11dupValue(Sindex,xIndex,0.0);
	  }
	setP11specValue(Sindex,0.0);
	
	Probability lossValue = BD_zero[Sindex];
	setLossValue(Sindex,lossValue);
      }
   }
  //----------------------------------------------------------------------
  // calcP11() In this version of the function P11(x,y)-values are calculated only for x,p(x)...   
  //----------------------------------------------------------------------
  void 
  fastGEM_BirthDeathProbs::calcP11()
    {
      //cerr << "start fastGEM_BirthDeathProbs::calcP11 version3.0\n";
      calcBirthDeathProbs(*S.getRootNode());

      for (unsigned Sindex = 0; Sindex <= S.getNumberOfNodes()-1; Sindex++)
	{
	  Node* Snode = S.getNode(Sindex);
	  Real SnodeTime = Snode->getNodeTime();

	  Node* Sparent;
	  Real SparentNodeTime;

	  if (Snode->isRoot())
	    {
	      SparentNodeTime = 2.0;
	    }
	  else
	    {
	      Sparent = Snode->getParent();
	      SparentNodeTime = Sparent->getNodeTime();
	    }

	  Real xTime = SnodeTime;
	  Real pxTime = getPxTime(Sindex, 0); 
	  //cerr << "pxSpecTime: " << pxTime << " xTime: " << xTime << "\n";
	      
	  Probability P11item = calcP11item(pxTime,xTime,*Snode);
	  //cerr << "SnodeTime: " << SnodeTime << " SparentNodeTime: " << SparentNodeTime << " xTime: " << xTime << " pxTime : " << pxTime << " P11item: " << P11item << "\n";		  
	  setP11specValue(Sindex,P11item);	      
	  	  
	  for (unsigned xIndex = 1; xIndex <= noOfDiscrPoints-1; xIndex++)
	    //	  for (unsigned xIndex = 0; xIndex <= noOfDiscrPoints-1; xIndex++)
	    {
	      xTime = xIndex * timeStep; 
	      if ((xTime >= SnodeTime) && (xTime < SparentNodeTime))
		{

		  //unsigned pxIndex = xIndex + 1;
		  //pxTime = pxIndex * timeStep;
		  Real pxTime = getPxTime(Sindex,xIndex); 
		  //cerr << "pxDupTime: " << pxTime << " xTime: " << xTime << "\n";

		  P11item = calcP11item(pxTime,xTime,*Snode);		  
		  //cerr << "SnodeTime: " << SnodeTime << " SparentNodeTime: " << SparentNodeTime << " xTime: " << xTime << " pxTime : " << pxTime << " P11item: " << P11item << "\n";
		  setP11dupValue(Sindex,xIndex,P11item);
	      
		  xTime = pxTime;
		}
	    }
	}
      //cerr << "end fastGEM_BirthDeathProbs::calcP11 version3.0\n";
    }

  //----------------------------------------------------------------------
  // get P11Values()
  //----------------------------------------------------------------------
  Probability 
  fastGEM_BirthDeathProbs::getP11dupValue(unsigned Sindex,unsigned xIndex)
  {
    return P11dup(Sindex,xIndex);
  }

  Probability 
  fastGEM_BirthDeathProbs::getP11specValue(unsigned Sindex)
  {
    return P11spec.at(Sindex);
  }

  Probability 
  fastGEM_BirthDeathProbs::getLossValue(unsigned Sindex)
  {
    return loss.at(Sindex);
  }

  Real fastGEM_BirthDeathProbs::getPxTime(unsigned Sindex,unsigned xIndex)
  {
    return pxTimes(Sindex,xIndex);    
  }

  std::vector<double>* fastGEM_BirthDeathProbs::getDiscrPoints()
  {
    return discrPoints;
  }

  //---------------------------------------------------------------------
  // I/O
  //---------------------------------------------------------------------
  
  //---------------------------------------------------------------------
  //
  // Implementation
  //
  //---------------------------------------------------------------------

  Probability
  fastGEM_BirthDeathProbs::calcP11item(const Real pxTime, const Real xTime, Node &Snode) const
  {
    assert(pxTime > xTime);

    Real t = pxTime - xTime;
    Probability Pt;
    Probability Ut;
    calcPt_Ut(t, Pt, Ut);    

    Probability D;
    Probability tmp;
    Probability Q;

    Real SnodeTime = Snode.getNodeTime();
    if (SnodeTime == xTime) // x is a speciation point -> Q == BD_const[y] but for time t
      {
	if (Snode.isLeaf() == true)
	  {
	    Q = Pt * (1.0 - Ut);
	  }
	else
	  {
	    Node &left = *(Snode.getLeftChild());
	    Node &right = *(Snode.getRightChild());
	    
	    D = BD_zero[left] * BD_zero[right]; // i.e. XV
	    tmp = 1.0 - (Ut * D);
	    Q = Pt * (1.0 - Ut) / (tmp * tmp);
	  }
      }
    else
      {
	Real tXA = xTime - SnodeTime;
	Probability PtXA;
	Probability UtXA;
	calcPt_Ut(tXA, PtXA, UtXA);    

	if (Snode.isLeaf() == true)
	  {
	    D = 1.0 - PtXA; // i.e. XA for XV == 0
	  }
	else
	  {
	    Node &left = *(Snode.getLeftChild());
	    Node &right = *(Snode.getRightChild());
	    
	    Probability XV = BD_zero[left] * BD_zero[right];
	    D = 1.0 - (PtXA * (1.0 - XV) / (1.0 - UtXA * XV)); // i.e. XA
	  }
	      
	tmp = 1.0 - (Ut * D);
	Q = Pt * (1.0 - Ut) / (tmp * tmp);
      }    

    return Q;
  }

  void 
  fastGEM_BirthDeathProbs::setP11dupValue(unsigned Sindex,unsigned xIndex, Probability p)
  {
    P11dup(Sindex,xIndex) = p;
  }

  void 
  fastGEM_BirthDeathProbs::setP11specValue(unsigned Sindex, Probability p)
  {
    P11spec.at(Sindex) = p;
  }

  void 
  fastGEM_BirthDeathProbs::setLossValue(unsigned Sindex, Probability p)
  {
    loss.at(Sindex) = p;
  }

  void fastGEM_BirthDeathProbs::fillPxTimeTable()
  {
    for (unsigned Sindex = 0; Sindex <= S.getNumberOfNodes()-1; Sindex++)
      {
	Node* Snode = S.getNode(Sindex);
	Real SnodeTime = Snode->getNodeTime();
	Node* Sparent;
	Real SparentNodeTime;

	if (Snode->isRoot())
	  {
	    SparentNodeTime = 2.0;
	  }
	else
	  {
	    Sparent = Snode->getParent();	
	    SparentNodeTime = Sparent->getNodeTime();
	  }

	Real pxTime = 0.0;
	Real xTime = 0.0;
	for (unsigned xIndex = 0; xIndex <= (noOfDiscrPoints-1); xIndex++)
	  {
	    if (xIndex == 0)
	      {
		for (unsigned xSearch = 1; xSearch <= (noOfDiscrPoints-1); xSearch++)
		  {
		    Real eps = 0.0001;
		    if ((*discrPoints).at(xSearch) > (SnodeTime + eps))
		      {
			pxTime = (*discrPoints).at(xSearch);
			xTime = SnodeTime;
			break;
		      }
		  }
	      }
	    else
	      {
		pxTime = (*discrPoints).at(xIndex+1);
		xTime = (*discrPoints).at(xIndex);
	      }
	    if (pxTime > SparentNodeTime)
	      {
		pxTime = SparentNodeTime;
	      }
	    
	    if ((xTime < SnodeTime) || (pxTime < xTime))
	      {
		pxTime = -1;
	      }
	    //cerr << "Sindex: " << Sindex << " SnodeTime: " << SnodeTime << " xIndex: " << xIndex << " xTime: " << xTime << " pxTime: " << pxTime << "\n";
	    pxTimes(Sindex,xIndex) = pxTime;
	  }
      }
  }
}//end namespace beep
