#include "NormalDensity.hh"

#include <assert.h>
#include "AnError.hh"
#include "DiscreteGamma.hh"
#include "Probability.hh"

#include <cmath>
#include <sstream>

//----------------------------------------------------------------------
//
// Class NormalDensity
// implements the Normal density distribution
//
// Note! alpha = mean and beta = variance of distribution
//
// Author: Bengt Sennblad, copyright the MCMC-club, SBC
//
//----------------------------------------------------------------------

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Formal defs of static const members
  // I haven't found where this is stated in the standard, but
  // apparently we now need to do this? /bens
  //
  //----------------------------------------------------------------------
  const double NormalDensity::pi;

  //----------------------------------------------------------------------
  //
  // Construct/Destruct
  //
  //----------------------------------------------------------------------
  NormalDensity::NormalDensity(Real mean, Real variance, bool embedded)
    : Density2P_common(mean, variance, "LogNorm"),
      c()
  {
    if(embedded)
      {
	setEmbeddedParameters(mean, variance);
      }
    else
      {
	setParameters(mean, variance);
      }
  };

  NormalDensity::NormalDensity(const NormalDensity& df)
    : Density2P_common(df),
      c(df.c)
  {
  };

  NormalDensity::~NormalDensity() 
  {};

  NormalDensity& 
  NormalDensity::operator=(const NormalDensity& df)
  {
    if(&df != this)
      {
	Density2P_common::operator=(df);
	c = df.c;
      }
    return *this;
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------

  // Density-, distribution-, and sampling functions
  //----------------------------------------------------------------------
  Probability 
  NormalDensity::operator()(const Real& x) const
  { 
    return pdf(x);
//     return Probability::setLogProb( -0.5 * std::pow(x - alpha, 2)/beta
// 					+ c, 1);
 }

  Probability 
  NormalDensity::pdf(const Real& x) const
  {
    return Probability::setLogProb( -0.5 * std::pow(x - alpha, 2)/beta
					+ c, 1);
  }

  Probability
  NormalDensity::cdf(const Real& y) const
  {
	  if (y < 1e-100)     { return Probability(0.0); }
	  else if (y > 1e100) { return Probability(1.0); }

	  Real x = (y - alpha) / beta;

	  const Real b1 =  0.319381530;
	  const Real b2 = -0.356563782;
	  const Real b3 =  1.781477937;
	  const Real b4 = -1.821255978;
	  const Real b5 =  1.330274429;
	  const Real p  =  0.2316419;
	  const Real c  =  0.39894228;

	  if (x >= 0.0)
	  {
		  Real t = 1.0 / ( 1.0 + p * x );
		  return (1.0 - c * exp( -x * x / 2.0 ) * t *
				  ( t *( t * ( t * ( t * b5 + b4 ) + b3 ) + b2 ) + b1 ));
	  }
	  else
	  {
		  Real t = 1.0 / ( 1.0 - p * x );
		  return ( c * exp( -x * x / 2.0 ) * t *
				  ( t *( t * ( t * ( t * b5 + b4 ) + b3 ) + b2 ) + b1 ));
	  }
  }

  Real
  NormalDensity::sampleValue(const Real& p) const
  {
    assert(0.0 < p && p < 1.0); // check precondition

    // joelgs: Below is erroneous. Changing to from var. to std. dev.
    //return gauinv(p) * beta + alpha;
    return (gauinv(p) * std::sqrt(beta) + alpha);
  }

  Real
  NormalDensity::getTruncatedMean(const Real& x) const
  {
    return (alpha - beta * pdf(x) / cdf(x)).val();
  }


  // Access parameters
  //----------------------------------------------------------------------
  Real
  NormalDensity::getMean() const 
  {
    return alpha;
  }
    
  Real
  NormalDensity::getVariance() const 
  {
    return beta;
  }
    
  // Set Parameters
  //----------------------------------------------------------------------
  void 
  NormalDensity::setParameters(const Real& mean, const Real& variance)
  {
    assert(isInRange(mean) && isInRange(variance)); // check precondition

    alpha = mean;
    beta = variance;
    c = -0.5 * std::log(2 * pi * beta); // \log(\sqrt{2\pi\beta})

    return;
  }
    
  void 
  NormalDensity::setMean(const Real& mean)
  {
    assert(isInRange(mean)); // check precondition

    alpha = mean;

    return;
  }
    
  void 
  NormalDensity::setVariance(const Real& variance)
  {
    assert(isInRange(variance)); // cheak precondition

    beta = variance;
    c = -0.5 * std::log(2 * pi * beta); // \log(\sqrt{2\pi\beta})

    return;
  }


  //------------------------------------------------------------------
  // I/O
  //------------------------------------------------------------------
  std::string
  NormalDensity::print() const
  {
    std::ostringstream oss;
    oss << "Normal distribution N("
	<< alpha
	<< ", "
	<< beta
	<< ")\n" 
      ;
    return oss.str();
  }

}//end namespace beep

