#include "BDTreeGenerator.hh"

#include "AnError.hh"
#include "GammaMap.hh"
#include "LambdaMap.hh"
#include "PRNG.hh"
#include "Tree.hh"

#include <cmath>

namespace beep
{
  using namespace std;
  //-------------------------------------------------------------------
  //
  // Construct/destruct
  //
  //-------------------------------------------------------------------
  BDTreeGenerator::BDTreeGenerator(Tree& S, Real birthRate, Real deathRate)
    : TreeGenerator(),
      lambda(birthRate),
      mu(deathRate),
      toptime(S.getTopTime()),
      S(&S),
      G(0),
      times(),
      index(S), 
      gs(),
      gamma(S.getNumberOfNodes())
  {}
  
  BDTreeGenerator::~BDTreeGenerator()
  {};

  //-------------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------------
  void 
  BDTreeGenerator::setTopTime(Real time)
  {
    toptime = time;
  }

  Real 
  BDTreeGenerator::getTopTime()
  {
    return toptime;
  }

  bool 
  BDTreeGenerator::generateTree(Tree& G_in, bool noTopTime)
  {
    Real useTime = toptime;
//     if(noTopTime)
//       {
// 	useTime = S.rootToLeafTime();
//       }
    
    if (useTime <= 0.0) {
      throw AnError("The host tree has no 'top time', i.e., does not allow duplications above the root of the species tree.", 1);
    }
    G = &G_in;
    assert(G != 0);
    G->clear();
    index.clearValues();
    times.clear();
    gs.clearMap();
    gamma = std::vector<SetOfNodes>(S->getNumberOfNodes());
    
    Node* r = generateX(S->getRootNode(), useTime);
    if(r != NULL)
      {
	if(noTopTime)
	  {
	    Node* r2 = generateX(S->getRootNode(), useTime);
	    if(r2 != NULL)
	      {    
		r = G->addNode(r,r2);
		assert(r != NULL);
		assert(times.find(r) == times.end());
		times[r] = useTime;	    
	      }
	    else
	      {
		G->clear();
		return false;
	      }
	  }
	G->setRootNode(r);
	assert(G->getRootNode() != NULL);
	RealVector* tmp = new RealVector(*G);
	
	for(unsigned i = 0; i < G->getNumberOfNodes(); i++)
	  {
	    Node* u = G->getNode(i);
	    (*tmp)[u] = times[u];
	  }
	G->setTimes(*tmp, true);
	G->setTopTime(toptime + S->rootToLeafTime() 
		      - G->getTime(*G->getRootNode()));
	return true;
      }
    else
      {
	G->clear();
	return false;
      }
  }	 
  StrStrMap 
  BDTreeGenerator::exportGS()
  {
    if(gs.size() == 0)
      {
	throw AnError("No gs has been generated to return");
      }
    return gs;
  }

  GammaMap 
  BDTreeGenerator::exportGamma()
  {
    if(gamma.empty())
      {
	throw AnError("No gamma has been generated to return");
      }
    GammaMap tmpGamma(*G, *S, BDTreeGenerator::exportGS());
    createTrueGamma(tmpGamma);
    return tmpGamma;
  }
     
  //-------------------------------------------------------------------
  // I/O
  //-------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, const BDTreeGenerator& BDG)
  {
    return o << "Output is not yet implemented in BDTreeGenerator\n";
  }

  //-------------------------------------------------------------------
  //
  // Implementation
  //
  //-------------------------------------------------------------------
  Node* 
  BDTreeGenerator::generateV(Node* x)
  {
    assert(x !=0);
    if(x->isLeaf())
      {
	Node* u =G->addNode(0,0);
	assert(times.find(u) == times.end());
	times[u] = 0;
	
	//get unique name
	std::ostringstream oss;
	oss << x->getName() <<  "_" << index[x];
	u->setName(oss.str());
	index[x]++;
	gs.insert(u->getName(),x->getName());
	gamma[x->getNumber()].insert(u);
	return u;
      }
    else
      {
	Node* y = x->getLeftChild();
	Node* z = x->getRightChild();
	Node* left = generateX(y, y->getTime());
	Node* right = generateX(z, z->getTime());
	
	if(left)
	  {
	    if(right)
	      {
		Node* u =  G->addNode(left, right);
		assert(times.find(u) == times.end());
		times[u] = S->getTime(*x);
		gamma[x->getNumber()].insert(u);		
		return u;
	      }
	    else
	      {
		gamma[x->getNumber()].insert(left);
		return left;
	      }
	  }
	else if(right)
	  {
	    gamma[x->getNumber()].insert(right);
	    return right;
	  }
	else
	  {
	    return 0;
	  }
      }
  }

  Node* 
  BDTreeGenerator::generateX(Node* x, Real maxT)
  {
    assert(x !=0);
    assert(maxT >= 0);

    Real p = rand.genrand_real3(); // Actually 1-p, but they have same distr
    
    Real t = -std::log(p)/(lambda+mu);
    if(t < 0)
      {
	throw AnError("BDTreeGenerator::generateX\n"
		      "negative time generated\n", 1);
      }
    if(t >= maxT)
      {
	return generateV(x);
      }
    else
      {
	p=rand.genrand_real3();
	if(p > lambda/(lambda+mu))
	  {
	    return 0;
	  }
	else
	  {
	    Node* left = generateX(x, maxT-t);
	    Node* right = generateX(x, maxT-t);
	
	    if(left)
	      {
		if(right)
		  {
		    Node* u = G->addNode(left, right);
		    assert(times.find(u) == times.end());
		    times[u] = S->getTime(*x) + maxT-t;
		    return u;
		  }
		else
		  {
		    return left;
		  }
	      }
	    else if(right)
	      {
		return right;
	      }
	    else
	      {
		return 0;
	      }
	  }
      }
  }

  // createTrueGamma
  // reads gamma and converts to a GammaMap stored in tmpGamma
  //---------------------------------------------------------------------
  void 
  BDTreeGenerator::createTrueGamma(GammaMap& tmpGamma) const
  {
    for(unsigned i = 0; i < gamma.size(); i++)
      {
	Node* sc = S->getNode(i);
	for(unsigned j = 0; j < gamma[i].size(); j++)
	  {
	    Node* gc = gamma[i][j];
	    tmpGamma.addToSet(sc, gc);
	  }
      }
    return;
  }

}//end namespace beep

