//**********************************************************************************************
// FOR REFERENCE, THIS CLASS CONTAINS A SNAPSHOT OF PETER/MATTIAS' EXTENDED GSR IMPLEMENTATION
// AS OF 2010-06-10.
// NO COMPATILIBILITY TESTS HAVE BEEN MADE, I.E. EXPECT COMPILATION ERRORS TO START WITH.
// HOWEVER, IT SHOULD EASE THE TRANSITION TO A MORE COHERENT STRUCTURE. NOTE THAT PETER/MATTIAS'
// EXTENDED VERSION MAY EXIST IN A MORE RECENT STATE IN A SEPARATE SVN-BRANCH.
//**********************************************************************************************

///* Estimates which discretization points that are likely to correspond to
// * a large scale duplication. This done by calculating the following sum
// * for each discretization point, and returning the discretization points
// * that maximizes the sum.
// *
// * d(x) = 1/n*\sum_i=1^n Pr[G, l_i, \theta, u on x] / Pr [ G, l_i, \theta ]
// *
// * File:   ApproximateEstimator.hh
// * Author: fmattias
// *
// * Created on November 23, 2009, 2:39 PM
// */
//
//#ifndef _APPROXIMATEESTIMATOR_HH
//#define	_APPROXIMATEESTIMATOR_HH
//
//#include <ostream>
//
//#include "lsd/LSDEstimator.hh"
//#include "mcmc/PosteriorSampler.hh"
//#include "EdgeDiscPtMapIterator.hh"
//#include "io/LSDEstimatesFormat.hxx"
//
//namespace beep {
//
//class ApproximateEstimator : public LSDEstimator {
//public:
//    /**
//     * Constructor
//     *
//     * speciesTree - Discretized species tree, the discretization points will
//     *               be evaluated for their potential to correspond to a large
//     *               scale duplication.
//     * mcmc - An MCMC instance for integrating out the branch lengths.
//     * burnInIterations - The number of burn in iterations.
//     * fixedIterations - The number of iterations when holding G fixed.
//     */
//    ApproximateEstimator(EdgeDiscTree &speciesTree,
//                         PosteriorSampler &mcmc,
//                         int burnInIterations,
//                         int fixedIterations,
//                         int mapIterations);
//
//    /**
//     * getLSDs
//     *
//     * Creates a list of discretization points which might correspond to
//     * a large scale duplication. Currently only the maximum element
//     * is considered.
//     *
//     * lsdEstimates - Probable large scale duplication vertices will be
//     *                saved in this list.
//     * integrationIterations - The number of samples drawn when performing
//     *                         the monte carlo integration.
//     */
//    void getLSDs(LSDProbs &lsdEstimates, int integrationIterations);
//
//    /**
//     * Returns a list of the expected number of duplications on each
//     * discretization vertex.
//     */
//    EdgeDiscPtMap<Real> *getDUPDensity();
//
//    void writeToXML(std::ostream &output);
//
//    virtual ~ApproximateEstimator();
//private:
//
//    /**
//     * Updates the Monte Carlo integration with the new state given from
//     * the chain. It is assumed that the elements of discPointScore are
//     * zero upon the first call.
//     *
//     * discPointScore - List of score for each discretization point.
//     * state - A state in the GSR model.
//     * iteration - The current iteration.
//     *
//     * Returns the sum of all convergence differences.
//     */
//    Real updatePointScore(EdgeDiscPtMap<Real> &discPointScore, EdgeDiscGSR &state, int iteration);
//
//
//    /**
//     * Returns the best gene tree from an SimpleMCMC by reading the best
//     * state string. This is a VERY ugly solution and it should really
//     * be rewritten.
//     *
//     * mcmc - The mcmc chain from which the best state is read.
//     * geneTree - The gene tree in the best state.
//     */
//    void getBestGeneTree(SimpleMCMC *mcmc, Tree &geneTree);
//
//    /**
//     * Performs a burn in and estimates the maximum a posteriori gene tree.
//     *
//     * geneTree - The MAP gene tree will be stored here.
//     * mapIterations - The number of iterations used to find the MAP estiamte.
//     */
//    void findMAPTree(Tree &mapGeneTree, int mapIterations);
//
//    // The discretized species tree
//    EdgeDiscTree &m_speciesTree;
//
//    // The MCMC chain that is sampled when getLSDs is called.
//    PosteriorSampler &m_mcmc;
//
//    // The score for each duplication vertex
//    EdgeDiscPtMap<Real> m_discPointScore;
//
//    // Determines whether a burn in should be performed in getLSDs.
//    bool m_doBurnIn;
//
//    // Number of burn in iterations.
//    int m_burnInIterations;
//
//    // Number of iterations performed with the gene tree fixed.
//    int m_fixedIterations;
//
//    // Number of iterations performed when finding the MAP gene tree
//    int m_mapIterations;
//
//    // A sample from the MCMC chain is drawn every SAMPLE_INTERVAL.
//    static const int SAMPLE_INTERVAL = 100;
//
//};
//
//} /* Namespace end */
//
//#endif	/* _APPROXIMATEESTIMATOR_HH */
