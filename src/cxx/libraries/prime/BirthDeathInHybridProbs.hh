#ifndef BIRTHDEATHINHYBRIDPROBS_HH
#define BIRTHDEATHINHYBRIDPROBS_HH

#include <iostream>
#include <string>
#include <iomanip>

#include "BirthDeathProbs.hh"

namespace beep
{
  class HybridTree;
  //! This class extends the base class BirthDeathProbs to allow hybrid
  //! networks as the host
  //
  class BirthDeathInHybridProbs : public BirthDeathProbs
  {
  public:
    //----------------------------------------------------------------------
    //
    //Constructors and Destructors and Assignment
    //
    //----------------------------------------------------------------------
    BirthDeathInHybridProbs(HybridTree &S, const Real& birth_rate, 
		    const Real& death_rate, Real* topTime = 0);


    BirthDeathInHybridProbs(const BirthDeathInHybridProbs &M);
    virtual ~BirthDeathInHybridProbs();

    // Assignment
    //----------------------------------------------------------------------
    virtual BirthDeathInHybridProbs& operator=(const BirthDeathInHybridProbs& dsp);

    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
    HybridTree& getStree();

    //!
    //! Force an update of precomputed values
    //!
    void update();

    //! The conditional probability of n gene copies over an arc (x,y)
    //---------------------------------------------------------------------
    Probability partialProbOfCopies(const Node &y, unsigned n_kids) const;

    //! We treat the top slice differently since we actually don't know the
    //! time scale of the top slice. Therefore, we are adapting an exponential
    //! prior on the time before the species root.
    //---------------------------------------------------------------------
    virtual Probability topPartialProbOfCopies(unsigned n_kids) const;


    //! The probability of extinction below a node v. 
    Probability extinctionProbability(Node *v) const;


    //---------------------------------------------------------------------
    // I/O
    //---------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const BirthDeathInHybridProbs& tsm);
    virtual std::string print() const;
    


    //---------------------------------------------------------------------
    //
    // Implementation
    //
    //---------------------------------------------------------------------
  protected:
    // Used to calculate the Pt-value and Ut-value of a child Node. 
    //---------------------------------------------------------------------
    void calcPt_Ut(const Real t, Probability & Pt, Probability & Ut) const;

    // calcBirthDeathInHybridProbs()
    //******TO BEOVERLOADED IN INTEGRALDUPSPECPROBS*****
    //----------------------------------------------------------------------
    virtual void calcBirthDeathInHybridProbs(Node &sn);

    // calcBirthDeathInHybridProbs_recursive()
    // This function is called for nodes below the root.
    //----------------------------------------------------------------------
    void calcBirthDeathInHybridProbs_recursive(Node &sn);

  protected:
    //------------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------------
    HybridTree* H;			//!< Species tree

  };

}//end namespace beep

#endif
