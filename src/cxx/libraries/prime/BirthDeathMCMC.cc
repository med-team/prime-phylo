#include "BirthDeathMCMC.hh"

#include "AnError.hh"
#include "MCMCObject.hh"
#include "PRNG.hh"
#include "Tree.hh"

#include <sstream>

// Author: Bengt Sennblad, � the MCMC-club, SBC, all rights reserved
namespace beep
{

  using namespace std;

  //-------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //-------------------------------------------------------------
  BirthDeathMCMC::BirthDeathMCMC(MCMCModel& prior, Tree &S, 
				 Real birthRate, Real deathRate,
				 Real* topTime)
    : StdMCMCModel(prior, 2, S.getName()+"_DupLoss"),
      BirthDeathProbs(S, birthRate, deathRate, topTime),
      old_birth_rate(birthRate),
      old_death_rate(deathRate),
      estimateRates(true),
      suggestion_variance(0.1 * (birthRate+deathRate) / 2.0)
  {
  }

  BirthDeathMCMC::~BirthDeathMCMC()
  {
  }

  //-------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------
  void 
  BirthDeathMCMC::fixRates()
  {
    estimateRates = false;
    n_params = 0;
    updateParamIdx();
  }

  MCMCObject
  BirthDeathMCMC::suggestOwnState()
  {
    MCMCObject MOb(1.0, 1.0);
    // Choose what parameter to perturb using 'paramIdx':
    Real Idx = paramIdx / paramIdxRatio;
    getRates(old_birth_rate, old_death_rate);
    Real max_rate = MAX_INTENSITY / (getStree().rootToLeafTime());// + 
    //				     getStree().getRootNode()->getTime());
    if(getStree().rootToLeafTime() == 0)
      {
	if(getStree().getRootNode()->getTime() != 0)
	  {
	    max_rate = MAX_INTENSITY / getStree().getRootNode()->getTime();
	  }
	else
	  {
	    max_rate = MAX_INTENSITY;
	  }
      }
	
    if(Idx >0.5)
      {
 	setRates(perturbLogNormal(old_birth_rate, 
				  suggestion_variance,
				  Real_limits::min(), 
				  max_rate,
//  			       MAX_INTENSITY / getStree().rootToLeafTime(),
				  MOb.propRatio),
		 old_death_rate, true);
      }
    else			// Loss rate
      {
	setRates(old_birth_rate, 
		 perturbLogNormal(old_death_rate,
				  suggestion_variance,
				  Real_limits::min(), 
 				  max_rate,
//  				  MAX_INTENSITY / getStree().rootToLeafTime(),
				  MOb.propRatio),
		 true);
      }
    return MOb;  // uniform prior on birth/death rates
  }

  void 
  BirthDeathMCMC::commitOwnState()
  {
  }

  void
  BirthDeathMCMC::discardOwnState()
  {
    setRates(old_birth_rate, old_death_rate, true);
  }

  string
  BirthDeathMCMC::ownStrRep() const
  {
    ostringstream oss;

    if(estimateRates)
      {
	Real b, d;
	getRates(b, d);
	oss << b
	    << ";\t"
	    << d
	    << ";\t"
	  ;
      }
    return oss.str();
  }

  string
  BirthDeathMCMC::ownHeader() const
  {
    ostringstream oss;
    if(estimateRates)
      {
	oss << "birthRate(float);\tdeathRate(float);\t"
	  ;
      }
    return oss.str();
  }

  Probability BirthDeathMCMC::updateDataProbability()
  {
    update();
    Real max_rate = MAX_INTENSITY / (getStree().rootToLeafTime() + 
				     getStree().getRootNode()->getTime());
    if(birth_rate > max_rate  || death_rate > max_rate )
      {
	return 0;
      }
    else
      {
	return 1.0;
      }
  }

  //-------------------------------------------------------------
  // I/O
  //-------------------------------------------------------------
  ostream& 
  operator<<(ostream &o, const BirthDeathMCMC& A)
  {
    return o << A.print();
  }

  string 
  BirthDeathMCMC::print() const
  {
    ostringstream oss;
    oss << "Birth and death parameters ";
    if(estimateRates)
      {
	oss << "are estimated during MCMC.\n";
      }
    else
      {
	Real b, d;
	getRates(b, d);
	oss << "are fixed to "
	    << b
	    << " and "
	    << d
	    << ", respectively.\n"
	  ;
      }
    oss << StdMCMCModel::print();
    return oss.str();
  }
  


//   //-------------------------------------------------------------
//   //
//   // Implementation
//   //
//   //-------------------------------------------------------------

//   // Utility function for changing birth/death rates.
//   // Returns a number in the interval [0.8, 1.25].
//   //-------------------------------------------------------------
//   Real
//   BirthDeathMCMC::rateChange()
//   {
//     Real r = R.genrand_real2(); 

//     if(r < 5/9)			// Set rate in [0.8, 1.0)
//       {
// 	r = r * 9/5;		// From [0, 5/9) to [0, 1.0)
// 	return  0.8 + 0.2 * r;  // [0.8, 1.0)
//       }
//     else			// Set rate in (1.0, 1.25]
//       {
// 	r = r * 9/4;		// From [5/9, 1.0) to [0, 1.0)
// 	return 1.25 - 0.25 * r; // (1.0, 1.25]
//       }  
//   }


}//end namespace beep
