void checkpoint(char *location);
void checkpointOneInt(char *location, int theInt);
double ranDoubleUpToOne(void);
void setBigQFromRMatrixDotPiVec(double **theBigQ, double **theRMatrix, double *piVec, int dim);
void normalizeBigQ(double **theBigQ, double *piVec, int dim);
int indexOfIntInArray(int theInt, int *theIntArray, int arrayLength);

// I swiped this from bambe.  Thanks, guys!
#ifdef ALPHA
#define SAFE_EXP(x) ((x)<-200.0 ? 0.0 : exp(x))
#else
#define SAFE_EXP(x) (exp(x))
#endif
