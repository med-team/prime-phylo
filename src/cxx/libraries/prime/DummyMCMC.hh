#ifndef DUMMYMCMC_HH
#define DUMMYMCMC_HH

#include <iostream>

#include "Beep.hh"
#include "MCMCModel.hh"

//----------------------------------------------------------------------
//
// class DummyMCMC
// subclass of MCMCModel that implements a MCMCModel interface
// that works as an endpoint in a chain of priors/MCMCModel
//
// Since numberOfStates returns 0, suggestState should never be called
//
//  Author Bengt Sennblad, 
//  copyright the MCMC club, SBC, allrights reserved
//
//----------------------------------------------------------------------


namespace beep
{
  // Forward declarations
  class MCMCObject;

  class DummyMCMC : public MCMCModel
  {
  public: 
    //----------------------------------------------------------------------
    //
    // Constructor/Destructor
    //
    //----------------------------------------------------------------------
    DummyMCMC();
    DummyMCMC(const DummyMCMC& crm);
    virtual ~DummyMCMC();
    DummyMCMC& operator=(const DummyMCMC& crm);

    //
    // Named constructor for easy instantiation
    //
    static DummyMCMC TheEmptyMCMCModel();


    // Deprecated! Do not use! Will be phased out! Only for back-compatibility!
    //----------------------------------------------------------------------
    DummyMCMC(PRNG& R);
    static DummyMCMC TheEmptyMCMCModel(PRNG& R);

    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------

    // Inherited from MCMCModel
    // Should never be called
    //----------------------------------------------------------------------
    MCMCObject suggestNewState();
    MCMCObject suggestNewState(unsigned x);
    Probability initStateProb();
    Probability currentStateProb();
    void commitNewState();
    void commitNewState(unsigned x);
    void discardNewState();
    void discardNewState(unsigned x);
    std::string strRepresentation() const;
    std::string strHeader() const;
    virtual std::string getAcceptanceInfo() const;

    // Return number of states in prior (probably only used first time!)
    // This will always be 0 
    //----------------------------------------------------------------------
    unsigned nParams() const;

    //----------------------------------------------------------------------
    //
    // I/O
    //
    //----------------------------------------------------------------------
    
    // Always define an ostream operator!
    // This should provide a neat output describing the model and its 
    // current settings for output to the user. It should list current 
    // parameter values by linking to their respective ostream operator
    //------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const DummyMCMC& dm);
    std::string print() const;

  };
}//end namespace beep

#endif
