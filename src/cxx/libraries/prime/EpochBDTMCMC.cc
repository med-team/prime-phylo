#include "EpochBDTMCMC.hh"
#include "DiscTree.hh"
#include "MCMCObject.hh"

#include <iostream>
#include <fstream>

namespace beep
{

using namespace std;

EpochBDTMCMC::EpochBDTMCMC(
		MCMCModel& prior,
		EpochBDTProbs& BDTProbs,
		const Real& suggestRatio) :
	StdMCMCModel(prior, 3, BDTProbs.getTreeName()+"_DupLossTrans", suggestRatio),
	m_BDTProbs(BDTProbs),
	m_fixRates(3, false),
	m_rndBorders(0, 0),
	//m_birthSuggestionVar(-1),
	//m_deathSuggestionVar(-1),
	//m_transferSuggestionVar(-1),
	m_bAccPropCnt(0, 0),
	m_dAccPropCnt(0, 0),
	m_tAccPropCnt(0, 0)
{
	if (BDTProbs.getBirthRate() == 0)
	{
		m_fixRates[0] = true;
		--n_params;
	}
	if (BDTProbs.getDeathRate() == 0)
	{
		m_fixRates[1] = true;
		--n_params;
	}
	if (BDTProbs.getTransferRate() == 0)
	{
		m_fixRates[2] = true;
		--n_params;
	}
	
	// IMPORTANT: The suggestion variance was previously set as a
	// portion of the initial value. Not so any more, see getProposalVariance().
	//m_birthSuggestionVar = 0.1 * BDTProbs.getBirthRate();
	//m_deathSuggestionVar = 0.1 * BDTProbs.getDeathRate();
	//m_transferSuggestionVar = 0.1 * BDTProbs.getTransferRate();
	
	updateBorders();
	updateParamIdx();

}


EpochBDTMCMC::~EpochBDTMCMC()
{
}


void
EpochBDTMCMC::fixRates()
{
	m_fixRates.assign(3, true);
	n_params = 0;
	updateParamIdx();
}


void
EpochBDTMCMC::updateBorders()
{
	Real eps = 1e-5;
	unsigned cz = 4 * (m_fixRates[2] ? 0 : 1) +
		2 * (m_fixRates[1] ? 0 : 1) + 1 * (m_fixRates[0] ? 0 : 1);
	switch (cz)
	{
		case 0: break;
		case 1: m_rndBorders = pair<Real,Real>(0-eps, 0-eps); break;
		case 2: m_rndBorders = pair<Real,Real>(0-eps, 1+eps); break;
		case 3: m_rndBorders = pair<Real,Real>(0-eps, 1.0/2); break;
		case 4: m_rndBorders = pair<Real,Real>(1+eps, 1+eps); break;
		case 5: m_rndBorders = pair<Real,Real>(1.0/2, 1.0/2); break;
		case 6: m_rndBorders = pair<Real,Real>(1.0/2, 1+eps); break;
		case 7: m_rndBorders = pair<Real,Real>(1.0/3, 2.0/3); break;
		default: break;
	}
}


MCMCObject
EpochBDTMCMC::suggestOwnState()
{
	// Turn off notifications just to be sure.
	bool notifStat = m_BDTProbs.setPertNotificationStatus(false);
	
	Real br, dr, tr;
	m_BDTProbs.getRates(br, dr, tr);
	
	// Cache current values.
	m_BDTProbs.cache();
		
	// Choose what parameter to perturb using 'paramIdx'.
	MCMCObject mcmcObj(1.0, 1.0);
	Real idx = paramIdx / paramIdxRatio;
	
	// Equal chance of perturbing each of non-fixed rates.
	if (idx > m_rndBorders.second)
	{
		m_which = 0;
		++m_bAccPropCnt.second;
		br = perturbTruncatedNormal(br, 0.5, StdMCMCModel::FIFTY_PCT, Real_limits::min(),
				m_BDTProbs.getMaxAllowedRate(), mcmcObj.propRatio);
	}
	else if (idx > m_rndBorders.first)
	{
		m_which = 1;
		++m_dAccPropCnt.second;
		dr = perturbTruncatedNormal(dr, 0.5, StdMCMCModel::FIFTY_PCT, Real_limits::min(),
				m_BDTProbs.getMaxAllowedRate(), mcmcObj.propRatio);
	}
	else
	{
		m_which = 2;
		++m_tAccPropCnt.second;
		tr = perturbTruncatedNormal(tr, 0.5, StdMCMCModel::FIFTY_PCT, Real_limits::min(),
				m_BDTProbs.getMaxAllowedRate(), mcmcObj.propRatio);
	}
	m_BDTProbs.setRates(br, dr, tr);
	
	// Notify listeners.
	m_BDTProbs.setPertNotificationStatus(notifStat);
	PerturbationEvent pe(PerturbationEvent::PERTURBATION);
	m_BDTProbs.notifyPertObservers(&pe);
	
	return mcmcObj;
}


void
EpochBDTMCMC::commitOwnState()
{
	switch (m_which)
	{
	case 0: ++m_bAccPropCnt.first; break;
	case 1: ++m_dAccPropCnt.first; break;
	case 2: ++m_tAccPropCnt.first; break;
	default: break;
	}
}


void
EpochBDTMCMC::discardOwnState()
{
	// Turn off notifications just to be sure.
	bool notifStat = m_BDTProbs.setPertNotificationStatus(false);
	
	// Restore cached values.
	m_BDTProbs.restoreCache();
	
	// Notify listeners.
	m_BDTProbs.setPertNotificationStatus(notifStat);
	PerturbationEvent pe(PerturbationEvent::RESTORATION);
	m_BDTProbs.notifyPertObservers(&pe);
}


string
EpochBDTMCMC::ownStrRep() const
{
	ostringstream oss;
	if (!m_fixRates[0]) { oss << m_BDTProbs.getBirthRate() << ";\t";    }
	if (!m_fixRates[1]) { oss << m_BDTProbs.getDeathRate() << ";\t";    }
	if (!m_fixRates[2]) { oss << m_BDTProbs.getTransferRate() << ";\t"; }
	return oss.str();
}


string
EpochBDTMCMC::ownHeader() const
{
	ostringstream oss;
	if (!m_fixRates[0]) { oss << "birthRate(float);\t";    }
	if (!m_fixRates[1]) { oss << "deathRate(float);\t";    }
	if (!m_fixRates[2]) { oss << "transferRate(float);\t"; }
	return oss.str();
}


string
EpochBDTMCMC::getAcceptanceInfo() const
{
	std::ostringstream oss;
	if (n_params > 0)
	{
		unsigned totAcc = m_bAccPropCnt.first
				+ m_dAccPropCnt.first + m_tAccPropCnt.first;
		unsigned totProp = m_bAccPropCnt.second
				+ m_dAccPropCnt.second + m_tAccPropCnt.second;
		oss << "# Acc. ratio for " << name << ": "
			<< totAcc << " / " << totProp << " = "
			<< (totAcc / (Real) totProp) << endl;
		oss << "#    of which birth param: "
			<< m_bAccPropCnt.first << " / " << m_bAccPropCnt.second << " = "
			<< (m_bAccPropCnt.first / (Real) m_bAccPropCnt.second) << endl
			<< "#    and death param:      "
			<< m_dAccPropCnt.first << " / " << m_dAccPropCnt.second << " = "
			<< (m_dAccPropCnt.first / (Real) m_dAccPropCnt.second) << endl
			<< "#    and transfer param:   "
			<< m_tAccPropCnt.first << " / " << m_tAccPropCnt.second << " = "
			<< (m_tAccPropCnt.first / (Real) m_tAccPropCnt.second) << endl;
	}
	if (prior != NULL)
	{
		oss << prior->getAcceptanceInfo();
	}
	return oss.str();
}


Probability
EpochBDTMCMC::updateDataProbability()
{
	// This MCMC has no prior of its own.
	// Note: m_BDTProbs should already be up-to-date if observer pattern
	//       utilized properly.
	return 1.0;
}


ostream&
operator<<(ostream &o, const EpochBDTMCMC& obj)
{
	return (o << obj.print());
}


string
EpochBDTMCMC::print() const
{
	ostringstream oss;
	oss << name << ": Birth, death, transfer params are";
	if (m_fixRates[0]) { oss << " fixed to " << m_BDTProbs.getBirthRate(); }
	else { oss << " estimated"; }
	if (m_fixRates[1]) { oss << ", fixed to " << m_BDTProbs.getDeathRate(); }
	else { oss << ", estimated"; }
	if (m_fixRates[2]) { oss << ", fixed to " << m_BDTProbs.getTransferRate(); }
	else { oss << ", estimated"; }
	oss << " respectively." << endl;
	oss << StdMCMCModel::print();
	return oss.str();
}


} //end namespace beep

