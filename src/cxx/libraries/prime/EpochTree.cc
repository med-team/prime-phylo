#include <algorithm>
#include <cassert>
#include <cmath>
#include <limits>

#include "AnError.hh"
#include "EpochTree.hh"

namespace beep
{

using namespace std;


EpochTree::EpochTree(Tree& S, unsigned minNoOfIvsPerEp, Real maxTimestep) :
	m_S(S),
	m_minNoOfIvs(minNoOfIvsPerEp),
	m_maxTimestep(maxTimestep),
	m_epochs(),
	m_splits(),
	m_nodeAboves(S)
{
	update();
}


EpochTree::~EpochTree()
{
}


void
EpochTree::update()
{
	if (m_minNoOfIvs < 2)
		throw AnError("Need at least 2 intervals per epoch on EpochTree.", 1);
	if (m_S.getTopTime() <= 0)
		throw AnError("Cannot create EpochTree without top time edge.", 1);
	
	m_epochs.clear();
	m_nodeAboves.clearValues();
	m_splits.clear();
	
	// Lowermost epoch contains all leaf edges. Use these as starting point.
	nodevec q = nodevec();
	q.reserve(m_S.getNumberOfLeaves());
	addLeavesLeftToRight(q, m_S.getRootNode());
	for (nodevec::const_iterator it = q.begin(); it != q.end(); ++it)
	{
		m_nodeAboves[*it] = 0;  // Epoch index of all leaves.
	}
	
	const Node* nLo = q.front();         // Lower node of epoch.
	Real tLo = m_S.getTime(*q.front());  // Lower time of epoch.
	Real tUp;                            // Upper time of epoch.
		
	// Find epochs.
	int i = 0;
	m_splits.push_back(0); // Actually undefined, since leaf epoch.
	while (q.size() > 1)
	{
		unsigned nUp = 0;
		tUp = numeric_limits<Real>::max();
		for (unsigned j = 0; j < q.size(); ++j)
		{
			const Node* n = q[j]->getParent();
			if (m_S.getTime(*n) < tUp)
			{
				nUp = j;
				tUp = m_S.getTime(*n);
			}
		}
		
		// Make sure there is at least a certain time span for each epoch.
		if (tUp + MIN_SPLICE_DELTA < tLo) { tUp = tLo + MIN_SPLICE_DELTA; }
		assert(tLo < tUp);
		
		// Create epoch, etc.
		m_epochs.push_back(EpochPtSet(q, tLo, tUp, getNoOfIntervals(tLo, tUp)));
		m_splits.push_back(nUp);
		m_nodeAboves[nLo] = i;
		
		// Update edges for next set. Note "order conservation".
		q[nUp] = q[nUp]->getParent();
		q.erase(q.begin() + nUp + 1);
		nLo = q[nUp];
		tLo = tUp;
		i++;
	}
	
	// The root should remain.
	assert(q.size() == 1 && nLo == q.front() && nLo == m_S.getRootNode());
	
	// Add epoch for top time edge.
	tUp = m_S.getTopToLeafTime();
	if (tUp + MIN_SPLICE_DELTA < tLo) { tUp = tLo + MIN_SPLICE_DELTA; }
	assert(tLo < tUp);
	m_epochs.push_back(EpochPtSet(q, tLo, tUp, getNoOfIntervals(tLo, tUp)));
	m_nodeAboves[nLo] = i;  // Actually undefined, since top time edge.
}


unsigned
EpochTree::getNoOfIntervals(Real tLo, Real tUp) const
{
	if (m_maxTimestep <= 0.0)
		return m_minNoOfIvs;
	Real noOfIvs = ceil((tUp - tLo) / m_maxTimestep  - 1e-6);
	return max(m_minNoOfIvs, static_cast<unsigned>(noOfIvs));
}


EpochTree::const_iterator
EpochTree::begin() const
{
	return m_epochs.begin();
}


EpochTree::const_iterator
EpochTree::end() const
{
	return m_epochs.end();
}


EpochTree::const_reverse_iterator
EpochTree::rbegin() const
{
	return m_epochs.rbegin();
}


EpochTree::const_reverse_iterator
EpochTree::rend() const
{
	return m_epochs.rend();
}


const EpochPtSet&
EpochTree::operator[] (unsigned epochNo) const
{
	return m_epochs[epochNo];
}


void
EpochTree::addLeavesLeftToRight(nodevec& v, const Node* n) const
{
	if (n->isLeaf())
	{
		v.push_back(n);
	}
	else
	{
		addLeavesLeftToRight(v, n->getLeftChild());
		addLeavesLeftToRight(v, n->getRightChild());
	}
}


Real
EpochTree::getTopTime() const
{
	return m_epochs.back().getTimeSpan();
}


Real
EpochTree::getTopToLeafTime() const
{
	return m_epochs.back().getUpperTime();
}


Real
EpochTree::getRootToLeafTime() const
{
	return m_epochs.back().getLowerTime();
}


Real
EpochTree::getTime(const Node* node) const
{
	return m_epochs[m_nodeAboves[node]].getLowerTime();
}


Real
EpochTree::getTime(const EpochTime& et) const
{
	return m_epochs[et.first].getTime(et.second);
}


unsigned
EpochTree::getEpochAbove(const Node* node) const
{
	return m_nodeAboves[node];
}


unsigned
EpochTree::getEpochBelow(const Node* node) const
{
	return (m_nodeAboves[node] - 1);
}


unsigned
EpochTree::getNoOfEpochs() const
{
	return m_epochs.size();
}


unsigned
EpochTree::getNoOfEdges(unsigned epochNo) const
{
	return m_epochs[epochNo].getNoOfEdges();
}


Real
EpochTree::getTimestep(unsigned epochNo) const
{
	return m_epochs[epochNo].getTimestep();
}


Real
EpochTree::getMinTimestep() const
{
	Real ts = numeric_limits<Real>::max();
	for (const_iterator it = begin(); it != end(); ++it)
	{
		if (ts > (*it).getTimestep()) ts = (*it).getTimestep();
	}
	return ts;
}


unsigned 
EpochTree::getTotalNoOfTimes(bool unique) const
{
	unsigned sum = 0;
	for (const_iterator it = begin(); it != end(); ++it)
	{
		sum += (*it).getTimes().size();
	}
	if (unique) { sum -= (m_epochs.size() - 1); } 
	return sum;
}


unsigned
EpochTree::getTotalNoOfPoints() const
{
	unsigned sum = 0;
	for (const_iterator it = begin(); it != end(); ++it)
	{
		sum += (*it).getNoOfPoints();
	}
	return sum;
}


unsigned 
EpochTree::getSplitIndex(unsigned epochNo) const
{
	return m_splits[epochNo];
}


EpochTime
EpochTree::getEpochTimeAtTop() const
{
	return EpochTime(m_epochs.size() - 1,
			m_epochs.back().getNoOfTimes() - 1);
}


EpochTime
EpochTree::getEpochTimeBelow(const EpochTime& et) const
{
	return (et.second == 0 ?
		EpochTime(et.first - 1, m_epochs[et.first - 1].getNoOfTimes() - 1) :
		EpochTime(et.first, et.second - 1));
}


EpochTime
EpochTree::getEpochTimeBelowStrict(const EpochTime& et) const
{
	return (et.second == 0 ?
		EpochTime(et.first - 1, m_epochs[et.first - 1].getNoOfTimes() - 2) :
		EpochTime(et.first, et.second - 1));
}


EpochTime
EpochTree::getEpochTimeAbove(const EpochTime& et) const
{
	return (et.second + 1 >= m_epochs[et.first].getNoOfTimes() ?
		EpochTime(et.first + 1, 0) :
		EpochTime(et.first, et.second + 1));
}

EpochTime
EpochTree::getEpochTimeAboveStrict(const EpochTime& et) const
{
	return (et.second + 1 >= m_epochs[et.first].getNoOfTimes() ?
		EpochTime(et.first + 1, 1) :
		EpochTime(et.first, et.second + 1));
}


EpochTime
EpochTree::getEpochTimeAboveNotLast(const EpochTime& et) const
{
	return (et.second + 2 >= m_epochs[et.first].getNoOfTimes() ?
			EpochTime(et.first + 1, 0) :
			EpochTime(et.first, et.second + 1));
}


bool
EpochTree::isLastEpochTime(const EpochTime& et) const
{
	return (et.second + 1 == m_epochs[et.first].getNoOfTimes());
}


Tree&
EpochTree::getOrigTree() const
{
	return m_S;
}


const Node*
EpochTree::getOrigRootNode() const
{
	return m_S.getRootNode();
}
	

const Node*
EpochTree::getOrigNode(unsigned index) const
{
	return m_S.getNode(index);
}


string
EpochTree::getDebugInfo(bool inclEpochInfo, bool inclNodeInfo) const
{
	ostringstream oss;
	oss << "# ===================================== EPOCHTREE =====================================" << endl
		<< "# Discretization type: ";
	if (m_maxTimestep <= 0)
	{
		oss << "Every epoch comprises " << m_minNoOfIvs << " intervals" << endl;
	}
	else
	{
		oss << "Approx. timestep is " << m_maxTimestep
			<< ", min no of intervals per epoch is " << m_minNoOfIvs << endl;
	}
	oss << "# No. of epochs: " << getNoOfEpochs() << endl
		<< "# No. of nodes: " << m_S.getNumberOfNodes() << endl
		<< "# Total no. of times: " << getTotalNoOfTimes(false)
			<< ", of which unique: " << getTotalNoOfTimes(true) << endl
		<< "# Total no. of pts: " << getTotalNoOfPoints() << endl;
	oss << "# Top time: " << getTopTime() << ", root-to-leaf time: " << getRootToLeafTime()
		<< ", top-to-leaf time: " << getTopToLeafTime() << endl;
	if (inclEpochInfo)
	{
		oss << "# Epoch:\tNo. of pts:\tTimestep:\tTime span:\tEdges:\tSplit index:" << endl;
		for (unsigned i = getNoOfEpochs(); i > 0; --i)
		{
			const EpochPtSet& ep = (*this)[i-1];
			oss
				<< "# " << (i-1) << '\t'
				<< ep.getNoOfEdges() << '*' << ep.getNoOfTimes() << '=' << ep.getNoOfPoints() << '\t'
				<< ep.getTimestep() << '\t'
				<< ep.getLowerTime() << "..." << ep.getUpperTime() << '\t'
				<< '{';
			const nodevec& es = ep.getEdges();
			for (nodevec::const_iterator it = es.begin(); it != es.end(); ++it)
			{
				oss << (*it)->getNumber() << ',';
			}
			oss << "}\t";
			oss << m_splits[i-1] << endl;
		}
	}
	if (inclNodeInfo)
	{
		//Prints node-based info.
		for (unsigned i = 0; i < m_S.getNumberOfNodes(); ++i)
		{
			const Node* n = m_S.getNode(i);
			oss
				<< "# Node: "
				<< i
				<< ", time: " << getTime(n)
				<< ", epoch above: " << getEpochAbove(n)
				<< ", epoch below: " << getEpochBelow(n)
				<< endl;
		}
	}
	oss << "# =====================================================================================" << endl;
	return oss.str();
}

} // end namespace beep.
