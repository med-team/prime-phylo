#ifndef PRNG_HH
#define PRNG_HH

#include "Beep.hh"

namespace beep
{
  // Period parameters 
  //---------------------------------------------------------------
  static const unsigned N = 624;
  static const unsigned M = 397;
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

  const double DEFAULT_LARGE_PERCENTILE = 0.001; // See PRNG::exponential!
  
  //---------------------------------------------------------------
  //
  //! This is a class interfacing to the mersenne twister PRNG. 
  //! The code have been copied out of the original code found in
  //! PRNG-mt19937ar/mt19937ar.c
  //! Please notice the copyright notice on that code reproduced below!
  //! Author: Lars Arvestad, SBC, � the MCMC-club, SBC, all rights reserved
  //
  //---------------------------------------------------------------
  class PRNG
  {
  private: 
    //---------------------------------------------------------------
    //
    //! Helper class of PRNG
    //
    //---------------------------------------------------------------
    class Impl
    {
    public:
      Impl();
      ~Impl() {};
      unsigned long getSeed();	//!< Return the starting seed of the PRNG.
      void init_genrand(unsigned long seed); //!< Set the seed of the PRNG
      unsigned long genrand_int32();
      
    public:
      unsigned long seed;    //! Save the first state
      unsigned long mt[N];   //!< the array for the state vector 

      //! \todo{ -Wall warns for comparison unsigned w signed This is
      //! because mti is int while all other parameters(e.g., N) are
      //! unsigned. We should check if mti could be unsigned as well (but
      //! we want ot test that this works properly /bens}
      unsigned mti; //!< mti==N+1 means mt[N] is not initialized 
      
    };

  public:
    //---------------------------------------------------------------
    //
    // Constructors
    //
    //---------------------------------------------------------------
    PRNG(unsigned long seed); //!< Construct using user-provided seed
    PRNG();	              //!< Constructs using the process id as seed

    virtual ~PRNG();

    //---------------------------------------------------------------
    //
    // Interface
    //
    //---------------------------------------------------------------
    unsigned long getSeed() const;	  //!< Return the starting seed.
    //! Start the PRNG at a specific seed. Don't use more than once, 
    //!and only before the first generatione of a number. 
    void setSeed(unsigned long);	

    //---------------------------------------------------------------
    //! \name Generators
    //---------------------------------------------------------------
    //@{
    //! generates a random number on [0,0xffffffff]-interval
    unsigned long genrand_int32(); 

    //! generates a random number on [0,1]-real-intervalk
    double genrand_real1();	

    //! generates a random number on [0,1)-real-interval
    double genrand_real2();	

    //! generates a random number on (0,1)-real-interval
    double genrand_real3();	

    // Lasse's additions
    //! Generates a uniformly chosen integer in the range [0, modulus-1],
    //! based on calls to genrand_int32.
    unsigned genrand_modulo(unsigned modulus);

    //! Generate samples from an exponential distribution. 
    //! Very large samples are discarded.  Samples are generated until
    //! it is found below the 0.1 percentile. (This is adjustable by 
    //! set_large_percentile.)
    //!
    //! Notice that lambda is 1 over the mean, or lambda = 1/beta.
    //!
    //! \todo{One could argue that this stuff should be in a subclass 
    //! since it has its own attribute! /arve}
    double exponential(double lambda);

    //! Adjust percentile used in PRNG::exponential(...) 
    void   set_large_percentile(double p);
    //@}

  private:
    static Impl x;		//!< Contains actual state of PRNG

    //!< This is a per-PRNG attribute, since it is for the 
    //!exponential distribution.
    double large_percentile;	

  };
}// end namespace beep

#endif


  //
  // Original copyright notice on the PRNG:
  //
  /* 
     A C-program for MT19937, with initialization improved 2002/1/26.
     Coded by Takuji Nishimura and Makoto Matsumoto.

     Before using, initialize the state by using init_genrand(seed)  
     or init_by_array(init_key, key_length).

     Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
     All rights reserved.                          

     Redistribution and use in source and binary forms, with or without
     modification, are permitted provided that the following conditions
     are met:

     1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote 
     products derived from this software without specific prior written 
     permission.

     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
     "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
     LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
     A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
     CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
     PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
     PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
     LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
     NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


     Any feedback is very welcome.
     http://www.math.keio.ac.jp/matumoto/emt.html
     email: matumoto@math.keio.ac.jp
  */
