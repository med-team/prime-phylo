#ifndef SEQUENCEGENERATOR_HH
#define SEQUENCEGENERATOR_HH

#include <vector>
#include <string>
#include <map>
#include <iostream>

#include "Beep.hh"
#include "EdgeWeightHandler.hh"
#include "Node.hh"
#include "PRNG.hh"
#include "SequenceData.hh"
#include "SequenceType.hh"
#include "SiteRateHandler.hh"
#include "MatrixTransitionHandler.hh"
#include "Tree.hh"

namespace beep
{

  class SequenceGenerator 
  {
  
  public:
    //----------------------------------------------------------------------
    //
    // Construct/Destruct
    //
    //----------------------------------------------------------------------
  
    // Construct 
    //----------------------------------------------------------------------
    SequenceGenerator(Tree& T_in, 
		      MatrixTransitionHandler& Q_in, 
		      SiteRateHandler& siteRates_in,
		      EdgeWeightHandler& ewh_in,
		      PRNG& R_in);

    // Destructor
    //----------------------------------------------------------------------
    ~SequenceGenerator();

    // Copy Constructor
    //----------------------------------------------------------------------
    SequenceGenerator(const SequenceGenerator& sg);

    // Assigment operator
    //----------------------------------------------------------------------
    SequenceGenerator& operator=(const SequenceGenerator& sg);

  
    //----------------------------------------------------------------------
    //
    // Public interface
    //
    //----------------------------------------------------------------------

    // For simulations
    //----------------------------------------------------------------------
    SequenceData generateData(const unsigned& nchar, 
			      const bool ancestral = false);

    //-----------------------------------------------------------------
    //
    // I/O
    //
    //-----------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream& os, 
				    const SequenceGenerator& dt);
    std::string print() const;

  private:
  
    //----------------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------------

    // Helper function for simulateSequence
    //----------------------------------------------------------------------
    void recursivelyGenerate(const Node& n, 
			     const std::vector<unsigned>& start, 
			     const std::vector<unsigned>& posRate,
			     SequenceData& D, const bool& ancestral);

    // Helper function for operator<<
    //----------------------------------------------------------------------
    std::string printSequence(const std::vector<unsigned>& V) const;

    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------
    SequenceType seqType;		// DNA, aminod acids, etc
    Tree& T;
    std::vector<MatrixTransitionHandler> Q;
    SiteRateHandler& siteRates;
    EdgeWeightHandler* ewh;
    PRNG& R;
  }
  ;
  
}//end namespace beep
#endif
