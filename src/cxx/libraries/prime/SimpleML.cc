#include "SimpleML.hh"

#include "AnError.hh"
#include "Hacks.hh"
#include "MCMCObject.hh"

#include <iostream>
#include <sstream>
#include <unistd.h>
#include <cmath>

// Author: Bengt Sennblad, � the MCMC-club, SBC, all rights reserved
namespace beep
{
  using namespace std;
  
  //-------------------------------------------------------------
  //
  // Construct / Destruct / Assign
  //
  //-------------------------------------------------------------
  SimpleML::SimpleML(MCMCModel& M, unsigned thin)
    : SimpleMCMC(M, thin)
//     model(M),
//       R(M.getPRNG()),
//       iteration(0),
//       thinning(thin),
//       cout_buf(NULL),
//       show_diagnostics(true)
  {
    p = model.initStateProb();    // To set stateProbs
    localOptimum = p;
    bestState = model.strRepresentation();
    model.commitNewState();       // To set old_stateProbs, 
  }
  SimpleML::~SimpleML()
  {
//     // If we have redirected cout, then we want to make sure to set it back!
//     if (cout_buf != NULL) 
//       {
// 	os.close();		 // Close output file
// 	cout.rdbuf(cout_buf);	 // Assign the old buffer
// 	cout_buf = NULL;	 // Reset temp buffer pointer
//       }
  }

  //---------------------------------------------------------------
  //
  // Standardized interface from MCMCModel
  // You should never really need to change this!
  // 
  //---------------------------------------------------------------
  // Overloading iterate
  void
  SimpleML::iterate(unsigned n_iters, unsigned print_factor)
  { 
    p = model.initStateProb();    // To set stateProbs
   
    start_time = time(NULL);

    // First print the settings
    cout << "#  Starting ML with the following settings:\n#  "
	 << n_iters 
	 << print()
	 << "#\n";

    // and the mcmc-header
    cout << "# L N "
	 << model.strHeader()
	 << endl;

    bool error_stream_is_a_terminal = true;//isatty(prime_fileno(cerr));     // arve hack in Hacks.hh. See SimpleMCMC.

    if (error_stream_is_a_terminal // Check if file descriptor is a TTY
	&& show_diagnostics)
      {
	cerr.width(15);
	cerr << "L";
	cerr.width(15);
	cerr << "N";
	cerr.width(15);
	cerr << "alpha";
	cerr.width(15);
	cerr << "time";
	cerr.width(15);
	cerr << "no_update"
	     << endl;
      }
    
    // This variable controls output to cerr
    unsigned printing = thinning * print_factor;
    unsigned no_update = 0;

    string output = model.strRepresentation();
    while(iteration < n_iters && no_update < 100) //10000)
      {
	try 
	  {
	    MCMCObject proposal = model.suggestNewState();
 	    Probability alpha = proposal.stateProb;	      

	    if(alpha > p)
	      {
		model.commitNewState();		
		output = model.strRepresentation();
                p = proposal.stateProb;
		no_update = 0;

		localOptimum = p;
		bestState = model.strRepresentation();
	      }
	    else
	      {
		model.discardNewState();
		no_update++;
	      }     	    
 	    //cout << "\n";
	  }
	catch (AnError& e)
	  {
	    cerr << "At iteration " 
		 << iteration 
		 << ".\nState is "
		 << model.strRepresentation()
		 << endl;
	    e.action();
	  }
	
	if (iteration % thinning == 0)
	  {
	    if (error_stream_is_a_terminal // Check if file descriptor is a TTY
		&& show_diagnostics 
		&& iteration % printing == 0) 
	      {
		cerr.width(15);
		cerr.precision(10);
		cerr << p;
		cerr.width(15);
		cerr << iteration;
		cerr.width(15);
		cerr << model.getAcceptanceRatio();
		cerr.width(15);
		cerr << estimateTimeLeft(iteration, n_iters);
		cerr.width(15);
		cerr << no_update
		  //	cerr << estimateTimeLeft(i, n_iters)
		     << endl;
	      }
	    
	    cout << p
		 << "\t"
		 << iteration
		 << "\t"
  		 << output
		 << endl;
	  }
	iteration = iteration + 1;
      }
    cout << "# no_update = " << no_update << "\n";
    cout << "# acceptance ratio = " << model.getAcceptanceRatio() << "\n";
    cout << "local optimum = " << localOptimum << "\n";
    cout << "best state " << bestState << "\n";    
  }

//   void
//   SimpleML::setOutputFile(char *filename)//, char *header)
//   {
//     if (cout_buf)  // Already have a file open. Close and reassign.
//       {
// 	os.close();
// 	cout.rdbuf(cout_buf);
//       }

//     os.open(filename);
//     cout_buf = cout.rdbuf();
//     cout.rdbuf(os.rdbuf());  
//   }

//   void
//   SimpleML::setThinning(unsigned i)
//   {
//     thinning = i;
//   }

//   bool
//   SimpleML::setShowDiagnostics(bool yes_no)
//   {
//     bool old_val = show_diagnostics;
//     show_diagnostics = yes_no;
//     return old_val;
//   }

//   string
//   SimpleML::estimateTimeLeft(unsigned iteration, unsigned when_done)
//   {
//     if (iteration < 10) {
//       return "";
//     }

//     unsigned cur_time = time(NULL);
//     float d = cur_time - start_time; 
//     float efficiency = d / iteration;
//     unsigned to_go = lrint(efficiency * (when_done - iteration));

//     return readableTime(to_go);    
//   }

  Probability 
  SimpleML::getLocalOptimum()
  {
    return localOptimum;
  }

  string 
  SimpleML::getBestState()
  {
    return bestState;
  }

  //----------------------------------------------------------------------
  // I/O
  //----------------------------------------------------------------------
//   ostream& 
//   operator<<(ostream &o, const SimpleML& A)
//   {
//     return o << A.print();
//   }
  string 
  SimpleML::print() const
  {
    ostringstream oss;
    oss << " ML iterations, saving every "
	<< thinning
	<< " iteration.\n"
	<< indentString(model.print(), "#  ")
      ;
    return oss.str();
  }


}//end namespace beep
