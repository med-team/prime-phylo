#include <cmath>

#include "AnError.hh"
#include "TreeDiscretizers.hh"

namespace beep
{

  std::ostream& 
 operator<<(std::ostream& o, 
	    const  std::pair<const Node*, unsigned>&p)
  {
    return o << "<" 
	     << (p.first)->getNumber()
	     << ":" 
	     << p.second 
	     << ">";
  };

  using namespace std;

  EquiSplitEdgeDiscretizer::EquiSplitEdgeDiscretizer(unsigned noOfIvs, unsigned noOfTopEdgeIvs) :
    EdgeDiscretizer(),
    m_noOfIvs(noOfIvs),
    m_noOfTopEdgeIvs(noOfTopEdgeIvs == 0 ? noOfIvs : noOfTopEdgeIvs)
  {
    if (noOfIvs < 2)
      throw AnError("Cannot discretize tree edges into fewer than 2 segments.");
  }


  void
  EquiSplitEdgeDiscretizer::discretize(Tree& S, BeepVector< vector<Real> >& pts) const
  {
    for (Tree::const_iterator it = S.begin(); it != S.end(); ++it)
      {	
	const Node* n = (*it);
	discretizeEdge(n, pts[n]);
      }
  }


  void
  EquiSplitEdgeDiscretizer::discretizeEdge(const Node* n, vector<Real>& pts) const
  {
    pts.clear();
    Real nt = n->getNodeTime();
    Real t = n->getTime();
    unsigned noOfIvs = n->isRoot() ?
      (t < 1e-8 ? 0 : m_noOfTopEdgeIvs) : m_noOfIvs;
    Real stepSz = t / noOfIvs;

    // First point on node.
    pts.push_back(nt);

    // Intermediate points.
    for (unsigned j = 1; j <= noOfIvs; ++j)
      {
	pts.push_back(nt + (j-0.5) * stepSz);
      }

    // Point on very tip if top edge.
    if (n->isRoot() && t >= 1e-8) { pts.push_back(nt + t); }
  }


  StepSizeEdgeDiscretizer::StepSizeEdgeDiscretizer(Real targetStepSz,
						   unsigned minNoOfIvs, unsigned noOfTopEdgeIvs) :
    EdgeDiscretizer(),
    m_targetStepSz(targetStepSz),
    m_minNoOfIvs(minNoOfIvs),
    m_noOfTopEdgeIvs(noOfTopEdgeIvs)
  {
    if (targetStepSz < 1e-8)
      throw AnError("Cannot discretize tree with such small timestep.");
    if (minNoOfIvs < 2)
      throw AnError("Cannot discretize tree edges into fewer than 2 segments.");
    if (noOfTopEdgeIvs == 1)
      throw AnError("Cannot discretize top time edge into one single segment.");
  }


  void
  StepSizeEdgeDiscretizer::discretize(Tree& S, BeepVector< vector<Real> >& pts) const
  {
    for (Tree::const_iterator it = S.begin(); it != S.end(); ++it)
      {	
	const Node* n = (*it);
	discretizeEdge(n, pts[n]);
      }
  }


  void
  StepSizeEdgeDiscretizer::discretizeEdge(const Node* n, vector<Real>& pts) const
  {
    pts.clear();
    Real nt = n->getNodeTime();
    Real t = n->getTime();
    unsigned noOfIvs = 0;
    if (!n->isRoot() || (m_noOfTopEdgeIvs == 0 && t >= 1e-8))
      {
	noOfIvs = static_cast<unsigned>(ceil(t / m_targetStepSz  - 1e-6));
	noOfIvs = max(m_minNoOfIvs, noOfIvs);
      }
    else if (m_noOfTopEdgeIvs > 0)
      {
	noOfIvs = max(m_minNoOfIvs, m_noOfTopEdgeIvs);
      }
    Real stepSz = t / noOfIvs;

    // First point on node.
    pts.push_back(nt);

    // Intermediate points.
    for (unsigned j = 1; j <= noOfIvs; ++j)
      {
	pts.push_back(nt + (j-0.5) * stepSz);
      }

    // Point on very tree tip if top edge.
    if (n->isRoot() && t >= 1e-8) { pts.push_back(nt + t); }
  }

} //end namespace beep

