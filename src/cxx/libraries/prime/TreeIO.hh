#ifndef TREEIO_HH
#define TREEIO_HH

#include "Beep.hh"
#include "NHXtree.h"
#include "NHXnode.h"
#include "TreeIOTraits.hh"

#include <string>
#include <vector>
#include <map>

// The size of a char buffer when reading gene-species info:
#define LINELENGTH 10000   //1024

namespace beep
{
  // Forward declarations
  class GammaMap;
  class Node;
  class SetOfNodes;
  class StrStrMap;
  class Tree;
  
  //--------------------------------------------------------------------
  //
  // TreeIO
  //
  //! Provides methods for reading and writing phylo trees. 
  //!
  //! + When reading gene trees, any edge weight (time) annotations are 
  //!   totally disregarded.
  //! + For species trees, edge weights are honoured, and if they would 
  //!   be missing, a negative time is inserted.
  //! Author: Bengt Sennblad, LArs Arvestad � the MCMC-club, SBC, 
  //! all rights reserved
  //
  //--------------------------------------------------------------------
  class TreeIO 
  {
    //! convenience alias 
    enum TreeSource {notInitialized, readFromStdin, readFromFile, readFromString};

  public:
    //--------------------------------------------------------------------
    //
    // Constructors
    // 
    //--------------------------------------------------------------------

    //! Only create TreeIO objects through *named constructors* (see C++ FAQ 
    //! lite). Actual constructor is protected, and does actually not do 
    //! anything! There is one exception: If you want to read from STDIN, 
    //! use the empty constructor, TreeIO(). But you get the same behaviour
    //! if the filename is empty.
    //--------------------------------------------------------------------
  protected:
    TreeIO(enum TreeSource src, const std::string s);

  public:
    TreeIO();	    	//! "Empty" constructor, allows reading from STDIN.
    virtual ~TreeIO();
    TreeIO(const TreeIO &io);
    virtual TreeIO& operator=(const TreeIO &io);

    /// \name Named constructors! 
    //! Usage: 
    //!   TreeIO io = fromFile("Nisse"); /* read from file Nisse */
    //!   TreeIO io = fromString("(a,(b,c));"); /* read from this string */
    //!   TreeIO io = fromFile("");      /* read from STDIN  */
    //!   TreeIO io;                     /* read from STDIN  */
    //--------------------------------------------------------------------
    //@{
    static TreeIO fromFile(const std::string &filename);
    static TreeIO fromString(const std::string &treeString);
    //@}

    //! \name
    //! Change source using these utilities:
    //--------------------------------------------------------------------
    //@{
    void setSourceFile(const std::string &filename);
    void setSourceString(const std::string &str); 
    //@}

    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
  public:

    //----------------------------------------------------------------------
    //! \name Reading trees
    //----------------------------------------------------------------------
    //@{

    //! Basic function for reading trees in PRIME format
    //! ID and name of nodes are always read, reads everything there is, 
    //! unless told otherwise by traits.
    //! precondition: (useET && useNT) != true
    //----------------------------------------------------------------------
    Tree readBeepTree(std::vector<SetOfNodes> *AC, StrStrMap *gs);
    Tree readBeepTree(const TreeIOTraits& traits, std::vector<SetOfNodes> *AC,
		      StrStrMap *gs);

    //! Convenience front to readBeepTree(...)
    //! Reads times from NT, ET or NW and nothing more
    //----------------------------------------------------------------------
    Tree readHostTree();


    //! Convenience front to readBeepTree(...)
    //! Reads edge lengths from BL or NW and what else there is
    //! Reads antichains info and gene species maps
    //----------------------------------------------------------------------
    Tree readGuestTree(std::vector<SetOfNodes>* AC, StrStrMap* gs);
    
    //! Convenience front to readGuestTree(...)
    //! Reads edge lengths from BL or NW and what else there is
    //! Doese not read antichains info and gene species maps
    //----------------------------------------------------------------------
    Tree readGuestTree();
    
    //! Convenience front to readBeepTree(...)
    //! Reads a plain newick tree with branch lengths from NW only
    //----------------------------------------------------------------------
    Tree readNewickTree();


    //! Convenience front to readAllBeepTrees(...)
    //! Reads everything there is 
    //! Reads antichains info and gene species maps
    //----------------------------------------------------------------------
    std::vector<Tree> readAllBeepTrees(std::vector<SetOfNodes>* AC, 
				       std::vector<StrStrMap>* gs);

    //! Basic function for reading multiple trees in PRIME format
    //! reads everything there is
    //! precondition: (useET && useNT) != true
    //----------------------------------------------------------------------
    std::vector<Tree> readAllBeepTrees(const TreeIOTraits& traits,
				       std::vector<SetOfNodes>* AC,
				       std::vector<StrStrMap>* gs);
   
    //! Convenience front to readBeepTree(...)
    //! Reads edge times from ET, NT or NW and nothing more
    //----------------------------------------------------------------------
    std::vector<Tree> readAllHostTrees();
    
    //! Convenience front to readAllBeepTrees(...)
    //! Reads edge lengths from BL or NW and what else there is
    //! Reads antichains info and gene species maps
    //----------------------------------------------------------------------
    std::vector<Tree> readAllGuestTrees(std::vector<SetOfNodes>* AC, 
					std::vector<StrStrMap>* gs);
    
    //! Convenience front to readGuestTrees(...)
    //! Reads edge lengths from BL or NW and what else there is
    //! Doese not read antichains info and gene species maps
    //----------------------------------------------------------------------
    std::vector<Tree> readAllGuestTrees();
    
    //! Convenience front to readAllBeepTrees(...)
    //! Reads a plain newick tree with branch lengths from NW and nothing more
    //----------------------------------------------------------------------
    std::vector<Tree> readAllNewickTrees();

    //@}

    //----------------------------------------------------------------------
    //! \name Writing trees
    //----------------------------------------------------------------------
    //@{

    //! Basic function for writing tree T in newick format, with the tags 
    //! indicated by traits included in PRIME markup. If gamma != NULL then AC 
    //! markup will also be included.
    //! Precondition: (useET && useNT) == false
    //----------------------------------------------------------------------
    static std::string writeBeepTree(const Tree& T, 
				     const TreeIOTraits& traits,
				     const GammaMap* gamma);

    //! convenience front function for writeBeepTree(...) 
    //! writes tree S with all attributes 
    //----------------------------------------------------------------------
    static std::string writeBeepTree(const Tree& S, const GammaMap* gamma=0);

    //! convenience front function for writeBeepTree(...) 
    //! writes tree S with edge times
    //----------------------------------------------------------------------
    static std::string writeHostTree(const Tree& S);
  
    //! convenience front function for writeBeepTree(...) 
    //! writes tree G with lengths and with gamma/AC info
    //----------------------------------------------------------------------
    static std::string writeGuestTree(const Tree& G, const GammaMap* gamma);
  
    //! convenience front function for writeGeneTree(...) 
    //! writes tree G with lkengths but without AC info
    //----------------------------------------------------------------------
    static std::string writeGuestTree(const Tree& G);

    //! convenience front function for writeBeepTree(...) 
    //! writes plain newick tree T with branch lengths
    //----------------------------------------------------------------------
    static std::string writeNewickTree(const Tree& T);
  
    //----------------------------------------------------------------------
    //! \name Read associations
    //----------------------------------------------------------------------
    //@{

    //! Map leaves in the gene tree to leaves in the species tree
    //! \todo{This is a bit incongruent with the rest of the code and should 
    //! probably get its own class! /arve}
    //! \todo{ It should definitely have its name changed /bens}
    //--------------------------------------------------------------------
    static StrStrMap readGeneSpeciesInfo(const std::string& filename);
    static std:: vector<StrStrMap> 
    readGeneSpeciesInfoVector(const std::string& filename);
    //@}

    //----------------------------------------------------------------------
    //! \name Check trees
    //----------------------------------------------------------------------
    //@{
    //! Precheck what tags are present in the read NHX-tree. Since ID,
    //! Names of nodes and trees are always read - these are not checked
    struct NHXtree* checkTagsForTree(TreeIOTraits &traits);
    //@}

  
    //----------------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------------
  protected:
    //! The basic function for reading NHX trees
    //----------------------------------------------------------------------
    Tree readBeepTree(struct NHXtree *t, const TreeIOTraits& traits, 
		      std::vector<SetOfNodes> *AC, StrStrMap *gs);
    //! The basic recursion function for reading node info from NHX structs
    //----------------------------------------------------------------------
    Node* extendBeepTree(Tree &T, struct NHXnode *v, 
			 const TreeIOTraits& traits,
			 std::vector<SetOfNodes> *AC, StrStrMap *gs,
			 std::map<const Node*, Node*>* otherParent,
			 std::map<const Node*, unsigned>* extinct);

    //! Handle the various rules for how to set the time over an edge
    Real   decideEdgeTime(struct NHXnode *v, const TreeIOTraits& traits, bool isHY);
    std::string decideNodeName(struct NHXnode *v);
    void   sanityCheckOnTimes(Tree& S, Node *node, struct NHXnode *v, 
			      const TreeIOTraits& traits);
    void   handleBranchLengths(Node *node, struct NHXnode *v, bool NWIsET);

    // Basic helper function for writing trees in PRIME format
    static std::string 
    recursivelyWriteBeepTree(Node& u, std::string& least, 
			     const TreeIOTraits& traits,
			     const GammaMap* gamma,
			     std::map<const Node*,Node*>* otherParent,
			     std::map<const Node*,unsigned>* extinct,
			     std::map<unsigned, unsigned>* id);
    static void decideSubtreeOrder(Node& u, std::map<Node*, std::string> order);
    static std::string 
    recursivelyWriteBeepTree(Node& u, std::map<Node*, std::string> least,
			     const TreeIOTraits& traits,
			     const GammaMap* gamma,
			     std::map<const Node*,Node*>* otherParent,
			     std::map<const Node*,unsigned>* extinct,
			     std::map<unsigned, unsigned>* id);
    
    //! Collect info for newickString
    //! Compute markup for the anti-chains on node u
    //----------------------------------------------------------------------
    static std::string getAntiChainMarkup(Node &u, const GammaMap &gamma); 

    //! Recursively checks what tags are given for all nodes in subtree T_v
    //! Precondition: All bool argument has proper values. Assume a specific
    //! bool argument, 'A' has incoming value 'a', and the value for the 
    //! current subtree is 'b', then on return, A = a && b, i.e., false if 
    //! either a or b is false.
    //! postcondition: return statement is true if v != 0
    //----------------------------------------------------------------------
    bool recursivelyCheckTags(struct NHXnode* v, TreeIOTraits& traits);

    //! Checks what tags are given for node v
    //----------------------------------------------------------------------
    void checkTags(struct NHXnode& v, TreeIOTraits& traits);

    //! Helper function that reads NHXtrees:
    //----------------------------------------------------------------------
    NHXtree* readTree();

    //! \todo{add comments on what this do /bens}
    //----------------------------------------------------------------------
    void updateACInfo(struct NHXnode *v, Node *new_node, 
		      std::vector<SetOfNodes> &AC);

    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------

  protected:
    enum TreeSource source; //!< Where do we read trees from?
    std::string stringThatWasPreviouslyNamedS;          //!< filename of current file to read from

    static std::string antiChainMarkupTag; //!< \todo{is this used? /bens

  };

}//end namespace beep


#endif
