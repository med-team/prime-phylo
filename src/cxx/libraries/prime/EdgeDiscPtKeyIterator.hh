/* Specifies an iterator for iterating over the points in a EdgeDiscPtMap,
 * whereas EdgeDiscPtMapIterator only iterates over certain paths in a
 * discretized species tree. This iterator will iterate over all points
 * in a discretized tree, i.e. the 'keys' in the PtMap.
 *
 * File:   EdgeDiscPtMapInnerIterator.hh
 * Author: fmattias
 *
 * Created on November 26, 2009, 12:04 PM
 */

#ifndef _EDGEDISCPTMAPINNERITERATOR_HH
#define	_EDGEDISCPTMAPINNERITERATOR_HH
#include <stdexcept>

#include "TreeDiscretizers.hh"
#include "EdgeDiscPtMaps.hh"

namespace beep {

template<typename T>
class EdgeDiscPtKeyIterator {
public:

    /**
     * Constructor.
     *
     * Creates a new iterator for the specified point map. The iterator
     * will iterate over the points ('keys') in the given map.
     *
     * map - The map which keys will be iterated over.
     * isEnd - If true an iterator that represents the end of the iteration
     *         will be returned.
     */
    EdgeDiscPtKeyIterator(const EdgeDiscPtMap<T> *map, bool isEnd = false) :
                                            m_map(map)

    {
        Tree &S = map->getTree();
        m_speciesTreeIterator = S.begin();
        if((m_speciesTreeIterator != m_map->getTree().end()) && !isEnd){
            m_currentPoint.first = *m_speciesTreeIterator;
            m_currentPoint.second = 0;
        }
        else {
            m_currentPoint.first = 0;
            m_currentPoint.second = 0;
        }
    }

    /**
     * Copy constructor
     *
     * Creates a new iterator with the same state as the provided iterator.
     *
     * other - The iterator whose state will be copied.
     */
    EdgeDiscPtKeyIterator(const EdgeDiscPtKeyIterator<T> &other) :
                                                        m_map(other.m_map)
    {
        m_speciesTreeIterator = other.m_speciesTreeIterator;
        m_currentPoint.first = other.m_currentPoint.first;
        m_currentPoint.second = other.m_currentPoint.second;
    }

    /**
     * Assignment operator
     *
     * The iterators become the same.
     *
     * other - The iterator whose state will be copied.
     */
    EdgeDiscPtKeyIterator<T> &
    operator=(const EdgeDiscPtKeyIterator& other)
    {
        if(&other != this) {
            m_map = other.m_map;
            m_currentPoint.first = other.m_currentPoint.first;
            m_currentPoint.second = other.m_currentPoint.second;
            m_speciesTreeIterator = other.m_speciesTreeIterator;
        }
    }

    /**
     * Comparrisson operator
     *
     * Returns true if the iterators are at the same element. If the iterators
     * are for different maps the result is undefined.
     *
     * other - Another iterator.
     */
    bool
    operator==(const EdgeDiscPtKeyIterator& other) const
    {
        return m_currentPoint.first == other.m_currentPoint.first &&
                m_currentPoint.second == m_currentPoint.second;
    }

    /**
     * Not equal operator
     *
     * Returns true if the iterators are at different elements. If the iterators
     * are for different maps the result is undefined.
     *
     * other - Another iterator.
     */
    bool
    operator!=(const EdgeDiscPtKeyIterator& other) const
    {
        return !EdgeDiscPtKeyIterator::operator==(other);
    }


    /**
     * Prefix operator
     *
     * Advances iterator one point. If the iterator is at the end a
     * std::out_of_bound exeception is thrown.
     */
    EdgeDiscPtKeyIterator<T>
    operator++()
    {
        /* Check that we are not at the end */
        if(m_currentPoint.first == 0){
            throw std::out_of_range("The end of the iterator has been reached.");
        }

        /* Check if there are more discretization points on this edge */
        unsigned int numberOfEdgePoints = m_map->getNoOfPts(m_currentPoint.first);
        if(m_currentPoint.second < (numberOfEdgePoints - 1)) {
            m_currentPoint.second++;
        }
        else {
            /* Check if are at the end of the species tree */
            m_speciesTreeIterator++;
            Tree::const_iterator treeEnd = m_map->getTree().end();
            if(m_speciesTreeIterator != treeEnd){
                m_currentPoint.first = *m_speciesTreeIterator;
                m_currentPoint.second = 0;
            }
            else {
                m_currentPoint.first = 0;
                m_currentPoint.second = 0;
            }
        }
        return *this;
    }

    /**
     * Postfix operator
     *
     * Advances iterator one point. If the iterator is at the end a
     * std::out_of_bound exeception is thrown.
     */
    EdgeDiscPtKeyIterator<T>
    operator++(int)
    {
        if(m_currentPoint.first == 0){
            throw std::out_of_range("The end of the iterator has been reached.");
        }
        return EdgeDiscPtKeyIterator::operator++();
    }

    /**
     * Returns a reference to the current element in the iterator.
     */
    const EdgeDiscretizer::Point &
    operator*() const
    {
        return m_currentPoint;
    }

    ~EdgeDiscPtKeyIterator()
    {
        // Nothing to clean up at the moment
    }

private:
    // The map we will iterate over
    const EdgeDiscPtMap<T> *m_map;

    // An iterator for the underlying species tree
    Tree::const_iterator m_speciesTreeIterator;

    // The current point of the iterator.
    EdgeDiscretizer::Point m_currentPoint;
};

}

#endif	/* _EDGEDISCPTMAPINNERITERATOR_HH */

