#include <algorithm>
#include <cctype>
#include <sstream>
#include <cassert>

#include "AnError.hh"
#include "Beep.hh"
#include "SequenceType.hh"
#include "LA_Vector.hh"

using namespace std;
using namespace beep;

DNA       beep::myDNA;
AminoAcid beep::myAminoAcid;
Codon     beep::myCodon;

/**
 * Protected constructor that defines the alphabet, 
 * used only by the base classes.
 */
SequenceType::SequenceType(const string &alpha, const string& amb)
: type(),
alphabet(alpha),
ambiguityAlphabet(amb),
leafLike(),
alphProb((1.0 - 0.001) / alphabetSize()),
ambiguityProb(0.001)
{
}

/**
 * Copy constructor.
 */
SequenceType::SequenceType(const SequenceType &dt)
: type(dt.type),
alphabet(dt.alphabet),
ambiguityAlphabet(dt.ambiguityAlphabet),
leafLike(dt.leafLike),
alphProb(dt.alphProb),
ambiguityProb(dt.ambiguityProb)
{
}

SequenceType::SequenceType()
{
}

/**
 * If you have a string description of a sequence type, the 
 * following method returns a statically allocated object of
 * that type.
 */
SequenceType
SequenceType::getSequenceType(string s)
{
	//TODO: This could use beep::capitalize(s), see Beep.hh /bens
	capitalize(s);
	assert(s.length() < MAXTYPELEN);
	if (s == "DNA")
	{
		return myDNA;
	}
	else if (s == "PROTEIN" || s == "AMINO ACID")
	{
		return myAminoAcid;
	}
	else if (s == "CODON")
	{
		return myCodon;
	}
	else
	{
		throw AnError("String not recognized as a sequence type", s);
	}
}


/**
 * Asignment operator.
 */
SequenceType& 
SequenceType::operator=(const SequenceType &dt)
{
	if (&dt != this)
	{
		type = dt.type;
		alphabet = dt.alphabet;
		ambiguityAlphabet = dt.ambiguityAlphabet;
		leafLike = dt.leafLike;
		ambiguityProb = dt.ambiguityProb;
		alphProb = dt.alphProb;
	}
	return *this;
}

// Equality operator
//-----------------------------------------------------------------
bool
SequenceType::operator==(const SequenceType& dt) const
{
	return type == dt.type;
}


//-----------------------------------------------------------------
//
// Interface
//
//-----------------------------------------------------------------


/*
 * Get a description of the type of the sequence. E.g. "DNA" for type DNA.
 */
string
SequenceType::getType() const
{
	assert(type.length() < MAXTYPELEN);  
	return type;
}

/** 
 * The number of states in the alphabet. For DNA this is 4. 
 * Alphabet integer coding for alternativ alphabet use
 * values above the size.
 */
unsigned int
SequenceType::alphabetSize() const
{
	return alphabet.size();
}

unsigned int
SequenceType::operator()(const char& state) const
{
	return char2uint(state);
}

// Returning a vector of likelihoods for a leaf with state i 
//-----------------------------------------------------------------
/**
 * Returning a vector of likelihoods for a leaf with character . 
 * The size of the vector equals the alphabet size and the 
 * likelihood is the probability that the leaf is in a specific
 * state.
 */
const beep::LA_Vector&
SequenceType::getLeafLike(const char& state) const
{
	return leafLike[char2uint(state)];
}

/**
 * Get the integer value for a specific state in the sequence 
 * alphabet.
 * Alternativ (ambiguity) alphabet use values>alphabetSize().
 * @param c A state from the alphabet.
 * @return the integer code
 */
unsigned int
SequenceType::char2uint(const char c) const
{
	char my_c = tolower(c);
	string::size_type ret = alphabet.find(my_c);
	if(ret != alphabet.npos)
	{
		return ret;
	}
	ret = ambiguityAlphabet.find(my_c);
	if(ret != ambiguityAlphabet.npos)
	{
		return ret + alphabetSize();
	}
	else
	{
	  ostringstream what;
	  what << "'" << c << "'";
	  throw AnError("Not a valid alphabet state", what.str(), 1);
	}
}

/**
 * Get the specific state in the sequence alphabet from the integer value.
 * Alternativ (ambiguity) alphabet use values>alphabetSize().
 * @param i the integer code.
 * @return A state from the alphabet
 */
char
SequenceType::uint2char(const unsigned i) const 
{
	if (i > alphabetSize())  
	{
		return ambiguityAlphabet[i - alphabetSize()];
	}
	else
	{
		return alphabet[i];
	}
}

/**
 * Convert a string with states in text format to a vector of state values.
 */
vector<unsigned>
SequenceType::stringTranslate(const string &s) const
{
	vector<unsigned> vec(s.size(), 4711);

	for (unsigned i=0; i < s.size(); i++)
	{
		vec[i] = char2uint(s[i]);
	}

	return vec;
}


/** 
 * How likely is it, roughly, that the given sequence came 
 * from the present alphabet? The alternative alphabet is
 * included.
 */
Probability
SequenceType::typeLikelihood(const string &s) const
{
	Probability p(1.0);        
	for (unsigned i = 0; i < s.length(); i++)
	{
		char c = tolower(s[i]);
		if (alphabet.find(c) < alphabet.npos)
		{
			p *= alphProb;
		}
		else if (ambiguityAlphabet.find(c) < ambiguityAlphabet.npos)
		{
			p *= ambiguityProb;
		}
		else
		{
			return Probability(0.0);
		}
	}
	return p;
}


/**
 * Check all the states in the given vector if they are 
 * valid within the alphabet. Alternative alphabet is not included.
 * 
 */
bool
SequenceType::checkValidity(const std::vector<unsigned int> &v) const
{
	for (unsigned i = 0; i< v.size(); i++)
	{
		//TODO: Should v be a vector of unsigneds? /bens
		if (v[i] >= alphabet.size())
		{
			return false;
		}
	}
	return true;
}

/**
 * Pretty print to an ostream.
 */
ostream& beep::operator<<(ostream& os, const SequenceType& dt)
{
	return os <<  dt.print();
}

/**
 * Pretty print the type.
 */
string
SequenceType::print() const
{
	ostringstream oss;
	oss << "SequenceType " << type;
	return oss.str();
}

/**
 * The alphabet has 4 states.
 * The alternative alphabet has 13 states and represents
 * different combination of the 4 basic nucleotides.
 * See http://www.hgu.mrc.ac.uk/Softdata/Misc/ambcode.htm for more info.
 */
DNA::DNA()
: SequenceType("acgt", "mrwsykvhdbxn-.") 
{
	type = "DNA";
	Real l[] = 
	{
	  // acgt
	  1.0,0,0,0, 
	  0,1.0,0,0,
	  0,0,1.0,0,
	  0,0,0,1.0,

	  // mrwsyk
	  0.5,0.5,0,0,
	  0.5,0,0.5,0,
	  0.5,0,0,0.5, 
	  0,0.5,0.5,0,
	  0,0.5,0,0.5,
	  0,0,0.5,0.5,

	  // vhdb
	  1.0/3,1.0/3,1.0/3,0,
	  1.0/3,1.0/3,0,1.0/3,
	  1.0/3,0,1.0/3,1.0/3,  
	  0,1.0/3,1.0/3,1.0/3,

	  // xn-.
	  0.25,0.25,0.25,0.25,
	  0.25,0.25,0.25,0.25,
	  0.25,0.25,0.25,0.25,
	  0.25,0.25,0.25,0.25
	};
	Real* lp = l;
	for(unsigned i = 0; i < alphabet.size() + ambiguityAlphabet.size(); i++)
	{
		beep::LA_Vector a(4, lp);
		lp += 4;
		leafLike.push_back(a);
	}
}


/**
 * Construct an AminoAcid type.
 * The alphabet has 20 states that represents the different aminoacids:
 * The alternative alphabet has 4 states and represents
 * different combination of the 20 basic AA.
 */
AminoAcid::AminoAcid()
: SequenceType("arndcqeghilkmfpstwyv", "bzx-.") 
{
	type = "Amino acid";
	Real l[] = 
	{
	  // arndcqeghilkmfpstwyv
	  1.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  
	  0,1.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  
	  0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  
	  0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  
	  0,0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	  0,0,0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	  0,0,0,0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	  0,0,0,0,0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,0,  
	  0,0,0,0,0,0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,  
	  0,0,0,0,0,0,0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,
	  0,0,0,0,0,0,0,0,0,0,1.0,0,0,0,0,0,0,0,0,0,
	  0,0,0,0,0,0,0,0,0,0,0,1.0,0,0,0,0,0,0,0,0,  
	  0,0,0,0,0,0,0,0,0,0,0,0,1.0,0,0,0,0,0,0,0,  
	  0,0,0,0,0,0,0,0,0,0,0,0,0,1.0,0,0,0,0,0,0,  
	  0,0,0,0,0,0,0,0,0,0,0,0,0,0,1.0,0,0,0,0,0, 
	  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1.0,0,0,0,0,  
	  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1.0,0,0,0,  
	  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1.0,0,0,  
	  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1.0,0,  
	  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1.0, 
	  
	  // b and z
	  0,0,0.5,0.5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	  0,0,0,0,0,0,0.5,0.5,0,0,0,0,0,0,0,0,0,0,0,0,
	  
	  // x
	  0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,
	  0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,

	  // -
	  0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,
	  0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,

	  // .
	  0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,
	  0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05
	};

	Real* lp = l;
	for(unsigned i = 0; i < alphabet.size() + ambiguityAlphabet.size(); i++)
	{
		beep::LA_Vector a(20, lp);
		lp += 20;
		leafLike.push_back(a);
	}

}

/**
 * Construct a Codon type.
 * Instead of using three letter symbols we invent a char representation.
 */
Codon::Codon()
: SequenceType("abcdefghijklmnopqrstuvwxyz_.,1234567890!#�%&/()=?+@�${[]}+?|<", "*") 
{
	type = "Codon";

	for(unsigned i = 0; i < 61; i++) 
	{
		Real v[61];
		for(unsigned j = 0; j < 61; j++) 
		{
			v[j] = 0.0;
		}
		v[i] = 1.0;
		beep::LA_Vector a(61, v);
		leafLike.push_back(a);
	}

	Real v[61];
	for(unsigned j = 0; j < 61; j++) 
	{
		v[j] = 1.0 / 61.0;
	}
	beep::LA_Vector a(61, v);
	leafLike.push_back(a);

}

/**
 * Get the integer value of the triplet.
 * Codon need to override default conversion 
 * due to the 3 letter coding.
 */
unsigned int
Codon::str2uint(const string &codon_str)
{
	assert(codon_str.length() == 3);

	// joelgs: Changed to be compatible with gcc 4.3.
	//char* codons[] = {
	string codons[] = {
			"AAA", "AAC", "AAG", "AAT",
			"ACA", "ACC", "ACG", "ACT",
			"AGA", "AGC", "AGG", "AGT",
			"ATA", "ATC", "ATG", "ATT",
			"CAA", "CAC", "CAG", "CAT",
			"CCA", "CCC", "CCG", "CCT",
			"CGA", "CGC", "CGG", "CGT",
			"CTA", "CTC", "CTG", "CTT",
			"GAA", "GAC", "GAG", "GAT",
			"GCA", "GCC", "GCG", "GCT",
			"GGA", "GGC", "GGG", "GGT",
			"GTA", "GTC", "GTG", "GTT",
			/*TAA*/ "TAC",/*TAG*/ "TAT",
			"TCA", "TCC", "TCG", "TCT",
			/*TGA*/ "TGC", "TGG", "TGT",
			"TTA", "TTC", "TTG", "TTT"
	};

	// joelgs: Changed to be compatible with gcc 4.3. NOT TESTED!
	string codonUC(codon_str);
	transform(codonUC.begin(), codonUC.end(), codonUC.begin(), (int(*)(int))toupper);
	for (int i=0; i<61; i++)
	{
		if (codonUC == codons[i])
		{
			return i;
		}
	}
	return alphabet.size() + 1;	// Ambiguity
}


/**
 * Get the triplet from the integer value.
 * Codon need to override default conversion 
 * due to the 3 letter coding.
 */
string
Codon::uint2str(const unsigned int &codon)
{
	assert(codon <= 61);

	// joelgs: Changed to be compatible with gcc 4.3.
	string codons[] = {		// Stop codons are commented out!
	//char* codons[] = {		// Stop codons are commented out!
			"AAA", "AAC", "AAG", "AAT",
			"ACA", "ACC", "ACG", "ACT",
			"AGA", "AGC", "AGG", "AGT",
			"ATA", "ATC", "ATG", "ATT",
			"CAA", "CAC", "CAG", "CAT",
			"CCA", "CCC", "CCG", "CCT",
			"CGA", "CGC", "CGG", "CGT",
			"CTA", "CTC", "CTG", "CTT",
			"GAA", "GAC", "GAG", "GAT",
			"GCA", "GCC", "GCG", "GCT",
			"GGA", "GGC", "GGG", "GGT",
			"GTA", "GTC", "GTG", "GTT",
			/*TAA*/ "TAC",/*TAG*/ "TAT",
			"TCA", "TCC", "TCG", "TCT",
			/*TGA*/ "TGC", "TGG", "TGT",
			"TTA", "TTC", "TTG", "TTT"
	};

	if(codon < 61)
	{
		return codons[codon];
	} 
	return string("NNN");   //Ambiguity
}
