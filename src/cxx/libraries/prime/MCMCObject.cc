//----------------------------------------------------------------------
//Author: Martin Linder, � the MCMC-club, SBC, all rights reserved

//----------------------------------------------------------------------

//----------------------------------------------------------------------
//
// Class MCMCObject
// This holds the output from MCMCModel::suggestNewState(). It
// consists of the "probability" of the new state and the proposal
// ratio between old and new state. The attributes are directly 
// accessible.
//
//----------------------------------------------------------------------


#include "MCMCObject.hh"

namespace beep
{
  MCMCObject::MCMCObject() 
    : stateProb( 0.0), propRatio(1.0){}
  MCMCObject::MCMCObject( Probability p, Probability r)
    : stateProb( p), propRatio( r){}
  MCMCObject::MCMCObject( const MCMCObject& MOb)
    : stateProb( MOb.stateProb), propRatio( MOb.propRatio){}
  MCMCObject::~MCMCObject(){}


}//end namespace beep


