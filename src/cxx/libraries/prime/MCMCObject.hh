//----------------------------------------------------------------------
//Author: Martin Linder, � the MCMC-club, SBC, all rights reserved

//----------------------------------------------------------------------

//----------------------------------------------------------------------
//
// Class MCMCObject
// This holds the output from MCMCModel::suggestNewState(). It
// consists of the "probability" of the new state and the proposal
// ratio between old and new state. The attributes are directly 
// accessible.
//
//----------------------------------------------------------------------


#ifndef MCMCOBJECT_HH
#define MCMCOBJECT_HH

#include "Probability.hh"

namespace beep
{
  class MCMCObject
  {
  public:
    MCMCObject();
//       : stateProb( 0.0), propRatio(1.0){};
    MCMCObject( Probability p, Probability r=1.0);
//       : stateProb( p), propRatio( r){};
    MCMCObject( const MCMCObject& MOb);
//       : stateProb( MOb.stateProb), propRatio( MOb.propRatio){}
    ~MCMCObject();
//     {};
    Probability stateProb;
    Probability propRatio;
  };
}//end namespace beep

#endif

