#include "EdgeDiscBDMCMC.hh"
#include "DiscTree.hh"
#include "MCMCObject.hh"

namespace beep
{

  using namespace std;

  EdgeDiscBDMCMC::EdgeDiscBDMCMC(MCMCModel& prior, 
				 EdgeDiscBDProbs* BDProbs,
				 const Real& suggestRatio) 
    : StdMCMCModel(prior, 2, "DupLoss", suggestRatio),
      m_BDProbs(BDProbs),
      m_fixRates(false),
      m_which(0),
      m_bAccPropCnt(0, 0),
      m_dAccPropCnt(0, 0)
  {
  }


  EdgeDiscBDMCMC::~EdgeDiscBDMCMC()
  {
  }


  void
  EdgeDiscBDMCMC::fixRates()
  {
    m_fixRates = true;
    n_params = 0;
    updateParamIdx();
  }


  MCMCObject
  EdgeDiscBDMCMC::suggestOwnState()
  {
    // Turn off notifications just to be sure.
    bool notifStat = m_BDProbs->setPertNotificationStatus(false);
	
    // Cache old values.
    m_BDProbs->cache();
	
    // Choose what parameter to perturb using 'paramIdx'.
    MCMCObject mcmcObj(1.0, 1.0);
    Real idx = paramIdx / paramIdxRatio;
	
    Real br = m_BDProbs->getBirthRate();
    Real dr = m_BDProbs->getDeathRate();

    // Equal chance of perturbing birth/death rate.
    // Update m_BDProbs when changing its rates.
    if (idx > 0.5)
      {
	// Perturb birth rate.
	m_which = 0;
	++m_bAccPropCnt.second;
	br = perturbTruncatedNormal(br, 0.5, StdMCMCModel::FIFTY_PCT,
				    Real_limits::min(), 
				    m_BDProbs->getMaxAllowedRate(), 
				    mcmcObj.propRatio);
      }
    else
      {
	// Perturb death rate.
	m_which = 1;
	++m_dAccPropCnt.second;
	dr = perturbTruncatedNormal(dr, 0.5, StdMCMCModel::FIFTY_PCT,
				    Real_limits::min(), 
				    m_BDProbs->getMaxAllowedRate(), 
				    mcmcObj.propRatio);
      }
    m_BDProbs->setRates(br, dr);
	
    // Notify listeners.
    m_BDProbs->setPertNotificationStatus(notifStat);
    PerturbationEvent pe(PerturbationEvent::PERTURBATION);
    m_BDProbs->notifyPertObservers(&pe);
	
    return mcmcObj;
  }


  void
  EdgeDiscBDMCMC::commitOwnState()
  {
    if (m_which == 0) { ++m_bAccPropCnt.first; }
    else              { ++m_dAccPropCnt.first; }
  }


  void
  EdgeDiscBDMCMC::discardOwnState()
  {
    // Turn off notifications just to be sure.
    bool notifStat = m_BDProbs->setPertNotificationStatus(false);
	
    // Restore cached values.
    m_BDProbs->restoreCache();
	
    // Notify listeners.
    m_BDProbs->setPertNotificationStatus(notifStat);
    PerturbationEvent pe(PerturbationEvent::RESTORATION);
    m_BDProbs->notifyPertObservers(&pe);
  }


  string
  EdgeDiscBDMCMC::ownStrRep() const
  {
    ostringstream oss;
    if (!m_fixRates)
      {
	oss << m_BDProbs->getBirthRate() 
	    << ";\t" 
	    << m_BDProbs->getDeathRate()
	    << ";\t";
      }
    return oss.str();
  }


  string
  EdgeDiscBDMCMC::ownHeader() const
  {
    ostringstream oss;
    if (!m_fixRates)
      {
	oss << "birthRate(float);\tdeathRate(float);\t";
      }
    return oss.str();
  }

  void 
  EdgeDiscBDMCMC::updateToExternalPerturb(Real newLambda, Real newMu)
  {
    if(newLambda != m_BDProbs->getBirthRate() ||
       newMu != m_BDProbs->getDeathRate())
      {
	if(newLambda > m_BDProbs->getMaxAllowedRate() || 
	   newMu > m_BDProbs->getMaxAllowedRate())
	  {
	    ostringstream oss;
	    oss << "Trying to set too high BD parameters: ("
		<< newLambda
		<< "," 
		<< newMu
		<< endl;
	    throw AnError(oss.str(), 1);
	  }
	// Turn off notifications just to be sure.
	bool notifStat = m_BDProbs->setPertNotificationStatus(false);
     	
	// Do the update      
	m_BDProbs->setRates(newLambda, newMu, true);
      
	// Notify listeners.
	m_BDProbs->setPertNotificationStatus(notifStat);
	PerturbationEvent pe(PerturbationEvent::PERTURBATION);
	m_BDProbs->notifyPertObservers(&pe);
      }
  }


  string
  EdgeDiscBDMCMC::getAcceptanceInfo() const
  {
    std::ostringstream oss;
    if (n_params > 0)
      {
	unsigned totAcc = m_bAccPropCnt.first + m_dAccPropCnt.first;
	unsigned totProp = m_bAccPropCnt.second + m_dAccPropCnt.second;
	oss << "# Acc. ratio for "
	    << name 
	    << ": "
	    << totAcc 
	    << " / " 
	    << totProp 
	    << " = "
	    << (totAcc / (Real) totProp) 
	    << "\n";
	oss << "#    of which birth param: "
	    << m_bAccPropCnt.first 
	    << " / " 
	    << m_bAccPropCnt.second 
	    << " = "
	    << (m_bAccPropCnt.first / (Real) m_bAccPropCnt.second) 
	    << endl
	    << "#    and death param:      "
	    << m_dAccPropCnt.first 
	    << " / " 
	    << m_dAccPropCnt.second 
	    << " = "
	    << (m_dAccPropCnt.first / (Real) m_dAccPropCnt.second) 
	    << "\n";
      }
    if (prior != NULL)
      {
	oss << prior->getAcceptanceInfo();
      }
    return oss.str();
  }


  ostream&
  operator<<(ostream &o, const EdgeDiscBDMCMC& A)
  {
    return (o << A.print());
  }


  string
  EdgeDiscBDMCMC::print() const
  {
    ostringstream oss;
    oss << name << ": Birth and death parameters ";
    if (!m_fixRates)
      {
	oss << "are estimated during MCMC.\n";
      }
    else
      {
	oss << "are fixed to " 
	    << m_BDProbs->getBirthRate()
	    << " and " 
	    << m_BDProbs->getDeathRate()	
	    << ", respectively.\n";
      }
    oss << StdMCMCModel::print();
    return oss.str();
  }

} //end namespace beep

