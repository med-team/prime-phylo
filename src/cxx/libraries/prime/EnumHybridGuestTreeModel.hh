#ifndef ENUMHYBRIDGUESTTREEMODEL_HH
#define ENUMHYBRIDGUESTTREEMODEL_HH

#include "GuestTreeModel.hh"
#include "ReconciledTreeTimeModel.hh"
#include "StrStrMap.hh"

namespace beep
{
  class HybridTree;
  //----------------------------------------------------------------
  //
  // class EnumHybridGuestTreeModel
  //! Currently brute force extension of GuesTreeModel to hybrid
  //! species networks. Such a network can be represented by its
  //! binary homeolog tree. Each guest tree can be replaced by a 
  //! set of identical trees remapped to the homeolog (this 
  //! enumeration is exponential in genes affected by hybridizations)
  //! Each enumerated guest tree can be modeled by a GuestTreeModel
  //! or (optionally) by a Reconciled TreeTime model.
  //
  //----------------------------------------------------------------
  class EnumHybridGuestTreeModel : public ProbabilityModel
  {
  public:
    //----------------------------------------------------------------
    //
    //! \name Construct/destruct/assign
    //"{
    //----------------------------------------------------------------
    EnumHybridGuestTreeModel(Tree& G_in, HybridTree& S_in, 
			 StrStrMap& gs_in, BirthDeathProbs& bdp_in);
    ~EnumHybridGuestTreeModel();
    EnumHybridGuestTreeModel(const EnumHybridGuestTreeModel& hgm);
    EnumHybridGuestTreeModel& operator=(const EnumHybridGuestTreeModel& hgm);
    //@}
    //----------------------------------------------------------------
    //
    // Interface
    // 
    //----------------------------------------------------------------
    void update();
    Probability calculateDataProbability();

    //! Use divergence times (fixed) in G as additional modeldata 
    void useDivergenceTimes();
    
    //----------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------
    //! Set up enumerated models
    void inits();
    //! Recursively create leaf-to-leaf map between hybrid network and 
    //! homeolog tree. Argument pos indicates what existing map should
    //! be used as the template for new maps
    //! PRE: There is at least one leaf-to-leaf map in maps!
    void fillMaps(Node* n, unsigned pos);
    
    //----------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------
    Tree* G;                     //!< Guest tree
    HybridTree* S;               //!< species network, contains homeolog tree
    StrStrMap gs;                //!< leaf-to-leaf map between G and S
    BirthDeathProbs* bdp;        //!< BD-process of homeolog tree
    std::vector<StrStrMap> maps; //!< map between S and homeolog tree

    bool useDivTimes;            //!< include G divergence times or not
    //!\name enumerated guest tree models w or w/o divergence times
    //@{
    std::vector<GuestTreeModel> models; 
    std::vector<ReconciledTreeTimeModel> divmodels;
    //@}
  };
  
} //end namespace beep
#endif
