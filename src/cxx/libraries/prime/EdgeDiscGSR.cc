#include <vector>
#include <cassert>
#include <math.h>

#include "AnError.hh"
#include "EdgeDiscPtMaps.hh"
#include "EdgeDiscBDProbs.hh"
#include "EdgeDiscGSR.hh"
#include "EdgeDiscPtMapIterator.hh"
#include "EdgeDiscTree.hh"
#include "Node.hh"
#include "Probability.hh"
#include "StrStrMap.hh"
#include "TreeIO.hh"
#include "TreePerturbationEvent.hh"
#include "PRNG.hh"
#include "EdgeDiscPtKeyIterator.hh"

namespace beep {

    using namespace std;

    EdgeDiscGSR::EdgeDiscGSR(Tree* G,
            EdgeDiscTree* DS,
            StrStrMap* GSMap,
            Density2P* edgeRateDF,
            EdgeDiscBDProbs* BDProbs,
            UnsignedVector* fixedGNodes) :
    EdgeWeightModel(),
    PerturbationObserver(),
    m_G(G),
    m_DS(DS),
    m_edgeRateDF(edgeRateDF),
    m_BDProbs(BDProbs),
    m_sigma(*G, DS->getTree(), *GSMap),
    m_GSMap(*GSMap),
    m_lengths(NULL),
    m_fixedGNodes(fixedGNodes),
    m_loLims(*G),
    m_upLims(*G),
    m_ats(*G, ProbabilityEdgeDiscPtMap(DS, 0.0)),
    p_ats(*G, ProbabilityEdgeDiscPtMap(DS, 0.0)),
    m_belows(*G, ProbabilityEdgeDiscPtMap(DS, 0.0)),
    m_at_bars(*G, ProbabilityEdgeDiscPtMap(DS, 0.0)),
    p_at_bars(*G, ProbabilityEdgeDiscPtMap(DS, 0.0)),
    m_calculatedAtAndBelow(false),
    m_calculatedAtBars(false),
    m_rateDelta(0.0)
    //		m_rateDelta(0.0)
    {       
        // Set G lengths, creating new if they don't exist.
        if (m_G->hasLengths()) {
            m_lengths = &(m_G->getLengths());
        } else {
            // G itself responsible for destruction of lengths.
            m_lengths = new RealVector(m_G->getNumberOfNodes(),
                    m_edgeRateDF->getMean());
            m_G->setLengths(*m_lengths, true);
        }

        for (unsigned i = 0; i < m_G->getNumberOfNodes() + 1; i++) {
            sum_ats.assign(i, Probability());
            sum_aboves.assign(i, Probability());
        }

        // Set rate delta to a quarter of smallest timestep (for instance).
        //m_rateDelta = DS.getMinTimestep() / 4;

        // Compute values to have something to start with.
        updateHelpStructures();
        updateProbsFull();

        // Register as listener on parameter holders we depend on.
        m_DS->getTree().addPertObserver(this);
        m_G->addPertObserver(this);
        m_BDProbs->addPertObserver(this);
        m_edgeRateDF->addPertObserver(this);
    }

    EdgeDiscGSR::~EdgeDiscGSR() {
    }

    Tree&
    EdgeDiscGSR::getTree() const {
        return (*m_G);
    }

    unsigned
    EdgeDiscGSR::nWeights() const {
        // Top time edge in G is never perturbed, but both of root's children.
        return (m_G->getNumberOfNodes() - 1);
    }

    RealVector&
    EdgeDiscGSR::getWeightVector() const {
        return *m_lengths;
    }

    Real
    EdgeDiscGSR::getWeight(const Node& node) const {
        return (*m_lengths)[node];
    }

    void
    EdgeDiscGSR::setWeight(const Real& weight, const Node& u) {
        (*m_lengths)[u] = weight;
    }

    void
    EdgeDiscGSR::getRange(Real& low, Real& high) {
        m_edgeRateDF->getRange(low, high);
    }

    string
    EdgeDiscGSR::print() const {
        ostringstream oss;
        oss << "The rate probabilities are modeled using a \n"
                << m_edgeRateDF->print();
        return oss.str();
    }

    Probability
    EdgeDiscGSR::getPlacementProbability(const Node *u, const Point *x) {
        /**
         * Note:
         * 'calculateDataProbability' does not actually give a probability,
         * it is a density.
         *
         * This is the probability Pr[u on x | G, l, theta, S] approximated on the
         * small intervall dt.
         */
        if (u->isLeaf()) {
            if (m_loLims[u] == *x) {
                return Probability(1.0);
            } else {
                return Probability(0.0);
            }
        }

        Probability placementDensity = getJointTreePlacementDensity(u, x);
        //Real timeStep = m_DS->getTimestep(x->first);

        Probability jointTreeLengthDensity = calculateDataProbability();

        //Probability placeProb = placementDensity * timeStep / jointTreeLengthDensity;

        Probability placeProb = placementDensity / jointTreeLengthDensity;

        return placeProb;
    }
    
    Probability
    EdgeDiscGSR::getPlacementProbabilityAlternate(const Node *u, const Point *x) {
        /**
         * Note:
         * 'calculateDataProbability' does not actually give a probability,
         * it is a density.
         *
         * This is the probability Pr[u on x | G, l, theta, S] approximated on the
         * small intervall dt.
         */
        if (u->isLeaf()) {
            if (m_loLims[u] == *x) {
                return Probability(1.0);
            } else {
                return Probability(0.0);
            }
        }

        if (!m_calculatedAtAndBelow) {
            updateProbsFull();
        }

        if (!m_calculatedAtBars) {
            calculateAtBarProbabilities();
            m_calculatedAtBars = true;
        }

        // Check if x < lolim
        if (!(m_DS->isAncestor(*x, m_loLims[u]))) {
            return 0.0;
        }

        //Speciation is only allowed if it is the lowest possible placement of u
        if (m_DS->isSpeciation(*x) && x->first->getNumber() != m_loLims[u].first->getNumber()) {
            return 0.0;
        }

        return p_at_bars[u](*x) * p_ats[u](*x);        
    }

    Probability
    EdgeDiscGSR::getJointTreePlacementDensity(const Node *u, const Point *x) {
        if (!m_calculatedAtAndBelow) {
            updateProbsFull();
        }

        if (!m_calculatedAtBars) {
            calculateAtBarProbabilities();
            m_calculatedAtBars = true;
        }

        // Check if x < lolim
        if (!(m_DS->isAncestor(*x, m_loLims[u]))) {
            return 0.0;
        }

        //Speciation is only allowed if it is the lowest possible placement of u
        if (m_DS->isSpeciation(*x) && x->first->getNumber() != m_loLims[u].first->getNumber()) {
            return 0.0;
        }

        return m_at_bars[u](*x) * m_ats[u](*x);
    }

    void
    EdgeDiscGSR::perturbationUpdate(const PerturbationObservable* sender,
            const PerturbationEvent* event) {
        // We have these scenarios:
        //
        // 1) Perturbation event, where sender==
        //  a) DS, lacking info on time change =>  Recreate discretization,
        //                                         full update.
        //  b) DS, detailed info on time change    =>  See a). 
        //                                    TODO: Optimize similar to e)!
        //  c) G, lacking info                     =>  Full update.
        //  d) G, detailed info on topology change =>  Full update (partial 
        //                                             update is tricky).
        //  e) G, detailed info on length change   =>  Partial update.
        //  f) BDProbs (birth/death rate change)   =>  Full update.
        //  g) rateDF (edge rate change)           =>  Full update.
        // 2) Restoration event, where sender==
        //  a) DS        =>  Restore dependent and internal caches.
        //  b) Any other =>  Restore internal cache.
        //
        // We always do a full update on help structures. HOWEVER: In 
        // case of a restoration, we do it afterwards to make sure we 
        // restore exactly cached parts. This latter update is not 
        // really necessary, but might cause confusion when debugging 
        // otherwise.

        //========== RESTORATION ==========
        if (event != NULL &&
                event->getType() == PerturbationEvent::RESTORATION) {
            if (sender == m_DS) {
                // Case 2a).
                m_BDProbs->restoreCache(); // Dependent.
                restoreCachedProbs(); // Internal.
                updateHelpStructures();
            } else {
                // Case 2b).
                restoreCachedProbs();
                updateHelpStructures();
            }
            return;
        }

        //========== PERTURBATION ==========

        static long iter = 0;

        // If 1b), 1d) or 1e), details will not be null.
        // Occasionally, we replace a partial update with a full one
        // to avoid accumulated numeric error drift.
        const TreePerturbationEvent* details =
                dynamic_cast<const TreePerturbationEvent*> (event);
        bool doFull = (details == NULL || iter % 20 == 0);
        updateHelpStructures();
        if (sender == m_DS) {
            // Case 1a), 1b) and 1c). Case 1b) could be broken out to be
            // optimized similar to 1e), i.e. only rediscretize and update
            // the affected part of host and guest tree.
            cacheProbs(NULL); // Internal.
            m_BDProbs->cache(); // Dependent.
            BeepVector<ProbabilityEdgeDiscPtMap>::iterator it;
            for (it = m_ats.begin(); it != m_ats.end(); ++it) {
                (*it).rediscretize(0.0);
            }
            for (it = m_belows.begin(); it != m_belows.end(); ++it) {
                (*it).rediscretize(0.0);
            }
            m_BDProbs->update(true);
            updateProbsFull();
        } else if (sender == m_G) {
            if (doFull || details->getTreePerturbationType() !=
                    TreePerturbationEvent::EDGE_WEIGHT) {
                // Case 1c) and 1d).
                cacheProbs(NULL);
                updateProbsFull();
            } else {
                // Case 1b).
                // Since we only allow partial updates at edge weight events 
                // (and the two root child edges are considered separate 
                // entities) we only consider the first root path. p2 may 
                // contain a node, but its weight should (and must) be untouched.
                const Node* p1;
                const Node* p2;
                details->getRootPaths(p1, p2);
                cacheProbs(p1);
                updateProbsPartial(p1);
            }
        } else {
            // 	if(sender == m_BDProbs)
            // 	  std::cerr  << "m_BDProbs" << endl;
            // 	if(sender == m_edgeRateDF)
            // 	  std::cerr  << "m_edgeRateDF" << endl;

            // Case 1f) and 1g).
            cacheProbs(NULL);
            updateProbsFull();
        }
        ++iter;
    }

    void
    EdgeDiscGSR::update() {
    }

    void
    EdgeDiscGSR::updateHelpStructures() {
        // Note: Order of invocation matters.
        m_sigma.update(*m_G, m_DS->getTree(), &m_GSMap);
        const Node* uRoot = m_G->getRootNode();
        updateLoLims(uRoot);
        updateUpLims(uRoot);
    }

    void
    EdgeDiscGSR::updateLoLims(const Node* u) {
        const Node* sigma = m_sigma[u];

        if (u->isLeaf()) {
            m_loLims[u] = Point(sigma, 0);
        } else {
            const Node* lc = u->getLeftChild();
            const Node* rc = u->getRightChild();

            // Update children first.
            updateLoLims(lc);
            updateLoLims(rc);

            Point lcLo = m_loLims[lc];
            Point rcLo = m_loLims[rc];

            // Set the lowest point at the left child to begin with.
            Point lo(lcLo.first, lcLo.second + 1);

            // Start at the left child.
            const Node* curr = lcLo.first;

            // Start at the lowest placement of the left child and move
            // on the path from u towards the root.
            while (curr != NULL) {
                // If we are at sigma(u) and we haven't marked it as
                // the lowest point of u, do so.
                if (curr == sigma && lo.first != sigma) {
                    lo = Point(sigma, 0);
                }

                // If we are at the same lowest edge as the right child.
                if (curr == rcLo.first) {
                    if (lo.first == curr) {
                        // u also has this edge as its lowest point.
                        lo.second = std::max(lo.second, rcLo.second + 1);
                    } else {
                        // The right child is higher up in the tree
                        // than the left child.
                        lo = Point(rcLo.first, rcLo.second + 1);
                    }
                }

                curr = curr->getParent();
            }

            // If we have moved outside edge's points, choose next pure disc. pt.
            if (lo.second == m_DS->getNoOfPts(lo.first)) {
                lo = Point(lo.first->getParent(), 1);
                if (lo.first == NULL) {
                    throw AnError("Insufficient no. of discretization points "
                            "(errtype 3).\n"
                            "Try using denser discretization for 1) "
                            "top edge, 2) remaining vertices.", 1);
                }
            }
            m_loLims[u] = lo;
        }
    }

    void
    EdgeDiscGSR::updateUpLims(const Node* u) {
        const Node* sigma = m_sigma[u];

        if (u->isLeaf()) {
            m_upLims[u] = Point(sigma, 0);
        } else if (m_fixedGNodes != NULL && (*m_fixedGNodes)[u]) {
            if (sigma == m_sigma[u->getLeftChild()] ||
                    sigma == m_sigma[u->getRightChild()]) {
                throw AnError("EdgeDiscGSR::updateUpLims: "
                        "Cannot fix node in EdgeDiscGSR.\n"
                        "Fixed node incompatible with reconciliation.", 1);
            }
            m_upLims[u] = Point(sigma, 0);
        } else if (u->isRoot()) {
            // We disallow placement on very tip.
            m_upLims[u] = m_DS->getTopmostPt();
            --(m_upLims[u].second);
        } else {
            // Normal case: set u's limit just beneath parent's limit.
            Point pLim = m_upLims[u->getParent()];
            if (pLim.second >= 2) {
                // There is a disc point available below the parent.
                m_upLims[u] = Point(pLim.first, pLim.second - 1);
            } else if (pLim.second == 1 && pLim.first == sigma) {
                // We place u at speciation since upper and lower limits 
                // here coincide with sigma.
                m_upLims[u] = Point(pLim.first, 0);
            } else {
                // We can't place u below its sigma.
                if (sigma == pLim.first) {
                    throw AnError("Insufficient no. of discretization points "
                            "(errtype 1).\n"
                            "Try using denser discretization for "
                            "1) top edge, 2) remaining vertices.", 1);
                }

                // No disc point available on this edge; find the edge below.
                const Node* n = sigma;
                while (n->getParent() != pLim.first) {
                    n = n->getParent();
                }
                m_upLims[u] = Point(n, m_DS->getNoOfPts(n) - 1);
            }
        }

        // Catch insufficient discretizations.
        if ((m_loLims[u].first == m_upLims[u].first &&
                m_loLims[u].second > m_upLims[u].second)
                || (m_loLims[u].first == m_upLims[u].first->getParent())) {
            throw AnError("Insufficient no. of discretization points"
                    "(errtype 2).\n"
                    "Try using denser dicretization for "
                    "1) top edge, 2) remaining vertices.", 1);
        }

        // Update children afterwards.
        if (!u->isLeaf()) {
            updateUpLims(u->getLeftChild());
            updateUpLims(u->getRightChild());
        }
    }

    Probability
    EdgeDiscGSR::calculateDataProbability() {
        return m_belows[m_G->getRootNode()].getTopmost();
        //cout << "Tree Density is " << m_belows[m_G->getRootNode()].getTopmost().p << endl;
        //cout << "Tree Density is " << m_belows[m_G->getRootNode()].getTopmost().val() << endl;
        // POTENTIAL VARIANT: We divide density with p11 for entire host
        // tree top edge so as to reduce its effect on BD rate scaling!
        //return (m_belows[m_G->getRootNode()].getTopmost() / m_BDProbs->getOneToOneProb(m_DS->getRootNode()));
    }

    void
    EdgeDiscGSR::updateProbsFull() {
        // Full recursive update.
        updateAtProbs(m_G->getRootNode(), true);
        calculateAtBarProbabilities();
        convertDensitiesToProbabilities();
    }

    void EdgeDiscGSR::convertDensitiesToProbabilities() {
        //for (unsigned i = 0; i < sum_ats.size(); i++)
            //cout << sum_ats[i] << " and tree probablility is " << calculateDataProbability() << endl;          

        // first for ats
        for (int i = 0; i < m_G->getNumberOfNodes(); i++) {
            Node *u = m_G->getNode(i);
            // Retrieve placement bounds. End is here also a valid placement.
            EdgeDiscTreeIterator x = m_DS->begin(m_loLims[u]);
            EdgeDiscTreeIterator xend = m_DS->begin(m_upLims[u]);
            double total = 0.0;
            // For each valid placement x.
            if (sum_ats[i] > 0) {
                while (true) {
                    //cout << "sum_ats is " << sum_ats[u->getNumber()] << endl;
                    Point xpt = x.getPt();
                    p_ats[u](x) = m_ats[u](x) / sum_ats[i];
                    total += p_ats[u](x).val();
                    //cout << "Node is " << i <<". Point is " << xpt.first->getNumber() << "," << xpt.second << endl;
                    //cout << "m_ats is " << m_ats[u](x).val() << " and p_ats is " << p_ats[u](x).val() << endl;
                    if (x == xend) {
                        break;
                    }
                    x.pp();
                }
                //cout << "Total is " << total << endl;
            }
        }

        // now for aboves
        // first for root
        EdgeDiscTreeIterator rootPlacement_it, rootPlacementEnd;
        rootPlacement_it = m_DS->begin(m_loLims[ m_G->getRootNode() ]);
        rootPlacementEnd = m_DS->end();
        Node *root = m_G->getRootNode();
        unsigned rootid = m_G->getRootNode()->getNumber();
        double total = 0.0;
        // Iterate through vertices in DS and calculate at bar for
        // the root at every point
        for (; rootPlacement_it != rootPlacementEnd; rootPlacement_it.pp()) {
            // Calculate at bar for root
            // abar[root][x] = p_11(origin, x)
            //Probability tmp = m_BDProbs->getOneToOneProb(rootPlacementEnd.getPt(), rootPlacement_it.getPt()); // p11(w(s'),x)
            //tmp *= calcRateDensity((*m_lengths)[root], (*m_DS)(rootPlacementEnd) - (*m_DS)(rootPlacement_it));
            //m_at_bars[root](rootPlacement_it) = tmp;                        
            p_at_bars[root](rootPlacement_it) = m_at_bars[root](rootPlacement_it) / sum_aboves[rootid];
            //cout << "m_above is " << m_at_bars[root](rootPlacement_it).val();
            //cout << " and p_above is " << p_at_bars[root](rootPlacement_it).val() << endl;
            total += p_at_bars[root](rootPlacement_it).val();
        }
        //cout << "Total is " << total << endl;
        
        //cout << "Tree is " << *m_G << endl;

        // now for non-root nodes
        for (int i = 0; i < m_G->getNumberOfNodes(); i++) {
            total = 0.0;
            Node *u = m_G->getNode(i);
            Node *parent = u->getParent();

            // Length of edge between u and its parent            
            unsigned nodeid = u->getNumber();
            if ( (u != m_G->getRootNode()) && (sum_aboves[nodeid] > 0)) {
                //cout << "node id is " << nodeid;
                //cout << " and sum_aboves for this is " << sum_aboves[nodeid].val() << endl;
                // Create an iterator that moves over all possible discretization
                // points on which u can be placed.
                EdgeDiscTreeIterator uPlacement_it, uPlacementEnd;
                uPlacement_it = m_DS->begin(m_loLims[u]);
                uPlacementEnd = m_DS->begin(m_upLims[u]);
                //cout << uPlacement_it.getPt() << " and " << uPlacementEnd.getPt() << endl;
                // Calculate at bar for all possible discretization points
                for (; m_DS->isAncestor(uPlacementEnd.getPt(), uPlacement_it.getPt()); uPlacement_it.pp()) {
                    Point uPlacement = uPlacement_it.getPt();                    

                    p_at_bars[u](uPlacement) = m_at_bars[u](uPlacement) / sum_aboves[u->getNumber()];
                        //cout << "m_above is " << m_at_bars[u](uPlacement).val();
                        //cout << " and p_above is " << p_at_bars[u](uPlacement).val() << endl;
                        total += p_at_bars[u](uPlacement).val();                    
                }
                //cout << "Total is " << total << endl;
            }
        }

        //cout << "done" << endl;
    }

    void
    EdgeDiscGSR::updateProbsPartial(const Node* rootPath) {
        // We currently only support partial updates for edge weight
        // changes. Therefore, we only consider partial caching along a
        // single root path.

        // Do a non-recursive update along the changed root path.
        while (rootPath != NULL) {
            updateAtProbs(rootPath, false);
            rootPath = rootPath->getParent();
        }
    }

    void
    EdgeDiscGSR::updateAtProbs(const Node* u, bool doRecurse) {
        if (u->isLeaf()) {
            m_ats[u](m_loLims[u]) = Probability(1.0);
        } else {
            const Node* lc = u->getLeftChild();
            const Node* rc = u->getRightChild();

            // Must do children first, if specified.
            if (doRecurse) {
                updateAtProbs(lc, true);
                updateAtProbs(rc, true);
            }

            // Retrieve placement bounds. End is here also a valid placement.
            EdgeDiscTreeIterator x = m_DS->begin(m_loLims[u]);
            EdgeDiscTreeIterator xend = m_DS->begin(m_upLims[u]);

            // For each valid placement x.
            while (true) {
                EdgeDiscretizer::Point xPt = x.getPt();
                m_ats[u](x) = m_belows[lc](x) * m_belows[rc](x) *
                        duplicationFactor(xPt);
                //cout << sum_ats[u->getNumber()]  << endl;
                sum_ats[u->getNumber()] += m_ats[u](x);
                if (x == xend) {
                    break;
                }
                x.pp();
            }
        }

        // Update planted tree probs. afterwards.
        updateBelowProbs(u);
    }

    void
    EdgeDiscGSR::updateBelowProbs(const Node* u) {
        // x refers to point of tip of planted tree G^u.
        // y refers to point where u is placed (strictly below x).

        Real l = (*m_lengths)[u];

        // Get limits for x (both are valid placements).
        EdgeDiscTreeIterator x, xend;
        if (u->isRoot()) {
            x = xend = m_DS->end();
        } else {
            x = m_DS->begin(m_loLims[u->getParent()]);
            xend = m_DS->begin(m_upLims[u->getParent()]);
        }

        // Get limits for y (both valid placements).
        EdgeDiscTreeIterator y;
        EdgeDiscTreeIterator yend = m_DS->begin(m_upLims[u]);

        // For each x.
        while (true) {
            // For each y strictly below x.
            m_belows[u](x) = Probability(0.0);
            for (y = m_DS->begin(m_loLims[u]); y < x; y.pp()) {
                Probability rateDens = u->isRoot() ?
                        1.0 : calcRateDensity(l, (*m_DS)(x) - (*m_DS)(y));
                // 	    m_belows[u](x) += rateDens * 
                // 	      m_BDProbs->getOneToOneProb(x, y) * m_ats[u](y);
                Probability tmp = 1.0;
                tmp *= rateDens;
                tmp *= m_BDProbs->getOneToOneProb(x, y);
                tmp *= m_ats[u](y);

                //if (!(u->isLeaf())) {
                //      tmp *= m_DS->getTimestep(y.getPt().first);
                //}

                /* This is the way Peter/Mattias did it */
                /*
                 * Probability rateDens = u->isRoot() ?
                        1.0 : calcRateDensity(l, m_DS(x) - m_DS(y));
                   Probability updateFactor = rateDens * m_BDProbs.getOneToOneProb(x, y) * m_ats[u](y);
                  
                 * // Here is IMPORTANT difference
                   if (!(u->isLeaf())) {
                       updateFactor *= m_DS.getTimestep(y.getPt().first);
                   }
                   m_belows[u](x) += updateFactor;
                 */

                m_belows[u](x) += tmp;

                if (y == yend) {
                    break;
                }
            }
            if (x == xend) {
                break;
            }
            x.pp();
        }
    }

    void
    EdgeDiscGSR::calculateAtBarProbabilities() {
        std::vector< std::vector< Node * > > levels;
        Node *root = m_G->getRootNode();
        createLevels(root, levels);

        // Calculate at bar probabilities for all root placements
        calculateRootAtBarProbability(root);

        // Initialize iterator for levels
        std::vector< std::vector<Node*> >::iterator level_it = levels.begin();
        level_it++; /* Skip the root placements */

        // Calculate at bar for each level from root's children downwards
        for (; level_it != levels.end(); level_it++) {
            // Initalize iterator over vertices in the current level
            std::vector<Node*>::iterator levelVertex_it;
            levelVertex_it = (*level_it).begin();

            // For each vertex in the level calculate abar
            for (; levelVertex_it != (*level_it).end(); levelVertex_it++) {
                calculateNodeAtBarProbability(*levelVertex_it);
            }
        }
    }

    void
    EdgeDiscGSR::createLevels(Node *root, std::vector< std::vector <Node *> > &levels) {
        // Create vector for the leaves
        std::vector< Node* > leaves;

        // Level 0 only contains the root.
        levels.push_back(std::vector<Node*>(1));
        levels[0][0] = root;

        // Create all other levels recursively
        int i = 1;
        while (true) {
            std::vector<Node*>::iterator level_it;

            // Create the current level
            levels.push_back(std::vector<Node*>());

            /* Create the current level by adding all non-leaf children of the
             * vertices in the previous level */
            for (level_it = levels[i - 1].begin(); level_it != levels[i - 1].end(); level_it++) {
                // Get the two children
                Node *leftChild = (*level_it)->getLeftChild();
                Node *rightChild = (*level_it)->getRightChild();

                // If the child isn't a leaf then add it to the current level
                // otherwise put it in the leaf level
                if (!leftChild->isLeaf()) {
                    levels[i].push_back(leftChild);
                } else {
                    leaves.push_back(leftChild);
                }

                if (!rightChild->isLeaf()) {
                    levels[i].push_back(rightChild);
                } else {
                    leaves.push_back(rightChild);
                }
            }

            // If the last created level is empty, we are done.
            if (levels[i].size() == 0) {
                // Set the last level to the leaves
                levels[i] = leaves;
                break;
            }

            i++;
        }
    }

    void
    EdgeDiscGSR::calculateRootAtBarProbability(const Node *root) {
        // Get lowest and highest point of placement for the root.
        EdgeDiscTreeIterator rootPlacement_it, rootPlacementEnd;
        rootPlacement_it = m_DS->begin(m_loLims[ m_G->getRootNode() ]);
        rootPlacementEnd = m_DS->end();
        unsigned rootid = m_G->getRootNode()->getNumber();

        // Iterate through vertices in DS and calculate at bar for
        // the root at every point
        for (; rootPlacement_it != rootPlacementEnd; rootPlacement_it.pp()) {
            // Calculate at bar for root
            // abar[root][x] = p_11(origin, x)
            //Probability tmp = m_BDProbs->getOneToOneProb(rootPlacementEnd.getPt(), rootPlacement_it.getPt()); // p11(w(s'),x)
            //tmp *= calcRateDensity((*m_lengths)[root], (*m_DS)(rootPlacementEnd) - (*m_DS)(rootPlacement_it));
            //m_at_bars[root](rootPlacement_it) = tmp;            
            m_at_bars[root](rootPlacement_it) =
                    m_BDProbs->getOneToOneProb(rootPlacementEnd.getPt(), rootPlacement_it.getPt());
            sum_aboves[rootid] += m_at_bars[root](rootPlacement_it);
        }
    }

    void
    EdgeDiscGSR::calculateNodeAtBarProbability(const Node *u) {
        Node *parent = u->getParent();

        // Length of edge between u and its parent
        Real edgeLength = (*m_lengths)[u];
        unsigned nodeid = u->getNumber();

        // Create an iterator that moves over all possible discretization
        // points on which u can be placed.
        EdgeDiscTreeIterator uPlacement_it, uPlacementEnd;
        uPlacement_it = m_DS->begin(m_loLims[u]);
        uPlacementEnd = m_DS->begin(m_upLims[u]);

        // Calculate at bar for all possible discretization points
        for (; m_DS->isAncestor(uPlacementEnd.getPt(), uPlacement_it.getPt()); uPlacement_it.pp()) {
            Point uPlacement = uPlacement_it.getPt();
            // Determine the placement of the parent
            EdgeDiscTreeIterator parentPlacement_it, parentPlacementEnd, lowestParentPlacement_it;
            lowestParentPlacement_it = m_DS->begin(m_loLims[parent]);
            parentPlacement_it = uPlacement_it;
            parentPlacement_it++; // Parent of u must be placed above u

            // Place the parent at the highest of loLims[parent(u)] and the
            // discretization point above uPlacement
            // ikram: does highest mean max of two? Apparently below if statement
            // puts the parentPlacement_it at "two" point above childPlacement
            if (parentPlacement_it != lowestParentPlacement_it && m_DS->isSpeciation(parentPlacement_it.getPt())) {
                parentPlacement_it++;
            }
            // ikram: why do we need above if is we gonna place parentPlacement_it
            // at lowestParentPlacement_it after all?
            if (m_DS->isProperAncestor(lowestParentPlacement_it.getPt(), parentPlacement_it.getPt())) {
                parentPlacement_it = lowestParentPlacement_it;
            }

            // The highest placement of the parent of u
            parentPlacementEnd = m_DS->begin(m_upLims[parent]);

            // Iterate through all valid placements of the parent of u
            m_at_bars[u](uPlacement) = Probability(0.0); // start probability value for current placement
            for (; m_DS->isAncestor(parentPlacementEnd.getPt(), parentPlacement_it.getPt()); parentPlacement_it.pp()) {
                // Placement of the parent of u
                Point parentPlacement = parentPlacement_it.getPt();

                Real edgeTime = (*m_DS)(parentPlacement) - (*m_DS)(uPlacement);
                Probability rateDens = calcRateDensity(edgeLength, edgeTime);

                // Calculate at bar probability         // def as per s(x,u) on page 40 of Mattias thesis
                m_at_bars[u](uPlacement) += duplicationFactor(parentPlacement) * // 1 or 2
                        m_BDProbs->getOneToOneProb(parentPlacement, uPlacement) * // p11(y,x)
                        rateDens * // p( <par(u),u>, y,x)
                        m_at_bars[parent](parentPlacement) * // (placement prob) or a-(y,par(u))
                        m_belows[ u->getSibling() ](parentPlacement); // * // (below prob) b^(y,v)                                
                // Error: timestep has already been calculated in duplicationFactor()
                // won't be done twice 
                // Remark: seems to working right now, so not disturbing temporarily
                //m_DS->getTimestep(parentPlacement.first); // delta-t
            }
            sum_aboves[nodeid] += m_at_bars[u](uPlacement);
        }
        
        //cout << "sum_aboves for node " << nodeid << " is " << sum_aboves[nodeid].val() << endl;
    }

    void
    EdgeDiscGSR::cacheProbs(const Node* rootPath) {
        clearAllCachedProbs();

        // This mirrors full and partial updates.
        // See partialUpdate() for more info.
        if (rootPath == NULL) {
            // Recursively store all values.
            cacheNodeProbs(m_G->getRootNode(), true);
        } else {
            // Store only values along path to root.
            while (rootPath != NULL) {
                cacheNodeProbs(rootPath, false);
                rootPath = rootPath->getParent();
            }
        }
    }

    void
    EdgeDiscGSR::cacheNodeProbs(const Node* u, bool doRecurse) {
        m_belows[u].cachePath(m_sigma[u]);
        if (!u->isLeaf()) {
            // Note: Leaf's "ats" are never changed.
            m_ats[u].cachePath(m_sigma[u]);
            if (doRecurse) {
                cacheNodeProbs(u->getLeftChild(), true);
                cacheNodeProbs(u->getRightChild(), true);
            }
        }
    }

    void
    EdgeDiscGSR::restoreCachedProbs() {
        for (Tree::const_iterator it = m_G->begin(); it != m_G->end(); ++it) {
            m_ats[*it].restoreCachePath(m_sigma[*it]);
            m_belows[*it].restoreCachePath(m_sigma[*it]);
        }
    }

    void
    EdgeDiscGSR::clearAllCachedProbs() {
        for (Tree::const_iterator it = m_G->begin(); it != m_G->end(); ++it) {
            m_ats[*it].invalidateCache();
            m_belows[*it].invalidateCache();
        }
    }

    Probability
    EdgeDiscGSR::getTotalPlacementDensity(const Node *u) {
        EdgeDiscTreeIterator x, xend;

        x = m_DS->begin(m_loLims[u]);
        xend = m_DS->begin(m_upLims[u]);
        
        cout << "For node " << u->getNumber() << " loLim is (" << x.getPt().first->getNumber() << "," << x.getPt().second << ")";
        cout << " and upLim is (" << xend.getPt().first->getNumber() << "," << xend.getPt().second << ")" << endl; 

        Probability totalDensity(0.0);
        Probability totalProbability(0.0);

        /* Sum probability over each placement for u */
        for (; m_DS->isAncestor(xend.getPt(), x.getPt()); x.pp()) {
            Point xPoint = x.getPt();

            Real timeStep = u->isLeaf() ? 1.0 : m_DS->getTimestep(xPoint.first);

            //totalDensity += getJointTreePlacementDensity(u, &xPoint) * timeStep;
            totalDensity += getJointTreePlacementDensity(u, &xPoint);
            //cout << "At (u,x)=(" << u->getNumber()<<",[" << xPoint.first->getNumber() << "," << xPoint.second << "])";
            //cout << " at_bar is " << m_at_bars[u](xPoint) << "," << m_ats[u](xPoint);
            //cout << " = " << m_at_bars[u](xPoint).val() * m_ats[u](xPoint).val() << endl;
            totalProbability = getPlacementProbability(u, &xPoint);
            cout << "Placement \"PROBABILITY\" for node " << u->getNumber();
            cout << " at (" << xPoint.first->getNumber() << "," << xPoint.second << ") is " << totalProbability.val() << endl;
        }

        cout << "Total Placement \"PROBABILITY\" for node " << u->getNumber() << " is " << totalDensity << endl;

        return totalDensity;
    }

    string
    EdgeDiscGSR::getDebugInfo(bool inclAts, bool inclBelows, bool inclAbove) {
        ostringstream oss;
        Tree::const_iterator it;

        for (it = m_G->begin(); it != m_G->end(); ++it) {
            const Node* u = (*it);
            oss
                    << "# " << u->getNumber() << '\t' << '\t'
                    << m_sigma[u]->getNumber() << '\t'
                    << '(' << m_loLims[u].first->getNumber() << ','
                    << m_loLims[u].second << ")\t"
                    << '(' << m_upLims[u].first->getNumber() << ','
                    << m_upLims[u].second << ")\t"
                    << endl;
        }
        if (inclAts) {
            oss << "# AT-PROBABILITIES:" << endl;
            for (it = m_G->begin(); it != m_G->end(); ++it) {
                oss << "# Node " << (*it)->getNumber() << ':' << endl
                        << m_ats[*it].printPath(m_sigma[*it]);
            }
        }
        if (inclBelows) {
            oss << "# BELOW-PROBABILITIES:" << endl;
            for (it = m_G->begin(); it != m_G->end(); ++it) {
                oss << "# Node " << (*it)->getNumber() << ':' << endl
                        << m_belows[*it].printPath(m_sigma[*it]);
            }
        }
        if (!m_calculatedAtBars) {
            calculateAtBarProbabilities();
            m_calculatedAtBars = true;
        }
        if (inclBelows) {
            oss << "\n\n# AT_BAR-PROBABILITIES:" << endl;
            for (it = m_G->begin(); it != m_G->end(); ++it) {
                oss << "# Node " << (*it)->getNumber() << ':' << endl
                        << m_at_bars[*it].printPath(m_sigma[*it]);
            }
        }
        return oss.str();
    }

    string
    EdgeDiscGSR::getRootProbDebugInfo() {
        ostringstream oss;
        const Node* u = m_G->getRootNode();
        EdgeDiscTreeIterator top = m_DS->begin(m_DS->getTopmostPt());
        EdgeDiscTreeIterator x = m_DS->begin(m_loLims[u]);
        while (x != top) {
            oss << (m_BDProbs->getOneToOneProb(top, x) * m_ats[u](x)) << " ";
            x = x.pp();
        }
        return oss.str();
    }


} // end namespace beep
