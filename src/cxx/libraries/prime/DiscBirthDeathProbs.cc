#include <cassert>
#include <cmath>

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "DiscBirthDeathProbs.hh"

namespace beep
{

using namespace std;

DiscBirthDeathProbs::DiscBirthDeathProbs(const DiscTree& DS, Real birthRate, Real deathRate) :
	PerturbationObservable(),
	m_DS(DS),
	m_birthRate(birthRate),
	m_deathRate(deathRate),
	m_BD_const(DS.getOrigTree()),
	m_BD_zero(DS.getOrigTree()),
	m_Pt(),
	m_ut(),
	m_base_BD_const(),
	m_base_BD_zero()
{
	if (birthRate <= 0)
	{ throw AnError("Cannot have zero or negative birth rate in DiscBirthDeathProbs."); }
	if (deathRate <= 0) 
	{ throw AnError("Cannot have zero or negative death rate in DiscBirthDeathProbs."); }
	
	// Create member vectors.
	for (unsigned i=0; i<m_BD_const.size(); ++i)
	{
		const Node* n = m_DS.getOrigNode(i);
		unsigned noOfPts = m_DS.getNoOfPtsOnEdge(n) + 1;
		m_BD_const[n] = new vector<Probability>();
		m_BD_const[n]->reserve(noOfPts);
	}
	m_base_BD_const.reserve(m_DS.getNoOfIvs() + 1);
	update();
}


DiscBirthDeathProbs::DiscBirthDeathProbs(const DiscBirthDeathProbs& dbdp) :
	m_DS(dbdp.m_DS),
	m_birthRate(dbdp.m_birthRate),
	m_deathRate(dbdp.m_deathRate),
	m_BD_const(dbdp.m_DS.getOrigTree()),
	m_BD_zero(dbdp.m_DS.getOrigTree()),
	m_Pt(dbdp.m_Pt),
	m_ut(dbdp.m_ut),
	m_base_BD_const(),
	m_base_BD_zero()
{
	// Deep-copy member vectors.
	for (unsigned i=0; i<m_BD_const.size(); ++i)
	{
		const Node* n = m_DS.getOrigNode(i);
		m_BD_const[n]->assign(dbdp.m_BD_const[n]->begin(), dbdp.m_BD_const[n]->end());
		m_BD_zero[n] = dbdp.m_BD_zero[n];
	}
}


DiscBirthDeathProbs::~DiscBirthDeathProbs()
{
	for (unsigned i=m_BD_const.size(); i>0; --i)
	{
		delete (m_BD_const[i-1]);
	}
}


void DiscBirthDeathProbs::update()
{
	calcPtAndUt(m_DS.getTimestep(), m_Pt, m_ut);
	m_base_BD_const.assign(1, Probability(1.0)); // Set a start value.
	m_base_BD_zero = Probability(0.0);           // Set a start value.
	calcBDProbs(m_DS.getOrigRootNode());
}


Real DiscBirthDeathProbs::getBirthRate() const
{
	return m_birthRate;
}


Real DiscBirthDeathProbs::getDeathRate() const
{
	return m_deathRate;
}


void DiscBirthDeathProbs::getRates(Real& birthRate, Real& deathRate) const
{
	birthRate = m_birthRate;
	deathRate = m_deathRate;
}


void DiscBirthDeathProbs::setRates(Real newBirthRate, Real newDeathRate, bool doUpdate)
{
	if (newBirthRate <= 0)
	{ throw AnError("Cannot have zero or negative birth rate in DiscBirthDeathProbs."); }
	if (newDeathRate <= 0)
	{ throw AnError("Cannot have zero or negative death rate in DiscBirthDeathProbs."); }

	m_birthRate = newBirthRate;
	m_deathRate = newDeathRate;
	
	if (doUpdate) { update(); }
}


const Tree& DiscBirthDeathProbs::getTree() const
{
	return m_DS.getOrigTree();
}


string DiscBirthDeathProbs::getTreeName() const
{
	return m_DS.getOrigTree().getName();
}


Real DiscBirthDeathProbs::getRootToLeafTime() const
{
	return m_DS.getRootToLeafTime();
}
	
	
Real DiscBirthDeathProbs::getTopTime() const
{
	return m_DS.getTopTime();
}
	
	
Real DiscBirthDeathProbs::getTopToLeafTime() const
{
	return m_DS.getTopToLeafTime();
}


Probability DiscBirthDeathProbs::getConstLinValForEdge(const Node* Y) const
{
	return m_BD_const[Y]->back();
}


Probability DiscBirthDeathProbs::getConstLinValForSeg(DiscTree::Point y) const
{
	unsigned offset = m_DS.getRelativeIndex(y);
	assert(offset < m_BD_const[y.second]->size()-1);
	return ((*m_BD_const[y.second])[offset + 1] / (*m_BD_const[y.second])[offset]);
}


Probability DiscBirthDeathProbs::getConstLinValForSeg(const Node* Y) const
{
	assert(m_BD_const[Y]->size() >= 2);
	return (*m_BD_const[Y])[1];
}


const Node* DiscBirthDeathProbs::getConstLinValsForPath(vector<Probability>& lins,
		DiscTree::Point x, DiscTree::Point y, bool singleLin) const
{
	assert(x.first > y.first);
	if (singleLin)
	{
		// Add prob. for edge segment below x and exclude edge segment below y.
		Probability lin = (*m_BD_const[x.second])[m_DS.getRelativeIndex(x)] /
			(*m_BD_const[y.second])[m_DS.getRelativeIndex(y)];
		assert(lin <= Probability(1.0));

		// Add probs. (incl. loss) for all edges in between.
		const Node* nodeBelow = y.second;
		while (y.second != x.second)
		{
			lin *= m_BD_const[y.second]->back() * m_BD_zero[y.second->getSibling()];
			nodeBelow = y.second;
			y.second = y.second->getParent();
		}
		
		// We don't want to include loss where the lineage ends at a node.
		if (x.first == m_DS.getGridIndex(x.second))
		{
			lin /= m_BD_zero[nodeBelow->getSibling()];
			lins.push_back(lin);
			return nodeBelow;
		}
		lins.push_back(lin);
		assert(lin <= Probability(1.0));
		return y.second;
	}
	else
	{
		// Add all segments first, multiply together later.
		lins.reserve(m_DS.getNoOfIvs() - y.first);
		while (true)
		{
			lins.push_back(getConstLinValForSeg(y));
			y.first++;
			if (y.first == x.first) { break; }
			
			// End of edge. Add loss and carry on with next edge.
			if (m_DS.isAboveEdge(y.first, y.second))
			{
				lins.back() *= m_BD_zero[y.second->getSibling()];
				y.second = y.second->getParent();
			}
		}
		
		// Multiply together the probs. in a Horner's rule-like manner.
		for (unsigned i=lins.size()-1; i>0; --i)
		{
			lins[i-1] *= lins[i];
			assert(lins[i-1] <= Probability(1.0));
		}
		return y.second;
	}
}


Probability DiscBirthDeathProbs::getLossVal(const Node* Y) const
{
	return m_BD_zero[Y];
}


void DiscBirthDeathProbs::calcBDProbs(const Node* Y)
{
	// Compute children first.
	if (!Y->isLeaf())
	{
		calcBDProbs(Y->getLeftChild());
		calcBDProbs(Y->getRightChild());
	}
	
	vector<Probability>* probs = m_BD_const[Y]; 
	probs->clear();
	unsigned noOfPts = Y->isRoot() ? m_DS.getNoOfPtsOnEdge(Y) : m_DS.getNoOfPtsOnEdge(Y) + 1;
	if (Y->isLeaf())
	{
		// Copy precalculated values, then calculate loss value explicitly.
		copyLeafBProbs(*probs, noOfPts);
		Probability Pt, ut;
		calcPtAndUt(m_DS.getEdgeTime(Y), Pt, ut);
		m_BD_zero[Y] = 1.0 - Pt;
	}
	else
	{
		// By definition, probability from first point to itself is 1.
		probs->push_back(Probability(1.0));
		
		// D equals Pr[going extinct over edge or below].
		Probability D = m_BD_zero[Y->getLeftChild()] * m_BD_zero[Y->getRightChild()];
		
		// Calculate prob. for remaining segments down to node. 
		for (unsigned i=1; i<noOfPts; ++i)
		{
			probs->push_back(probs->back() * m_Pt * (1.0 - m_ut) /
					((1.0 - m_ut * D) * (1.0 - m_ut * D)));
			D = 1.0 - m_Pt * (1.0 - D) / (1.0 - m_ut * D);
		}
		m_BD_zero[Y] = D;
	}
	assert(probs->front() <= Probability(1.0));
	assert(probs->back() <= Probability(1.0));
	assert(probs->front() >= probs->back());
}


void DiscBirthDeathProbs::copyLeafBProbs(vector<Probability>& leafProbs, unsigned noOfPts)
{	
	// Make sure the base vector is long enough.
	// It always contains at least one element already.
	if (m_base_BD_const.size() < noOfPts)
	{
		for (unsigned i=m_base_BD_const.size(); i<noOfPts; ++i)
		{
			m_base_BD_const.push_back(m_base_BD_const.back() * m_Pt * (1.0 - m_ut) /
					((1.0 - m_ut * m_base_BD_zero) * (1.0 - m_ut * m_base_BD_zero)));
			m_base_BD_zero = 1.0 - m_Pt * (1.0 - m_base_BD_zero) / (1.0 - m_ut * m_base_BD_zero);
		}
	}
	
	// Copy the requested number of probs. from the base vector into the other.
	leafProbs.assign(m_base_BD_const.begin(), m_base_BD_const.begin() + noOfPts);
}


void DiscBirthDeathProbs::calcPtAndUt(Real t, Probability& Pt, Probability& ut) const
{
	if (m_deathRate == m_birthRate)
	{
		Probability denom(1.0 + (m_deathRate * t));
		Pt = Probability(1.0) / denom;
		ut = Probability(m_deathRate * t) / denom;
	}
	else if (m_deathRate == 0)	//TODO: Not allowed at the moment (see constructor). Why?
	{
		Pt = Probability(1.0);
		ut = Probability(1.0 - exp(-m_birthRate * t));
	}
	else
	{
		Real dbDiff = m_deathRate - m_birthRate;
		Probability E = Probability(exp(dbDiff * t));
		Probability denom = Probability(m_birthRate - (m_deathRate * E));
		Pt = -dbDiff / denom; 
		ut = Probability(m_birthRate * (1.0 - E)) / denom;
	}
}


void DiscBirthDeathProbs::debugInfo(bool printNodeInfo) const
{
	const Tree& tree = m_DS.getOrigTree();
	unsigned noOfNodes = tree.getNumberOfNodes();
	cerr << "# ============================== DiscBirthDeathProbs ==================================" << endl;
	cerr << "# Birth rate: " << m_birthRate << endl;
	cerr << "# Death rate: " << m_deathRate << endl;
	cerr << "# P(t) for timestep: " << m_Pt.val() << endl;
	cerr << "# u_t for timestep: " << m_ut.val() << endl;
	if (printNodeInfo)
	{
		Real tt = m_DS.getTopTime();
		BirthDeathProbs bdp(m_DS.getOrigTree(), m_birthRate, m_deathRate, &tt);
		cerr << "# Node no.:\tConst lin. val:\t(actual:)\tLoss val:\t(actual:)" << endl;
		for (unsigned i=0; i<noOfNodes; ++i)
		{
			const Node* n = m_DS.getOrigNode(i);
			cerr << "# "
				<< i << '\t'
				<< getConstLinValForEdge(n).val() << '\t'
				<< '(' << bdp.partialProbOfCopies(*n, 1).val() << ")\t"
				<< getLossVal(n).val() << '\t'
				<< '(' << bdp.partialProbOfCopies(*n, 0).val() << ")\t"
				<< endl;
		}
	}
	cerr << "# =====================================================================================" << endl;
}

} // end namespace beep.

