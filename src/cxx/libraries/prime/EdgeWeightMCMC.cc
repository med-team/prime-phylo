#include "EdgeWeightMCMC.hh"

#include "AnError.hh"
#include "MCMCObject.hh"
#include "TreeIO.hh"
#include "TreePerturbationEvent.hh"

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Class EdgeWeightMCMC
  // Interface base class for MCMC-shells of EdgeRateModel subclasses.
  //
  //----------------------------------------------------------------------
  EdgeWeightMCMC::EdgeWeightMCMC(MCMCModel& prior, EdgeWeightModel& ewm,
				 Real suggestRatio, bool useTruncNormalSugg) :
    StdMCMCModel(prior, ewm.nWeights(), 
		 ewm.getTree().getName()+"_weights", 
		 suggestRatio),
    model(&ewm),
    oldWeight(0),
    idxWeight(0),
    // suggestion_variance(1.0)//!\todo{how handle this?}
    suggestion_variance(0.1),//!\todo{how handle this?}  
    // joelgs: This might not be so bad, since
    // the sugg. var. scales roughly with
    // the length being perturbed. It's probably
    // more dangerous to set a dynamic value.
    detailedNotifInfo(false),
    accPropCnt(0, 0),
    useTruncatedNormalSugg(useTruncNormalSugg)
  {
  }

  EdgeWeightMCMC::EdgeWeightMCMC(MCMCModel& prior, EdgeWeightModel& ewm,
				 const string& name_in, Real suggestRatio, 
				 bool useTruncNormalSugg) :
    StdMCMCModel(prior, ewm.nWeights(), "EdgeWeights", suggestRatio),
    model(&ewm),
    oldWeight(0),
    idxWeight(0),
    suggestion_variance(0.1),//!\todo{how handle this?}  
    // joelgs: This might not be so bad, since
    // the sugg. var. scales roughly with
    // the length being perturbed. It's probably
    // more dangerous to set a dynamic value.
    detailedNotifInfo(false),
    accPropCnt(0, 0),
    useTruncatedNormalSugg(useTruncNormalSugg)
  {
    name = name_in;
  }

  EdgeWeightMCMC::EdgeWeightMCMC(const EdgeWeightMCMC& ewm) :
    StdMCMCModel(ewm), 
    model(ewm.model),
    oldWeight(ewm.oldWeight),
    idxWeight(ewm.idxWeight),
    suggestion_variance(ewm.suggestion_variance),
    detailedNotifInfo(ewm.detailedNotifInfo),
    accPropCnt(ewm.accPropCnt),
    useTruncatedNormalSugg(ewm.useTruncatedNormalSugg)
  {
  }

  EdgeWeightMCMC& 
  EdgeWeightMCMC::operator=(const EdgeWeightMCMC& ewm)
  {
    if(&ewm != this)
      {
	StdMCMCModel::operator=(ewm);
	model = ewm.model;
	oldWeight = ewm.oldWeight;
	idxWeight = ewm.idxWeight;
	suggestion_variance = ewm.suggestion_variance;
	detailedNotifInfo = ewm.detailedNotifInfo;
	accPropCnt = ewm.accPropCnt;
	useTruncatedNormalSugg = ewm.useTruncatedNormalSugg;
      }
    return *this;
  }

  EdgeWeightMCMC::~EdgeWeightMCMC()
  {
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------

  // fix parameters
  //----------------------------------------------------------------------
  void
  EdgeWeightMCMC::fixWeights()
  {
    n_params = 0;
    updateParamIdx();
  }
  
  void EdgeWeightMCMC::showCurrentTree() {
      RealVector t = model->getTree().getLengths();
        for (int i = 0; i < t.size(); i++)
            cout << t[i] << "\t";
      cout << endl;
    }

  //! Generates a random sample of edgeRates. Base version
  // Works for, e.g., ConstRateModel
  //----------------------------------------------------------------------
  void 
  EdgeWeightMCMC::generateWeights(bool setRootRate, Real newWeight)
  {
    const Tree& T = model->getTree();
	
    // Turn off notifications just to be sure.
    bool notifStat = T.setPertNotificationStatus(false);
	
    for(unsigned i = 0; i < T.getNumberOfNodes(); i++)
      {
	Node* n = T.getNode(i);
	//!\todo{model->sampleValue(R.genrand_real3());}
	if (!n->isRoot() || setRootRate)
	  {
	    model->setWeight(newWeight, *n);
	  }
      }
#ifdef PERTURBED_NODE
    //    T.perturbedNode(idxWeight);
    T.perturbedNode(T.getRootNode());
#endif
	
    // Notify listeners. Don't specify details, since whole tree has changed.
    T.setPertNotificationStatus(notifStat);
    PerturbationEvent pe(PerturbationEvent::PERTURBATION);
    T.notifyPertObservers(&pe);
  };


  void
  EdgeWeightMCMC::setDetailedNotification(bool doSendDetails)
  {
    detailedNotifInfo = doSendDetails;
  }

  //----------------------------------------------------------------------
  // Inherited from StdMCMCModel
  //----------------------------------------------------------------------

  // Perturbs with equal probability either the mean or the variance of 
  // underlying density or one of the edge rates.
  MCMCObject  
  EdgeWeightMCMC::suggestOwnState()
  {
    assert(n_params > 0);

    ++accPropCnt.second;

    // Create return object
    MCMCObject MOb(1.0, 1.0);

    MOb.propRatio = perturbWeight(); 

    // Since any perturbation might have changed likelihood we must update it
    MOb.stateProb = updateDataProbability();

#ifdef PERTURBED_NODE
    //     model->getTree().perturbedNode(idxWeight);
#endif

    return MOb;
  }

  void  
  EdgeWeightMCMC::commitOwnState()
  {
    ++accPropCnt.first;
  }

  void  
  EdgeWeightMCMC::discardOwnState()
  {
    const Tree& T = model->getTree();
	
    // Turn off notifications just to be sure.
    bool notifStat = T.setPertNotificationStatus(false);
	
    model->setWeight(oldWeight, *idxWeight);

#ifdef PERTURBED_NODE
    // If possible, only recalculate substlike for perturbed part of tree
    // If other node also perturbed or if idx_node is a child of root (since
    // root's children share same rate) recalculate substlike for whole tree
    if(T.perturbedNode() || idxWeight->getParent()->isRoot()) 
      {
	T.perturbedNode(T.getRootNode());
      }
    else // It suffices to calculate above idx_node
      {
	T.perturbedNode(idxWeight);
      }
#endif
    // #ifdef PERTURBED_NODE
    //     model->getTree().perturbedNode(idxWeight);
    // #endif
	
    // Notify listeners.
    T.setPertNotificationStatus(notifStat);
    PerturbationEvent pe(PerturbationEvent::RESTORATION);
    T.notifyPertObservers(&pe);
  };  

  std::string  
  EdgeWeightMCMC::ownStrRep() const
  {
    std::ostringstream oss;
    oss << model->calculateDataProbability() << ";\t";
    if(n_params > 0)
      {
	oss << weightsStr();
      }
    if (model->hasOwnStatus())
      {
	oss << model->ownStatusStrRep();
      }
    return oss.str();
  };

  std::string  
  EdgeWeightMCMC::ownHeader() const
  {
    std::ostringstream oss;
    oss << "EdgeWeightLike(logfloat);" << "\t";
    if (n_params > 0)
      {
	oss << weightsHeader();
      }
    if (model->hasOwnStatus())
      {
	oss << model->ownStatusHeader();
      }
    return oss.str();
  };


  string
  EdgeWeightMCMC::getAcceptanceInfo() const
  {
    std::ostringstream oss;
    if (n_params > 0)
      {
	oss << "# Acc. ratio for " << name << ": "
	    << accPropCnt.first << " / " << accPropCnt.second << " = "
	    << (accPropCnt.first / (Real) accPropCnt.second) << endl;
      }
    if (prior != NULL)
      {
	oss << prior->getAcceptanceInfo();
      }
    return oss.str();
  }


  Probability  
  EdgeWeightMCMC::updateDataProbability()
  {
    model->update();
    return model->calculateDataProbability();
  };


  //----------------------------------------------------------------------
  // I/O
  //----------------------------------------------------------------------
  std::string  
  EdgeWeightMCMC::print() const
  {
    std::ostringstream oss;
    oss << model->print();
    oss << "The edge weights ";
    if(n_params == 0)
      {
	oss << "are fixed to: \n";
	for(unsigned i = 0; i < model->nWeights(); i++)
	  {
	    Node* u = model->getTree().getNode(i);
	    oss << "edgeWeight[" << i << "]\t"<< model->getWeight(*u) << "\n";
	  }
      }
    else
      {
	oss << " are estimated during analysis";
      }

    oss << ".\n";

    return name + ": " + oss.str() + StdMCMCModel::print();
  };    

  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------


  //! perturbRate() Documnet!
  //! Returns propRatio!
  //----------------------------------------------------------------------
  Probability
  EdgeWeightMCMC::perturbWeight()
  {
    Probability propRatio(1.0);   // create return object
    const Tree& T = model->getTree();
	
    // Turn off notifications just to be sure.
    bool notifStat = T.setPertNotificationStatus(false);

    // First pick a node and check its father is O.K
    Node* p; 
    unsigned c = 0;
    bool doCont = true;
    do
      {  
	idxWeight = T.getNode(R.genrand_modulo(T.getNumberOfNodes()));
	p = idxWeight->getParent();         
	c++;
	switch (model->getRootWeightPerturbation())
	  {
	  case EdgeWeightModel::BOTH:
	    doCont = idxWeight->isRoot();
	    break;
	  case EdgeWeightModel::RIGHT_ONLY:
	    doCont = (idxWeight->isRoot() || (p->isRoot() && p->getLeftChild()==idxWeight));
	    break;
	  case EdgeWeightModel::NONE:
	    doCont = (idxWeight->isRoot() || p->isRoot());
	    break;
	  }
      }
    while (doCont);

    oldWeight = model->getWeight(*idxWeight); // Save previous value

    // Get the allowed range of weights
    Real Min;
    Real Max;
    model->getRange(Min,Max);

    // #ifdef PERTURBED_NODE
    //     T.perturbedNode(idxWeight);
    // #endif
    // Change the rate

    if (useTruncatedNormalSugg)
      {
	Real nw = perturbTruncatedNormal(oldWeight, 0.5, StdMCMCModel::FIFTY_PCT, 0, Max, propRatio);
	model->setWeight(nw, *idxWeight);
      }
    else
      {
	model->setWeight(perturbLogNormal(oldWeight, suggestion_variance, Min, Max, propRatio), *idxWeight);
      }


#ifdef PERTURBED_NODE
    // If possible, only recalculate substlike for perturbed part of tree
    // If other node also perturbed or if idx_node is a child of root (since
    // root's children share same rate) recalculate substlike for whole tree
    if(T.perturbedNode() || idxWeight->getParent()->isRoot()) 
      {
	T.perturbedNode(T.getRootNode());
      }
    else // It suffices to calculate above idx_node
      {
	T.perturbedNode(idxWeight); 
      }
#endif

    //     cout << "EdgeWeightMCMC perturbedNode: " << (*T.perturbedNode()).getNumber() << "\n";
    //     cout << "EdgeWeightMCMC perturbedFastGEMnode: " << (*T.perturbedFastGEMnode()).getNumber() << "\n";
	
    // Notify listeners.
    T.setPertNotificationStatus(notifStat);
    PerturbationEvent* pe = detailedNotifInfo ?
      TreePerturbationEvent::createEdgeWeightInfo(idxWeight) :
      new PerturbationEvent(PerturbationEvent::PERTURBATION);
    T.notifyPertObservers(pe);
    delete pe;
	
    return propRatio;
  };

  std::string 
  EdgeWeightMCMC::weightsStr() const
  {
    std::ostringstream oss;
    const Tree& T = model->getTree();
    //     Node* p;
    //     Node* n;
    //     for(unsigned i = 0; i < model->nWeights(); i++)
    //       {
    // 	n = T.getNode(i);            
    // 	p = n->getParent();
    //         if(n->isRoot())// || (p->isRoot() && p->getLeftChild() == n))
    //           {
    // 	    continue;
    // 	  }
    // 	oss << model->getWeight(*n) << ";\t";
    //       }
    oss << TreeIO::writeGuestTree(T) << ";\t";
    return oss.str();
  };

  std::string
  EdgeWeightMCMC::weightsHeader() const
  {
    std::ostringstream oss; 
    const Tree& T = model->getTree();
    //     Node* p;
    //     Node* n;
    //     for(unsigned i = 0; i < model->nWeights(); i++) 
    //       { 
    // 	n = T.getNode(i);            
    // 	p = n->getParent();
    // 	if(n->isRoot())// || (p->isRoot() && p->getLeftChild() == n))
    // 	  {
    // 	    continue;
    // 	  }
    // 	oss << "edgeWeight[" << i << "](float);\t";
    //       }
    if(T.getName().length() > 0)
      {
	oss <<  T.getName()
	    << "_Lengths(tree);\t";
      }
    else 
      {
	oss << "T_Lengths(tree);\t";
      }
    return oss.str();
  };



}//end namespace beep

