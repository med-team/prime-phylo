#include "InvGaussDensity.hh"

#include <assert.h>
#include "AnError.hh"
#include "Probability.hh"

#include <cmath>
#include <sstream>

//----------------------------------------------------------------------
//
// Class InvGaussDensity
// implements the logNormal density distribution
//
// Pr(x) = sqr(\lambda/(2*\pi*x^3)exp(-\lambda(x-\mu)/2\mu^2x)
// alpha = \mu = mean and beta = variance/\mu^3 = 1/ lambda!!!
//
// Author: Martin Linder, copyright the MCMC-club, SBC
//
//----------------------------------------------------------------------

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Formal defs of static const members
  // I haven't found where this is stated in the standard, but
  // apparently we now need to do this? /bens
  //
  //----------------------------------------------------------------------
  const double InvGaussDensity::pi;

  //----------------------------------------------------------------------
  //
  // Construct/Destruct
  //
  //----------------------------------------------------------------------
  InvGaussDensity::InvGaussDensity(Real mean, Real variance, bool embedded)
    : Density2P_positive(mean, variance, "InvGauss")
  {
    if(embedded)
      {
	setEmbeddedParameters(mean, variance);
      }
    else
      {
	setParameters(mean, variance);
      }
  }

  InvGaussDensity::InvGaussDensity(const InvGaussDensity& df)
    : Density2P_positive(df)
  {}

  InvGaussDensity::~InvGaussDensity() 
  {};

  InvGaussDensity& 
  InvGaussDensity::operator=(const InvGaussDensity& df)
  {
    if(&df != this)
      {
	Density2P_positive::operator=(df);
	c = df.c;
      }
    return *this;
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------
  // Density-, distribution-, and sampling functions
  //----------------------------------------------------------------------
  Probability 
  InvGaussDensity::operator()(const Real& x) const
  {
    if( x <= 0)
      return 0.0;
    else
      {
	return Probability::setLogProb( -0.5 * std::pow(x - alpha, 2)/
					(std::pow(alpha, 2) * beta * x) 
					- 1.5 * std::log(x) + c, 1);
      }
  }

  // I have note found any formula for the inverse function of F(x)
  //------------------------------------------------------------------
  Real
  InvGaussDensity::sampleValue(const Real& p) const
  {
    assert(0 < p && p < 1.0); // check precondition

    std::cerr << "InvGaussDensity: sampleValue(p):\n"
	      << "Warning! Can't sample from inverse gaussian distribution\n"
	      << "the following number is simply p*mean.\n";
//     throw AnError("InvGaussDensity::SampleValue(): I do not know how to "
// 		  "sample from the inverse Gaussian distribution\n");
	
    return p * getMean();  
  }


  // Access parameters
  //----------------------------------------------------------------------
  Real 
  InvGaussDensity::getMean() const
  {
    return alpha;
  }
    
  Real 
  InvGaussDensity::getVariance() const 
  {
    return beta * std::pow(alpha, 3);
  }

 
  // Set Parameters
  //----------------------------------------------------------------------
  void 
  InvGaussDensity::setParameters(const Real& mean, const Real& variance)
  {
    assert(isInRange(mean) && isInRange(variance)); // check precondition

    alpha = mean;
    beta = variance/std::pow(mean, 3);
    c = -0.5 * std::log(2 * pi * beta);
    
    //Check postcondition - note that we may have precision error
    assert(2 * std::abs(getMean() - mean) / (getMean() + mean) < 1e-5);
    assert(2*std::abs(getVariance()-variance)/(getVariance()+variance)<1e-5); 
    return;
  }
    
  void 
  InvGaussDensity::setMean(const Real& mean)
  {
#ifndef NDEBUG
    Real variance = getVariance();
#endif
    assert(isInRange(mean)); // cheak precondition

    beta *= std::pow(alpha/mean, 3);
    alpha = mean;
    c = -0.5 * std::log(2 * pi * beta);

    //Check postcondition - note that we may have precision error
    assert(2 * std::abs(getMean() - mean) / (getMean() + mean) < 1e-5);
    assert(2*std::abs(getVariance()-variance)/(getVariance()+variance)<1e-5); 
    return;
  }
    
  void 
  InvGaussDensity::setVariance(const Real& variance)
  {
#ifndef NDEBUG
    Real mean = getMean();
#endif
    assert(isInRange(variance)); // cheak precondition

    beta = variance/std::pow(alpha, 3);
    c = -0.5 * std::log(2 * pi * beta);

    //Check postcondition - note that we may have precision error
    assert(2 * std::abs(getMean() - mean) / (getMean() + mean) < 1e-5);
    assert(2*std::abs(getVariance()-variance)/(getVariance()+variance)<1e-5); 
    return;
  }
    

  //------------------------------------------------------------------
  // I/O
  //------------------------------------------------------------------
  std::string
  InvGaussDensity::print() const
  {
    std::ostringstream oss;
    oss << "Inverse Gaussian distribution, InvG("
	<< alpha
	<< ", "
	<< beta
	<< ")\n" 
      ;
    return oss.str();
  }

}//end namespace beep
