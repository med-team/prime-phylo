#ifndef EDGEDISCPTMAPITERATOR_HH
#define EDGEDISCPTMAPITERATOR_HH

#include "EdgeDiscPtMaps.hh"

namespace beep
{

/********************************************************************
 * Forward iterator for a path of an "edge discretized" point map,
 * at least partly in "STL-style".
 * 
 * Movement takes place in the leaf-to-root direction. As usual,
 * element access is provided by dereferencing the iterator with '*'.
 * 
 * However, it is also possible to retrieve the point corresponding to the iterator
 * by (e.g. implicit) type casting. This is convenient for using the iterator to access
 * elements at the same spot of another point map (based on the same discretized tree).
 * 
 * Furthermore, a call to method pp() will work analogously to
 * the pre-increment operator ++it, but skip any points corresponding to
 * nodes. Hence, one may call ++it to just move along all points towards
 * the root tip, or alternatively choose it.pp() to move along only pure
 * discretization points (although possibly starting from a node).
 * 
 * No in-bounds checking is performed when moving outside tree.
 * 
 * @author Joel Sjöstrand.
 ********************************************************************/
template<typename T>
class EdgeDiscPtMapIterator : public std::iterator<std::forward_iterator_tag, T>
{
private:
	
	/** Full access since this class didn't fit in the map's definition. */
	friend class EdgeDiscPtMap<T>;
	
	/**
	 * Private (friend) constructor.
	 */
	EdgeDiscPtMapIterator(EdgeDiscPtMap<T>* map, EdgeDiscretizer::Point pt) :
		std::iterator<std::forward_iterator_tag, T>(),
		m_map(map),
		m_pt(pt)
	{
	};
		
public:
	
	/**
	 * Default constructor. Will not refer to any map.
	 */
	EdgeDiscPtMapIterator() :
		std::iterator<std::forward_iterator_tag, T>(),
		m_map(NULL),
		m_pt(NULL, 0)
	{
	};
	
	/**
	 * Copy-constructor.
	 */
	EdgeDiscPtMapIterator(const EdgeDiscPtMapIterator<T>& it) :
		std::iterator<std::forward_iterator_tag, T>(),
		m_map(it.m_map),
		m_pt(it.m_pt)
	{
	};
	
	/**
	 * Destructor;
	 */
	virtual ~EdgeDiscPtMapIterator()
	{
	};
	
	/**
	 * Assignment operator.
	 */
	EdgeDiscPtMapIterator& operator=(const EdgeDiscPtMapIterator& it)
	{
		m_map = it.m_map;
		m_pt = it.m_pt;
		return (*this);
	}
	
	/**
	 * Equal-to operator.
	 */
	bool operator==(const EdgeDiscPtMapIterator& it) const
	{
		return (m_map == it.m_map && m_pt == it.m_pt);
	}
	
	/**
	 * Not-equal-to operator.
	 */
	bool operator!=(const EdgeDiscPtMapIterator& it) const
	{
		return (m_map != it.m_map || m_pt != it.m_pt);
	}
	
	/**
	 * Less-than operator. Works only if based on same map.
	 * NOTE: For practical reasons, always returns true if iterators
	 * refer to different edges!
	 */
	bool operator<(const EdgeDiscPtMapIterator& it) const
	{
		if (this->m_map != it.m_map)
			throw AnError("Cannot use < on EdgeDiscPtMapIterators based on different maps.", 1);
		return (this->m_pt.first != it.m_pt.first || this->m_pt.second < it.m_pt.second);
	}
	
	/**
	 * Less-than-or-equal-to operator. Works only if based on same map.
	 * NOTE: For practical reasons, always returns true if iterators
	 * refer to different edges!
	 */
	bool operator<=(const EdgeDiscPtMapIterator& it) const
	{
		if (this->m_map != it.m_map)
			throw AnError("Cannot use < on EdgeDiscPtMapIterators based on different maps.", 1);
		return (this->m_pt.first != it.m_pt.first || this->m_pt.second <= it.m_pt.second);
	}
	
	/**
	 * Greater-than operator. Works only if based on same map.
	 * NOTE: For practical reasons, always returns true if iterators
	 * refer to different edges!
	 */
	bool operator>(const EdgeDiscPtMapIterator& it) const
	{
		if (this->m_map != it.m_map)
			throw AnError("Cannot use < on EdgeDiscPtMapIterators based on different maps.", 1);
		return (this->m_pt.first != it.m_pt.first || this->m_pt.second > it.m_pt.second);
	}
	
	/**
	 * Greater-than-or-equal-to operator. Works only if based on same map.
	 * NOTE: For practical reasons, always returns true if iterators
	 * refer to different edges!
	 */
	bool operator>=(const EdgeDiscPtMapIterator& it) const
	{
		if (this->m_map != it.m_map)
			throw AnError("Cannot use < on EdgeDiscPtMapIterators based on different maps.", 1);
		return (this->m_pt.first != it.m_pt.first || this->m_pt.second >= it.m_pt.second);
	}
	
	/**
	 * Prefix increment operator. Moves to the point above in the
	 * discretized tree. See also 'pp()'.
	 */
	EdgeDiscPtMapIterator& operator++()
	{
		if (m_pt.second+1 >= m_map->getNoOfPts(m_pt.first))
		{
			m_pt.first = m_pt.first->getParent();
			m_pt.second = 0;
		}
		else
		{
			++(m_pt.second);
		}
		return (*this);
	}
	
	/**
	 * Postfix increment operator. Moves to the point above
	 * in the discretized tree. See also 'pp()'.
	 */
	EdgeDiscPtMapIterator operator++(int)
	{
		EdgeDiscPtMapIterator cpy(*this);
		++(*this);
		return cpy;
	}
	
	/**
	 * Analogous to prefix increment operator, but skips
	 * node-corresponding points! Will not skip the
	 * special point at the top edge "tip".
	 */
	EdgeDiscPtMapIterator& pp()
	{
		if (m_pt.second+1 >= m_map->getNoOfPts(m_pt.first))
		{
			m_pt.first = m_pt.first->getParent();
			m_pt.second = (m_pt.first == NULL) ? 0 : 1;
		}
		else
		{
			++(m_pt.second);
		}
		return (*this);
	}
	
	/**
	 * Element reference access.
	 */
	T& operator*()
	{
		return (*m_map)(m_pt);
	}
	
	/**
	 * Element pointer access.
	 */
	T* operator->()
	{
		return (&*(*this));
	}
	
	/**
	 * Cast operator for Point.
	 */
	operator EdgeDiscretizer::Point()
	{
		return m_pt;
	}
	
	/**
	 * Returns the point to which the iterator corresponds.
	 */
	EdgeDiscretizer::Point getPt() const
	{
		return m_pt;
	}
	
	
private:
	
	/** Map which iterator is working on. */
	EdgeDiscPtMap<T>* m_map;
	
	/** Iterator's position in map. */
	EdgeDiscretizer::Point m_pt;
	
};

typedef EdgeDiscPtMapIterator<Real> EdgeDiscTreeIterator;

} // end namespace beep

#endif // EDGEDISCPTMAPITERATOR_HH

