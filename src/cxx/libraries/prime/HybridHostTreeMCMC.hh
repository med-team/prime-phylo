#ifndef HYBRIDHOSTTREEMCMC_HH
#define HYBRIDHOSTTREEMCMC_HH

#include "StdMCMCModel.hh"
#include "HybridTree.hh"
#include "HybridHostTreeModel.hh"
#include "BeepVector.hh" 

#include <deque>
namespace beep
{

  //! Temporary class that preturb times of Hybrid tree and 
  //! parameters of HybridHostTreeModel
  class HybridHostTreeMCMC 
    : public StdMCMCModel, 
      public HybridHostTreeModel
  {
  public:
    //----------------------------------------------------------------
    //
    //!\name Construct/destruct/assign
    //@{
    //----------------------------------------------------------------
    HybridHostTreeMCMC(MCMCModel& prior,
		       HybridTree& hs, unsigned maxGhosts = 10);
    ~HybridHostTreeMCMC();    
    HybridHostTreeMCMC(const HybridHostTreeMCMC& hhtm);    
    HybridHostTreeMCMC operator=(const HybridHostTreeMCMC& hhtm);
    //@}
  public: 
    //----------------------------------------------------------------
    //
    // Interface
    //
    //---------------------------------------------------------------- 
    //! Fix model parameter to the argument values
    void fixBDHparameters(Real newLambda, Real newMu, Real newRho);
    void fixTree();

    // Interface inherited from ProbabilityModel
    //---------------------------------------------------------------- 
    MCMCObject suggestOwnState();    
    void commitOwnState();    
    void discardOwnState();    
    std::string ownStrRep() const;    
    std::string ownHeader() const;
    Probability updateDataProbability();

    //---------------------------------------------------------------- 
    // IO
    //---------------------------------------------------------------- 
    friend std::ostream& operator<<(std::ostream& o,
				    const HybridHostTreeMCMC& hhtm);
    std::string print() const;

  protected:
    //----------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------
    //! Initiate all parameters
    void initParameters();

  private:
    //----------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------
    //! \name backup last accepted states in these variables
    //@{
    Real oldValue;
    Node* idxNode;
    HybridTree oldS;   
    RealVector oldTimes;
    RealVector oldRates;
    RealVector oldLengths;
    //@}
    bool fixRates; //! model parameters are fixed
    bool treeFixed;  //! tree topology is fixed
    unsigned Idx;  //! Indicates what parameters to preturbe /restore
    Real suggestion_variance; //! parameter of proposal function for floats

  };
}
#endif
