#include <cmath>
#include "fastGEM.hh"

namespace beep
{
  using namespace std;
  
  //-------------------------------------------------------------
  //
  // Construct / Destruct / Assign
  //
  //-------------------------------------------------------------

  fastGEM::fastGEM(Tree& G, Tree& S, StrStrMap &gs, Density2P* df, 
		   fastGEM_BirthDeathProbs& fbdp, 
		   std::vector<double>* discrPoints, unsigned noOfDiscrPoints)
    :iidRateModel(*df,G),
     G(G),
     S(S),
     gs(gs),
     df(df),
     fbdp(fbdp),
     birthRate(fbdp.getBirthRate()),
     noOfSNodes(S.getNumberOfNodes()),
     noOfGNodes(G.getNumberOfNodes()),
     noOfDiscrPoints(noOfDiscrPoints),
     discrPoints(discrPoints),
     Sa(noOfDiscrPoints+1, noOfGNodes),
     Lb(noOfDiscrPoints+1, noOfGNodes, noOfDiscrPoints+1),
     Lt(noOfDiscrPoints+1, noOfGNodes, noOfDiscrPoints+1),
     SaLeft(noOfDiscrPoints+1, noOfGNodes), // Currently not used
     SaRight(noOfDiscrPoints+1, noOfGNodes), // Currently not used
     lambda(G, S, gs),
     timeStep(2.0/noOfDiscrPoints),
     discrPtBelowSnode(new std::vector<unsigned>),
     discrPtAboveSnode(new std::vector<unsigned>),
     specPtBelowDiscrPt(noOfDiscrPoints+1,noOfGNodes),
     withRates(true),
     longRootEdge(true)
  {
//     cerr << "# start init fastGEM\n";

//     cerr << "# birthRate: " << birthRate << "\n";
//     cerr << "# noOfSNodes: " << noOfSNodes << "\n";
//     cerr << "# noOfSLeaves: " << S.getNumberOfLeaves() << "\n";
//     cerr << "# noOfDiscrPoints: " << noOfDiscrPoints << "\n";

    updateSpeciesTreeDependent();
    updateGeneTreeDependent();
		
    //printLt();

//     cerr << "# end init fastGEM\n";
  }

  fastGEM::~fastGEM()
  {
  }

  //-------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------
  Probability fastGEM::calculateDataProbability()
  {
    assert(T = &G);
    update();
    Node* gRoot = G.getRootNode();
    unsigned gRootIndex = gRoot->getNumber();

    fbdp.calcP11();

    Probability ret = calcSumProb(gRootIndex);
    return ret;
  }
        
  Probability fastGEM::calcSumProb(unsigned gRootIndex)
  {
    reconcileRecursively(gRootIndex);
  
//     cerr << "start fastGEM::calcSumProb retrieve sum(Sa(xIndex,gRootIndex))\n";

    Probability sumReconProb(0);
    Probability sumProb(0);

    unsigned xIndex = noOfDiscrPoints - 1;
    for (unsigned iIndex = 0; iIndex <= xIndex; iIndex++)
      {
	Probability reconProb = getLbValue(xIndex,gRootIndex,iIndex);

 	sumReconProb = sumReconProb + reconProb;
	//cout << "iIndex: " << iIndex << " reconProb: " << reconProb << " sumReconProb: " << sumReconProb << "\n";
      }
    
    //cout << "sumProb: " << sumReconProb << " (" <<  sumReconProb.val() <<")\n";
    //    cerr << "end fastGEM::calcSumProb retrieve sum(Sa(xIndex,gRootIndex))\n"; 
    return sumReconProb;
  }

  unsigned fastGEM::calcMaxProb(unsigned gRootIndex)
  {
    reconcileRecursively(gRootIndex);
  
    //cerr << "start fastGEM::reconcile retrieve max(Sa(xIndex,gRootIndex))\n";
    unsigned gRootIindex = 0;
    Probability maxReconProb = 0;

    unsigned xIndex = noOfDiscrPoints - 1;
    for (unsigned iIndex = 0; iIndex <= xIndex; iIndex++)
      {
	//START calculation of the rate part of the root edge. I don't think it will ever contribute since length(rootedge) is so small.
	Node* gRoot = G.getRootNode();
	Real rootEdgeLength = gRoot->getLength();
	Real iIndexTime; 
	if (iIndex == 0)
	  {
	    unsigned Sindex = getSpecPtBelowDiscrPt(iIndex, gRootIndex);
	    iIndexTime = S.getNode(Sindex)->getNodeTime();
	  }
	else
	  {
	    iIndexTime = (*discrPoints).at(iIndex);
	  }
	Real rootEdgeTime = 2.0 - iIndexTime;
#ifndef RATE_INTERVAL
	Real rate;
	if (rootEdgeTime == 0)
	  {
	    rate = 0; 
	  }
	else
	  {
	    rate = rootEdgeLength/rootEdgeTime;
	  }
	Probability topRateProb = df->operator()(rate);
#else
	Real rate1;
	Real rate2;
	if (rootEdgeTime == 0)
	  {
	    rate1 = rate2 = 0; 
	  }
	else
	  {
	    rate1 = rootEdgeLength/(rootEdgeTime - timeStep/2);
	    rate2 = rootEdgeLength/(rootEdgeTime + timeStep/2);
	  }
	Probability topRateProb = df->cdf(rate1) - df->cdf(rate2);
#endif
	//END calculation of the rate part of the root edge.

	Probability reconProb = getLbValue(xIndex,gRootIndex,iIndex);

	if (reconProb > maxReconProb)
	  {
	    maxReconProb = reconProb;
	    gRootIindex = iIndex;
	  }
//  	cerr << "iIndex: " << iIndex << " reconProb: " << reconProb << " maxReconProb: " << maxReconProb << "\n";
      }
     
//     cout << "maxProb: " << maxReconProb <<"\n";
//     cerr << "end fastGEM::calcMaxProb retrieve max(Sa(xIndex,gRootIndex))\n";    

    return gRootIindex;
  }
    
  void fastGEM::backTrace(unsigned xIndex, unsigned uIndex)
  {
    Node* u = G.getNode(uIndex);

    Node* v = u->getLeftChild();
    Node* w = u->getRightChild();
    unsigned vxIndex = getLeftPointer(xIndex,uIndex);
    unsigned wxIndex = getRightPointer(xIndex,uIndex);

//     cerr << "Node: " << uIndex << " x: " << xIndex << "\n";

    if (!v->isLeaf()) 
      {	
	unsigned vIndex = v->getNumber();
	backTrace(vxIndex,vIndex);
      }
    if (!w->isLeaf()) 
      {
	unsigned wIndex = w->getNumber();	
	backTrace(wxIndex,wIndex);
      }
  }

  Probability fastGEM::getRootLbValue(unsigned xIndex, unsigned gRootIndex, unsigned iIndex)
  {
    return Lb(xIndex,gRootIndex,iIndex);
  }

  void
  fastGEM::update()
  {
    if(S.perturbedNode())
      {
	lambda.update(G, S);//, &gs);
	fillSpecPtBelowTable();    
	updateSpeciesTreeDependent();
//  	updateGeneTreeDependent();
      }
    if(G.perturbedNode())
      updateGeneTreeDependent();
  }

  //---------------------------------------------------------------------
  //
  // Implementation
  //
  //---------------------------------------------------------------------

  void fastGEM::reconcileRecursively(unsigned uIndex)
  {
#ifdef DEBUG_FASTGEM
    cout << "start fastGEM::reconcileRecursively with uIndex: " 
	 << uIndex << "\n";
#endif

    Node* u = G.getNode(uIndex);

    if (!u->isLeaf())
      {
	Node* v = u->getLeftChild();
	unsigned vIndex = v->getNumber();
	reconcileRecursively(vIndex);
		
	Node* w = u->getRightChild();
	unsigned wIndex = w->getNumber();
	reconcileRecursively(wIndex);
      }

    // Get lambda[u] = Snode and its index Sindex
    Node* Snode = lambda[u];
    unsigned Sindex = Snode->getNumber();
#ifdef DEBUG_FASTGEM
    cerr << "uIndex: " << uIndex << " Sindex: " << Sindex << "\n";
#endif

    unsigned xIndexAboveLambdaU = getDiscrPtAboveSnode(Sindex);
    Real SparentNodeTime;

    if (Snode->isRoot())
      {
	SparentNodeTime = 2.0;
      }
    else
      {
	Node* Sparent = Snode->getParent();
	SparentNodeTime = Sparent->getNodeTime();
      }
	
    Probability SaValue; 
    Probability LbValue; 
    Real LtValue;

    // Speciation case - i.e. xIndex = 0 
    //----------------------------------
    // First set Sa values
    if (Snode->isLeaf() && (!u->isLeaf()))
      {
	SaValue = 0.0;
      }
    else
      {
	SaValue = calcSa(Sindex,0,u);
      }
    setSaValue(0,uIndex,SaValue);
#ifdef DEBUG_FASTGEM
    cout << "setSaValue(" << 0 << "," << uIndex << "," << SaValue << ")\n";
#endif

    // Then Lb values
    if (Snode->isLeaf() && (!u->isLeaf()))
      {
	LbValue = 0.0;
      }
    else
      {
	LbValue = calcLb(Sindex,0,u,0);
      }
    setLbValue(0,uIndex,0,LbValue);
#ifdef DEBUG_FASTGEM
    cout << "setLbValue(" << 0 << "," << uIndex << "," 
	 << 0 << "," << LbValue << ")\n";
#endif

    LtValue = calcLt(Sindex,0,u,0);
    setLtValue(0,uIndex,0,LtValue);
#ifdef DEBUG_FASTGEM    
    cout << "setLtValue(" << 0 << "," << uIndex << "," 
	 << 0 << "," << LtValue << ")\n";
#endif

    // Duplication case - i.e. xIndex > 0
    //-----------------------------------
    for(unsigned xIndex = xIndexAboveLambdaU; 
	xIndex <= (noOfDiscrPoints - 1); xIndex++)
      {
	// First we need to adjust Sindex if we happen toget too high
	Real xTime = xIndex * timeStep; 
	if (xTime >= SparentNodeTime)
	  {
	    Snode = Snode->getParent();
	    assert(Snode != NULL);
	    Sindex = Snode->getNumber();

	    if (Snode->isRoot())
	      {
		SparentNodeTime = 2.0;
	      }
	    else
	      {
		Node* Sparent = Snode->getParent();	
		SparentNodeTime = Sparent->getNodeTime();
	      }
	  }

	if(u->isLeaf())
	  {
	    SaValue = 0.0;
	  }
	else
	  {
	    SaValue = calcSa(Sindex,xIndex,u);
	  }
	setSaValue(xIndex,uIndex,SaValue);
#ifdef DEBUG_FASTGEM
 	cout << "setSaValue(" << xIndex << "," << 
	  uIndex << "," << SaValue << ")\n";
#endif

	/// Lb and Lt values for i = 0 ///
	if(Snode->isLeaf() && (!u->isLeaf()))
	  {
	    LbValue = 0.0;
	  }
	else
	  {
	    LbValue = calcLb(Sindex,xIndex,u,0);
	  }
	setLbValue(xIndex,uIndex,0,LbValue);
#ifdef DEBUG_FASTGEM
	cout << "setLbValue(" << xIndex << "," 
	     << uIndex << "," << 0 << "," << LbValue << ")\n";
#endif

	LtValue = calcLt(Sindex,xIndex,u,0);
	setLtValue(xIndex,uIndex,0,LtValue);
#ifdef DEBUG_FASTGEM
	cout << "setLtValue(" << xIndex << "," 
	     << uIndex << "," << 0 << "," << LtValue << ")\n";
#endif

	// Here it is good to iterate over all indices, as
	// we then reset the zero values
	for (unsigned iIndex = 1; iIndex <= xIndex; iIndex++)
	  {
 	    if (u->isLeaf() || 
		iIndex < getDiscrPtAboveSnode(lambda[u]->getNumber()))
	      {
		LbValue = 0.0;
	      }
	    else
	      {
		LbValue = calcLb(Sindex,xIndex,u,iIndex);
	      }
	    setLbValue(xIndex,uIndex,iIndex,LbValue);
#ifdef DEBUG_FASTGEM
	    cout << "setLbValue(" << xIndex << "," 
		 << uIndex << "," << iIndex << "," << LbValue << ")\n";
#endif

	    LtValue = calcLt(Sindex,xIndex,u,iIndex);
	    setLtValue(xIndex,uIndex,iIndex,LtValue);
#ifdef DEBUG_FASTGEM
	    cout << "setLtValue(" << xIndex << "," 
		 << uIndex << "," << iIndex << "," << LtValue << ")\n";
#endif
	  }

      }
#ifdef DEBUG_FASTGEM
    cerr << "end fastGEM::reconcileRecursively with uIndex: "
	 << uIndex << "\n";
#endif
  }

  void 
  fastGEM::setSaValue(unsigned xIndex, unsigned uIndex, Probability p)
  {
    Sa(xIndex,uIndex) = p;
  }
  
  void fastGEM::setLbValue(unsigned xIndex, unsigned uIndex, 
			   unsigned iIndex, Probability p)
  {
    Lb(xIndex,uIndex,iIndex) = p;
  }
    
  void fastGEM::setLtValue(unsigned xIndex, unsigned uIndex, 
			   unsigned iIndex, Real t)
  {
    Lt(xIndex,uIndex,iIndex) = t;
  }

  void fastGEM::setPointers(unsigned xIndex, unsigned uIndex, 
			    unsigned lVal, unsigned rVal)
  {
    SaLeft(xIndex, uIndex) = lVal;
    SaRight(xIndex, uIndex) = rVal;
  }

  Probability fastGEM::getSaValue(unsigned xIndex, unsigned uIndex)
  {
    return Sa(xIndex,uIndex);
  }
    
  Probability fastGEM::getLbValue(unsigned xIndex, unsigned uIndex, unsigned iIndex)
  {
    return Lb(xIndex,uIndex,iIndex);
  }
    
  Real fastGEM::getLtValue(unsigned xIndex, unsigned uIndex, unsigned iIndex)
  {
    return Lt(xIndex,uIndex,iIndex);
  }
    
  unsigned fastGEM::getLeftPointer(unsigned xIndex, unsigned uIndex)
  {
    unsigned l = SaLeft(xIndex,uIndex);
    return l;   
  }
    
   unsigned fastGEM::getRightPointer(unsigned xIndex, unsigned uIndex)
  {
    unsigned r = SaRight(xIndex,uIndex);
    return r;   
  }

  Probability fastGEM::calcLb(unsigned Sindex, unsigned xIndex, 
			      Node* u, unsigned iIndex)
  {
    assert(iIndex <= xIndex);

    unsigned uIndex = u->getNumber();

#ifdef DEBUG_FASTGEM
    cerr << "start fastGEM::calcLb with uIndex: " << uIndex << " Sindex: " 
	 << Sindex << " xIndex: " << xIndex << " iIndex: " << iIndex << "\n";
#endif
    Probability p;
    Probability SaValue = getSaValue(xIndex,uIndex);
#ifdef DEBUG_FASTGEM
    cout << "getSaValue(" << xIndex << "," << uIndex << "): " 
	 << SaValue << "\n";
#endif

    if(xIndex == 0) // speciation
      {
	p = SaValue;
      }
    else
      {
	Node* Slambda = lambda[u];
	unsigned SlambdaIndex = Slambda->getNumber();
	unsigned discrPtAboveSlambda = getDiscrPtAboveSnode(SlambdaIndex);
	// Get rid of the ridiculous cases
	if(iIndex > 0 && iIndex < discrPtAboveSlambda)
	  {
	    return 0;
	  }

	Probability P11value = fbdp.getP11dupValue(Sindex,xIndex);
#ifdef DEBUG_FASTGEM
	cout << "Sindex: " << Sindex << " xIndex: " << xIndex
	     << " P11: " << P11value << "\n";
	cout << "iSindex = " << getDiscrPtAboveSnode(Sindex) << endl;
#endif	

	unsigned yIndex;
	if (xIndex == discrPtAboveSlambda)
	  {
	    yIndex = 0;
	  }
	else
	  {
	    yIndex = xIndex -1;
	  }

	Probability oldLbValue; 
	oldLbValue = getLbValue(yIndex,uIndex,iIndex);

	// Start Loss case

	Probability lossStepProb = 1.0; 
	unsigned SunderXindex = getSpecPtBelowDiscrPt(xIndex,uIndex); 
	Node* SunderX = S.getNode(SunderXindex);
	unsigned xAboveSnode = getDiscrPtAboveSnode(SunderXindex);
	if (iIndex == xIndex)
	  {
	    if ((xIndex == xAboveSnode) && (SunderX->isLeaf() == false))
	      {
		SaValue = calcSaWithLoss(Sindex, xIndex, u);
	      }
	  }
	else
	  {
	    unsigned SunderIindex = getSpecPtBelowDiscrPt(iIndex,uIndex); 
	    Node* SunderI = S.getNode(SunderIindex);

	    if ((xIndex == xAboveSnode) && 
		(SunderX->getPorder() > SunderI->getPorder()))
	      {
		unsigned SunderXchildIndex = getSpecPtBelowDiscrPt((xIndex-1),
								   uIndex); 
		Node* SunderXchild = S.getNode(SunderXchildIndex);
		Node* SunderXchildSibling = SunderXchild->getSibling();
		unsigned SunderXchildSiblingIndex = SunderXchildSibling->getNumber();
		Probability lossProb = fbdp.getLossValue(SunderXchildSiblingIndex);
		//Node* Ssibling = SunderI->getSibling();
		//unsigned SsiblingIndex = Ssibling->getNumber();
		//Probability lossProb = fbdp.getLossValue(SsiblingIndex);
		Probability aboveLossP11value = fbdp.getP11specValue(Sindex);
		lossStepProb = aboveLossP11value*lossProb;
		
		oldLbValue *=lossStepProb;
#ifdef DEBUG_FASTGEM
		cout << "Sindex: " << Sindex << " xIndex: " << xIndex 
		     << " uIndex: " << uIndex << " iIndex: " << iIndex 
		     << " SunderXchildIndex: " << SunderXchildIndex  
		     << " SunderXchildSiblingIndex: " 
		     << SunderXchildSiblingIndex  
		     << " lossProb: " << lossProb 
		     << " aboveLossP11value: " << aboveLossP11value 
		     << " lossStepProb: " << lossStepProb << "\n";
#endif
	      }
	  }
	// End Loss case
	
	if (iIndex == xIndex)
	  {
	    p = SaValue;	    
	  }
	else
	  {
	    p = P11value*oldLbValue;
	  }
      }
#ifdef DEBUG_FASTGEM
    cerr << "end fastGEM::calcLb with uIndex: " << uIndex
	 << " Sindex: " << Sindex << " xIndex: " << xIndex
	 << " iIndex: " << iIndex << "\n";
#endif
    return p;
  }
        
  Real fastGEM::calcLt(unsigned Sindex, unsigned xIndex, 
		       Node* u, unsigned iIndex)
  {
    assert(iIndex <= xIndex);

    unsigned uIndex = u->getNumber();
#ifdef DEBUG_FASTGEM
    cerr << "start fastGEM::calcLt with uIndex: " << uIndex
	 << " Sindex: " << Sindex << " xIndex: " << xIndex
	 << " iIndex: " << iIndex << "\n";
#endif

    Real t; // Value to be inserted in matrix Lt. Equals t(x,p(x)) 
            // if xIndex is a speciation (i.e. xIndex = 0)
            // or if iIndex=xIndex in which case we are calculatin 
            // the "last" iIndex-position in the vector.

    Real xTime;
    Real pxTime; 
    Real px_xTime; // Time from x to p(x)

    Node* Snode = S.getNode(Sindex);
    Real SnodeTime = Snode->getNodeTime();

    if (xIndex == 0)
      {
	xTime = SnodeTime;
	pxTime = fbdp.getPxTime(Sindex,xIndex); 
	px_xTime = pxTime - xTime;
	t = px_xTime; 
      }
    else
      {	
	xTime = xIndex * timeStep; 
	pxTime = fbdp.getPxTime(Sindex,xIndex);
	px_xTime = pxTime - xTime;
	
#ifdef DEBUG_FASTGEM
	cerr << "calcLt Sindex: " << Sindex << " uIndex: " << uIndex
	     << " xIndex: " << xIndex << " xTime: " << xTime
	     << " pxTime: " << pxTime << " px_xTime: " << px_xTime << "\n";
#endif
	unsigned yIndex;
	Node* Slambda = lambda[u];
	unsigned SlambdaIndex = Slambda->getNumber();
	unsigned discrPtAboveSlambda = getDiscrPtAboveSnode(SlambdaIndex);

	// Get rid of the ridiculous cases
	if(iIndex > 0 && iIndex < discrPtAboveSlambda)
	  {
	    return 0;
	  }

	if (xIndex == discrPtAboveSlambda)
	  {
	    yIndex = 0;
	  }
	else
	  {
	    yIndex = xIndex -1;
	  }

	if (iIndex == xIndex)
	  {
	    t = px_xTime; 
	  }
	else
	  {
	    Real oldLtValue;
	    oldLtValue = getLtValue(yIndex,uIndex,iIndex);
#ifdef DEBUG_FASTGEM
	    cout << "getLtValue(" << yIndex << "," << 
	      uIndex << "," << iIndex << "): " << oldLtValue << "\n";
#endif

	    // Start Loss case
	    unsigned SunderXindex = getSpecPtBelowDiscrPt(xIndex,uIndex); 
	    Node* SunderX = S.getNode(SunderXindex);
	    unsigned xAboveSnode = getDiscrPtAboveSnode(SunderXindex);
	    	    
	    if ((xIndex == xAboveSnode) && (!SunderX->isLeaf()))
	      {
		Real aboveLossTime = fbdp.getPxTime(SunderXindex,0) - 
		  SunderX->getNodeTime();
		oldLtValue += aboveLossTime;
	      }
	    // End Loss case

	    t = px_xTime + oldLtValue;
	  }
      }
#ifdef DEBUG_FASTGEM
    cerr << "end fastGEM::calcLt with uIndex: " << uIndex
	 << " Sindex: " << Sindex << " xIndex: " << xIndex
	 << " iIndex: " << iIndex << "\n";
#endif
    return t;
  }
    
  Probability fastGEM::calcSa(unsigned Sindex, unsigned xIndex, Node* u)
  {
#ifdef DEBUG_FASTGEM
    cerr << "start fastGEM::calcSa with uIndex: " << u->getNumber()
	 << " Sindex: " << Sindex 
	 << " SindexTime: " << S.getNode(Sindex)->getNodeTime() 
	 << " xIndex: " << xIndex 
	 << " xTime: " << xIndex * timeStep << "\n";
#endif
    if(lambda[u]->strictlyDominates(*S.getNode(Sindex)))
      {
	return 0;
      }

    Probability pSum;
    unsigned yIndex;
    unsigned zIndex;

    Probability duplicationFactor = 1.0;
    Probability P11value;

    if(u->isLeaf()) 
      {
	if(xIndex == 0)
	  {
	    pSum = P11value = fbdp.getP11specValue(Sindex);
#ifdef DEBUG_FASTGEM
	    cout << "getP11specValue(" << Sindex << "): " << P11value << "\n";
#endif
	  }
	else
	  {
	    cerr << "We should never go here ! \n";
	  }
      }
    else
      {
	if(xIndex == 0)
	  {
	    P11value = fbdp.getP11specValue(Sindex);

	    unsigned discrPtBelowS = getDiscrPtBelowSnode(Sindex);
	    yIndex = discrPtBelowS;
            zIndex = discrPtBelowS;
	  }
	else
	  {
	    // Get rid of ridiculous cases
	    if(xIndex < getDiscrPtAboveSnode(Sindex))
	      {
		return 0;
	      }
	    P11value = fbdp.getP11dupValue(Sindex,xIndex);
#ifdef DEBUG_FASTGEM
	    cout << "getP11dupValue(" << Sindex << "," <<  xIndex << "): " 
		 << P11value << "\n";
#endif
	    yIndex = xIndex - 1;
	    zIndex = xIndex - 1;

	    unsigned isomorphyFactor = 2;
	    Real intervalFactor = timeStep;	    
	    Real duplRateFactor = birthRate;

	    duplicationFactor = isomorphyFactor*intervalFactor*duplRateFactor;
	  }

	Node* v = u->getLeftChild();
	unsigned vIndex = v->getNumber();
	Real lv = v->getLength();
	
	Node* w = u->getRightChild();
	unsigned wIndex = w->getNumber();    
	Real lw = w->getLength();

	// Get rid of impossible cases
	if(getDiscrPtAboveSnode(lambda[v]->getNumber()) > yIndex ||
	  getDiscrPtAboveSnode(lambda[w]->getNumber()) > zIndex)
	  {
	    return 0;
	  }

	Probability pijSum = 0;

	if ((longRootEdge == true) && (u->isRoot() == true))
	  {
	    unsigned ilambda_v = getDiscrPtAboveSnode(lambda[v]->getNumber());
	    for (unsigned iIndex = 0; iIndex <= yIndex; iIndex++)
	      {
		if(iIndex != 0 && (v->isLeaf() || iIndex < ilambda_v))
		  {
		    continue;
		  }
		Probability LbiProb = getLbValue(yIndex,vIndex,iIndex);
		Real LtiTime = getLtValue(yIndex,vIndex,iIndex);
		
		unsigned ilambda_w = getDiscrPtAboveSnode(lambda[w]->getNumber());
		for (unsigned jIndex = 0; jIndex <= zIndex; jIndex++)
		  {
		    if(jIndex != 0 && (w->isLeaf() || jIndex < ilambda_w))
		      {
			continue;
		      }
		    Probability LbjProb = getLbValue(zIndex,wIndex,jIndex);
		    Real LtjTime = getLtValue(zIndex,wIndex,jIndex);
#ifndef RATE_INTERVAL
		    Real rootRate;
		    if (LtiTime+LtjTime == 0)
		      {
			rootRate = 0;
		      }
		    else
		      {
			rootRate = (lv+lw)/(LtiTime+LtjTime);
		      }
		    Probability rateProb = df->operator()(rootRate);
#else
		    Real rate1;
		    Real rate2;
		    if (LtiTime+LtjTime == 0)
		      {
			rate1 = rate2 = 0; 
		      }
		    else
		      {
			rate1 = (lv+lw)/(LtiTime+LtjTime - timeStep/2);
			rate2 = (lv+lw)/(LtiTime+LtjTime + timeStep/2);
		      }
		    Probability rateProb = df->cdf(rate1) - df->cdf(rate2);
#endif
		    assert(rateProb >= 0);  // joelgs: Changed > to >=, hope this is OK too.

		    pijSum += LbiProb*LbjProb*rateProb;
		  }
	      }	    
	  }
	else
	  {
	    Probability piSum = 0;
	    unsigned ilambda_v = getDiscrPtAboveSnode(lambda[v]->getNumber());
	    for(unsigned iIndex = 0; iIndex <= yIndex; iIndex++)
	      {
		if(iIndex != 0 && (v->isLeaf() || iIndex < ilambda_v))
		  {
		    continue;
		  }
		Probability LbProb = getLbValue(yIndex,vIndex,iIndex);
// 		if(LbProb <= 0)
// 		  cerr << "getLbValue(" << yIndex << ", " << vIndex << ", " << iIndex << ") = " << LbProb << " in calcSa(" << Sindex << ", " << xIndex << ", "<< u->getNumber() << ")\n";
// 		assert(LbProb > 0);
		Real LtTime = getLtValue(yIndex,vIndex,iIndex);
		Probability rateProb; 
		if (withRates == true)
		  {
#ifndef RATE_INTERVAL
		    Real rate;
		    if (LtTime == 0)
		      {
			rate = 0;
		      }
		    else
		      {
			rate = lv/LtTime;
		      }
		    rateProb = df->operator()(rate);
#else
		    Real rate1;
		    Real rate2;
		    if (LtTime == 0)
		      {
			rate1 = rate2 = 0;
		      }
		    else
		      {
			rate1 = lv/(LtTime - timeStep/2);
			rate2 = lv/(LtTime + timeStep/2);
		      }
		    rateProb = df->cdf(rate1) - df->cdf(rate2);
#endif
		  }
		else
		  {
		    rateProb = 1.0;
		  }
		assert(rateProb >= 0); // joelgs: Changed > to >=, hope this is OK too.

		piSum += LbProb*rateProb;
	      }
	    
	    Probability pjSum = 0;
	    // H�r skulle man kunna undvika att ber�kna f�r on�diga index
	    unsigned ilambda_w = getDiscrPtAboveSnode(lambda[w]->getNumber());
	    for(unsigned jIndex = 0; jIndex <= zIndex; jIndex++)
	      {
		if(jIndex != 0 && (w->isLeaf() || jIndex < ilambda_w))
		  {
		    continue;
		  }
		Probability LbProb = getLbValue(zIndex,wIndex,jIndex);
	       
// 		if(LbProb <= 0)
// 		  cerr << "getLbValue(" << zIndex << ", " << wIndex << ", " << jIndex << ") in tree " << G.getName() << " in tree " << G.getName() << " in calcSa(" << Sindex << ", " << xIndex << ", "<< u->getNumber() << ")\n";
// 		assert(LbProb > 0);
		Real LtTime = getLtValue(zIndex,wIndex,jIndex);
		
		Probability rateProb; 
		if (withRates == true)
		  {
#ifndef RATE_INTERVAL
		    Real rate;
		    if (LtTime == 0)
		      {
			rate = 0;
		      }
		    else
		      {
			rate = lw/LtTime;
		      }
		    rateProb = df->operator()(rate);
#else
		    Real rate1;
		    Real rate2;
		    if (LtTime == 0)
		      {
			rate1 = rate2 = 0;
		      }
		    else
		      {
			rate1 = lw/(LtTime - timeStep/2);
			rate2 = lw/(LtTime + timeStep/2);
		      }
		    rateProb = df->cdf(rate1) - df->cdf(rate2);
#endif
		  }
		else
		  {
		    rateProb = 1.0;
		  }
		assert(rateProb >= 0);  // joelgs: Changed > to >=, hope this is OK too.

		pjSum += LbProb * rateProb;
	      }
	    pijSum = piSum * pjSum;
	  }
// 	assert(pijSum > 0);
        pSum = P11value * duplicationFactor * pijSum;
      }
    return pSum;
  }
    
  Probability fastGEM::calcSaWithLoss(unsigned Sindex, unsigned xIndex, 
				      Node* u)
  {
    Probability pSum;
    unsigned yIndex;
    unsigned zIndex;
    
    Probability P11value = fbdp.getP11dupValue(Sindex,xIndex);
#ifdef DEBUG_FASTGEM
    cout << "getP11dupValue(" << Sindex << "," 
	 <<  xIndex << "): " << P11value << "\n";
#endif

    Probability aboveSnodeP11value = fbdp.getP11specValue(Sindex);    
    Real aboveSnode_pxTime = fbdp.getPxTime(Sindex,0) - S.getNode(Sindex)->getNodeTime();
    unsigned isomorphyFactor = 2;
    Real intervalFactor = timeStep;	    
    Real duplRateFactor = birthRate;
    Probability duplicationFactor = isomorphyFactor*intervalFactor*duplRateFactor;

    Node* v = u->getLeftChild();
    unsigned vIndex = v->getNumber();
    Real lv = v->getLength();
    yIndex = xIndex - 1;
       
    Node* w = u->getRightChild();
    unsigned wIndex = w->getNumber();    
    Real lw = w->getLength();
    zIndex = xIndex - 1;
    
    Probability pijSum = 0;
    if ((longRootEdge == true) && (u->isRoot() == true))
      {
	for (unsigned iIndex = 0; iIndex <= yIndex; iIndex++)
	  {
	    Probability LbiProb = getLbValue(yIndex,vIndex,iIndex);
	    Real LtiTime = getLtValue(yIndex,vIndex,iIndex)+aboveSnode_pxTime;
	    
	    for (unsigned jIndex = 0; jIndex <= zIndex; jIndex++)
	      {
		Probability LbjProb = getLbValue(zIndex,wIndex,jIndex);
		Real LtjTime = getLtValue(zIndex,wIndex,jIndex)+aboveSnode_pxTime;
		
#ifndef RATE_INTERVAL
		Real rootRate;
		if (LtiTime+LtjTime == 0)
		  {
		    rootRate = 0;
		  }
		else
		  {
		    rootRate = (lv+lw)/(LtiTime+LtjTime);
		  }
		
		Probability rateProb = df->operator()(rootRate);
#else
		Real rate1;
		Real rate2;
		if (LtiTime+LtjTime == 0)
		  {
		    rate1 = rate2 = 0;
		  }
		else
		  {
		    rate1 = (lv+lw)/(LtiTime+LtjTime - timeStep);
		    rate2 = (lv+lw)/(LtiTime+LtjTime + timeStep);
		  }
		Probability rateProb = df->cdf(rate1) - df->cdf(rate2);
#endif		
		pijSum += LbiProb*LbjProb*rateProb;
	      }
	  }	    
      }
    else
      {
	Probability piSum = 0;
	for (unsigned iIndex = 0; iIndex <= yIndex; iIndex++)
	  {
	    Probability LbProb = getLbValue(yIndex,vIndex,iIndex);
	    Real LtTime = getLtValue(yIndex,vIndex,iIndex)+aboveSnode_pxTime;

	    Probability rateProb;
	    if (withRates == true)
	      {
#ifndef RATE_INTERVAL
		Real rate;
		if (LtTime == 0)
		  {
		    rate = 0;
		  }
		else
		  {
		    rate = lv/LtTime;
		  }
		    
		rateProb = df->operator()(rate);
#else
		Real rate1;
		Real rate2;
		if (LtTime == 0)
		  {
		    rate1 = rate2 = 0;
		  }
		else
		  {
		    rate1 = lv/(LtTime - timeStep/2);
		    rate2 = lv/(LtTime + timeStep/2);
		  }
		    
		rateProb = df->cdf(rate1) - df->cdf(rate2);
#endif
	      }
	    else
	      {
		rateProb = 1.0;
	      }

	    piSum += LbProb*rateProb;
	    //cout << "calcSaWithLoss uIndex: " << uIndex << " Sindex: " << Sindex << " xIndex: " << xIndex << " iIndex: " << iIndex << " Lbpart: " << LbProb << " time: " << LtTime << " length: " << lv << " rate: " << lv/LtTime << " ratepart: " << rateProb << " piSum: " << LbProb*rateProb << "\n";
	  }
	
	Probability pjSum = 0;
	for (unsigned jIndex = 0; jIndex <= zIndex; jIndex++)
	  {
	    Probability LbProb = getLbValue(zIndex,wIndex,jIndex);
	    Real LtTime = getLtValue(zIndex,wIndex,jIndex)+aboveSnode_pxTime;
	    
	    Probability rateProb; 
	    if (withRates == true)
	      {
#ifndef RATE_INTERVAL
		Real rate;
		if (LtTime == 0)
		  {
		    rate = 0;
		  }
		else
		  {
		    rate = lw/LtTime;
		  }
		rateProb = df->operator()(rate);
#else
		Real rate1;
		Real rate2;
		if (LtTime == 0)
		  {
		    rate1 = rate2 = 0;
		  }
		else
		  {
		    rate1 = lw/(LtTime - timeStep/2);
		    rate2 = lw/(LtTime + timeStep/2);
		  }
		    
		rateProb = df->cdf(rate1) - df->cdf(rate2);
#endif
	      }
	    else
	      {
		rateProb = 1.0;
	      }

	    pjSum += LbProb*rateProb;
	    //	cout << "calcSaWithLoss uIndex: " << uIndex << " Sindex: " << Sindex << " xIndex: " << xIndex << " jIndex: " << jIndex << " Lbpart: " << LbProb << " time: " << LtTime << " length: " << lw << " rate: " << lw/LtTime << " ratepart: " << rateProb << " pjSum: " << LbProb*rateProb << "\n";
	  }
	pijSum = piSum*pjSum;
      }
    Node* SleftChild = S.getNode(Sindex)->getLeftChild();  
    assert(SleftChild != NULL);
    unsigned SleftChildIndex = SleftChild->getNumber();
    Node* SrightChild = S.getNode(Sindex)->getRightChild(); 
    unsigned SrightChildIndex = SrightChild->getNumber();
    Probability lossFactor = fbdp.getLossValue(SleftChildIndex) * fbdp.getLossValue(SrightChildIndex);

    pSum = P11value*aboveSnodeP11value*aboveSnodeP11value*duplicationFactor*lossFactor*pijSum;
	
    return pSum;
  }
  
  void
  fastGEM::updateSpeciesTreeDependent()
  {
    // First update the classes we depend on
    fbdp.update();
    Probability pInit = 0.0;
    // These are stupid start values as they are used below as 
    // unsigned ints rather than as (signed) ints
    int leftPointer = -1;
    int rightPointer = -1;

    // This is probably meant to reset Sa and  all SaLeft and SaRight pointers
    for (unsigned xIndex = 0; xIndex <= (noOfDiscrPoints-1); xIndex++)
      {
	for (unsigned uIndex = 0; uIndex <= (noOfGNodes-1); uIndex++)
	  {
	    setSaValue(xIndex,uIndex,pInit);
	    setPointers(xIndex,uIndex,leftPointer,rightPointer);
	  }
      }
    
    // and also the Lb and Lt pointers
    Real tInit = 0.0;
    for (unsigned xIndex = 0; xIndex <= (noOfDiscrPoints-1); xIndex++)
      {
	for (unsigned uIndex = 0; uIndex <= (noOfGNodes-1); uIndex++)
	  {
	    for (unsigned iIndex = 0; iIndex <= (noOfDiscrPoints-1); iIndex++)
	      {
		setLbValue(xIndex,uIndex,iIndex,pInit);
	        setLtValue(xIndex,uIndex,iIndex,tInit);
	      }
	  }
      }

    // Then set the new values
    fillDiscrPtBelowAboveTables();// find discr.points surrounding speciations
    fbdp.calcP11();               // compute p11 probs
  }

  void
  fastGEM::updateGeneTreeDependent()
  {
    lambda.update(G, S);//, &gs);
    fillSpecPtBelowTable();    
    
    Probability pInit = 0.0;
    int leftPointer = -1;
    int rightPointer = -1;
    for (unsigned xIndex = 0; xIndex <= (noOfDiscrPoints-1); xIndex++)
      {
	for (unsigned uIndex = 0; uIndex <= (noOfGNodes-1); uIndex++)
	  {
	    setSaValue(xIndex,uIndex,pInit);
	    setPointers(xIndex,uIndex,leftPointer,rightPointer);
	  }
      }
    
    Real tInit = 0.0;
    for (unsigned xIndex = 0; xIndex <= (noOfDiscrPoints-1); xIndex++)
      {
	for (unsigned uIndex = 0; uIndex <= (noOfGNodes-1); uIndex++)
	  {
	    for (unsigned iIndex = 0; iIndex <= (noOfDiscrPoints-1); iIndex++)
	      {
		setLbValue(xIndex,uIndex,iIndex,pInit);
	        setLtValue(xIndex,uIndex,iIndex,tInit);
	      }
	  }
      }

    fbdp.calcP11();
        
//     for (unsigned uIndex = 0; uIndex <= noOfGNodes-1; uIndex++)
//       {
//         Node* u = G.getNode(uIndex);
//  	unsigned uLambda = lambda[u]->getNumber();
//   	cout << "# uIndex: " << uIndex << " uLambda: " << uLambda << " discrPtAboveLambda: " << getDiscrPtAboveSnode(uLambda) << "\n";
//       }  
  }
  
  // Concerning methods getDiscrPtBelowSnode(unsigned Sindex), getDiscrPtAboveSnode(unsigned Sindex), and getSpecPtBelowDiscrPt(unsigned xIndex, unsigned uIndex): The first is used to identify the discrPt up to which I will loop i and j when doing the summations in calcSa(); the second is used to identify which x is above x=0, the third is to identify where I am in the species tree. Used when calculating loss probabilities. 
 
  unsigned fastGEM::getDiscrPtBelowSnode(unsigned Sindex)
  {
    return discrPtBelowSnode->at(Sindex);
  }

  unsigned fastGEM::getDiscrPtAboveSnode(unsigned Sindex)
  {
    return discrPtAboveSnode->at(Sindex);
  }

  unsigned fastGEM::getSpecPtBelowDiscrPt(unsigned xIndex, unsigned uIndex)
  {
    return specPtBelowDiscrPt(xIndex,uIndex);
  }

  // Identifies and sets the discr. points just above and below each 
  // speciation, respectively
  void fastGEM::fillDiscrPtBelowAboveTables()
  { 
    discrPtBelowSnode->clear();
    discrPtAboveSnode->clear();
    
    //=====================================
    // THIS IS NOT RESET BETWEEN S CHANGES
    //=====================================
    Real eps = 0.0001;  // Allow some diff
    unsigned cxIndex;   // discr. point below
    unsigned pxIndex;   // discr. point above

    // Go through all nodes sindex in S
    for (unsigned Sindex = 0; Sindex <= S.getNumberOfNodes()-1; Sindex++)
      {
	Node* Snode = S.getNode(Sindex);
	if (Snode->isLeaf()) // We are at the bottom
	  {
	    cxIndex = 0;
	    pxIndex = 1;
	  }
	else
	  {
	    // Climb down from the top until we are at the right time
	    Real SnodeTime = Snode->getNodeTime();
	    for (unsigned xIndex = (noOfDiscrPoints-1); xIndex >= 0; xIndex--)
	      {
		if (((*discrPoints).at(xIndex) + eps) < SnodeTime)
		  {
		    cxIndex = xIndex;
		    if (((*discrPoints).at(xIndex+1) - eps) > SnodeTime)
		      {
			pxIndex = xIndex+1;
		      }
		    else
		      {
			pxIndex = xIndex+2; //DiscrPoint and SpecPoint coincides NOT TESTED
		      }
		    break;
		  }
	      }
	  }
	//cerr << "Sindex: " << Sindex << " cxIndex: " << cxIndex << " pxIndex: " << pxIndex << "\n";
	// Finally set the determined discr. points
	discrPtBelowSnode->push_back(cxIndex);
	discrPtAboveSnode->push_back(pxIndex);	
      }  
  }

  void fastGEM::fillSpecPtBelowTable()
  { 
    specPtBelowDiscrPt=GenericMatrix<unsigned>(noOfDiscrPoints+1,noOfGNodes);
    //=====================================
    // THIS iIS NOT RESET BETWEEN S CHANGES
    //=====================================

    for (unsigned uIndex = 0; uIndex <= G.getNumberOfNodes()-1; uIndex++)
      {
	Node* u = G.getNode(uIndex);
	Node* uLambda = lambda[u];
	unsigned uLambdaIndex = uLambda->getNumber();

	// Climb up from xIndex = lambda(u) (=sigma(u) until root and 
	// set all allowed discr. points for Sindex and uIndex
	unsigned Sindex = uLambdaIndex;
	while(Sindex <= S.getNumberOfNodes()-1)
	  {
	    unsigned xAboveSnode;
	    unsigned xBelowSparent;

	    Node* Snode = S.getNode(Sindex);
	    unsigned SparentIndex;
	    if(Sindex == uLambdaIndex) // it's a speciation -> 0
	      {
		xAboveSnode = 0;
	      }
	    else
	      {
		xAboveSnode = getDiscrPtAboveSnode(Sindex);
	      }
	    if (Snode->isRoot())
	      {
		SparentIndex = S.getNumberOfNodes(); // to stop the while-loop
		xBelowSparent = noOfDiscrPoints - 1;
	      }
	    else
	      {
		Node* Sparent = Snode->getParent();
		SparentIndex = Sparent->getNumber();
		xBelowSparent = getDiscrPtBelowSnode(SparentIndex);
	      }
	    
	    for (unsigned xIndex = xAboveSnode;
		 xIndex <= xBelowSparent; xIndex++)
	      {
		specPtBelowDiscrPt(xIndex, uIndex) = Sindex;
		//cerr << "uLambdaIndex: " << uLambdaIndex << " uIndex: " << uIndex << " xIndex: " << xIndex << " Sbelow: " << Sindex << "\n"; 
	      }
	    Sindex = SparentIndex;
	  }
      }  
  }

  void fastGEM::printSa()
  {
    Real zerosCounter = 0.0;
    Real totalCounter = 0.0;
    Real zeroTotalQuotient = 0.0;

    cout << "Sa:\n";
    for (unsigned uIndex = 0; uIndex <= (noOfGNodes-1); uIndex++)
      {
	for (unsigned xIndex = 0; xIndex <= (noOfDiscrPoints-1); xIndex++)
	  {
	    Probability SaValue = getSaValue(xIndex,uIndex);
	    cout << SaValue.val() << "\t";

	    if (SaValue.val() == 0.0)
	      {
		zerosCounter++;
	      }
	    totalCounter++;
	  }
       	cout << "\n";
      }
    zeroTotalQuotient = zerosCounter/totalCounter;
    //cout << "zerosCounter: " << zerosCounter << " totalCounter: " << totalCounter << " zeroTotalQuotient: " << zeroTotalQuotient << "\n";
    cout << "\n";
  }

  void fastGEM::printLb()
  {
    Real zerosCounter = 0.0;
    Real totalCounter = 0.0;
    Real zeroTotalQuotient = 0.0;

    cout << "Lb:\n";
    for (unsigned uIndex = 0; uIndex <= (noOfGNodes-1); uIndex++)
      {
	for (unsigned xIndex = 0; xIndex <= (noOfDiscrPoints-1); xIndex++)
	  {
	    for (unsigned iIndex = 0; iIndex <= (noOfDiscrPoints-1); iIndex++)
	      {
		Probability LbValue = getLbValue(xIndex,uIndex,iIndex);
		cout << LbValue.val() << "\t";

		if (LbValue.val() == 0.0)
		  {
		    zerosCounter++;
		  }
		totalCounter++;
	      }
	    cout << "\n";
	  }
	cout << "\n";
      }
    zeroTotalQuotient = zerosCounter/totalCounter;
    //cout << "zerosCounter: " << zerosCounter << " totalCounter: " << totalCounter << " zeroTotalQuotient: " << zeroTotalQuotient << "\n";
    cout << "\n";
  }

  void fastGEM::printLt()
  {
    Real zerosCounter = 0.0;
    Real totalCounter = 0.0;
    Real zeroTotalQuotient = 0.0;

    cout << "Lt:\n";
    for (unsigned uIndex = 0; uIndex <= (noOfGNodes-1); uIndex++)
      {
	for (unsigned xIndex = 0; xIndex <= (noOfDiscrPoints-1); xIndex++)
	  {
	    for (unsigned iIndex = 0; iIndex <= (noOfDiscrPoints-1); iIndex++)
	      {
		Real LtValue = getLtValue(xIndex,uIndex,iIndex);
		cout << LtValue << "\t";
		
		if (LtValue == 0.0)
		  {
		    zerosCounter++;
		  }
		totalCounter++;
	      }
	    cout << "\n";
	  }
	cout << "\n";
      }
    zeroTotalQuotient = zerosCounter/totalCounter;
    //cout << "zerosCounter: " << zerosCounter << " totalCounter: " << totalCounter << " zeroTotalQuotient: " << zeroTotalQuotient << "\n";
    cout << "\n";
  }
  
  string
  fastGEM::print() const
  {
    ostringstream oss;
    oss << "Edge weights probabilities are computed using discretized\n"
	<< "intergration over all rate x time combinations compliant with lengths,\n"
	<< "where the guest tree withtimes are modeled with the gene evolution\n"
	<< "model and rates are iid distributed with an underlying "
	<< df->print();
    return oss.str();
  }

}  // end namespace beep  

