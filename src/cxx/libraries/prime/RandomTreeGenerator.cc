#include "RandomTreeGenerator.hh"

namespace beep
{
  using namespace std;
  Tree 
  RandomTreeGenerator::generateRandomTree(vector<string> leaf_names)
  {
    RandomTreeGenerator tg;
    tg.G.setRootNode(tg.growTree(tg.addLeaves(leaf_names)));      
    return tg.G;
  }
  
  Node*
  RandomTreeGenerator::growTree(vector<Node*> leaves)
  {
    vector<Node*>::iterator nodeIter1;
    vector<Node*>::iterator nodeIter2;
    //Proceed with the births in reverse order
    while(leaves.size()>1)
      {
	//Create parent of random pair of adjacent leaves and substitute these 
	//for the parent in vector leaves
	nodeIter1 = leaves.begin() + R.genrand_modulo(leaves.size());
	do
	  {
	    nodeIter2 = leaves.begin() + R.genrand_modulo(leaves.size());
	  }
	while(nodeIter1 == nodeIter2);
	Node* parent = G.addNode(*nodeIter1, *nodeIter2);

        // TODO: FIXTHIS
        // cppcheck reports this: "(error) After insert, the iterator 'nodeIter1' may be invalid"
	// I found this:
	// http://stackoverflow.com/questions/6438086/iterator-invalidation-rules
        // We seem to use the iterators after they have been invalidated
 
	leaves.insert(leaves.erase(nodeIter2, nodeIter2+1), parent);
	leaves.erase(nodeIter1, nodeIter1+1);
      }
    return leaves[0];
  }


  vector<Node*>
  RandomTreeGenerator::addLeaves(vector<string> leaf_names)
  {
    vector<Node*> leaves;
    for(vector<string>::const_iterator i = leaf_names.begin();
	i != leaf_names.end(); i++)
      {
	leaves.push_back(G.addNode(0, 0, *i));
      }
    return leaves;
  }
  
}// end namespace beep
