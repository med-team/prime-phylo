#include "TreeInputOutput.hh"

#include "AnError.hh"
#include "GammaMap.hh"
//#include "HybridTree.hh"
#include "NHXannotation.h"
#include "Node.hh"
#include "StrStrMap.hh"
#include "Beep.hh"

#include <cstring>
#include <cassert>		// For early bug detection

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <string>


// Author: Lars Arvestad, Bengt Sennblad, � the MCMC-club, SBC, all rights reserved
namespace beep

{
  using namespace std;

  xmlNodePtr TreeInputOutput::leftNode( xmlNodePtr xmlNode ) {
    assert(xmlNode);
    return indexNode( xmlNode, 0 );
  }

  bool TreeInputOutput::isRoot( xmlNodePtr xmlNode ) {
    assert(xmlNode);
    xmlNodePtr parent = xmlNode->parent;
    if ( parent && parent->type  == XML_ELEMENT_NODE && xmlStrEqual( parent->name, BAD_CAST "tree" ))
      {
	return true;
      }
    return false;
  }

  bool TreeInputOutput::isLeaf( xmlNodePtr xmlNode ) {
    assert(xmlNode);
    if ( leftNode( xmlNode ) ) { return false; }
    return true;
  }

  bool TreeInputOutput::hasChild(  xmlNodePtr xmlNode, const char * childname ) {
    assert(xmlNode);
    for (xmlNodePtr cur = xmlNode->children; cur; cur = cur->next) {
      if ( cur->type  == XML_ELEMENT_NODE && xmlStrEqual( cur->name, BAD_CAST childname ))
	{
	  return true;
	}
    }
    return false;
  }

  xmlNodePtr TreeInputOutput::rightNode( xmlNodePtr xmlNode ) {
    assert(xmlNode);
    return indexNode( xmlNode, 1 );
  }

  xmlNodePtr TreeInputOutput::indexNode( xmlNodePtr xmlNode, int index ) { 
    int i = -1;
    assert( index >= 0 );
    assert( xmlNode );
    for (xmlNodePtr cur = xmlNode->children; cur; cur = cur->next) {
      //  Maybe we should check for the element name too? /Erik SJolund 2009-12-15
      //	  if (cur->type == XML_ELEMENT_NODE   &&     xmlStrEqual( cur->name, BAD_CAST "node" )) {
      if (cur->type == XML_ELEMENT_NODE ) {
	i++;
	if ( index == i ) { return cur; }
      }
    }
    return NULL;
  }
  
  unsigned  TreeInputOutput::treeSize(xmlNodePtr node) {
    if (node == NULL) {
      return 0;
    } else {
      return 1 
	+ treeSize(leftNode(node)) 
	+ treeSize(rightNode(node));
    }
  }

  void  TreeInputOutput::fromFileStream(FILE * f,  enum inputFormats format)
  {
    switch ( format ) {
    case inputFormatBeepOrHybrid : {

      NHXtree *tree = read_tree_from_file_stream(f);
      assert(tree);
      createXMLfromNHX(tree);
      delete_trees(tree);
      return;
    }; break;
    case inputFormatXml : // TODO ... write this.
      {
	std::string str;
	char buf[100];
	while ( ! feof( f ) ) {
	  size_t  bytes_read = fread(buf, 1 ,sizeof(buf) -1, f) ; 
	  if (ferror(f))
	    {
	      fprintf(stderr, "could not read\n");
	      abort(); // was exit(1);
	    }
	  buf[ bytes_read ]=NULL;
	  str.append( buf );
	}
	fromString( str   ,format);
      }
      break;
    }
  }

  void  TreeInputOutput::createXMLfromNHX(NHXtree *tree)
  {
    cleanup();
    assert(tree);
    LIBXML_TEST_VERSION;
    doc = xmlNewDoc(BAD_CAST "1.0");
    assert(doc);
    xmlroot = xmlNewNode(NULL, BAD_CAST "root");
    assert(xmlroot);
    xmlDocSetRootElement(doc,xmlroot);
    xmlNodePtr res = TreeInputOutput::createXMLfromNHX(tree,xmlroot);
    assert(res);
  }

  void  TreeInputOutput::cleanup()
  {
    if ( doc ) {  
      xmlFreeDoc(doc); doc=NULL; xmlroot=NULL;
      xmlCleanupParser(); 
    }
    if (inputFileStreamThatWasOpenedByMe) { fclose(inputFileStreamThatWasOpenedByMe); inputFileStreamThatWasOpenedByMe=NULL; }
  }

  void  TreeInputOutput::writeInputXML(FILE *fWrite, bool format)
  {
    assert(doc);
    assert(fWrite);
    int res2 = xmlDocFormatDump(fWrite, doc, format);
    if (res2 == -1) {
      throw AnError("Writing the XML data to a file failed for unknown reasons!", 1);
    }
    return;
  }

  TreeInputOutput::TreeInputOutput() : inputFileStreamThatWasOpenedByMe(NULL), doc(NULL), xmlroot(NULL)
  {
    return;
  }

  TreeInputOutput::~TreeInputOutput() 
  {
    cleanup();
  }

  // The basic recursion function for reading XML DOM trees
  //----------------------------------------------------------------------
  Tree
  TreeInputOutput::readBeepTree(TreeIOTraits& traits,
				vector<SetOfNodes> *AC, 
				StrStrMap *gs)
  {
    assert(xmlroot);
    for (xmlNodePtr cur = xmlroot->children; cur; cur = cur->next) {
      if (cur->type == XML_ELEMENT_NODE  &&     xmlStrEqual( cur->name, BAD_CAST "tree" ) ) {
	Tree tree;
        readBeepTree(cur,traits, AC, gs, tree,0,0);
	return tree;
      }
    }
    fprintf( stderr, "error: could not find any tree\n"); 
    abort(); // was exit(1);
  }


  /*
    xmlNodePtr
    TreeInputOutput::parseFromFileStreamd(int fd)
    {
    NHXtree *tree = read_tree_from_file_descriptor(fd);
    xmlNodePtr res =  TreeInputOutput::createXMLfromNHX(NHXtree *tree);
    return res;
    }
  */

  xmlNodePtr
  TreeInputOutput::createXMLfromNHX(NHXtree *tree,  xmlNodePtr   treeRoot)
  {
    assert(tree);
    assert(treeRoot);
    while ( tree ) {
      xmlNodePtr child = xmlNewChild(treeRoot, NULL, BAD_CAST "tree", NULL );
      assert(child);
      createXMLfromNHXrecursive( tree->root, child );
      tree = tree->next;
    }
    return treeRoot;
  }



  void
  TreeInputOutput::createXMLfromNHXrecursive2(struct NHXnode *v,  xmlNodePtr parent)
  {
    if(v ==0) { return;};
    xmlNodePtr child = xmlNewChild(parent, NULL, BAD_CAST "node", NULL );
    assert(child);
    createXMLfromNHXrecursive(v, child);
    return;
  }


  void
  TreeInputOutput::createXMLfromNHXrecursive(struct NHXnode *v,  xmlNodePtr child)
  {
    if(v ==0) { return;};



    if (v->name) {
      xmlNewProp(child, BAD_CAST "v_name", BAD_CAST v->name );
    }
    struct NHXannotation *a;

    a = v->l;
    vector<string> realAnnotations;
    realAnnotations.push_back("TT");
    realAnnotations.push_back("ET");
    realAnnotations.push_back("NT");
    realAnnotations.push_back("BL");
    realAnnotations.push_back("NW");

    vector<string> intAnnotations;
    intAnnotations.push_back("ID");
    intAnnotations.push_back("D");
    intAnnotations.push_back("EX");

    vector<string> strAnnotations;
    strAnnotations.push_back("NAME");
    strAnnotations.push_back("S");

    vector<string> listAnnotations;
    listAnnotations.push_back("AC");
    listAnnotations.push_back("HY");

    while (a != NULL) {
      vector<string>::const_iterator iter;
      char tmpBuff[LENGTH_OF_PRINTF_TMP_BUF];

      bool found = false;
      char * propValue= NULL;

      for(iter=realAnnotations.begin(); iter!=realAnnotations.end(); iter++)  {
	if ( strcmp( a->anno_type, iter->c_str() ) == 0 ) { 
	  // Ok the arg.t is of datatype float, but I guess datatype Real was meant /Erik Sjolund 2009-12-21
	  sprintfReal(tmpBuff, sizeof(tmpBuff), a->arg.t); propValue = tmpBuff; found=true; }
      }

      for(iter=intAnnotations.begin(); iter!=intAnnotations.end(); iter++)  {
	if ( strcmp( a->anno_type,iter->c_str()  ) == 0) { 
	  snprintf(tmpBuff, sizeof(tmpBuff), "%u",  a->arg.i); propValue = tmpBuff; found=true; }
      }

      for(iter=strAnnotations.begin(); iter!=strAnnotations.end(); iter++)  {
	if ( strcmp( a->anno_type, iter->c_str()  ) == 0) { propValue = a->arg.str; found=true; }
      }

      if ( found ) { 
	xmlNewProp(child, BAD_CAST a->anno_type, BAD_CAST propValue );
      }

      for(iter=listAnnotations.begin(); iter!=listAnnotations.end(); iter++)  {
	if ( strcmp( a->anno_type, iter->c_str()  ) == 0) {

	  xmlNodePtr listNode =  xmlNewChild(child, NULL, BAD_CAST a->anno_type, NULL );
	  assert( listNode );
	  struct int_list *il = a->arg.il;
	  while (il) 
	    {
	      sprintf( tmpBuff, "%i", il->i ); propValue = tmpBuff; found=true;
	      xmlNodePtr    intNode =    xmlNewChild(listNode, NULL, BAD_CAST "int", BAD_CAST  tmpBuff );
	      assert(intNode);
	      il = il->next;
	    }
	  found=true; 
        }
      }
      if ( ! found ) 
	{ 
	  fprintf(stderr , "annotation name \"%s\" not recognized!!", a->anno_type);
          abort(); // was exit(1);
	}
      a = a->next;
    }

    createXMLfromNHXrecursive2(v->left, child);
    createXMLfromNHXrecursive2(v->right, child);
  }

  //--------------------------------------------------------------------
  //
  // Constructors
  // 
  //--------------------------------------------------------------------

  void
  TreeInputOutput::fromFile(const string &filename, enum inputFormats format)
  {
    FILE * f;
    if ( ( f = fopen(filename.c_str(), "r")) == NULL ) {  fprintf(stderr,"error: could not open file %s\n", filename.c_str()); 
	      abort(); // was exit(1);
    }

    fromFileStream(f,format);
    inputFileStreamThatWasOpenedByMe=f;
    return;
  }

  void
  TreeInputOutput::fromString(const string &str, enum inputFormats format)
  {
    switch ( format ) {
    case inputFormatBeepOrHybrid : {
      NHXtree  *tree = read_tree(NULL);
      assert(tree);
      createXMLfromNHX(tree);
      delete_trees(tree);
    }; break;
    case inputFormatXml : {
      cleanup();
      LIBXML_TEST_VERSION;
      assert(doc == NULL);
      size_t len = str.length();
      doc = xmlReadMemory(str.c_str(), len, "", NULL, 0); 
      if (doc == NULL) {
	printf("error: could not parse xml\n");
      }
      xmlroot = xmlDocGetRootElement(doc);
    }
      break;
    }
    return;
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------
 
  //----------------------------------------------------------------------
  // Reading trees:
  //----------------------------------------------------------------------

  // Basic function for reading trees in BEEP format
  //----------------------------------------------------------------------
  Tree
  TreeInputOutput::readBeepTree(vector<SetOfNodes> *AC, StrStrMap *gs)
  {
    TreeIOTraits traits;
    checkTagsForTrees(traits); // Also reads the tree, apparently!
    return readBeepTree(traits, AC, gs); 
  }

  // Convenience front to readBeepTree(...)
  // Reads times and what else there is
  //----------------------------------------------------------------------
  Tree
  TreeInputOutput::readHostTree()
  {
    // reading all host trees, but we only need the first.
    vector<Tree> treeV = readAllHostTrees();
    assert(treeV.size() > 0);
    return  treeV[0];
  }

  // Convenience front to readBeepTree(...)
  // Reads 'NW tags' as edge lengths and what else there is
  // Reads antichains info and gene species maps
  //----------------------------------------------------------------------
  Tree
  TreeInputOutput::readGuestTree(vector<SetOfNodes>* AC, StrStrMap* gs)
  {
    vector<Tree> treeV = readAllGuestTrees();
    assert(treeV.size() > 0);
    return  treeV[0];
  }

  // Convenience front to readGuestTree(...)
  // Reads 'NW tags' as edge lengths and what else there is
  // Doese not read antichains info and gene species maps
  //----------------------------------------------------------------------
  Tree
  TreeInputOutput::readGuestTree()
  {
    return readGuestTree(0, 0);
  }

  // Convenience front to readBeepTree(...)
  // Reads a plain newick tree with branch lengths from NW
  //----------------------------------------------------------------------
  Tree
  TreeInputOutput::readNewickTree()
  {
    vector<Tree> treeV = readAllNewickTrees();
    assert(treeV.size() > 0);
    return  treeV[0];
  }

  void
  TreeInputOutput::readBeepTree(xmlNodePtr xmlNode, TreeIOTraits& traits,
				std::vector<SetOfNodes> *AC,   
				StrStrMap  *gs, Tree& tree,
                                map<const Node*, Node*>* otherParent, 
                                map<const Node*, unsigned>* extinct)
  {
    assert(xmlNode);
    traits.enforceStandardSanity();

    // Create BeepVectors to hold required 'tag' info
    if (traits.hasET() || traits.hasNT())
      {
	tree.setTimes(*new RealVector(treeSize(xmlNode)),true);
      }


    // todo check this. 
    // In each of TreeIO.cc and HybridTreeIO.cc there are two variants of this code. 3 of the variants have
    // only 
    // if (traits.hasBL())
    // and one of the variants have 
    //     if(traits.hasBL() || (traits.hasNW() && traits.hasNWisET() == false) )
    //
    // Just to pick one of them I choose the last one.
    // /Erik Sjolund 2010-01-07

    if(traits.hasBL() || (traits.hasNW() && traits.hasNWisET() == false) )
      {
	tree.setLengths(*new RealVector(treeSize(xmlNode)), true);
      }

    Node* r;

    r = TreeInputOutput::extendBeepTree(tree, xmlNode, traits, AC, gs, otherParent, extinct);
    xmlChar * nameProp;
    if ((nameProp = xmlGetProp(xmlNode, BAD_CAST "NAME" )) == NULL) {
      string str = "G";
      tree.setName(str);
    } else {
      string str = (char *) nameProp;
      tree.setName(str);
      xmlFree(nameProp);
    }
    // Set Top Time if requested and available
    // if useET then ET handles topTime
    if(traits.hasNT())
      {
	if ((nameProp = xmlGetProp(xmlNode, BAD_CAST "TT" )) != NULL) {
	  Real toptime = xmlReadReal( nameProp );
	  tree.setTopTime(toptime);
	  xmlFree(nameProp);
	}
      }
    assert(r);
    tree.setRootNode(r);
    if(tree.IDnumbersAreSane(*r) == false)
      {
	throw AnError("There are higher ID-numbers than there are nodes in tree", 
		      "TreeInputOutput::ReadBeepTree");
      }
    return;
  }

  // Reads and return a vector of trees from file, reads info 
  // as indicated by traits
  //----------------------------------------------------------------------
  vector<Tree>
  TreeInputOutput::readAllBeepTrees(TreeIOTraits& traits,
				    std::vector<SetOfNodes> *AC, 
				    std::vector<StrStrMap>* gsV)
  {
    assert(xmlroot);
    vector<Tree> GV;
    for (xmlNodePtr cur = xmlroot->children; cur; cur = cur->next) {
      if (cur->type == XML_ELEMENT_NODE && xmlStrEqual( cur->name, BAD_CAST "tree" ) ) {
	StrStrMap gsi;
	Tree tree;
        readBeepTree(cur,traits, AC, &gsi,tree,0,0);
	// maybe we could:      GV.push_back(*tree); 
	GV.push_back(tree);

    if ( gsV ) {
	gsV->push_back(gsi);
    }
      }
    }
    reverse(GV.begin(), GV.end());
    return GV;
  }

  // Convenience front to readAllBeepTrees(...)
  // Reads everything there is 
  // Reads antichains info and gene species maps
  //----------------------------------------------------------------------
  vector<Tree>
  TreeInputOutput::readAllBeepTrees(vector<SetOfNodes>* AC, vector<StrStrMap>* gs)
  {
    TreeIOTraits traits;
    checkTagsForTrees(traits); // Also reads the tree, apparently!
    return readAllBeepTrees(traits, AC, gs);
  }

  // Convenience front to readAllBeepTrees(...)
  // Reads edge times from ET, NT or NW
  //----------------------------------------------------------------------
  vector<Tree>
  TreeInputOutput::readAllHostTrees()
  {
    TreeIOTraits traits;
    checkTagsForTrees(traits);
    if(traits.containsTimeInformation() == false)
      {
	throw AnError("Host tree lacks time information for some of it nodes", 1);
      }
    traits.enforceHostTree();
    return readAllBeepTrees(traits, 0, 0);
  }

  // Convenience front to readAllBeepTrees(...)
  // Reads branch lengths from BL or NW and what else there is
  // Reads antichains info and gene species maps
  //----------------------------------------------------------------------
  vector<Tree>
  TreeInputOutput::readAllGuestTrees(vector<SetOfNodes>* AC, vector<StrStrMap>* gs)
  {
    TreeIOTraits traits;
    checkTagsForTrees(traits); 
    if(traits.hasGS() == false)
      {
	gs = 0;
      }
    if(traits.hasAC() == false)
      {
	AC = 0;
      }
    traits.enforceGuestTree();
    return readAllBeepTrees(traits, AC, gs);
  }

  // Convenience front to readAllGeneTrees(...)
  // Reads branch lengths from BL or NW and what else there is
  // Doese not read antichains info and gene species maps
  //----------------------------------------------------------------------
  vector<Tree>
  TreeInputOutput::readAllGuestTrees()
  {
    return readAllGuestTrees(0,0);
  }

  // Convenience front to readAllBeepTrees(...)
  // Reads a plain newick tree with branch lengths from NW
  //----------------------------------------------------------------------
  vector<Tree>
  TreeInputOutput::readAllNewickTrees()
  {
    TreeIOTraits traits;
    checkTagsForTrees(traits);

    traits.setET(false);
    traits.setNT(false);
    traits.setBL(traits.hasNW());
    traits.setNWisET(false);

    traits.enforceNewickTree();

    return readAllBeepTrees(traits, 0,0);
  }

  //----------------------------------------------------------------------
  // Writing trees
  //----------------------------------------------------------------------



  void TreeInputOutput::createRealAttribute( xmlNodePtr xmlNode, const char * str, Real val )
{
  assert( str );
  assert( xmlNode );
  char tmpbuf[LENGTH_OF_PRINTF_TMP_BUF];
  sprintfReal(tmpbuf,sizeof(tmpbuf),val);

  // I guess it should be an error if the attribute was already set /Erik Sjolund 2009-12-21
  xmlAttrPtr attr = xmlHasProp(xmlNode, BAD_CAST str);
  assert( attr == NULL );
  xmlNewProp(xmlNode, BAD_CAST str, BAD_CAST tmpbuf );
  return;
}

  void TreeInputOutput::createIntAttribute( xmlNodePtr xmlNode, const char * str, int val )
{
  assert( str );
  assert( xmlNode );

  char tmpbuf[LENGTH_OF_PRINTF_TMP_BUF];
  snprintf(tmpbuf, sizeof(tmpbuf), "%i",val);
  // I guess it should be an error if the attribute was already set /Erik Sjolund 2009-12-21
  xmlAttrPtr attr = xmlHasProp(xmlNode, BAD_CAST str);
  assert( attr == NULL );

  xmlNewProp(xmlNode, BAD_CAST str, BAD_CAST tmpbuf );
  return;
  }

void TreeInputOutput::createXMLfromBeepTree(const Tree& T, 
					    const TreeIOTraits& traits,
					    const GammaMap* gamma, xmlNodePtr treeXmlNode   )
 
{

  assert(treeXmlNode);
  assert((traits.hasET() && traits.hasNT()) == false);

  //     xmlNodePtr child = xmlNewChild(parent, NULL, BAD_CAST "node", NULL );


  string least = "";
  ostringstream name;

  if (traits.hasName()) {
    std::string nameString = T.getName();
    xmlNewProp(treeXmlNode, BAD_CAST "v_name", BAD_CAST nameString.c_str() );


    if(traits.hasNT())
      {
	Real  topTime =  T.getTopTime();
	createRealAttribute( treeXmlNode, "TT", topTime );
      }


  }
  recursivelyWriteBeepTree(*T.getRootNode(), least, traits,
			   gamma, 0, 0, 0, treeXmlNode);
  return;
}

  // Basic function for writing tree T in newick format, with the tags 
  // indicated by traits included in PRIME markup. If gamma != NULL then AC 
  // markup will also be included.
  // Precondition: (useET && useNT) == false
  //----------------------------------------------------------------------
std::string 
  TreeInputOutput::writeXmlBeepTree(const Tree& T, 
				 const TreeIOTraits& traits,
				 const GammaMap* gamma      )
  {
    LIBXML_TEST_VERSION;
    xmlDocPtr doc = xmlNewDoc(BAD_CAST "1.0");
    assert(doc);
    xmlNodePtr treeXmlNode = xmlNewNode(NULL, BAD_CAST "tree");
    assert(treeXmlNode);
    xmlDocSetRootElement(doc,treeXmlNode);
    createXMLfromBeepTree(T, traits, gamma, treeXmlNode);


    xmlChar * mem;
    int size;
    xmlDocDumpFormatMemory(doc,  &mem,  &size, 1 /*format*/);
    string s( (const char *) mem );
    xmlFree(mem);
    return s;
  }

  // convenience front function for writeBeeTree(...) 
  // writes tree T with all attributes there is
  //----------------------------------------------------------------------
  string 
  TreeInputOutput::writeBeepTree(const Tree& G, const GammaMap* gamma)
  {
    TreeIOTraits traits;
    traits.setID(true);
    if(G.hasTimes())
      {
	traits.setNT(true);
      }
    if(G.hasLengths())
      {
	traits.setBL(true);
      }
    return writeBeepTree(G, traits, gamma);
  }
    
  // convenience front function for writeBeeTree(...) 
  // writes tree S with node times as NW
  //----------------------------------------------------------------------
  string 
  TreeInputOutput::writeHostTree(const Tree& S)
  {
    TreeIOTraits traits;
    traits.setID(true);
    if(S.hasTimes())
      {
	traits.setNT(true);
      }
    return writeBeepTree(S, traits, 0);
  }
    
  // convenience front function for writeBeepTree(...) 
  // writes tree G with lengths and with gamma/AC info
  //----------------------------------------------------------------------
  string 
  TreeInputOutput::writeGuestTree(const Tree& G, const GammaMap* gamma)
  {
    TreeIOTraits traits;
    traits.setID(true);
    if(G.hasLengths())
      {
	traits.setBL(true);
      }
    return writeBeepTree(G, traits, gamma);
  }
    
  // convenience front function for writeGeneTree(...) 
  // writes tree G with lengths without AC info
  //----------------------------------------------------------------------
  string 
  TreeInputOutput::writeGuestTree(const Tree& G)
  {
    return writeGuestTree(G, 0);
  }
    
  // convenience front function for writeBeepTree(...) 
  // writes plain newick tree T with branch lengths
  //----------------------------------------------------------------------
  string 
  TreeInputOutput::writeNewickTree(const Tree& G)
  {
    TreeIOTraits traits;
    if(G.hasLengths())
      {
	traits.setBL(true);
	traits.setNWisET(false);
      }
    return writeBeepTree(G, traits, 0);
  }
    
  //----------------------------------------------------------------------
  // Read associations
  //----------------------------------------------------------------------

  // Map leaves in the gene tree to leaves in the species tree
  // This is a bit incongruent with the rest of the code and should 
  // probably get its own class! /arve
  // Expected line format: 
  // <whitespace>? <gene name> <whitespace> <species name> <whitespace>?
  // Line length is at most 1024 chars.
  //--------------------------------------------------------------------
  StrStrMap
  TreeInputOutput::readGeneSpeciesInfo(const string &filename)
  {
    ifstream is(filename.c_str());

    // This yields warning that line is unused TODO: remove /bens
    //     char line[LINELENGTH];
    int lineno = 1;

    StrStrMap gene2species;
    if(is.peek() == '#') // gs may start with a '#'
      {
	char dummy[LINELENGTH];
	is.getline(dummy, LINELENGTH);// >> dummy;
      }
    while (is.good()) 
      {
	string gene;
	string species;
	if (is >> gene)
	  {
	    if (is >> species)
	      {
		gene2species.insert(gene, species);
	      }
	    else
	      {
		ostringstream line_str;
		line_str << "Line " << lineno;
		is.close();
		throw AnError("The gene-to-species mapping seems to be "
			      "badly formatted. ", line_str.str());
	      }
	  }
	lineno++;
      }

    is.close();
    return gene2species;
  }

  vector<StrStrMap>
  TreeInputOutput::readGeneSpeciesInfoVector(const string &filename)
  {
    ifstream is(filename.c_str());

    // This yields warning that line is unused TODO: remove /bens
    //     char line[LINELENGTH];
    int lineno = 1;

    vector<StrStrMap> gene2speciesVec;
    StrStrMap gene2species;
    string gsmark;
    is >> gsmark;
    if(gsmark != "#")
      {
	cerr << "error in gs vector, every gs must be preceded by '#' line\n";
        abort(); // was exit(1);
      }
    while (is.good()) 
      {
	char dummy[LINELENGTH];
	is.getline(dummy, LINELENGTH);
	string gene;
	string species;
	if (is >> gene)
	  {
	    if(gene == "#")
	      {
		gene2speciesVec.push_back(gene2species);
		gene2species.clearMap();
	      }
	    else
	      {
		if (is >> species)
		  {
		    gene2species.insert(gene, species);
		  }
		else
		  {
		    ostringstream line_str;
		    line_str << "(Line " << lineno << ")";
		    throw AnError("The gene-to-species mapping seems to be "
				  "badly formatted. ", line_str.str());
		  }
	      }
	  }
	
	lineno++;
      }
    gene2speciesVec.push_back(gene2species);
    

    return gene2speciesVec;
  }

  //----------------------------------------------------------------------
  // Check trees
  //----------------------------------------------------------------------
  
  // READ THE TREE, then
  // precheck what tags are present in the read NHX-tree. Since ID,
  // names of nodes and trees are always read - these are not checked
  //----------------------------------------------------------------------

  void
  TreeInputOutput::checkTagsForTrees(TreeIOTraits& traits)
  {
    assert(xmlroot);

    // Reset all argmuents before starting
    traits.setNW(true);
    traits.setET(true);
    traits.setNT(true);
    traits.setBL(true);
    traits.setGS(true);

    traits.setAC(false);
    traits.setHY(false);

    int i = 0;


    for (xmlNodePtr cur = xmlroot->children; cur; cur = cur->next) {
      if (cur->type == XML_ELEMENT_NODE  &&     xmlStrEqual( cur->name, BAD_CAST "tree" ) ) {
	i++;
	if(recursivelyCheckTags(cur, traits) == false)
	  {
	    throw AnError("The input tree was empty!",
			  "TreeInputOutput::checkTagsForTrees()",
			  1);
	  }
      }
    }
    if (i == 0) 
      {
	throw AnError("No input trees!",
		      "TreeInputOutput::checkTagsForTrees()",
		      1);
      }

    return;
  }
    
  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------


  bool TreeInputOutput::intList( xmlNodePtr xmlNode, const char * str, std::vector<int> & list ) 
  {
    assert( xmlNode );
    for (xmlNodePtr cur = xmlNode->children; cur; cur = cur->next) {
      if (cur->type == XML_ELEMENT_NODE && xmlStrEqual( cur->name, BAD_CAST str ) ) {
        for (xmlNodePtr cur2 = cur->children; cur2; cur2 = cur2->next) {
	  if (cur2->type == XML_ELEMENT_NODE && xmlStrEqual( cur2->name, BAD_CAST "int" ) ) {
	    xmlChar * content =  xmlNodeGetContent(cur2);
	    int i=xmlReadInt(content);
            xmlFree(content);
	    list.push_back(i);
	  }
	}
	return true;
      }
    }
    return false;
  }

  Node *
  TreeInputOutput::extendBeepTree(Tree &S, xmlNodePtr xmlNode,
				  const TreeIOTraits& traits,
				  vector<SetOfNodes> *AC, 
				  StrStrMap *gs,
				  map<const Node*, Node*>* otherParent,
				  map<const Node*, unsigned>* extinct)
  {

    if ( ! xmlNode ) { 
return 0; }

    Node* new_node;

    xmlChar * idProp;
    bool idFound = false;
    int idInt;
    if ((idProp = xmlGetProp(xmlNode, BAD_CAST "ID" )) != NULL) {
      idInt = xmlReadInt(idProp);
      xmlFree(idProp);
      idProp = NULL;
      idFound= true;
    }
    if (idFound) {
      new_node = S.getNode(idInt);

      // We must have ID to be able to give HY, which gives
      // the other parent of a hybrid child
      if(new_node)
	{
	  std::vector<int>  list;

	  if ( intList( xmlNode, "HY", list ))   
	    {
	      if(otherParent)
		{
#ifndef NDEBUG
		  assert(new_node->isRoot() == false);
 
		  assert(list.size() >= 2);
		  assert(list[0] >= 0);

		  Node * parent = new_node->getParent();
		  assert(static_cast<unsigned>( list[0] ) == 
			 parent->getNumber() ||
			 static_cast<unsigned>( list[1] )
			 == parent->getNumber());
#endif
		  (*otherParent)[new_node] = new_node->getParent();
		  // 			if(static_cast<unsigned>(il->i) == 
		  // 			   new_node->getParent()->getNumber())
		  // 			  {
		  // 			    (*otherParent)[id->arg.i] = il->i;
		  // 			  }
		  // 			else
		  // 			  {
		  // 			    (*otherParent)[id->arg.i] = il->next->i;	   
		  // 			  }
		  S.setTopTime(new_node->getTime());
		  return new_node;
		}
	      else
		{
		  throw AnError("This is a HybridTree. Please use "
				"readHybridTree instead",
				"TreeInputOutput::extendBeepTree",
				1);
		}
	    }
	  else
	    {
	      ostringstream oss;
	      oss << "TreeInputOutput::extendBeepTree\n"
		  << "Found duplicate ID for non-hybrid node "
		  <<  idInt << endl;
	      //  		    oss << "Subtree: " 
	      //  			<< new_node->getNumber() << endl;
	      // 			<< S.subtree4os(new_node, "", "", false,false,false) << endl;
	      throw AnError(oss.str(),1);
	    }
	}
    }


    //Otherwise create new node
    //-------------------------
    string name = decideNodeName(xmlNode);
    Real leftTime = 0;
    Real rightTime = 0;

    // Pass on recursion -- topTime is used to temporarily store 
    // the edgeTime of a Node, remember to record them in left/rightTime

    Node* l = extendBeepTree(S, leftNode(xmlNode), traits, AC, gs, otherParent, extinct);
    if(traits.hasET() && l)
      {
	leftTime = S.getTopTime() + S.getTime(*l);
      }
    Node* r = extendBeepTree(S, rightNode(xmlNode), traits, AC, gs, otherParent, extinct);
    if(traits.hasET() && r)
      {
	rightTime = S.getTopTime() + S.getTime(*r);
      }
    // Now create the new node
    if(idFound)
      {
	new_node = S.addNode(l, r, idInt, name);
      }
    else 
      {
	new_node =  S.addNode(l, r, name); 
      }
    assert(new_node != NULL);
	    
    Real edge_time = decideEdgeTime(xmlNode, traits, otherParent);       
    if(traits.hasET())
      {
	if(r && l)
	  {
	    if((2 * abs(leftTime - rightTime) / (leftTime + rightTime)) >= 0.01)
	      {
		ostringstream oss;
		oss << "Tree time inconsistency at node  "
		    << new_node->getNumber() 
		    <<"\nAccording to left subtree, node time is "
		    << leftTime
		    << " but right subtree says it should be "
		    << rightTime
		    << ".\n";
		throw AnError("TreeInputOutput::extendBeepTree: " +
			      indentString(oss.str()));
	      } 
	  }
	// There is a problem when setting time for a hybrid parent
	// when the other parent is no yet created -- AnError is thrown
	// so we catch it here
	try
	  {
	    S.setTime(*new_node, leftTime);
	  }
	catch(AnError& e)
	  {
	    if(string(e.what()) != string("HybridTree::setTime():\n"
					  "op is NULL for hybridNode"))
	      {
		throw e;
	      }
	    assert(S.getTime(*new_node) == leftTime);
	  }
	S.setTopTime(edge_time);
      }

    sanityCheckOnTimes(S, new_node, xmlNode, traits);
	
    // Check if any existing branchLength should be used
    //-------------------------------------------------------------------
    if(traits.hasBL() || (traits.hasNW() && traits.hasNWisET() == false))
      {
	handleBranchLengths(new_node, xmlNode, traits.hasNWisET());
      }

    //Associate gene and species names
    //-------------------------------------------------------------------
    if (l == 0 && r == 0 && gs != 0) // If this is a leaf and we want to read gs
      {
	xmlChar * prop;
	if ((prop = xmlGetProp(xmlNode, BAD_CAST "S" )) != NULL) 
	  {
	    edge_time = xmlReadReal(prop);
	    gs->insert(name, string(( const char * ) prop ));
	    xmlFree(prop);
	    prop = NULL;
	  }
	else 
	  {
	    //! todo{ How should I handle the situation when we 
	    //! want to test if a gs info is given in the guest tree?
	    //! leave gs empty, as was done before, or provide a 
	    //! special test function if a gs exists /bens}
	    // 	        throw AnError("No species given for leaf!", name, 1);
	  }
      }
	
    // get antichain (gamma) info if requested
    //-------------------------------------------------------------------
    if (AC != 0)
      {
	if(AC->empty()) // if elements is not allocated in AC do so!
	  {
	    AC->resize(100); // Warning arbitrary default size
	  }
	updateACInfo(xmlNode, new_node, *AC);
      }
    if( xmlHasProp(xmlNode, BAD_CAST "EX"))
      {
	if(extinct)
	  {
	    if(new_node->isLeaf() == false)
	      throw AnError("TreeInputOutput::extinct node must be a leaf",1);
	    (*extinct)[new_node] = 1;
	  }
	else
	  {
	    throw AnError("TreeInputOutput::extendBeepTree\n"
			  "Please use readHybridTree",1);
	  }
      }
    return new_node;
  }

  // Collect info for newickString
  // Compute markup for the anti-chains on node u
  //----------------------------------------------------------------------
  void
  TreeInputOutput::getAntiChainMarkup(Node &u, const GammaMap &gamma, xmlNodePtr xmlNode)
  {
    assert(xmlNode);
    string ac = "";

      xmlNodePtr child = xmlNewChild(xmlNode, NULL, BAD_CAST "AC", NULL );
      assert(child);
    if (gamma.numberOfGammaPaths(u) > 0)
      {
	Node *lower = gamma.getLowestGammaPath(u);
	Node *higher = gamma.getHighestGammaPath(u);
	do 
	  {
	    char buf[5];
	    if (snprintf(buf, 4, "%d", lower->getNumber()) == -1) 
	      {
		throw AnError("Too many anti-chains (more than 9999!) "
			      "or possibly a programming error.");
	      }
              
              xmlNodePtr intNode = xmlNewChild(child, NULL, BAD_CAST "int", BAD_CAST buf  );
              assert(intNode);
	    lower = lower->getParent();
	  } 
	while (lower && higher->dominates(*lower)); 
      }
    return;
  }


  // Find the right value for edge time
  Real
  TreeInputOutput::decideEdgeTime(xmlNodePtr xmlNode, const TreeIOTraits& traits,
				  bool isHY)
  {
    Real edge_time = 0.0;
    xmlChar * prop = NULL;
    if(traits.hasET()) // Use edge time info from file
      {
	if(traits.hasNWisET())
	  {
	    if ((prop = xmlGetProp(xmlNode, BAD_CAST "NW" )) != NULL) 
	      {
		edge_time = xmlReadReal(prop);
		xmlFree(prop);
		prop = NULL;
	      }
	    else if (isRoot(xmlNode)) 
	      {
		edge_time = 0.0;
	      } 
	    else
	      {
		throw AnError("Edge without edge time found in tree.. ", 
			      1);
	      }
	  }
	else   if ((prop = xmlGetProp(xmlNode, BAD_CAST "ET" )) != NULL) 
	  {
	    edge_time = xmlReadReal(prop);
	    xmlFree(prop);
	    prop = NULL;
	  }
	else if (isRoot(xmlNode)) 
	  {
	    edge_time = 0.0;
	  } 
	else
	  {
	    throw AnError("Edge without edge time found in tree.", 1);
	  }
	// Check for sanity
	if(edge_time <= 0)
	  {
	    if(edge_time < 0)
	      {
		throw AnError("Tree contains an edge with negative time",1);
	      }
	    else if(isHY == false && !isRoot(xmlNode)) 
	      {
		throw AnError("Tree contains an edge with zero time.", 1);
	      }
	  }
      }
    return edge_time;
  }

  // Always include name , if it exists
  string
  TreeInputOutput::decideNodeName(xmlNodePtr xmlNode) 
  {
    string name = "";		// Default name is empty
    xmlChar * vnameProp;
    if ((vnameProp = xmlGetProp(xmlNode, BAD_CAST "v_name" )) == NULL) 
      {
	xmlChar * nameProp;
	if ((nameProp = xmlGetProp(xmlNode, BAD_CAST "S" )) != NULL) 
	  {
	    name = ( const char * ) nameProp;
	    xmlFree(nameProp);
	  };
      }
    else
      {
	name = ( const char * ) vnameProp;
	xmlFree(vnameProp);
      }
    return name;
  }


  // Sanity check for edge times
  void
  TreeInputOutput::sanityCheckOnTimes(Tree& S, Node *node, xmlNodePtr xmlNode, 
				      const TreeIOTraits& traits)
  {
    // Check if any existing info about node time should be used
    // Note that we don't allow using both ET and NT

    if(traits.hasNT())
      {
	// check for sanity - we only need one time measure!
	if(traits.hasET())
	  {
	    throw AnError("Superfluous time measure, use either ET or NT, "
			  "but not both");
	  }
	xmlChar * prop = NULL;
	if ((prop = xmlGetProp(xmlNode, BAD_CAST "NT" )) != NULL) 
	  {
	    Real f = xmlReadReal(prop);
	    xmlFree(prop);
	    prop = NULL;
	    try
	      {
		S.setTime(*node, f);
	      }
	    catch(AnError& e)
	      {
		if(string(e.what()) != string("HybridTree::setTime():\n"
					      "op is NULL for hybridNode"))
		  {
		    throw e;
		  }
		assert(S.getTime(*node) == f);
	      }
	  }
	else
	  {
	    throw AnError("Edge without node time found in tree.", 1);
	  }
      }
  }

  void
  TreeInputOutput::handleBranchLengths(Node *node, xmlNodePtr xmlNode, bool NWIsET)
  {
    xmlChar * prop = NULL;
    if ((prop = xmlGetProp(xmlNode, BAD_CAST "BL" )) != NULL) 
      {
	Real f = xmlReadReal(prop);
	xmlFree(prop);
	prop = NULL;
	node->setLength(f);
      }
    else if(NWIsET)
      {
	throw AnError("TreeInputOutput::extendBeepTree(...):\n"
		      "No branch length info found either in 'BL' and 'NW' is used for 'ET'",
		      234);
      }
    else

      if ((prop = xmlGetProp(xmlNode, BAD_CAST "NW" )) != NULL) 
	{
	  Real f = xmlReadReal(prop);
	  xmlFree(prop);
	  prop = NULL;
	  node->setLength(f);
	}
      else if ( ! isRoot(xmlNode) )	  
	{
	  throw AnError("TreeInputOutput::extendBeepTree(...):\n"
			"No branch length info found either in 'BL' or 'NW'",
			234);
	}
  }

  // Basic helper function for writing trees in BEEP format
  void
  TreeInputOutput::recursivelyWriteBeepTree(Node& u, string& least,
					    const TreeIOTraits& traits,
					    const GammaMap* gamma,
					    map<const Node*,Node*>* otherParent,
					    map<const Node*,unsigned>* extinct,
					    map<unsigned, unsigned>* id, xmlNodePtr xmlNode)
  {
    assert(xmlNode);
    assert((traits.hasID() && id) == false);
    string ret;

    // Determine what should be tagged in PRIME markup
    //-------------------------------------------------------------------
    std::ostringstream tagstr;
    std::ostringstream NWstr;


    if(traits.hasID())
      {
	createIntAttribute( xmlNode, "ID",u.getNumber() );
      }

    if (traits.hasET())
      {
	if (traits.hasNWisET())
	  {
  	createRealAttribute( xmlNode, "FIXTHIS",u.getTime() );
	//	    NWstr << std::showpoint <<":" << u.getTime();
	  }
	else
	  {
	    //	    tagstr << " ET=" << std::showpoint << u.getTime();
      	   createRealAttribute( xmlNode, "ET",u.getTime() );
	  }
      }
    
    if (traits.hasNT())
      {
	createRealAttribute( xmlNode, "NT",u.getNodeTime() );
      }

    if (traits.hasBL())
      {
	if (traits.hasNWisET())
	  {
     	    createRealAttribute( xmlNode, "BL",u.getLength() );
	  }
	else
	  {
	    NWstr << ":" << u.getLength();
	  }
      }

    // Now add node in newick format and gamma/AC if requested
    // This is done differently ifor leaves and internal nodes
    //-------------------------------------------------------------------
    if (u.isLeaf())  // leaves recursion stops and 'S' is set
      {
	if(id)
	  {
	    if(id->find(u.getNumber()) == id->end())
	      {
		unsigned i = id->size();
		(*id)[u.getNumber()] = i;
	      }
	    tagstr << " ID=" << (*id)[u.getNumber()];
	  }

	// add node in newick format
	// 	ret = least = u.getName();
	least = u.getName();

	// then add gamma to PRIME markup if requested
	if(gamma)
	  {
	    Node *species = gamma->getLowestGammaPath(u);
	    // check for consistency
	    if(species)
	      {
		// Add species info and AC
		//		tagstr << " S=" << species->getName() 
		//      << getAntiChainMarkup(u, *gamma);
	      }
	    else
	      {
		ostringstream err;
		err << "Cannot write AC, since leaf " 
		    << u.getNumber() 
		    <<" lacks a species";
		throw AnError(err.str());
	      }
	  }
      }
    else   // Internal nodes needs to send recursion on and sets 'D'
      {
	// First get the strings for subtrees
	string least_left;
	string least_right;
	/*		string left_str = recursivelyWriteBeepTree(*u.getLeftChild(), 
						   least_left, traits,
						   gamma, otherParent, extinct, id);
    	string right_str = recursivelyWriteBeepTree(*u.getRightChild(),
						    least_right, traits,
						    gamma, otherParent, extinct, id);
	*/
	if(id)
	  {
	    if(id->find(u.getNumber()) == id->end())
	      {
		unsigned i = id->size();
		(*id)[u.getNumber()] = i;
	      }
	    tagstr << " ID=" << (*id)[u.getNumber()];
	  }
	
	ret.reserve(1024); 	// Avoid too many internal resize (expensive)

	// Add node in newick format
	// Always order leaves in as alphabetical order as possible


	/*
	if (least_left < least_right)
	  {
	    least = least_left;
	    ret = "(" + left_str + ", " + right_str + ")";
	  }
	else
	  {
	    least = least_right;
	    ret = "(" + right_str + ", " + left_str + ")";
	  }
	*/

	// then add gamma/AC to BEEP markup if requested
	if(gamma)
	  {
	    if(gamma->isSpeciation(u))
	      {
		tagstr << " D=0";
	      }
	    else
	      {
		tagstr << " D=1";
	      }
	    //	    tagstr << getAntiChainMarkup(u, *gamma);
	  }
      }
    if(otherParent && otherParent->find(&u) != otherParent->end())
      {  
	unsigned pn = u.getParent()->getNumber();
	unsigned opn = (*otherParent)[&u]->getNumber();
	if(id)
	  {
	    if(id->find(pn) == id->end())
	      {
		unsigned i = id->size();
		(*id)[pn] = i;
	      }
	    if(id->find(opn) == id->end())
	      {
		unsigned i = id->size();
		(*id)[opn] = i;
	      }
            tagstr << " HY=(" << (*id)[pn] << " " << (*id)[opn] << ")";
	  }
      }
    if(extinct && extinct->find(&u) != extinct->end())
      {
	tagstr << " EX=1";
      }

    if(u.getName()!= "")
      {
	ret.append(u.getName());
      }

    ret.append(NWstr.str());

    if(tagstr.str() != "")
      {
	ret.append("[&&PRIME" + tagstr.str() + "]");
      }

    return;
  }

  void
  TreeInputOutput::decideSubtreeOrder(Node& u, map<Node*, string> order)
  {
    if(order.find(&u) != order.end())
      return;
    else if(u.isLeaf())
      order[&u] = u.getName();
    else
      {
	decideSubtreeOrder(*u.getLeftChild(), order);
	decideSubtreeOrder(*u.getRightChild(),order);
	order[&u] = min(order[u.getLeftChild()], order[u.getRightChild()]);
      }
    return;
  }
		      


  // Basic helper function for writing trees in BEEP format
  void
  TreeInputOutput::recursivelyWriteBeepTree(Node& u, map<Node*, string> least,
					    const TreeIOTraits& traits,
					    const GammaMap* gamma,
					    map<const Node*,Node*>* otherParent,
					    map<const Node*,unsigned>* extinct,
					    map<unsigned, unsigned>* id, xmlNodePtr xmlNode)
  {

    assert(xmlNode);
    assert((traits.hasID() && id) == false);
    string ret;

    // Determine what should be tagged in PRIME markup
    //-------------------------------------------------------------------
    std::ostringstream tagstr;
    std::ostringstream NWstr;

    createIntAttribute( xmlNode, "getNumber",u.getNumber() );

    if(id->find(u.getNumber()) == id->end())
      {
	unsigned i = id->size();
	(*id)[u.getNumber()] = i;
      }
    createIntAttribute(  xmlNode, "idMapNumber", (*id)[u.getNumber()] );
    createRealAttribute( xmlNode, "getTime", u.getTime()  );
    createRealAttribute( xmlNode, "getNodeTime", u.getTime()  );
    createRealAttribute( xmlNode, "getLength", u.getLength() );

    // Now add node in newick format and gamma/AC if requested
    // This is done differently ifor leaves and internal nodes
    //-------------------------------------------------------------------
    if (u.isLeaf())  // leaves recursion stops and 'S' is set
      {
	// then add gamma to PRIME markup if requested
	if(gamma)
	  {
	    Node *species = gamma->getLowestGammaPath(u);
	    // check for consistency
	    if(species)
	      {
		// Add species info and AC
		  string nameStr = species->getName();
                 xmlNewProp(xmlNode, BAD_CAST "S", BAD_CAST nameStr.c_str());
		  getAntiChainMarkup(u, *gamma, xmlNode);
	      }
	    else
	      {
		ostringstream err;
		err << "Cannot write AC, since leaf " 
		    << u.getNumber() 
		    <<" lacks a species";
		throw AnError(err.str());
	      }
	  }
      }
    else   // Internal nodes needs to send recursion on and sets 'D'
      {
	// First get the strings for subtrees
	// Add node in newick format
	// Always order leaves in as alphabetical order as possible


         xmlNodePtr leftNode = xmlNewChild(xmlNode, NULL, BAD_CAST "node", NULL  );
         assert(leftNode);
         xmlNodePtr rightNode = xmlNewChild(xmlNode, NULL, BAD_CAST "node", NULL  );
         assert(rightNode);


	if(least[u.getLeftChild()] < least[u.getRightChild()])
	  { 
            recursivelyWriteBeepTree(*u.getLeftChild(), least, traits,
				     gamma, otherParent, extinct, id, leftNode);
	    recursivelyWriteBeepTree(*u.getRightChild(),least, traits,
				     gamma, otherParent, extinct, id, rightNode);
	  }
	else
	  { 
            recursivelyWriteBeepTree(*u.getRightChild(), least, traits,
				     gamma, otherParent, extinct, id, leftNode);
	    recursivelyWriteBeepTree(*u.getLeftChild(),least, traits,
				     gamma, otherParent, extinct, id, rightNode);
	  }


	// then add gamma/AC to BEEP markup if requested
	if(gamma)
	  {
	    if(gamma->isSpeciation(u))
	      {
               createIntAttribute(  xmlNode, "D", 0);
	      }
	    else
	      {
               createIntAttribute(  xmlNode, "D", 1);
	      }
		 getAntiChainMarkup(u, *gamma, xmlNode);
	  }
      }

    if(otherParent && otherParent->find(&u) != otherParent->end())
      {  
	unsigned pn = u.getParent()->getNumber();
	unsigned opn = (*otherParent)[&u]->getNumber();
	if(id)
	  {
	    if(id->find(pn) == id->end())
	      {
		unsigned i = id->size();
		(*id)[pn] = i;
	      }
	    if(id->find(opn) == id->end())
	      {
		unsigned i = id->size();
		(*id)[opn] = i;
	      }
	  }

      xmlNodePtr child = xmlNewChild(xmlNode, NULL, BAD_CAST "HY", NULL );
      assert(child);

      char buf[5];

      snprintf( buf, sizeof(buf), "%u",  (*id)[pn] ); 
      xmlNodePtr intNode = xmlNewChild(child, NULL, BAD_CAST "int", BAD_CAST buf  );
      assert(intNode);

      snprintf( buf, sizeof(buf), "%u",  (*id)[opn] ); 
      xmlNodePtr intNode2 = xmlNewChild(child, NULL, BAD_CAST "int", BAD_CAST  buf  );
      assert(intNode2);

      }
    if(extinct && extinct->find(&u) != extinct->end())
      {
               createIntAttribute(  xmlNode, "EX", 1);
      }


    if(u.getName()!= "")
      {
		  string nameStr = u.getName();
                 xmlNewProp(xmlNode, BAD_CAST "getName", BAD_CAST nameStr.c_str());
		 getAntiChainMarkup(u, *gamma, xmlNode);
      }

    return;
  }



  // Recursively checks what tags are given for all nodes in subtree T_v
  // Precondition: All bool argument has proper values. Assume a specific
  // bool argument, 'A' has incoming value 'a', and the value for the 
  // current subtree is 'b', then on return, A = a && b, i.e., false if 
  // either a or b is false.
  // postcondition: return true if subtree is non-empty, i.e, v != 0 
  //----------------------------------------------------------------------
  bool 
  TreeInputOutput::recursivelyCheckTags(xmlNodePtr xmlNode, TreeIOTraits& traits)
  {
 
    if (xmlNode == NULL) // i.e. if the parent was not a leaf
      {
	return false;
      }
    // Note that we don't care if lower subtrees are empty!
    recursivelyCheckTags(leftNode(xmlNode), traits);
    recursivelyCheckTags(rightNode(xmlNode), traits);
    
    checkTags(xmlNode, traits);

    return true;
  }
    
    
  // Checks what tags are given for node v
  // Precondition: All bool argument has proper values. Assume a specific
  // bool argument, 'A' has incoming value 'a', and the value for the 
  // current node is 'b', then on return, A = a && b.
  //----------------------------------------------------------------------
  void
  TreeInputOutput::checkTags(xmlNodePtr xmlNode, TreeIOTraits& traits)
  {
    // Determine if NW is given
    //-------------------------------------------------------------------
    assert(xmlNode);

    if( ! xmlHasProp(xmlNode, BAD_CAST "NW") && !isRoot(xmlNode))
      {
	traits.setNW(false);
      }
    
    // Determine if ET is given
    //-------------------------------------------------------------------
    if( ! xmlHasProp(xmlNode, BAD_CAST "ET") && !isRoot(xmlNode))
      {
	traits.setET(false);
      }
    // Check if NT is given
    //-------------------------------------------------------------------
    if( ! xmlHasProp(xmlNode, BAD_CAST "NT") && !isLeaf(xmlNode))
      {
	traits.setNT(false);
      }
    
    // Check if BL is given
    //-------------------------------------------------------------------
    if( ! xmlHasProp(xmlNode, BAD_CAST "BL") && !isRoot(xmlNode)) 
      {
	traits.setBL(false);
      }
    
    // Check if AC is given. 
    //! \todo{The AC check behaves conversely from other checks. Any presence of 
    //! AC makes the hasAC argument true.}
    //-------------------------------------------------------------------

    if ( hasChild(xmlNode,"AC") )
      {
	traits.setAC(true);
      }
    
    // Check if GS is given for leaves.
    //-------------------------------------------------------------------

    if (leftNode(xmlNode) == NULL && rightNode(xmlNode) == NULL &&  xmlHasProp(xmlNode, BAD_CAST "S") == 0) 
      {
	traits.setGS(false);
      }

    // Check if there are hybrid annotations
    if(     hasChild(xmlNode,"HY")  ||  hasChild(xmlNode,"EX")  ||  hasChild(xmlNode,"OP")  )
      {
	traits.setHY(true);
      }
  }

  //! \todo{add comments on what this do /bens}
  //----------------------------------------------------------------------
  void
  TreeInputOutput::updateACInfo(xmlNodePtr xmlNode, Node *new_node, vector<SetOfNodes> &AC)
  {
    std::vector<int>  list;
    if ( intList( xmlNode, "AC", list ))
      {
	vector<int>::const_iterator iter;
	int i=0;
	for(iter=list.begin(); iter!=list.end(); iter++)  
	  {
	    AC[i].insert(new_node);
	    i++;
	  }
      }
  }
  // aaa










  // Basic helper function for writing trees in BEEP format
  string
  TreeInputOutput::recursivelyWriteBeepTree(Node& u, string& least,
				   const TreeIOTraits& traits,
				   const GammaMap* gamma,
				   map<const Node*,Node*>* otherParent,
				   map<const Node*,unsigned>* extinct,
				   map<unsigned, unsigned>* id)
  {
    assert((traits.hasID() && id) == false);
    string ret;

    // Determine what should be tagged in PRIME markup
    //-------------------------------------------------------------------
    std::ostringstream tagstr;
    std::ostringstream NWstr;

    if(traits.hasID())
      {
	tagstr << " ID=" << u.getNumber();
      }

    if (traits.hasET())
      {
	if (traits.hasNWisET())
	  {
	    NWstr << std::showpoint <<":" << u.getTime();
	  }
	else
	  {
	    tagstr << " ET=" << std::showpoint << u.getTime();
	  }
      }
    
    if (traits.hasNT())
      {
	tagstr << " NT=" << u.getNodeTime();
      }

    if (traits.hasBL())
      {
	if (traits.hasNWisET())
	  {
	    tagstr << " BL=" << u.getLength();
	  }
	else
	  {
	    NWstr << ":" << u.getLength();
	  }
      }

    // Now add node in newick format and gamma/AC if requested
    // This is done differently ifor leaves and internal nodes
    //-------------------------------------------------------------------
    if (u.isLeaf())  // leaves recursion stops and 'S' is set
      {
	if(id)
	  {
	    if(id->find(u.getNumber()) == id->end())
	      {
		unsigned i = id->size();
		(*id)[u.getNumber()] = i;
	      }
	    tagstr << " ID=" << (*id)[u.getNumber()];
	  }

	// add node in newick format
	// 	ret = least = u.getName();
	least = u.getName();

	// then add gamma to PRIME markup if requested
	if(gamma)
	  {
	    Node *species = gamma->getLowestGammaPath(u);
	    // check for consistency
	    if(species)
	      {
		// Add species info and AC
		tagstr << " S=" << species->getName() 
		       << getAntiChainMarkup(u, *gamma);
	      }
	    else
	      {
		ostringstream err;
		err << "Cannot write AC, since leaf " 
		    << u.getNumber() 
		    <<" lacks a species";
		throw AnError(err.str());
	      }
	  }
      }
    else   // Internal nodes needs to send recursion on and sets 'D'
      {
	// First get the strings for subtrees
	string least_left;
	string least_right;
	string left_str = recursivelyWriteBeepTree(*u.getLeftChild(), 
						   least_left, traits,
						   gamma, otherParent, extinct, id);
	string right_str = recursivelyWriteBeepTree(*u.getRightChild(),
						    least_right, traits,
						    gamma, otherParent, extinct, id);
	if(id)
	  {
	    if(id->find(u.getNumber()) == id->end())
	      {
		unsigned i = id->size();
		(*id)[u.getNumber()] = i;
	      }
	    tagstr << " ID=" << (*id)[u.getNumber()];
	  }
	
	ret.reserve(1024); 	// Avoid too many internal resize (expensive)

	// Add node in newick format
	// Always order leaves in as alphabetical order as possible
	if (least_left < least_right)
	  {
	    least = least_left;
	    ret = "(" + left_str + ", " + right_str + ")";
	  }
	else
	  {
	    least = least_right;
	    ret = "(" + right_str + ", " + left_str + ")";
	  }

	// then add gamma/AC to BEEP markup if requested
	if(gamma)
	  {
	    if(gamma->isSpeciation(u))
	      {
		tagstr << " D=0";
	      }
	    else
	      {
		tagstr << " D=1";
	      }
	    tagstr << getAntiChainMarkup(u, *gamma);
	  }
      }
    if(otherParent && otherParent->find(&u) != otherParent->end())
      {  
	unsigned pn = u.getParent()->getNumber();
	unsigned opn = (*otherParent)[&u]->getNumber();
	if(id)
	  {
	    if(id->find(pn) == id->end())
	      {
		unsigned i = id->size();
		(*id)[pn] = i;
	      }
	    if(id->find(opn) == id->end())
	      {
		unsigned i = id->size();
		(*id)[opn] = i;
	      }
  	    tagstr << " HY=(" << (*id)[pn] << " " << (*id)[opn] << ")";
	  }
      }
    if(extinct && extinct->find(&u) != extinct->end())
      {
	tagstr << " EX=1";
      }

    if(u.getName()!= "")
      {
	ret.append(u.getName());
      }

    ret.append(NWstr.str());

    if(tagstr.str() != "")
      {
	ret.append("[&&PRIME" + tagstr.str() + "]");
      }

    return ret;
  }

  // Basic helper function for writing trees in BEEP format
  string
  TreeInputOutput::recursivelyWriteBeepTree(Node& u, map<Node*, string> least,
				   const TreeIOTraits& traits,
				   const GammaMap* gamma,
				   map<const Node*,Node*>* otherParent,
				   map<const Node*,unsigned>* extinct,
				   map<unsigned, unsigned>* id)
  {
    assert((traits.hasID() && id) == false);
    string ret;

    // Determine what should be tagged in PRIME markup
    //-------------------------------------------------------------------
    std::ostringstream tagstr;
    std::ostringstream NWstr;

    if(traits.hasID())
      {
	tagstr << " ID=" << u.getNumber();
      }

    if (traits.hasET())
      {
	if (traits.hasNWisET())
	  {
	    NWstr << std::showpoint <<":" << u.getTime();
	  }
	else
	  {
	    tagstr << " ET=" << std::showpoint << u.getTime();
	  }
      }
    
    if (traits.hasNT())
      {
	tagstr << " NT=" << u.getNodeTime();
      }

    if (traits.hasBL())
      {
	if (traits.hasNWisET())
	  {
	    tagstr << " BL=" << u.getLength();
	  }
	else
	  {
	    NWstr << ":" << u.getLength();
	  }
      }

    // Now add node in newick format and gamma/AC if requested
    // This is done differently ifor leaves and internal nodes
    //-------------------------------------------------------------------
    if (u.isLeaf())  // leaves recursion stops and 'S' is set
      {
	if(id)
	  {
	    if(id->find(u.getNumber()) == id->end())
	      {
		unsigned i = id->size();
		(*id)[u.getNumber()] = i;
	      }
	    tagstr << " ID=" << (*id)[u.getNumber()];
	  }

	// then add gamma to PRIME markup if requested
	if(gamma)
	  {
	    Node *species = gamma->getLowestGammaPath(u);
	    // check for consistency
	    if(species)
	      {
		// Add species info and AC
		tagstr << " S=" << species->getName() 
		       << getAntiChainMarkup(u, *gamma);
	      }
	    else
	      {
		ostringstream err;
		err << "Cannot write AC, since leaf " 
		    << u.getNumber() 
		    <<" lacks a species";
		throw AnError(err.str());
	      }
	  }
      }
    else   // Internal nodes needs to send recursion on and sets 'D'
      {
	// First get the strings for subtrees
	// Add node in newick format
	// Always order leaves in as alphabetical order as possible
	if(least[u.getLeftChild()] < least[u.getRightChild()])
	  {
	    string left_str = recursivelyWriteBeepTree(*u.getLeftChild(), 
						       least, traits,
						       gamma, otherParent, extinct, id);
	    string right_str = recursivelyWriteBeepTree(*u.getRightChild(),
							least, traits,
							gamma, otherParent, extinct, id);
	    ret = "(" + left_str + ", " + right_str + ")";
	  }
	else
	  {
	    string right_str = recursivelyWriteBeepTree(*u.getRightChild(),
							least, traits,
							gamma, otherParent, extinct, id);
	    string left_str = recursivelyWriteBeepTree(*u.getLeftChild(), 
						       least, traits,
						       gamma, otherParent, extinct, id);
	    ret = "(" + right_str + ", " + left_str + ")";
	  }

	if(id)
	  {
	    if(id->find(u.getNumber()) == id->end())
	      {
		unsigned i = id->size();
		(*id)[u.getNumber()] = i;
	      }
	    tagstr << " ID=" << (*id)[u.getNumber()];
	  }
	
	ret.reserve(1024); 	// Avoid too many internal resize (expensive)


	// then add gamma/AC to BEEP markup if requested
	if(gamma)
	  {
	    if(gamma->isSpeciation(u))
	      {
		tagstr << " D=0";
	      }
	    else
	      {
		tagstr << " D=1";
	      }
	    tagstr << getAntiChainMarkup(u, *gamma);
	  }
      }
    if(otherParent && otherParent->find(&u) != otherParent->end())
      {  
	unsigned pn = u.getParent()->getNumber();
	unsigned opn = (*otherParent)[&u]->getNumber();
	if(id)
	  {
	    if(id->find(pn) == id->end())
	      {
		unsigned i = id->size();
		(*id)[pn] = i;
	      }
	    if(id->find(opn) == id->end())
	      {
		unsigned i = id->size();
		(*id)[opn] = i;
	      }
            tagstr << " HY=(" << (*id)[pn] << " " << (*id)[opn] << ")";
	  }
      }
    if(extinct && extinct->find(&u) != extinct->end())
      {
	tagstr << " EX=1";
      }

    if(u.getName()!= "")
      {
	ret.append(u.getName());
      }

    ret.append(NWstr.str());

    if(tagstr.str() != "")
      {
	ret.append("[&&PRIME" + tagstr.str() + "]");
      }

    return ret;
  }



  //----------------------------------------------------------------------
  // Writing trees
  //----------------------------------------------------------------------

  // Basic function for writing tree T in newick format, with the tags 
  // indicated by traits included in PRIME markup. If gamma != NULL then AC 
  // markup will also be included.
  // Precondition: (useET && useNT) == false
  //----------------------------------------------------------------------
  string 
  TreeInputOutput::writeBeepTree(const Tree& T, 
			const TreeIOTraits& traits,
			const GammaMap* gamma)
  {
    assert((traits.hasET() && traits.hasNT()) == false);
    string least = "";
    ostringstream name;

    if (traits.hasName()) {
      name << "[&&PRIME NAME=" << T.getName();

      if(T.getRootNode() == NULL)
	{
	  name << "] [empty tree!]"; 
	  return name.str();
	}
      else
	{
	  if(traits.hasNT())
	    {
	      name << " TT=" << T.getTopTime();
	    }
	  name << "]";
	}
    }
    return recursivelyWriteBeepTree(*T.getRootNode(), least, traits,
				    gamma, 0, 0, 0) + name.str();
  }

  // Collect info for newickString
  // Compute markup for the anti-chains on node u
  //----------------------------------------------------------------------
  string
  TreeInputOutput::getAntiChainMarkup(Node &u, const GammaMap &gamma)
  {
    string ac = "";

    if (gamma.numberOfGammaPaths(u) > 0)
      {
	Node *lower = gamma.getLowestGammaPath(u);
	Node *higher = gamma.getHighestGammaPath(u);

	do 
	  {
	    char buf[5];
	    if (snprintf(buf, 4, "%d", lower->getNumber()) == -1) 
	      {
		throw AnError("Too many anti-chains (more than 9999!) "
			    "or possibly a programming error.");
	      }
	    if (lower == higher)
	      {
		ac.append(buf);	// Last element
	      }
	    else 
	      {
		ac.append(buf);
		ac.append(" ");
	      }
	    lower = lower->getParent();
	  } 
	while (lower && higher->dominates(*lower)); 
	ac = " AC=(" + ac + ")";
      }
    return ac;
  }



}//end namespace beep
