#include <cassert>
#include <cmath>
#include <limits>

#include "AnError.hh"
#include "DiscTree.hh"

namespace beep
{

  using namespace std;

  DiscTree::DiscTree(Tree& S, unsigned noOfRootToLeafIvs) :
    m_S(S),
    m_noOfRootToLeafIvs(noOfRootToLeafIvs),
    m_noOfTopTimeIvs(0),
    m_timestep(0),
    m_gridTimes(),
    m_loGridIndices(S),
    m_upGridIndices(S)
  {
    update();
  }


  DiscTree::~DiscTree()
  {
  }


  void DiscTree::update()
  {    
    m_timestep = m_S.rootToLeafTime() / m_noOfRootToLeafIvs;
    Real tt = m_S.getTopTime();
    if(tt <= 0)
      {
	tt = 1.0;
      }
    m_noOfTopTimeIvs = static_cast<unsigned>(round(tt / m_timestep));
    // TODO: We could optimize this by looking at the TreePerturbationInfo
    // in the tree to see what has changed.
    createGridTimes();
    createGridIndices(m_S.getRootNode(), 
		      m_noOfRootToLeafIvs + m_noOfTopTimeIvs + 1);
  }


  void DiscTree::createGridTimes()
  {
    m_gridTimes.clear();
    unsigned noOfIvs = m_noOfRootToLeafIvs + m_noOfTopTimeIvs; 
    m_gridTimes.reserve(noOfIvs+1);
    for (unsigned i=0; i<=noOfIvs; ++i)
      {
	m_gridTimes.push_back(i * m_timestep);
      }
  }


  void DiscTree::createGridIndices(const Node* node, unsigned parentGridIndex)
  {
    unsigned gridIndex = 
      static_cast<unsigned>(round(m_S.getTime(*node) / m_timestep));
    if (gridIndex >= parentGridIndex)
      {
	throw AnError("To few discretization steps in DiscTree: "
		      "child node coincides with parent node.");
      }
    m_loGridIndices[node] = gridIndex;
    m_upGridIndices[node] = parentGridIndex - 1;
    if (!node->isLeaf())
      {
	createGridIndices(node->getLeftChild(), gridIndex);
	createGridIndices(node->getRightChild(), gridIndex);
      }
  }


  unsigned DiscTree::getNoOfPtsInTree() const
  {
    unsigned noOfPts = 0;
    BeepVector<unsigned>::const_iterator itLo = m_loGridIndices.begin();
    BeepVector<unsigned>::const_iterator itUp = m_upGridIndices.begin();
    for (; itLo !=  m_loGridIndices.end(); ++itLo, ++itUp)
      {
	noOfPts += (*itUp) - (*itLo) + 1;
      }
    return noOfPts; 
  }


  unsigned DiscTree::getNoOfPtsOnEdge(const Node* node) const
  {
    return (m_upGridIndices[node] - m_loGridIndices[node] + 1);
  }


  unsigned DiscTree::getNoOfIvs() const
  {
    return (m_noOfRootToLeafIvs + m_noOfTopTimeIvs);
  }


  unsigned DiscTree::getNoOfRootToLeafIvs() const
  {
    return m_noOfRootToLeafIvs;
  }


  unsigned DiscTree::getNoOfTopTimeIvs() const
  {
    return m_noOfTopTimeIvs;
  }


  Tree& DiscTree::getOrigTree() const
  {
    return m_S;
  }


  const Node* DiscTree::getOrigRootNode() const
  {
    return m_S.getRootNode();
  }


  const Node* DiscTree::getOrigNode(unsigned index) const
  {
    return m_S.getNode(index);
  }


  Real DiscTree::getTimestep() const
  {
    return m_timestep;
  }


  Real DiscTree::getRootToLeafTime() const
  {
    return getPtTime(m_S.getRootNode());
  }


  Real DiscTree::getTopTime() const
  {
    return (m_timestep * m_noOfTopTimeIvs);
  }


  Real DiscTree::getTopToLeafTime() const
  {
    return m_gridTimes.back();
  }


  DiscTree::Point DiscTree::getTopPt() const
  {
    return Point(getNoOfIvs(), m_S.getRootNode());
  }


  unsigned DiscTree::getRelativeIndex(Point pt) const
  {
    assert(pt.first >= m_loGridIndices[pt.second]);
    return (pt.first - m_loGridIndices[pt.second]);
  }


  unsigned DiscTree::getGridIndex(const Node* node) const
  {
    return m_loGridIndices[node];
  }


  pair<unsigned,unsigned> DiscTree::getEdgeGridIndices(const Node* node) const
  {
    return pair<unsigned,unsigned>(m_loGridIndices[node], 
				   m_upGridIndices[node]);
  }


  bool DiscTree::isWithinEdge(unsigned gridIndex, const Node* node) const
  {
    return (m_loGridIndices[node] <= gridIndex &&
	    m_upGridIndices[node] >= gridIndex);
  }


  bool DiscTree::isAboveEdge(unsigned gridIndex, const Node* node) const
  {
    return (m_upGridIndices[node] < gridIndex);
  }


  bool DiscTree::isBelowEdge(unsigned gridIndex, const Node* node) const
  {
    return (m_loGridIndices[node] > gridIndex);
  }


  DiscTree::Point DiscTree::getPt(unsigned gridIndex, const Node* node) const
  {
    if (isBelowEdge(gridIndex, node))
      { 
	throw AnError("Can't retrieve invalid point"); 
      }
    while (m_upGridIndices[node] < gridIndex)
      {
	node = node->getParent();
      }
    return Point(gridIndex, node);
  }


  Real DiscTree::getPtTime(unsigned gridIndex) const
  {
    return m_gridTimes[gridIndex];
  }


  Real DiscTree::getPtTime(const Node* node) const
  {
    return m_gridTimes[m_loGridIndices[node]];
  }


  void DiscTree::getPtTimes(const Node* node,
			    vector<Real>::const_iterator& itBegin, 
			    vector<Real>::const_iterator& itEnd) const
  {
    itBegin = m_gridTimes.begin();
    itBegin += m_loGridIndices[node];
    itEnd = m_gridTimes.begin();
    itEnd += (m_upGridIndices[node] + 1);
  }


  Real DiscTree::getEdgeTime(const Node* node) const
  {
    return (node->isRoot() ? getTopTime() :
	    (getPtTime(node->getParent()) - getPtTime(node)));
  }


  Real DiscTree::getMaxNodeTimeDiff() const
  {
    Real maxDiff = 0;
    for (unsigned i=0; i<m_S.getNumberOfNodes(); ++i)
      {
	const Node* n = m_S.getNode(i);
	Real diff = abs(getPtTime(n) - m_S.getTime(*n));
	if (diff > maxDiff) 
	  { 
	    maxDiff = diff; 
	  }
      }
    return maxDiff;
  }


  Real DiscTree::getMaxEdgeTimeDiff() const
  {
    Real maxDiff = 0;
    for (unsigned i=0; i<m_S.getNumberOfNodes(); ++i)
      {
	const Node* n = m_S.getNode(i);
	if (n->isRoot()) 
	  { 
	    continue; 
	  }
	Real et = m_S.getEdgeTime(*n);
	Real etd = getPtTime(n->getParent()) - getPtTime(n);
	Real diff = abs(et - etd);
	if (diff > maxDiff)
	  { 
	    maxDiff = diff; 
	  }
      }
    return maxDiff;
  }


  Real DiscTree::getMinOrigEdgeTime(bool includeTopTime) const
  {
    Real min = numeric_limits<Real>::max();
    for (unsigned i = 0; i < m_S.getNumberOfNodes(); ++i)
      {
	const Node* n = m_S.getNode(i);
	if (!n->isRoot())
	  {
	    Real et = m_S.getEdgeTime(*n);
	    if (et < min) { min = et; }
	  }
      }
    Real topTime = getTopTime();
    if (includeTopTime && topTime < min) 
      { 
	return topTime; 
      }
    return min;
  }


  bool DiscTree::containsNonDividedEdge() const
  {
    for (unsigned i=0; i<m_S.getNumberOfNodes(); ++i)
      {
	Node* n = m_S.getNode(i);
	bool nonDiv = (m_loGridIndices[n] == m_upGridIndices[n]);
	if (!n->isRoot() && nonDiv)
	  {
	    return true;
	  }
      }
    return false;
  }


  void DiscTree::debugInfo(bool printNodeInfo) const
  {
    unsigned noOfPts = getNoOfPtsInTree();
    unsigned noOfNodes = m_S.getNumberOfNodes();
    cerr << "# ======================== DiscTree===========================" 
	 << endl
	 << "# " << m_noOfRootToLeafIvs << " + " << m_noOfTopTimeIvs 
	 << " intervals from leaves to root to top, spanning time 0 to "
	 << m_S.rootToLeafTime() << " to " << getTopToLeafTime() << endl
	 << "# " << "Number of pts: " << noOfPts << " (of which "
	 << noOfNodes << " correspond to nodes)" << endl
	 << "# " << "Timestep size: " << m_timestep << endl
	 << "# " << "Min edge in original tree excl. top time: " 
	 << getMinOrigEdgeTime(false)
	 << ", incl. top time: " << getMinOrigEdgeTime(true) << endl
	 << "# " << "Max discrete-to-original node difference: " 
	 << getMaxNodeTimeDiff() << endl
	 << "# " << "Max discrete-to-original edge difference: " 
	 << getMaxEdgeTimeDiff() << endl
	 << "# " << "All edges have been divided: " 
	 << (containsNonDividedEdge() ? "NO!!!!" : "Yes" ) << endl;
	
    if (printNodeInfo)
      {
	cerr << "# Node:\tName:\tNT:\t(actual:)\tET:\t(actual:)\tPts:\tSpan:" 
	     << endl;
	for (Tree::iterator it=m_S.begin(); it!=m_S.end(); ++it)
	  {
	    const Node* n = *it;
	    cerr << "# "
		 << n->getNumber() << '\t'
		 << (n->isLeaf() ? n->getName() : (n->isRoot() ? "Root   " : "       ")) << '\t'
		 << getPtTime(n) << '\t'
		 << '(' << m_S.getTime(*n) << ")\t"
		 << getEdgeTime(n) << '\t'
		 << '(' << m_S.getEdgeTime(*n) << ")\t"
		 << getNoOfPtsOnEdge(n) << '\t'
		 << m_loGridIndices[n] << "..." << m_upGridIndices[n] << '\t'
		 << endl;
	  }
      }
    cerr << "# ===========================================================" 
	 << endl;
  }


} // end namespace beep.
