#ifndef LA_MATRIX_H
#define LA_MATRIX_H

#include <iostream>

#include "AnError.hh"
#include "beep2blas.hh" 
#include "LA_Vector.hh" 

namespace beep {
  // Forward declarations
  class LA_DiagonalMatrix;
  class LA_Matrix;
  LA_Matrix operator*(const Real& alpha, const LA_Matrix& A);

  class LA_Matrix
  {
    friend class LA_DiagonalMatrix;

    // transposed product of vectors x and y yields a matrix
    //------------------------------------------------------------------------
    friend LA_Matrix LA_Vector::col_row_product(const LA_Vector& x) const;


  public:
    //------------------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //------------------------------------------------------------------------
    //Dummy default constructor for use with MatrixCache
    LA_Matrix();
    
    LA_Matrix(const unsigned& dim, const Real in_data[]);

    // Create empty matrix
    //------------------------------------------------------------------------
      LA_Matrix(const unsigned& dim);

    // Copy constructor
    //------------------------------------------------------------------------
    LA_Matrix(const LA_Matrix& B);

    // Destructor
    //------------------------------------------------------------------------
    virtual ~LA_Matrix();

    // Assignment operator
    //------------------------------------------------------------------------
    LA_Matrix& operator=(const LA_Matrix& B);


  public:
    //------------------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------------------
  
    // Dimension of matrix
    //------------------------------------------------------------------------
    const unsigned& getDim() const;

    // Access to individual elements
    //------------------------------------------------------------------------
    Real& operator()(const unsigned& row, 
			      const unsigned& column);
    Real operator()(const unsigned& row, 
			      const unsigned& column) const;

    //------------------------------------------------------------------------
    // Linear algebra functions
    //------------------------------------------------------------------------
  
    // Transpose of dense matrix
    //------------------------------------------------------------------------
    LA_Matrix transpose() const;

    // Inverse of Matrix 
    //------------------------------------------------------------------------
    LA_Matrix inverse() const;

    // Trace (sum of diagonal elements) of Matrix A
    //------------------------------------------------------------------------
    Real trace() const;

    // Addition of vectors x and y 
    //------------------------------------------------------------------------
    LA_Matrix operator+(const LA_Matrix& B) const;

    // Multiplication of matrix A and a scalar 
    //------------------------------------------------------------------------
    LA_Matrix operator*(const Real& alpha) const;
    friend LA_Matrix operator*(const Real& alpha, const LA_Matrix& A);
    LA_Matrix operator/(const Real& alpha) const;

    // TODO: What does this do? /bens    
    LA_Vector col(const unsigned& ) const;

    // Multiplication of matrix A and a vector where element pos equals alpha
    // and the remaining elements are zero
    //------------------------------------------------------------------------
    LA_Vector col_mult(const Real& alpha, const unsigned& ) const;
    bool col_mult(LA_Vector& result, const Real& alpha, 
		  const unsigned& col) const;

    // Multiplication of matrix A and vector x 
    //------------------------------------------------------------------------
    virtual LA_Vector operator*(const LA_Vector& x) const;
    virtual void mult(const LA_Vector& x, LA_Vector& result) const;

    // Multiplication of matrices A and B 
    //------------------------------------------------------------------------
    LA_Matrix operator*(const LA_Matrix& B) const;
    void mult(const LA_Matrix& B, LA_Matrix& result) const;
    
    // Multiplication of a dense matrix and a diagonal matrix 
    //------------------------------------------------------------------------
    LA_Matrix operator*(const LA_DiagonalMatrix& D) const;
  
    // Elementwise multiplication of two matrices  
    //------------------------------------------------------------------------
    LA_Matrix ele_mult(const LA_Matrix& B) const;

    // Sum of elements  of matrix A
    //------------------------------------------------------------------------
    Real sum() const;

    // Eigensystem of dense matrix A (=this) for real reversible matrices
    // E vill hold the eigenvalues, V the eigenvectors and iV the inverse
    // of the eigenvectors
    //------------------------------------------------------------------------
    void eigen(LA_DiagonalMatrix& E, LA_Matrix& V, LA_Matrix& iV);
      
    
    //------------------------------------------------------------------------
    // I/O
    //------------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream& os, 
				    const beep::LA_Matrix& A);

  protected:
    //------------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------------
  
    unsigned dim;
    Real* data; //This is an array
  };
 
} /* namespace beep */

#endif




