#include "BDHybridTreeGenerator.hh"

#include "AnError.hh"
#include "BeepVector.hh"
#include "GammaMap.hh"
#include "HybridTree.hh"
#include "PRNG.hh"
#include "StrStrMap.hh"
#include "Tree.hh"

#include <cmath>

namespace beep
{
  using namespace std;
  //-------------------------------------------------------------------
  //
  // Construct/destruct
  //
  //-------------------------------------------------------------------
  BDHybridTreeGenerator::BDHybridTreeGenerator(Real birthRate, 
					       Real deathRate, 
					       Real hybridRate)
    : TreeGenerator(),
      lambda(birthRate),
      mu(deathRate),
      rho(hybridRate),
      toptime(1.0),
      G(),
      leaves(),
      times(),
      rand(),
      allowAuto(false)
  {}

  BDHybridTreeGenerator::~BDHybridTreeGenerator()
  {}


  //-------------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------------
  void 
  BDHybridTreeGenerator::allowAutoPloidy()
  {
    allowAuto = true;
  }

  void 
  BDHybridTreeGenerator::setTopTime(Real time)
  {
    toptime = time;
  }

  Real 
  BDHybridTreeGenerator::getTopTime()
  {
    return toptime;
  }

  bool 
  BDHybridTreeGenerator::generateTree(Tree& G_in, bool noTopTime)
  {
    std::cerr << "Please use generateHybridTree(HybridTree&) instead\n";
    abort(); // was exit(1);
    return false;
  }

  bool 
  BDHybridTreeGenerator::generateHybridTree(HybridTree& G_in)
  {
//     cerr << "\n\n\n\nGenerate\n";
    G = &G_in;
    if(G->getRootNode())
      {
	G->clear();
	assert(G->getNumberOfNodes() == 0);
      }
    leaves.clear();
    assert(leaves.size() == 0);
    generateX(1, toptime);
    if(leaves.size() > 1)
      {
	throw AnError("leaves > 1",1);
      }
    assert(leaves.size() <= 1);
    if(leaves.size() == 1)
      {
	G->setRootNode(leaves.back());
	RealVector& tmp = *new RealVector(G->getNumberOfNodes());
	
	for(unsigned i = 0; i < G->getNumberOfNodes(); i++)
	  {
	    Node* u = G->getNode(i);
	    tmp[u] = times[u];
	  }
	G->setTimes(tmp);
	G->setTopTime(toptime - G->getTime(*G->getRootNode()));
	return true;
      }
    else
      {
	return false;
      }
  } 

  StrStrMap 
  BDHybridTreeGenerator::exportGS()
  {
    if(G && G->getRootNode())
      {
	std::cerr << "Dummy gs for hybrid tree\n";
	StrStrMap gs;
	Tree S = Tree::EmptyTree();
	std::string x = S.getRootNode()->getName();
	for(unsigned i = 0; i < G->getNumberOfNodes(); i++)
	  {
	    Node* u = G->getNode(i);
	    if(u->isLeaf())
	      {
		gs.insert(u->getName(), x);
	      } 
	  }
	return gs;
      }
    else
      {
	throw AnError("DBHybridTreeGenerator::exportGS\n"
		      "no tree has been generate to export gs from",1);
      }
  }
   
  GammaMap 
  BDHybridTreeGenerator::exportGamma()
  {
    Tree S = Tree::EmptyTree();
    //       StrStrMap gs = exportGS();
    //       GammaMap tmpGamma = GammaMap::MostParsimonious(*G, S, gs);
      
    StrStrMap gs;
    std::string x = S.getRootNode()->getName();
    Tree& B = G->getBinaryTree();
    for(unsigned i = 0; i < B.getNumberOfNodes(); i++)
      {
	Node* u = B.getNode(i);
	if(u->isLeaf())
	  {
	    gs.insert(u->getName(), x);
	  } 
      }
    GammaMap tmpGamma = GammaMap::MostParsimonious(B, S, gs);

    return tmpGamma;
  }

    
  //-------------------------------------------------------------------
  // I/O
  //-------------------------------------------------------------------
  std::ostream& operator<<(std::ostream &o, const BDHybridTreeGenerator& BDG)
  {
    return o;
  };

  //-------------------------------------------------------------------
  //
  // Implementation
  //
  //-------------------------------------------------------------------
  void 
  BDHybridTreeGenerator::generateV(unsigned k)
  {
    assert(k > 0);
    for(unsigned i = 0; i < k; i++)
      {
	//get unique name
	std::ostringstream oss;
	oss << "Leaf_" << G->getNumberOfNodes();
	Node* u =G->addNode(0,0, G->getNumberOfNodes(), oss.str());
	times[u] = 0;
	leaves.push_back(u);
      }
    if(leaves.size() > k)
      {
	throw AnError("leaves > k",1);
      }
//     cerr << "leaving leaves " << k << " leaves = " << leaves.size() <<endl;
    assert(leaves.size() <= k);
    assert(leaves.size() == k);
    return;
  }

  void 
  BDHybridTreeGenerator::generateX(unsigned k, Real maxT)
  {
    assert (k > 0);
    unsigned k2 =k;
    // sample time t for next event
    Real p = rand.genrand_real3();
    Real t = -std::log(p)/(k *(lambda+mu+rho));
    assert(t > 0);
    if(t > maxT) // We have reached a leaf or an internal node in host tree
      {
	return generateV(k);
      }
    else
      {
	p=rand.genrand_real3();
	if(p <= mu/(lambda+mu+rho))             // death
	  {
	    if(--k > 0)
	      {
		generateX(k, maxT-t);
		assert(leaves.size() <= k);
	      }
// 	    cerr << "leaving death " << k2 << " k = " << k <<" leaves = " << leaves.size()<< endl;
	    if(leaves.size() > k2)
	      {
		throw AnError("leaves > k2",1);
	      }
	    assert(leaves.size() <= k2);
	    return;
	  }
	else if(p <= (mu+lambda)/(lambda+mu+rho) || (k == 1 && allowAuto == false) )  // birth
	  {
	    generateX(++k, maxT-t);
	    if(leaves.size() == 0)
	      {
		return;
	      }
	    assert(leaves.size() <= k);
	    unsigned leaf1 = rand.genrand_modulo(k);
	    unsigned leaf2 = rand.genrand_modulo(k-1);
	    if(//leaves.size() > 1 &&
	       leaf1 < leaves.size()&&
	       leaf2 < leaves.size()-1)
	      {
		Node* left = leaves[leaf1];
		leaves.erase(leaves.begin() + leaf1);
		Node* right = leaves[leaf2];
		leaves.erase(leaves.begin() + leaf2);
		Node* u = G->addNode(left, right, G->getNumberOfNodes(), "");
		leaves.push_back(u);
		times[u] = maxT-t;
		if(leaves.size() > k2)
		  {
		    throw AnError("leaves > k2",1);
		  }
		assert(leaves.size() <= k2);
	      }
	  }
	else                                       // hybridization
	  {
	    bool autopolyploidy = false; //
	    if(allowAuto)
	      {
		autopolyploidy = (rand.genrand_modulo(k) == 0);
	      }

	    generateX(++k, maxT-t);
	    if(leaves.size() == 0)
	      {
		return;
	      }
	    assert(leaves.size() <= k);
	    // Create the hybridization from randomly chosen children
	    unsigned aLeaf = rand.genrand_modulo(k--);
	    if(aLeaf < leaves.size())   // hybrid survived
	      {
		Node* hybrid = leaves[aLeaf];
		leaves.erase(leaves.begin() + aLeaf);
// 		cerr << "extracted hybrid leav es = " << leaves.size()<< "k = " << k <<endl;
		Node* sibling1 = 0;
		Node* sibling2 = 0;
		  
		if(autopolyploidy == false && k > 0&&
		   (aLeaf = rand.genrand_modulo(k--)) < leaves.size())
		  {
		    sibling1 = leaves[aLeaf];
		    leaves.erase(leaves.begin() + aLeaf);
		  }
		else // sibl1 dies or autopolyploidy
		  {
		    std::ostringstream oss;
		    oss << "Extinct_" << G->getNumberOfNodes();
		    sibling1 = G->addNode(0, 0, G->getNumberOfNodes(),
					  oss.str(), true);
		    times[sibling1] =  0;
		  }
		assert(sibling1 != 0);
// 		cerr << "extracted sibli1  leav es = " << leaves.size()<< "k = " << k <<endl;
		aLeaf = rand.genrand_modulo(k); 
		if(aLeaf < leaves.size()) // sibl2 survives
		  {
		    sibling2 = leaves[aLeaf];
		    leaves.erase(leaves.begin() + aLeaf);
		    k--;
		  }
		else  // sibl2 dies
		  {
		    std::ostringstream oss;
		    oss << "Extinct_" << G->getNumberOfNodes();
		    sibling2 = G->addNode(0, 0, G->getNumberOfNodes(), 
					  oss.str(), true);
		    times[sibling2] =  0;
		  }
		assert(sibling2 != 0);
// 		cerr << "extracted sibli2  leav es = " << leaves.size()<< "k = " << k <<endl;

		// Create the two parents (note that they may
		// be extinction nodes if siblings dies)
		Node* parent1 = G->addNode(sibling1, hybrid,
					   G->getNumberOfNodes(), "");
		Node* parent2 = G->addNode(sibling2, hybrid,
					   G->getNumberOfNodes(), "");
		times[parent1] = maxT-t;
		times[parent2] = maxT-t;

		G->setOtherParent(*hybrid, parent1);
		  
		if(autopolyploidy)
		  {
		    Node* u = G->addNode(parent1, parent2, 
					 G->getNumberOfNodes(), "");
		    times[u] = maxT-t;
		    leaves.push_back(u);
		  }
		else
		  {
		    leaves.push_back(parent1);
		    leaves.push_back(parent2);
		  }		      
// 		cerr << "leaving hybridization " << k2 << " leaves = " << leaves.size() <<endl;
		if(leaves.size() > k2)
		  {
		    throw AnError("leaves > k2",1);
		  }
	      }
	    return;
	  }
      }
  }


  // createTrueGamma
  // reads gamma and converts to a GammaMap stored in tmpGamma
  //---------------------------------------------------------------------
  void 
  BDHybridTreeGenerator::createTrueGamma(GammaMap& tmpGamma) const
  {};

}//end namespace beep

