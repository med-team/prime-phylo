#ifndef GAMMADENSITY_HH
#define GAMMADENSITY_HH

#include "Density2P_positive.hh"


namespace beep
{
  //----------------------------------------------------------------------
  //
  //! Class GammaDensity implements the Gamma density distribution.
  //!
  //! \f[ 
  //!  f(x) =  
  //!  \begin{cases}
  //!   \frac{\beta^{\alpha}}{\Gamma(\alpha)} x^{\alpha-1} e^{-\beta x}, &x > 0 \\  0, & \mbox{else}, 
  //!  \end{cases}
  //! \f]
  //! where \f$ \Gamma(\alpha)\f$ is the complete Gamma function of
  //! \f$ \alpha \f$.
  //!
  //! Note that the definition of the parameters varies among different 
  //! notations. Sometimes \f$ 1/\beta \f$ is used instead of \f$ \beta\f$
  //! (often referred to as $\theta$, with corresponding adjustment of
  //! the formula). This easily
  //! causes confusion when comparing notation from different sources!
  //! In short, we use the notation where $mean=\alpha/\beta$ as used in
  //! Mathematics Handbook Beta.
  //!
  //! Author: Martin Linder, adapted by Bengt Sennblad
  //! copyright the MCMC-club, SBC
  //
  //----------------------------------------------------------------------
class GammaDensity : public Density2P_positive
  {
  public:
    //----------------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //----------------------------------------------------------------------
    GammaDensity(Real mean, Real variance, bool embedded = false);
    GammaDensity(const GammaDensity& df);
    ~GammaDensity();

    GammaDensity& operator=(const GammaDensity& df);

    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
    // Density-, distribution-, and sampling functions
    //----------------------------------------------------------------------
    Probability operator()(const Real& x) const; 
    Probability pdf(const Real& x) const;
    Probability cdf(const Real& x) const;
    Real sampleValue(const Real& p) const; 

    // Access parameters
    //----------------------------------------------------------------------
    Real getMean() const;
    Real getVariance() const;

    // Set Parameters
    //----------------------------------------------------------------------
    void setParameters(const Real& mean, const Real& variance);    
    void setMean(const Real& mean);    
    void setVariance(const Real& variance);

    //------------------------------------------------------------------
    //
    // I/O
    //
    //------------------------------------------------------------------
    virtual std::string print() const;

  private:
    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------
    // The attribute c is a constant function of alpha and beta
    // in the log density function.
    //---------------------------------------------------------
    Real c; //!< constant \f$ c=\frac{\beta^{\alpha}}{G(\alpha)} \f$
  };

}//end namespace beep
#endif
