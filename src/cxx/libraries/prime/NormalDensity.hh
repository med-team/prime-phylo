#ifndef NORMALDENSITY_HH
#define NORMALDENSITY_HH

#include "Density2P_common.hh"


namespace beep
{
  //----------------------------------------------------------------------
  //
  //! Class NormalDensity implements the Normal density distribution
  //
  //! \f[
  //!  f(x) = 
  //!  \begin{cases}
  //!   \frac{1}{\sqrt{2 \pi \beta}}
  //!      e^{-\frac{\left(x-\alpha\right)^2}{2\beta}} & 
  //!        x > 0,\\  0, & \mbox{else},
  //!  \end{cases}
  //! \f]
  //! where \f$ \alpha\f$ = mean and \f$ \beta \f$ = variance of underlying 
  //! normal distribution
  //!
  //! Note! In conventional notation, \f$ \mu, \sigma^2 \f$ is used 
  //! instead of \f$ \alpha, \beta \f$.
  //!
  //! Invariants: variance > 0
  //!             mean     > 0
  //!
  //! Author: Martin Linder, adapted by Bengt Sennblad
  //! copyright the MCMC-club, SBC
  //
  //----------------------------------------------------------------------
  class NormalDensity : public Density2P_common
  {
  public:
    //----------------------------------------------------------------------
    //
    // Construct/Destruct
    //
    //----------------------------------------------------------------------
    NormalDensity(Real mean, Real variance, bool embedded = false);
    NormalDensity(const NormalDensity& df);
    ~NormalDensity();
    NormalDensity& operator=(const NormalDensity& df);

    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
    // Density-, distribution-, and sampling functions
    //----------------------------------------------------------------------
    Probability operator()(const Real& x) const;
    Probability pdf(const Real& x) const;
    Probability cdf(const Real& x) const;
    Real sampleValue(const Real& p) const;

    //! mean of truncated normal distribution 
    Real getTruncatedMean(const Real& x) const;

    // Access parameters
    //----------------------------------------------------------------------
    Real getMean() const;
    Real getVariance() const;

    // Set Parameters
    //----------------------------------------------------------------------
    void setParameters(const Real& mean, const Real& variance);    
    void setMean(const Real& mean);    
    void setVariance(const Real& variance);
    
    //------------------------------------------------------------------
    // I/O
    //------------------------------------------------------------------
    virtual std::string print() const;

  private:
    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------
    static const Real pi = 3.14159265358979; //! math. constant \f$ \pi \f$.
    
    //The attribute c is a constant term, given the parameter 
    //values, in the log density function.
    //! \f$ c = \ln(\frac{1}{\sqrt{2\pi\beta}}) \f$
    //---------------------------------------------------------
    Real c;

  };

}//end namespace beep
#endif
