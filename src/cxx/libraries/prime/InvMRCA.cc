#include <sstream>
#include <stack>

#include "InvMRCA.hh"

// Author: Bengt Sennblad, � the MCMC-club, SBC, all rights reserved

using namespace std;
using namespace beep;

//----------------------------------------------------------
//
// Construct/destruct/assign
//
//----------------------------------------------------------
InvMRCA::InvMRCA(Tree& T_in)
  : T(&T_in),
    invMRCA(T_in)
{
  update();
};

InvMRCA::InvMRCA(const InvMRCA& m)
  : T(m.T),
    invMRCA(m.invMRCA)
{};

InvMRCA::~InvMRCA() {};

InvMRCA 
InvMRCA::operator=(InvMRCA m)
{
  if(this != &m)
    {
      T = m.T;
      invMRCA = m.invMRCA;
    }
  return *this;
}

//----------------------------------------------------------
//
// Interface
//
//----------------------------------------------------------
string 
InvMRCA::getStrRep(Node& u, Probability p) const
{
  NodeSetPairs im = invMRCA[u];
  std::ostringstream oss;

  for(std::vector<unsigned>::const_iterator i = im.first.begin();
      i != im.first.end(); i++)
    {
      for(std::vector<unsigned>::const_iterator j = im.second.begin();
	  j != im.second.end(); j++)
	{
	  string first = T->getNode(*i)->getName();
	  string second = T->getNode(*j)->getName();

	  oss << "[";
	  if(first < second)
	    {
	      oss << first
		  << ","
		  << second;
	    }
	  else
	    {
	      oss << second
		  << ","
		  << first;
	    }
	  oss << "]="
	      << p.val();
	}
    }

  return oss.str();
};

void 
InvMRCA::update()
{
  for(unsigned i = 0; i < T->getNumberOfNodes(); i++)
    {
      Node* u = T->getNode(i);
      if(u->isLeaf() == false)
	{
	  getSubtreeLeaves(u->getLeftChild(), invMRCA[u].first);
	  getSubtreeLeaves(u->getRightChild(), invMRCA[u].second);
	}      
    }
};

// Helper for update
//----------------------------------------------------------------------
void
InvMRCA::getSubtreeLeaves(Node* u, vector<unsigned>& v) const
{
  stack<Node*> s;
  s.push(u);
  while(s.empty() == false)
    {
      u = s.top();
      s.pop();
      if(u->isLeaf())
	{
	  v.push_back(u->getNumber());
	}
      else
	{
	  s.push(u->getLeftChild());
	  s.push(u->getRightChild());
	}	  
    }
  return;
}
