#ifndef BDTREEGENERATOR_HH
#define BDTREEGENERATOR_HH

#include "BeepVector.hh"
#include "PRNG.hh"
#include "StrStrMap.hh"
#include "TreeGenerator.hh"

#include <map>
#include <cmath>
#include <vector>

namespace beep
{
  // Forward declarations
  class GammaMap;
  class Tree;



  //-------------------------------------------------------------------
  //
  //! Generation of guest trees on a host tree under the gene evolution 
  //! model, or of host trees under a linear birth-death model. The 
  //! latter uses the fact that the birth-death model corresponds to
  //!  the special case of the gene evolution model occuring over a 
  //! single edged tree.
  //
  //-------------------------------------------------------------------
  class BDTreeGenerator : public TreeGenerator
  {
  public:
    //-------------------------------------------------------------------
    //
    // Construct/destruct
    //
    //-------------------------------------------------------------------
    BDTreeGenerator(Tree& S, Real birthRate, Real deathRate);
    ~BDTreeGenerator();

    //-------------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------------
    virtual void setTopTime(Real time);
    virtual Real getTopTime();
    virtual bool generateTree(Tree& G_in, bool noTopTime = false);
    virtual StrStrMap exportGS();
    virtual GammaMap exportGamma();
     
    //-------------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const BDTreeGenerator& BDG);

    //-------------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------------
    Node* generateV(Node* x);
    Node* generateX(Node* x, Real maxT);

    // createTrueGamma
    // reads gamma and converts to a GammaMap stored in tmpGamma
    //---------------------------------------------------------------------
    void createTrueGamma(GammaMap& tmpGamma) const;

    //-------------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------------
    Real lambda;
    Real mu;
    Real toptime;
    Tree* S;
    Tree* G; 
    std::map<Node*, Real> times;
    RealVector index;
    PRNG rand;
    StrStrMap gs;
    std::vector<SetOfNodes> gamma; 


  };
}//end namespace beep

#endif
