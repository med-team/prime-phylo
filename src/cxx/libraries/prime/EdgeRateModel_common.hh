#ifndef EDGERATEMODEL_COMMON_HH
#define EDGERATEMODEL_COMMON_HH

#include "EdgeRateModel.hh"

#include <iostream>
#include <sstream>

#include "Beep.hh"
#include "BeepVector.hh"

namespace beep
{
  // Forward declarations
  class Density2P;
  class Node;
  class Probability;
  class Tree;
  //----------------------------------------------------------------------
  //
  // class EdgeRateModel_common
  //! Base class defining a common interface and defaults for EdgeRateModels
  //
  //! These classes model rate (average over positions) of each node in the
  //! Tree T, with the underlying density function rateProbs. 
  //!
  //! Subclasses of type EdgeRateModel_common provides models for rates that
  //! are constant over edges, ConstRateModel, or that vary between edges, 
  //! VarRateModel.
  //!
  //!  Authors Bengt Sennblad, 
  //!  copyright the MCMC club, SBC
  //
  //----------------------------------------------------------------------
  
  class EdgeRateModel_common : public virtual EdgeRateModel
  {

  public:
    //----------------------------------------------------------------------
    // Constructor/Destructor
    //----------------------------------------------------------------------
    EdgeRateModel_common(Density2P& rateProb, const Tree& T, EdgeWeightModel::RootWeightPerturbation rwp);
    EdgeRateModel_common(const EdgeRateModel_common& erm);
    virtual ~EdgeRateModel_common();
    EdgeRateModel_common& operator=(const EdgeRateModel_common& erm);

    //----------------------------------------------------------------------
    //
    // Interface
    // Provides default return values for subclasses
    //
    //----------------------------------------------------------------------

    // Access to shared members 
    // These return const objects, as a reasonable encapsulation compromise
    //----------------------------------------------------------------------
    const Density2P& getDensity() const;
    const Tree& getTree() const;
    RealVector& getRateVector() const;

    // Interface inherited from ProbabilityModel 
    //! Default value for likelihood = 1.0
    //----------------------------------------------------------------------
    virtual Probability calculateDataProbability();
    virtual void update();
    virtual std::string type() const;

    // Access parameters
    //----------------------------------------------------------------------

    //! Returns mean of underlying density.
    virtual Real getMean() const;
    //! Returns variance of underlying density. 
    virtual Real getVariance() const;

    //! Returns rate for (incoming edge to) node. Pointer version
    //! precondition: node!=NULL
    virtual Real getRate(const Node* node) const;
    //! Returns rate for (incoming edge to) node. Reference version
    virtual Real getRate(const Node& node) const;

    //! Returns mean of underlying density. 
    // TODO: The operator versions should perhaps be deprecated /bens
    //! Returns rate for (incoming edge to) node. Reference version
    virtual Real operator[](const Node& node) const;
    //! Returns rate for (incoming edge to) node. Pointer version
    virtual Real operator[](const Node* node) const;

    // set parameters
    //----------------------------------------------------------------------

    virtual void setMean(const Real& newValue);
    //! Returns variance of underlying density.
    virtual void setVariance(const Real& newValue);

    //! sets rate for (incoming edge to) node. Pointer version
    virtual void setRate(const Real& newRate, const Node* node) = 0;
    //! sets rate for (incoming edge to) node. Reference version
    virtual void setRate(const Real& newRate, const Node& node) = 0;

    //----------------------------------------------------------------------
    // I/O
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream& o, const EdgeRateModel_common& rm);
    virtual std::string print() const;

    // Overloading functions inherited from EdgeWeightModel
    //------------------------------------------------------------------
    unsigned nWeights() const;
    RealVector& getWeightVector() const;
    void setWeight(const Real& weight, const Node& u);
    Real getWeight(const Node& u) const;
    void getRange(Real& low, Real& high);
    virtual RootWeightPerturbation getRootWeightPerturbation() const;
    virtual void setRootWeightPerturbation(RootWeightPerturbation rwp);

  protected:
    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------
    // TODO: Attribute rateProb probably does not need to be external
    // but could be an object specific for this class. The constructor should 
    // then instead take a Real for the variance instead of a Density2P
    // Contra this is that with an external rateProb, it could be shared among
    // genes and the variance could be handled in a separate class. 
    // Additionally, I would have to templatize EdeRateModel and all its 
    // subclasses with respect to rateProb, hmm! /bens
    Density2P* rateProb;  //!< density function for rates
    const Tree* T;        //!< The Tree 
    mutable RealVector edgeRates; //!< Keeps rates for all nodes/edges in tree T
    EdgeWeightModel::RootWeightPerturbation perturbedRootEdges; //! Guides perturbation of edges around root.
  };


}//end namespace beep

#endif
