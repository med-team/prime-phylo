#ifndef BIRTHDEATHMCMC_HH
#define BIRTHDEATHMCMC_HH

#include <string>
#include <iostream>

#include "BirthDeathProbs.hh"
#include "StdMCMCModel.hh"

namespace beep
{
  // Forward declarations
  class PRNG;
  class Tree;

  //-------------------------------------------------------------
  //
  //! Provides MCMC interface to BirthDeathProbs and perturbation
  //! of its parameters 
  //! Author: Bengt Sennblad, SBC, � the MCMC-club, SBC, all rights reserved
  //
  //-------------------------------------------------------------
  class BirthDeathMCMC : public StdMCMCModel,
			 public BirthDeathProbs
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //-------------------------------------------------------------
    BirthDeathMCMC(MCMCModel& prior,Tree &S, Real birthRate, 
		   Real deathRate, Real* topTime = 0);
    ~BirthDeathMCMC();


    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
    void fixRates();
    void multiplySuggestionVariance(Real multiplier)
    {
      suggestion_variance = multiplier * (birth_rate+death_rate) / 2.0;
    }

    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();
    std::string ownStrRep() const;
    std::string ownHeader() const;
    Probability updateDataProbability();

    //-------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const BirthDeathMCMC& A);
    std::string print() const;

  private:
//     //-------------------------------------------------------------
//     //
//     // Implementation
//     //
//     //-------------------------------------------------------------

//     // Utility function for changing birth/death rates.
//     // Returns a number in the interval [0.8, 1.25].
//     //-------------------------------------------------------------
//     Real rateChange();

    //-------------------------------------------------------------
    //
    // Attributes - none
    //
    //-------------------------------------------------------------
    Real old_birth_rate;
    Real old_death_rate;
    bool estimateRates;

    //! The suggestion function uses a normal distribution around the current
    //! value. For the proposal-ratios to function right, we need to keep the 
    //! variance to the distribution constant. This is a problem since we do not
    //! know the location/scale of the distribution ahead of time. As a fix, we
    //! store a variance initiated from the start values of dupl/loss rates.
    Real suggestion_variance;
  };
}//end namespace beep


#endif
