#ifndef HYBRIDTREE_HH
#define HYBRIDTREE_HH

#include "Tree.hh"

#include <map>
#include <iostream>

namespace beep
{
  //! Extension of Tree to model hybridization networks
  class HybridTree : public Tree
  {
  public:
    //-----------------------------------------------------------
    //
    // Construct/destruct/assign
    //
    //-----------------------------------------------------------
    HybridTree();
    HybridTree(const Tree& T);
    ~HybridTree();
    HybridTree(const HybridTree& T);
    HybridTree& operator=(const HybridTree& T);
   
    //-----------------------------------------------------------
    //
    // Interface
    //
    //-----------------------------------------------------------
    //! \name{Overloading selected functions from Tree}
    //@{
    //! Extinction nodes aren't really leaves
    unsigned getNumberOfLeaves() const; 

    Node* addNode(Node* leftChild, Node* rightChild, unsigned ID, 
		  std::string name, bool extinct = false);
    
    void setTime(const Node& n, Real t) const;
    void setEdgeTime(const Node& n, Real t) const;
    //@}

    //!�\name{Access and test of to hybrid-specific relations}
    //@{
    //! Get other parent of n, i.e., the hybrid parent not accessible 
    //! throgh getParent(),  NULL if n is not hybrid
    Node* getOtherParent(const Node& n) const;
    void setOtherParent(const Node& child, Node* parent); // parent can be NULL
    void switchParents(Node& child);

    //! Returns the other child of n's other parent i.e., the hybrid
    //! parent NOT accessible through u.getParent(), NULL if n is not hybrid
    Node* getOtherSibling(const Node& n) const;

    //! If n has hybrid child it is returned, otherwise NULL is returned
    Node* getHybridChild(const Node& u) const;

    bool isHybridNode(const Node& n) const;
    bool isExtinct(const Node& n) const;
    bool isBinaryNode(const Node& n) const;
 
    Tree& getBinaryTree() const;

    std::vector<Node*> getCorrespondingBinaryNodes(Node* h);
    Node* getCorrespondingHybridNode(Node* b);
 
    unsigned countHybrids();
    unsigned countExtinct();


    //@}

    //---------------------------------------------------------------
    //! \name{Access to hybrid specific attributes, used by TreeIO}
    //@{
    //---------------------------------------------------------------
    std::map<const Node*, Node*>* getOPAttribute() const;
    std::map<const Node*, unsigned>* getEXAttribute() const;
    //@}

    //---------------------------------------------------------------
    //! IO
    //@{
    //---------------------------------------------------------------
    std::string print(bool useET, bool useNT, bool useBL, bool useER) const;
    std::string print() const;
    
    std::string printBinary2Hybrid() const;
    std::string printHybrid2Binary() const;
    //@}
    //! \name{This function is not defined for class hybridTree}
    //@{
    unsigned getHeight() const;
    Real imbalance() const;
    Node* mostRecentCommonAncestor(Node* a, Node* b) const;
    //@}

  private:
    //-----------------------------------------------------------
    //
    // Implementation
    //
    //-----------------------------------------------------------
    //! \name{Overloading selected functions from Tree}
    //@{
    void clearTree();
    Node* copyAllNodes(const Node *v);
    std::string subtree4os(Node *v, std::string indent_left, 
			   std::string indent_right, bool useET, 
			   bool useNT, bool useBL) const;
    //@}

    Node* buildFromBinaryTree(const Node* u);
    void updateBinaryTree() const;
    Node* copyAllHybridNodes(Node *v) const;

    //! Delete all nodes lower in the tree. The current node is not deleted.
    void deleteHybridSubtree(Node* n);
    void deleteHybridNode(Node* n, Node* op);
    
    void renameLeaves(const Node& n, Node& n2) const;

  public:
    //-----------------------------------------------------------
    //
    // Attributes
    //
    //-----------------------------------------------------------
    //! otherParent holds for each hybrid child node its othe parent,
    // i.e., the one that its parent pointer is NOT pointing to
    mutable std::map<const Node*, Node*> otherParent;
    mutable std::map<const Node*, unsigned> extinct;

    mutable std::map<const Node*, std::vector<Node*> > hybrid2Binary;
    mutable std::map<const Node*, Node*> binary2Hybrid; 
    mutable Tree bTree;




    //-----------------------------------------------------------
    //
    // Not functional here
    //
    //-----------------------------------------------------------
  //overloading 
    Node* addNode(Node* leftChild, Node* rightChild, unsigned ID, 
		  std::string name, Real dummyTime, bool extinct = false)
    {
      std::cerr << "addNode with dummyTime is Deprecated\n";
      return addNode(leftChild, rightChild, ID, name, extinct);
    }
 

  };


}//end namespace beep

#endif
