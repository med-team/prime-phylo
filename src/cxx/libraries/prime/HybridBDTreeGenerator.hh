#ifndef HYBRIDBDTREEGENERATOR_HH
#define HYBRIDBDTREEGENERATOR_HH

#include "BDTreeGenerator.hh"
#include "HybridTree.hh"

namespace beep
{
  class HybridBDTreeGenerator : public BDTreeGenerator
  {
  public:
    //-------------------------------------------------------------------
    //
    // Construct/destruct
    //
    //-------------------------------------------------------------------
    HybridBDTreeGenerator(HybridTree& S_in, Real birthRate, Real deathRate);
    ~HybridBDTreeGenerator();

    //-------------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------------
    StrStrMap exportGS();
    //! Please note, this gamma maps the generated gene tree to the 
    //! binary tree corresponding to S
    GammaMap exportGamma();
              
    //-------------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------------
//     friend std::ostream& operator<<(std::ostream &o, 
// 				    const HybridBDTreeGenerator& BDG);

    //-------------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------------
    HybridTree* H;
  };
}//end namespace beep

#endif
