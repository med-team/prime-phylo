#ifndef MPIMCMC_HH
#define MPIMCMC_HH

#include "MCMCModel.hh"

#include "Probability.hh"
#include "AbstractMCMC.hh"

#include <vector>

#include <boost/mpi/collectives.hpp>
#include <boost/serialization/string.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/mpi/nonblocking.hpp>
#include <iostream>


//-------------------------------------------------------------
//
// A very simple version of the Metropolis algorithm.
//
//! Author: Lars Arvestad, SBC, � the MCMC-club, SBC, all rights reserved
//
//-------------------------------------------------------------

namespace mpi = boost::mpi;

namespace beep
{

  class PRNG;
  typedef   std::vector< std::pair<int,int> > pairVec;
  class MpiMCMC : public AbstractMCMC
  {
  public:

    MpiMCMC(MCMCModel & model, float temperature, mpi::communicator *world );
    ~MpiMCMC();
    virtual bool iterate( IterationObserver & iterObs, unsigned int * iterations_done );

 static int randomWorkerNodeIndex( int nrWorkerNodes, PRNG & R_ );
 static void fillRandomIndex( pairVec & pV, int nrWorkerNodes, int steps, PRNG & R_ );
    static  void fillRandomFloat( std::vector<float> & floatV, int steps, PRNG & R_ );
  protected:
    bool swapNeeded(float myTemperature, float otherTemperature, Probability myProb, Probability otherProb, float random);


    //    MCMCModel &model;
    MCMCModel & model;
    float temperature;
    PRNG &R;		       // Source of randomness
    Probability p;	       // Probability of the model's current state
    mpi::communicator *world;


  };

}//end namespace beep
#endif
