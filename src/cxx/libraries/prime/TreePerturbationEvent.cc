#include "TreePerturbationEvent.hh"

#include <cassert>

// Author: Joel Sj�gren, � the MCMC-club, SBC, all rights reserved

namespace beep
{

using namespace std;

TreePerturbationEvent::TreePerturbationEvent(TreePerturbationType treePertType, 
		const Node* rootPath, const Node* rootPath2) :
	PerturbationEvent(PERTURBATION),
	m_treePertType(treePertType),
	m_subtrees(),
	m_rootPath(rootPath),
	m_rootPath2(rootPath2)
{
	assert((rootPath == NULL && rootPath2 == NULL) || rootPath != rootPath2);
}


TreePerturbationEvent::~TreePerturbationEvent()
{
}


TreePerturbationEvent::TreePerturbationType TreePerturbationEvent::getTreePerturbationType() const
{
	return m_treePertType;
}


const set<const Node*>& TreePerturbationEvent::getSubtrees() const
{
	return m_subtrees;
}


void TreePerturbationEvent::getRootPaths(const Node*& path1, const Node*& path2) const
{
	path1 = m_rootPath;
	path2 = m_rootPath2;
}


void TreePerturbationEvent::insertSubtree(const Node* subroot)
{
	assert(m_subtrees.find(subroot) == m_subtrees.end());
	m_subtrees.insert(subroot);
}


TreePerturbationEvent* TreePerturbationEvent::createReRootInfo(const Node* v)
{
	// Find node just above root.
	const Node* p = v->getParent();
	while (!p->getParent()->isRoot())
	{
		p = p->getParent();
	}
	
	TreePerturbationEvent* info = new TreePerturbationEvent(REROOT, p->getSibling());
	if (v->isLeaf())
	{
		info->insertSubtree(v);
	}
	else
	{
		// Seems that edges to v's children may change during rotation...
		info->insertSubtree(v->getLeftChild());
		info->insertSubtree(v->getRightChild());
		info->m_rootPath2 = v;
	}
	
	if (v->getParent() != p)
	{
		// Add sibling subtrees from v and upwards.
		const Node* q = v;
		while (q != p)
		{
			info->insertSubtree(q->getSibling());
			q = q->getParent();
		}
	}
	return info;
}


TreePerturbationEvent* TreePerturbationEvent::createNNIInfo(const Node* v)
{
	TreePerturbationEvent* info = new TreePerturbationEvent(NNI, v->getParent());
	const Node* w = v->getParent()->getSibling();
	info->insertSubtree(v);
	info->insertSubtree(w);
	return info;
}


TreePerturbationEvent* TreePerturbationEvent::createSPRInfo(const Node* u_c, const Node* u_c_new)
{
	TreePerturbationEvent* info;
	
	const Node* u = u_c->getParent();
	const Node* u_s = u->getSibling();
	const Node* u_oc = u_c->getSibling();
	const Node* u_p = u->getParent();
	
	// We have essentially three cases:
	// 1) u_c_new = u_s.
	// 2) u_c_new is an ancestor of u.
	// 3) None of the above, in which case we're working with separate subtrees.
	if (u_c_new == u_s)
	{
		info = new TreePerturbationEvent(SPR, u);
		info->insertSubtree(u_oc);
		info->insertSubtree(u_c_new);
	}
	else if ((*u) < (*u_c_new))
	{
		info = new TreePerturbationEvent(SPR, u_p);
		info->insertSubtree(u_oc);
		const Node* v = u;
		while (v != u_c_new)
		{
			info->insertSubtree(v->getSibling());
			v = v->getParent();
		}
		info->insertSubtree(u_c);
	}
	else
	{
		info = new TreePerturbationEvent(SPR, u, u_p);
		info->insertSubtree(u_oc);
		info->insertSubtree(u_c_new);
		info->insertSubtree(u_c);
	}
	return info;
}


TreePerturbationEvent* 
TreePerturbationEvent::createEdgeWeightInfo(const Node* v)
{
	// Safety precaution:
	// Depending on how the perturbation is made in the future, it might
	// be the case that the root's child edges are treated as shared, implying
	// that if one of them is pertubed, so is the other.
	TreePerturbationEvent* info = (!v->isRoot() && v->getParent()->isRoot()) ?
			new TreePerturbationEvent(EDGE_WEIGHT, v, v->getSibling()) :
			new TreePerturbationEvent(EDGE_WEIGHT, v);
	return info;
}


string TreePerturbationEvent::print() const
{
	switch (m_treePertType)
	{
	case REROOT:       return "TreePerturbationEvent: REROOT";
	case NNI:          return "TreePerturbationEvent: NNI";
	case SPR:          return "TreePerturbationEvent: SPR";
	case EDGE_WEIGHT:  return "TreePerturbationEvent: EDGE_WEIGHT";
	}
	return "TreePerturbationEvent: Unknown type!!!!";
}


void TreePerturbationEvent::debugInfo() const
{
	cerr << "Root path 1: "
		<< (m_rootPath == NULL ? -1 : static_cast<int>(m_rootPath->getNumber()))
		<< ", Root path 2: "
		<< (m_rootPath2 == NULL ? -1 : static_cast<int>(m_rootPath2->getNumber()))
		<< endl;
	set<const Node*>::iterator it = m_subtrees.begin();
	for (; it != m_subtrees.end(); ++it)
	{
		cerr << "\tSubtree root: " << (*it)->getNumber() << endl;
	}
}

} // end namespace beep.

