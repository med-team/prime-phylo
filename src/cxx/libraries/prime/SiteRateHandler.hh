#ifndef SITERATEHANDLER_HH
#define SITERATEHANDLER_HH

#include <iostream>
#include <string>
#include <vector>

#include "Beep.hh"
#include "EdgeRateModel.hh"
#include "Node.hh"
#include "Probability.hh"
#include "ProbabilityModel.hh"



namespace beep
{
//----------------------------------------------------------------------
//
//! Handles rate variation using discretized gamma distribution.
//! This class returns site-specific rates from a discretized gamma 
//! distribution (with mean 1.0) and calculates the probabilities of 
//! these rates given a shape parameter alpha (this prob equals 1.0)
//! Uses the DiscreteGamma class always!
//
//----------------------------------------------------------------------
  class SiteRateHandler 
  {
    typedef std::vector<Real> RateVec;
  public: 
    //----------------------------------------------------------------------
    //
    // Construct/destruct/assign
    //
    //----------------------------------------------------------------------
    
    // Assumes no rate variation
    //----------------------------------------------------------------------
    SiteRateHandler(unsigned ncats, EdgeRateModel& alpha);
    SiteRateHandler(const SiteRateHandler& srm);
    virtual ~SiteRateHandler() ;
    SiteRateHandler& operator=(const SiteRateHandler& srm);
    
    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
    
    void update();

    //! returns number of rate for categories
    //----------------------------------------------------------------------
    unsigned nCat() const;

    //! returns value of rate for category rate_cat
    //----------------------------------------------------------------------
    virtual Real getRate(const unsigned& rate_cat) const;

    //! Change alpha parameter
    virtual bool setAlpha(const Real& alpha);
    virtual Real getAlpha();


    //----------------------------------------------------------------------
    // I/O
    //----------------------------------------------------------------------
    friend std::ostream& 
    operator<<(std::ostream &o, const SiteRateHandler& srm);

    virtual std::string print() const;



  protected:
    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------
    EdgeRateModel* alpha;         //!< parameter for rate distribution
    RateVec siteRates;            //!< holds the rates for each category
    static const Real MAX = 1e10; //!< rates do not change for alpha > MAX.
                                  //!< In practice, the limit is probably 
                                  //! much lower
                                  // TODO: Find practical limit /bens

  };

}//end namespace beep
#endif
