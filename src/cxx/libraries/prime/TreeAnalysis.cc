#include <vector>

#include "NodeMap.hh"
#include "LambdaMap.hh"
#include "GammaMap.hh"
#include "TreeAnalysis.hh"

// Author: LArs Arvestad, � the MCMC-club, SBC, all rights reserved
namespace beep
{

  using namespace std;


  //----------------------------------------------------------------
  //
  // Constructor
  // 
  //----------------------------------------------------------------
  TreeAnalysis::TreeAnalysis(Tree &Tin)
    : T(Tin)
  {
  }


  //----------------------------------------------------------------
  //
  // Interface
  // 
  //----------------------------------------------------------------
  
  // Count the number of leaves for every subtree. The vector is indexed
  // by the id of the root of each subtree.
  //----------------------------------------------------------------
  NodeMap<unsigned>
  TreeAnalysis::subtreeSize()
  {
    NodeMap<unsigned> sizes(T, 0);
    recursiveSubtreeSize(sizes, T.getRootNode());
    return sizes;
  }

  // Return a boolean vector telling you whether the node's subtrees are
  // isomorphic or not. The vector is indexed by the node id. Note that
  // no consideration is taken regarding reconciliations.
  //----------------------------------------------------------------
  NodeMap<bool>
  TreeAnalysis::isomorphicSubTrees(beep::LambdaMap &l)
  {
    NodeMap<bool> iso(T, false);

    computeIsomorphicTrees(iso, l, T.getRootNode());
    return iso;

  }

  // Return a boolean vector telling you whether the node's reconciled 
  // subtrees are isomorphic or not.
  NodeMap<bool>
  TreeAnalysis::isomorphicSubTrees(beep::GammaMap &gamma)
  {
    NodeMap<bool> iso(T, false);

    computeIsomorphicTrees(iso, gamma, *T.getRootNode());
    return iso;

  }

  //----------------------------------------------------------------
  //
  // Implementation
  // 
  //----------------------------------------------------------------
  
  // Supports subtreeSize above.
  unsigned
  TreeAnalysis::recursiveSubtreeSize(NodeMap<unsigned> &sizes, Node *v)
  {
    if(v->isLeaf()) 
      {
	sizes[v] = 1;
	//      sizes[v->getNumber()] = 1;
	return 1;
      }
    else
      {
	unsigned l = recursiveSubtreeSize(sizes, v->getLeftChild());
	unsigned r = recursiveSubtreeSize(sizes, v->getRightChild());
	unsigned sum = l + r;
	sizes[v] = sum;
	//      sizes[v->getNumber()] = sum;
	return sum;
      }
  }



  // Fill NodeMap<bool> iso with the result of
  // recursiveIsomorphicTrees(beep::GammaMap &gamma,Node *v1,Node *v2)
  void
  TreeAnalysis::computeIsomorphicTrees(NodeMap<bool> &iso, 
				       beep::LambdaMap &l, Node *r)
  {
    if(r->isLeaf()) 
      {
	iso[r] = false;
	return;
      } 
    else 
      {
	Node *left = r->getLeftChild();
	Node *right = r->getRightChild();
	if(recursiveIsomorphicTrees(l, left, right)) 
	  {
	    iso[r] = true;
	  }
	computeIsomorphicTrees(iso, l, left);
	computeIsomorphicTrees(iso, l, right);
      }
  }

  // Fill NodeMap<bool> iso with the result of
  // recursiveIsomorphicTrees(beep::GammaMap &gamma,Node *v1,Node *v2)
  void
  TreeAnalysis::computeIsomorphicTrees(NodeMap<bool> &iso, 
				       beep::GammaMap &gamma,
				       Node& r)
  {
    iso[r] = false;                // default value

    if(r.isLeaf() == false)       // Leaves cannot have isomorphic subtrees
      {
	Node& left = *r.getLeftChild();
	Node& right = *r.getRightChild();
	// Pass recursion on
	computeIsomorphicTrees(iso, gamma, left);
	computeIsomorphicTrees(iso, gamma, right);

	if(iso[left] == iso[right]) // Only then can subtrees be isomorphic!
	  {
	    // Now run check for isomorphic reconciled subtrees
	    iso[r] = recursiveIsomorphicTrees(gamma, left, right);
	  }
      }
  }

  // Supports isomorphicSubTrees above. Is similar to DupSpecModel::isEqual.
  // Return true if the subtrees rooted at v1 and v2 are isomorphic, 
  // otherwise return false.
  bool
  TreeAnalysis::recursiveIsomorphicTrees(beep::LambdaMap &l, Node *v1,
					 Node *v2)
  {
    if(v1->isLeaf() && v2->isLeaf()) 
      {
	if(l[v1] == l[v2]) 
	  {
	    return true;
	  } 
	else 
	  {
	    return false;
	  }
      } 
    else if(v1->isLeaf() || v2->isLeaf()) 
      {
	return false;
      } 
    else
      {
	Node *left_left = v1->getLeftChild();
	Node *left_right = v1->getRightChild();
	Node *right_left = v2->getLeftChild();
	Node *right_right = v2->getRightChild();
      
	if(recursiveIsomorphicTrees(l, left_left, right_left)
	    && recursiveIsomorphicTrees(l, left_right, right_right))
	  {
	    return true;
	  } 
	else if(recursiveIsomorphicTrees(l, left_left, right_right)
		 && recursiveIsomorphicTrees(l, left_right, right_left))
	  {
	    return true;
	  }
	else
	  {
	    return false;
	  }
      }
  }

  // Isomorphy of reconciled trees: return true if 
  // $(G_{v1}, \gamma_{v1})\cong{G_{v2},\gamma_{v2})$.
  bool
  TreeAnalysis::recursiveIsomorphicTrees(beep::GammaMap& gamma, 
					 Node& v1, Node& v2)
  {
    // 1) If both are leaves and map to same host, then true, else false
    if(v1.isLeaf() && v2.isLeaf()) 
      {
	if(gamma.getLowestGammaPath(v1)==gamma.getLowestGammaPath(v2) &&
	   gamma.getHighestGammaPath(v1)==gamma.getHighestGammaPath(v2))
	  {
	    return true;
	  } 
      } 
    // 2) If both are internal vertices we must recurse to see if reconciled 
    // subtrees are isomorphic all the way. 
    // Note that there are two alternatives for true:
    //
    // a) $(G_{v1_left),\gamma_{v1_left})\cong(G_{v2_left),\gamma_{v2_left})
    // && (G_{v1_right),\gamma_{v1_right})\cong(G_{v2_right),\gamma_{v2_right})$
    // or
    // b) $(G_{v1_left),\gamma_{v1_left})\cong(G_{v2_right),\gamma_{v2_right})
    // && (G_{v1_right),\gamma_{v1_right})\cong(G_{v2_left),\gamma_{v2_left})$
    else if(v1.isLeaf() == false && v2.isLeaf() == false) 
      {
	Node& v1_left = *v1.getLeftChild();
	Node& v1_right = *v1.getRightChild();
	Node& v2_left = *v2.getLeftChild();
	Node& v2_right = *v2.getRightChild();

	if((recursiveIsomorphicTrees(gamma, v1_left, v2_left)
	    && recursiveIsomorphicTrees(gamma, v1_right, v2_right))  //a)
	   ||                                                        //or
	   (recursiveIsomorphicTrees(gamma, v1_left, v2_right)       //b)
	    && recursiveIsomorphicTrees(gamma, v1_right,  v2_left)))
	  {
	  // If matching subtrees and anti-chains on the nodes, we better 
	  //check how the anti-chains are matching up too.
	  if(gamma.getLowestGammaPath(v1)==gamma.getLowestGammaPath(v2) &&
	     gamma.getHighestGammaPath(v1)== gamma.getHighestGammaPath(v2))
	    {
	      return true;
	    }
	  }
      }
    // If we do not return true anywhere above, we end up here,
    // and the reconciled trees are not isomorphic
    return false;  
  }
  
}//end namespace beep
