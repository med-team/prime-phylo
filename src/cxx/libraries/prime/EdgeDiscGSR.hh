#ifndef EDGEDISCGSR_HH
#define EDGEDISCGSR_HH

#include <list>
#include <time.h>
#include <vector>
#include <cmath>

#include "Beep.hh"
#include "BeepVector.hh"
#include "Density2P.hh"
#include "EdgeWeightModel.hh"
#include "GenericMatrix.hh"
#include "LambdaMap.hh"
#include "PerturbationObservable.hh"
#include "EdgeDiscBDProbs.hh"
#include "lsd/LSDProbs.hh"


#include <vector>

#include "Density2P.hh"
#include "Probability.hh"
#include "BeepVector.hh"



#include "GammaDensity.hh"
#include "EdgeDiscPtMapIterator.hh"
#include "StrStrMap.hh"

namespace beep {

    // Forward declarations.
    class EdgeDiscBDProbs;
    class EdgeDiscTree;
    class Node;
    class Probability;
    class TreePerturbationEvent;

    /**
     * Model based on the Gene Evolution Model (GEM) that uses a 
     * discretization of the host tree to numerically integrate over 
     * all possible "realisations" of the current guest tree topology 
     * (i.e. dated instances of the reconciliation).
     *
     * Two simplifications are made:
     * 1) Only one duplication event may occur within one discretization 
     *    segment.
     * 2) The midpoint of a discretization segment is used for 
     *    approximations pertaining to an event occurring there within, 
     *    i.e. the "density" of the duplication and for computing the 
     *    edge rate.
     *
     * Relaxed molecular clock: rates are drawn IID from e.g. a Gamma 
     * distribution. (Note that, for a realisation, the edge rate is 
     * computed as the length of the edge in the guest tree, divided 
     * by the time it spans in the host tree).
     *
     * Moreover, as an EdgeWeightModel, this class will act as a model 
     * for an MCMC perturbing the lengths of G.
     *
     * This class listens for perturbation updates on the host tree 
     * times, birth-death parameters, the guest tree topology and 
     * lengths, and the rate density function, so any classes perturbing 
     * these must make sure to notify this class.
     *
     * NOTE: Host tree topology changes are not allowed, but hopefully 
     * time alterations can now be made!
     */
    class EdgeDiscGSR
    : public EdgeWeightModel, public PerturbationObserver {
        typedef EdgeDiscretizer::Point Point;
        typedef std::set<const Node*> nodeset;

    public:

        /**
         * Constructor.
         * @param G the guest tree.
         * @param DS the discretized version of the host tree S.
         * @param GSMap the G-to-S leaf mapping.
         * @param edgeRateDF the rate density function.
         * @param BDProbs the birth and death probs. for the 
         *        duplication-loss process.
         * @param fixedGNodes a map stating known speciations u of G that 
         *        should be kept fixed at sigma(u). Used as if containing 
         *        bools. May only be set when not perturbing the guest tree.
         */
        EdgeDiscGSR(Tree* G,
                EdgeDiscTree* DS,
                StrStrMap* GSMap,
                Density2P* edgeRateDF,
                EdgeDiscBDProbs* BDProbs,
                UnsignedVector* fixedGNodes = NULL);

        /**
         * Destructor.
         */
        virtual ~EdgeDiscGSR();

        /**
         * Returns the guest tree.
         * @return the guest tree.
         */
        virtual Tree& getTree() const;

        /**
         * Returns the number of weights, i.e. (roughly) the number of
         * edges in the guest tree.
         * @return the number of perturbable lengths.
         */
        virtual unsigned nWeights() const;

        /**
         * Returns the edge lengths of the guest tree.
         * @return the lengths of the guest tree.
         */
        virtual RealVector& getWeightVector() const;

        /**
         * Returns the length of an edge in the guest tree.
         * @param node the lower node of the edge.
         * @return the length of the edge.
         */
        virtual Real getWeight(const Node& node) const;

        /**
         * Sets the length of an edge in the guest tree.
         * @param weight the length to be set.
         * @param u the lower node of the edge.
         */
        virtual void setWeight(const Real& weight, const Node& u);

        /**
         * Returns the valid range of the underlying rate density function.
         * TODO: Is this sound?
         * @param low the lower value of the range.
         * @param high the upper value of the range.
         */
        virtual void getRange(Real& low, Real& high);

        /**
         * getPlacementProbability
         *
         * Returns the probability Pr[u on x | G, l, \theta]. Which is the
         * probability of u being placed in the time interval induced by x.
         * 
         *
         * ASSUMPTIONS:
         * - at bar has been computed for all placements of u.
         * - at has been com computed for all placements of u.
         *
         * @param u the node of G.
         * @param x .
         *
         * @author Peter Andersson (peter9), Mattias Frånberg (fmattias)
         */
        Probability getPlacementProbability(const Node *u, const Point *x);

        /**
         * getPlacementProbabilityAlternate
         *
         * Returns the probability Pr[u on x | G, l, \theta]. Which is the
         * probability of u being placed in the time interval induced by x.
         * This is an alternate way of finding the probability
         *
         * ASSUMPTIONS:
         * - at bar has been computed for all placements of u.
         * - at has been com computed for all placements of u.
         *
         * @param u the node of G.
         * @param x .
         *
         * @author Ikram Ullah (ikramu)
         */
        Probability getPlacementProbabilityAlternate(const Node *u, const Point *x);

        /**
         * getJointTreePlacementDensity
         *
         * Returns the joint density Pr[G,l,u on x | \theta] of a certain
         * placement of u on x.
         *
         * ASSUMPTIONS:
         * - at bar has been computed for all placements of u.
         * - at has been computed for all placements of u.
         *
         * @param u the node of G.
         * @param x the vertex where u is placed.
         *
         * @author Peter Andersson (peter9), Mattias Frånberg (fmattias)
         */
        Probability getJointTreePlacementDensity(const Node *u, const Point *x);

        /**
         * getTotalPlacementDensity
         *
         * Return the total placement density of a node, i.e. the density
         * sum_{x in DS} Pr[G,l, u on x], which of course should be equal to
         * the tree density Pr[G,l]. Can be used as a test.
         *
         * @param u A node in G
         * @author Peter Andersson, Mattias Frånberg (January 2010).
         *
         */
        Probability getTotalPlacementDensity(const Node *u);

        /**
         * Returns info on how root edge weights are perturbed.
         * @return perturbation technique.
         */
        virtual RootWeightPerturbation getRootWeightPerturbation() const {
            return EdgeWeightModel::BOTH;
        }

        /**
         * Has no effect.
         * @param rwp.
         */
        virtual void setRootWeightPerturbation(RootWeightPerturbation rwp) {
        }

        /**
         * Returns a string with information about the rate
         * probabilities of the guest tree.
         * @return an info string.
         */
        virtual std::string print() const;

        /**
         * Callback method for notifying this object about changes to
         * the underlying parameter holders (host tree times, birth-death
         * rates, guest tree, and edge rate density function).
         * Updates or restores the internal probabilities depending
         * on event. For changes to the guest tree, there is support
         * for making an optimized partial update in case detailed 
         * information (a TreePerturbationEvent) is specified.
         * @param sender the address of the parameter holder which
         *        has been perturbed.
         * @param event the kind of perturbation. Null value results
         *        in full update.
         */
        virtual void
        perturbationUpdate(const PerturbationObservable* sender,
                const PerturbationEvent* event);

        /**
         * Currently does nothing. All updates are made in (callbacks to)
         * perturbationUpdate().
         */
        virtual void update();

        /**
         * Computes the data probability (with respect to all possible
         * realisations) given that a single lineage starts at the top of
         * the host tree.
         * Requires that help and primary data structures are up-to-date.
         * @return the data probability.
         */
        virtual Probability calculateDataProbability();

        EdgeDiscTree* getDiscretizedHostTree() {
            return m_DS;
        }

        StrStrMap getGSMap() {
            return m_GSMap;
        }

        EdgeDiscBDProbs* getEdgeDiscBDProbs() {
            return m_BDProbs;
        }

        Density2P* getEdgeRateDensity() {
            return m_edgeRateDF;
        }

        /**
         * TBD.
         */
        virtual std::string getDebugInfo(bool inclAts = false,
                bool inclBelows = false, bool inclAbove = false);


        /**
         * TBD.
         * @return
         */
        std::string getRootProbDebugInfo();

        /**
         * Convenience method for printing a discretization point.
         * @param x a host tree discretization point.
         * @return the point as '(nodeNo,index)'
         */
        static std::string pt2str(const Point &x) {
            using namespace std;
            stringstream ss;
            ss << "(" << x.first->getNumber() << ", " << x.second << ")";
            return ss.str();
        }

        /**
         * return the lambda map (sigma)
         * @return LambdaMap
         */
        LambdaMap getSigma() {
            return m_sigma;
        }

    private:

        /**
         * Helper. Updates help data structures. Invoke before updating
         * primary data structures (partially as well as fully).
         */
        void updateHelpStructures();

        /**
         * Recursive helper. For each node u of G, finds and stores the 
         * lowest possible point in DS where u can be placed. Recursively 
         * processes all nodes in the subtree below (and including) the 
         * specified node.
         * @param u the root of the subtree of G.
         */
        void updateLoLims(const Node* u);

        /**
         * Recursive helper. For each node u of G, finds and stores the 
         * highest possible point in DS where  u can be placed. 
         * Recursively processes all nodes in the subtree below (and 
         * including) the specified node.
         * @param u the root of the subtree of G.
         */
        void updateUpLims(const Node* u);

        /**
         * Makes a clean update of probability data structures. Requires
         * that help data structures are up-to-date.
         */
        void updateProbsFull();

        /**
         * Helper. Makes a partial update of probability data along a path
         * to the root. Requires that help data structures are up-to-date.
         * @param rootPath the lower node of the path. Must not be null.
         */
        void updateProbsPartial(const Node* details);

        /**
         * Helper. For all valid placements in DS for u of G, computes the
         * partial probabilities of rooted tree G_u (by summing over 
         * possible realisations of G_u). Invokes 'updateBelowProbs(u)' 
         * afterwards. Requires that help structures are up-to-date, and 
         * child probabilities in case of a non-recursive call.
         * @param u the node of G.
         * @param doRecurse true to process children recursively first.
         */
        void updateAtProbs(const Node* u, bool doRecurse);

        /**
         * Helper. For all valid placements in DS for the start of an edge 
         * going down to u of G, computes the partial probabilities of 
         * planted tree G^u (by summing over possible realisations of G^u).
         * Requires that help structures and child probabilities are 
         * up-to-date.
         * @param u the node of G.
         */
        void updateBelowProbs(const Node* u);

        /**
         * For all valid placements x in DS for u of G, computes the
         * partial probabilities of tree G\{G_u}U{u} when u is placed at x.
         *
         * @author Peter Andersson (peter9), Mattias Frånberg (fmattias)
         */
        void calculateAtBarProbabilities();

        /**
         * Convert the ats and at_bars densities to corresponding probabilities
         * This is used in alternative calculation of placement probabilities
         */
        void convertDensitiesToProbabilities();

        /**
         * A level is all non-leaf children of the previous level, the first
         * level contains only the root, the last level contains all leaves.
         * This function creates the levels for the tree with the specified
         * root.
         *
         * @param root - The root of a tree.
         * @param levels - The levels will be stored in this vector.
         * @author Peter Andersson (peter9), Mattias Frånberg (fmattias)
         */
        void createLevels(Node *root, std::vector< std::vector <Node *> > &levels);

        /**
         * Calculates the at bar probability for all placements of a
         * specific node u.
         *
         * ASSUMPTIONS:
         * - At bar has been computed for all placements of the parent of u.
         * - Below has been computed for all placements of the sibling of u.
         * - loLim and upLim has been computed for u and the parent of u.
         *
         * @param u - The node which we want to compute the at bar probability
         *            for.
         * @author Peter Andersson (peter9), Mattias Frånberg (fmattias)
         */
        void calculateNodeAtBarProbability(const Node *u);

        /**
         * Calculates the at bar probability for all placements of the root.
         *
         * ASSUMPTIONS:
         * - loLim and upLim has been computed for the root.
         *
         * @param root - The root of a tree.
         * @author Peter Andersson (peter9), Mattias Frånberg (fmattias)
         */
        void calculateRootAtBarProbability(const Node *root);

        /**
         * Helper. Calculates the probability density function for the
         * edge substitution rate.
         * @param l the length.
         * @param t the time.
         * @return pdf value of l/t.
         */
        Probability calcRateDensity(Real l, Real t) const {
            assert(std::isnan(l) == false);
            assert(std::isnan(t) == false);
            assert(l > 0);
            assert(t > 0);

            // NOTE: There may be need for a more robust solution than 
            // just returning the PDF value. This may have to do with 
            // the PDF on input like 0+eps becoming ill-conditioned, or 
            // that it may prove beneficial to peak the PDF at a fix value 
            // even though surrounding density values are almost zero. 
            // By employing some sort of simple interpolation technique 
            // we may be able to avoid this.

            // EX. 0) No rate. Use e.g. when debugging.
            //return 1.0;

            // EX. 1) Plain PDF. May give convergence problems on small trees?
            return m_edgeRateDF->pdf(l / t);

            // EX. 2) Averaged PDF by dividing CDF with small interval length.
            //Real r = l / t;
            //Real dr = std::min(1e-6, r / 2);
            //return ((m_edgeRateDF->cdf(r + dr) - 
            //        m_edgeRateDF->cdf(r - dr)) / (2 * dr));

            // EX.  3) Average PDF by dividing CDF with larger interval
            // length based  on timestep.
            //Real min, max;
            //m_edgeRateDF->getRange(min, max);
            //Real r = l / t;
            //if (r < min || r > max) { return 0.0; }
            //Real r2 = l / (t - m_rateDelta), r1 = l / (t + m_rateDelta);
            //return ((m_edgeRateDF->cdf(r2) - 
            //        m_edgeRateDF->cdf(r1)) / (r2 - r1));
        }

        /**
         * Helper. Stores current probabilities for the entire guest tree, 
         * or just parts of it based on specified information. To cache
         * entire tree, pass along null.
         * @param rootPath set to null to cache entire tree, or node 
         * to cache only along path to the root.
         */
        void cacheProbs(const Node* rootPath);

        /**
         * Helper. Stores the current probabilities for a node u of G.
         * @param u the node of the guest tree.
         * @param doRecurse true to cache all of subtree rooted at u.
         */
        void cacheNodeProbs(const Node* u, bool doRecurse);

        /**
         * Helper. Restores cached probabilities. These may concern 
         * parts of or all of the guest tree G.
         */
        void restoreCachedProbs();

        /**
         * Invalidates all cached data.
         */
        void clearAllCachedProbs();

        /**
         * Returns 2*lambda*timestep if a vertex in DS is not a 
         * speciation (where
         * 2 is for isomorphism), otherwise returns 1.
         * @param x discretization point in host tree.
         * @return duplication factor.
         */
        inline Real duplicationFactor(Point& x) {
            return (x.second == 0 ?
                    1.0 : 2 * m_BDProbs->getBirthRate() * m_DS->getTimestep(x.first));
        }

    private:

        Tree* m_G; /**< The guest tree G. */
        EdgeDiscTree* m_DS; /**< The discretized host tree. */
        Density2P* m_edgeRateDF; /**< Rate density function. */
        EdgeDiscBDProbs* m_BDProbs; /**< Birth and death probs. for DS. */
        LambdaMap m_sigma; /**< Guest-to-host tree sigma map. */
        StrStrMap m_GSMap; /**< Needed for Parallel 
				     implementation MpiMultiGSR */
        std::vector<Probability> sum_ats;
        std::vector<Probability> sum_aboves;
        mutable RealVector* m_lengths; /**< The lengths of edges of G. */

        /**
         * For each node u of G, contains a map stating if the node 
         * should be fixed or not at sigma(u). If a node u has a 
         * descendant v so that sigma(u)=sigma(v), an error will be thrown.
         * Vector is used as if consisting of bools. G topology must not be
         * perturbed if this has been set. Normally null.
         */
        UnsignedVector* m_fixedGNodes;

        /**
         * For each node u of G, contains the lowermost valid placement, 
         * x, of u in DS. Three situations may arise:
         * 1) u may be a speciation and x is therefore on the node sigma(u).
         * 2) Hopefully rarely: u may be a speciation but cannot be 
         *    placed as low as the node sigma(u) due to too few 
         *    discretizations steps. x is then a pure discretization 
         *    point somewhere above.
         * 3) u may not be a speciation (since it has a descendant v 
         *    so that sigma(v)=sigma(u)). x is then a pure discretization 
         *    point somewhere above.
         */
        BeepVector<Point> m_loLims;

        /**
         * For each node u of G, contains the uppermost valid placement, 
         * x, of u in DS. The following goes:
         * 1) If u is a leaf or has been specified as fixed on a node, 
         *    x is placed there, on the node sigma(u).
         * 2) Otherwise, x is a point above, or in rare cases on u's
         *    lower limit. x will always be a pure discretization point
         *    unless x=loLim(u)=upLim(u)=sigma(u).
         */
        BeepVector<Point> m_upLims;

        /**
         * For each node u of G, contains probability of rooted tree
         * G_u for valid placements of u in DS.
         */
        BeepVector<ProbabilityEdgeDiscPtMap> m_ats;
        /**
         * This is the normalized (probability) version of m_ats
         */
        BeepVector<ProbabilityEdgeDiscPtMap> p_ats;

        /**
         * For each node u of G, contains probability of planted
         * tree G^u for valid placements of u in DS.
         */
        BeepVector<ProbabilityEdgeDiscPtMap> m_belows;

        /**
         * The data structure m_at_bar[u](x) stores the probability of u
         * placed on x and all realisations of the nodes above u.
         */
        BeepVector<ProbabilityEdgeDiscPtMap> m_at_bars;
        /**
         * This is the normalized (probability) version of m_at_bars
         */
        BeepVector<ProbabilityEdgeDiscPtMap> p_at_bars;

        /**
         * Indicates if the m_at_bars values have been computed.
         */
        bool m_calculatedAtBars;

        /**
         * Indicates if the m_ats values and m_belows values have been computed.
         */
        bool m_calculatedAtAndBelow;

        /**
         * For interpolating rates.
         */
        Real m_rateDelta;

        //**
        // * The minimum number of discretization points required
        // * on the top edge of DS, the number 4 corresponds to 2
        // * intervals since the top edge has an extra point at the tip.
        // */
        //static const unsigned int MIN_DISC_POINTS_TOP = 4;

    };

} // end namespace beep

#endif  /* EDGEDISCGSR_HH */

