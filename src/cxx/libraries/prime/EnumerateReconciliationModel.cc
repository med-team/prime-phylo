#include "EnumerateReconciliationModel.hh"

#include <cmath>

#include "AnError.hh"

namespace beep
{
  //------------------------------------------------------------------------
  //
  // Class EnumerateReconciliationModel allows counting of number of 
  // possible reconciliations between the given gene tree and species
  // tree. It can also assign unique numbers to these reconciliations
  // and thereby allows enumerating them and, as it inherits from
  // ReconciledTreeModel, to compute their probability.
  //
  // author: Bengt Sennblad 
  // copyright: The MCMC-club, SBC
  // 
  //------------------------------------------------------------------------

  //------------------------------------------------------------------
  //
  // Construct / Destruct / Assign
  //
  //------------------------------------------------------------------
  EnumerateReconciliationModel::
  EnumerateReconciliationModel(Tree &G_in, StrStrMap &gs_in,
			       BirthDeathProbs &bdp_in)
    : ReconciledTreeModel(G_in, gs_in, bdp_in),
      N_V(*S,*G, 0),
      N_X(*S,*G, 0)
  {
    inits();		   // Compute important helper params
  };
  
  EnumerateReconciliationModel::
  EnumerateReconciliationModel(Tree &G_in, StrStrMap &gs_in, 
			       BirthDeathProbs &bdp_in, 
			       std::vector<SetOfNodes>& AC)
    : ReconciledTreeModel(G_in, gs_in, bdp_in, AC),
      N_V(*S,*G),
      N_X(*S,*G)
  {
    inits();		   // Compute important helper params
  };

  EnumerateReconciliationModel::
  EnumerateReconciliationModel(const EnumerateReconciliationModel &M)
    : ReconciledTreeModel(M),
      N_V(M.N_V),
      N_X(M.N_X)
  {
    inits();		   // Compute important helper params
  };

  EnumerateReconciliationModel::
  ~EnumerateReconciliationModel()
  {};

  EnumerateReconciliationModel&
  EnumerateReconciliationModel::operator=(const EnumerateReconciliationModel &M)
  {
    if (this != &M)
	{
	  ReconciledTreeModel::operator=(M);
	  N_V = M.N_V;
	  N_X = M.N_X;
	}
    
    return *this;
  };
  
  //------------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------------
  unsigned 
  EnumerateReconciliationModel::getNumberOfReconciliations()
  {
    return N_X(S->getRootNode(), G->getRootNode());
  }
  
  unsigned 
  EnumerateReconciliationModel::computeGammaID()
  {
    return compute_u(S->getRootNode(),G->getRootNode());
  }
  
  void 
  EnumerateReconciliationModel::setGamma(unsigned unique)
  {
    gamma.reset();
    setGamma(S->getRootNode(), G->getRootNode(), unique);
    inits();
    return;
  };
  

  //-------------------------------------------------------------------
  //
  // I/O
  //
  //-------------------------------------------------------------------
  std::string 
  EnumerateReconciliationModel::print() const
  {
    std::ostringstream oss;
    oss << "enumerateReconciliationModel: Counts number of possible\n"
	<< "reconciliations between guest tree G and host tree S.\n"
	<< "It can also assign unique ID's to reconciliations and\n"
	<< "retrieve reconciliations given such a number; Thus it can/n"
	<< "be used to enumerate reconciliations for G and S. By \n"
	<< "inheriting from ReconciledTreeModel it alos computes the\n"
	<< "probability of a reconciled tree (G, gamma), where gamma\n"
	<< "is a reonciliation of the guest tree G to a host tree S\n"
	<< indentString(ReconciliationModel::print());
    return oss.str();
  };
  
  std::string 
  EnumerateReconciliationModel::printh(Node* x) const
  {
    std::ostringstream oss;
    if(x->isLeaf() == false)
      {
	oss << printh(x->getLeftChild())
	    << printh(x->getRightChild());
      }
    oss <<  x->getNumber() << "    " << "\t";
    return oss.str();
  }
  
  std::string 
  EnumerateReconciliationModel::printu(Node* x, Node* u) const
  {
    std::ostringstream oss;
    oss << u->getNumber() << "\t" << printx(x,u) << "\n";
    if(u->isLeaf() == false)
      {
	oss << printu(x, u->getLeftChild());
	oss << printu(x, u->getRightChild());
      }      
    return oss.str();
  };
  
  std::string 
  EnumerateReconciliationModel::printx(Node* x, Node* u) const
  {
    std::ostringstream oss;
    if(x->isLeaf() == false)
      {
	oss << printx(x->getLeftChild(), u);
	oss << printx(x->getRightChild(), u);
      }
    oss <<  N_X(*x,*u) << " | " << N_V(*x,*u) << "\t";
    return oss.str();
  };      
  
  //------------------------------------------------------------------
  //
  // Implementation
  //
  //------------------------------------------------------------------
  void 
  EnumerateReconciliationModel::inits()    
  {
    ReconciledTreeModel::inits();
    compute_N(S->getRootNode(), G->getRootNode());
  };
  
  void 
  EnumerateReconciliationModel::compute_N(Node* x, Node* u)
  {
    unsigned& i = N_V(x,u);
    unsigned& j = N_X(x,u);
    
    if(u->isLeaf())
      {
	i = j = 1;
	if(x->isLeaf() == false)
	  {
	    Node* y = x->getDominatingChild(sigma[u]);
	    compute_N(y,u);
	  }
	else
	  {
	    assert(sigma[u] == x);
	    return;
	  }
      }
    else
      {
	if(sigma[u] != x)
	  {
	    Node* y = x->getDominatingChild(sigma[u]);
	    compute_N(y,u);
	  }
	
	Node* v = u->getLeftChild();
	Node* w = u->getRightChild();
	compute_N(x,v);
	compute_N(x,w);
	
	if(*sigma[u] > *x)
	  {
	    i = j = 0;
	  }
	else
	  {
	    if(slice_L(x,u) == 1)
	      {
		Node* y = x->getDominatingChild(sigma[v]);
		Node* z = x->getDominatingChild(sigma[w]);
		if(sigma[u] == x)
		  {
		    i = N_X(y,v) * N_X(z,w);
		  }
		else
		  {
		    y = x->getDominatingChild(sigma[u]);
		    i = N_X(y,u);
		  }
	      }
	    else
	      {
		i = 0;
	      }	      
	    if(isomorphy[u])
	      {
		j = (N_X(x,v) * (N_X(x,w)+1))/2 + i;
	      }
	    else
	      {
		j = N_X(x,v) * N_X(x,w) + i;
	      }
	  }
      }
    return;
  };
    
  
  unsigned 
  EnumerateReconciliationModel::compute_u(Node* x,Node* u)
  {
    assert(x!=0);
    assert(u!=0);
    // assert(gamma.isInFullGamma(x,u));
    unsigned ret = 0;
    if(gamma.isInGamma(u,x))
      {
	if(sigma[u] == x)
	  {
	    if(u->isLeaf() == false)
	      {
		Node* v = u->getLeftChild();
		Node* w = u->getRightChild();
		Node* y = x->getDominatingChild(sigma[v]);
		Node* z = x->getDominatingChild(sigma[w]);
		ret = compute_u(y,v) * N_X(z,w) + compute_u(z,w);
	      }
	  }
	else
	  {
	    Node* y = x->getDominatingChild(sigma[u]);
	    ret = compute_u(y,u);
	  }
      }
    else
      {
	Node* v = u->getLeftChild();
	Node* w = u->getRightChild();
	if(isomorphy[u])
	  {
	    unsigned left = compute_u(x,v);
	    unsigned right = compute_u(x,w);
	    
	    if(right < left)
	      {
		unsigned tmp = right;
		right = left;
		left = tmp;
	      }
	    unsigned a = 0;
	    while(a < left)
	      {
		ret += (N_X(x,w) - a);
		a++;
	      }
	    ret += right - a  + N_V(x,u);
	    //test Isaac way of doing it
	    left = N_X(x,w) - left;;
	    right++;// = N_X(x,w) - right;;
	    unsigned tmpret = (N_X(x,w)*(N_X(x,w) - 1) - left * (left - 1)
			       + 2 * right) / 2 + N_V(x,u) - 1;
	    if(ret != tmpret)
	      {
		std::ostringstream oss;
		oss << "Isaac was wrong:" << "ret = " << ret << " and tmpret = " <<tmpret << "\n";  
		throw AnError(oss.str(),1);
		}
	  }
	else
	  {
	    ret = compute_u(x,v) * N_X(x,w) + compute_u(x,w) + N_V(x,u);
	  }
	}
    return ret;
  }
  
  void 
  EnumerateReconciliationModel::setGamma(Node* x,Node* u, unsigned unique)
  {
    assert(x!=0);
    assert(u!=0);
    
    if(unique < N_V(x,u))
      {
	if(sigma[u] == x)
	  {
	    if(x->isLeaf())
	      {
		assert(unique == 0);
		assert(sigma[u] == x);
	      }
	    else
	      {
		Node* v = u->getLeftChild();
		Node* w = u->getRightChild();
		Node* y = x->getDominatingChild(sigma[v]);
		Node* z = x->getDominatingChild(sigma[w]);
		
		setGamma(y,v, unique / N_X(z,w));
		setGamma(z,w, unique % N_X(z,w));
	      }
	  }
	else
	  {
	    Node* y = x->getDominatingChild(sigma[u]);
	    setGamma(y,u,unique);
	  }
	gamma.addToSet(x,u);
      }
    else
      {
	Node* v = u->getLeftChild();
	Node* w = u->getRightChild();
	unique -= N_V(x,u);
	
	if(isomorphy[u])
	  {
	    unsigned left = 0;
	    unsigned right = unique;
	    while(N_X(x,w) <= right)
	      {
		left ++;
		right -= (N_X(x,w) - left);
	      }
	    
	    // Testing Isaacs formula
	    unsigned left2 =(1.0 + 
			     std::sqrt(1.0-8*(unique+1) + 4 * N_X(x,w) 
				       * (N_X(x,w) + 1)))/2;
	    unsigned right2 = (2 * (unique+1) - N_X(x,w) * (N_X(x,w) - 1) + 
			       left2 * (left2 - 1)) / 2;
	    
	    left2 = N_X(x,w) - left2;
	    right2--;
	    if(left != left2 || right != right2)
	      {
		std::ostringstream oss;
		  oss << "Isaac was wrong: left = " 
		      << left 
		      << ", right = " << right
		      << ", while left2 = " << left2 
		      << " and right2 = " << right2 << "\n";
		  throw AnError(oss.str(), 1);
	      }
	    setGamma(x,v, left);
	    setGamma(x,w, right);
	  }
	else
	  {	      
	    setGamma(x,v, unique / N_X(x,w));
	    setGamma(x,w, unique % N_X(x,w));
	  }
      }
    return;
  }
  







  //------------------------------------------------------------------------
  //
  // Class EnumerateLabeledReconciliationModel allows counting of number of 
  // possible reconciliations between the given gene tree and species
  // tree. It can also assign unique numbers to these reconciliations
  // and thereby allows enumerating them and, as it inherits from
  // LabeledReconciledTreeModel, to compute their probability.
  //
  // author: Bengt Sennblad 
  // copyright: The MCMC-club, SBC
  // 
  //------------------------------------------------------------------------

  //------------------------------------------------------------------
  //
  // Construct / Destruct / Assign
  //
  //------------------------------------------------------------------
  EnumerateLabeledReconciliationModel::
  EnumerateLabeledReconciliationModel(Tree &G_in, StrStrMap &gs_in,
			       BirthDeathProbs &bdp_in)
    : LabeledReconciledTreeModel(G_in, gs_in, bdp_in),
      N_V(*S,*G, 0),
      N_X(*S,*G, 0)
  {
    inits();		   // Compute important helper params
  };
  
  EnumerateLabeledReconciliationModel::
  EnumerateLabeledReconciliationModel(Tree &G_in, StrStrMap &gs_in, 
			       BirthDeathProbs &bdp_in, 
			       std::vector<SetOfNodes>& AC)
    : LabeledReconciledTreeModel(G_in, gs_in, bdp_in, AC),
      N_V(*S,*G),
      N_X(*S,*G)
  {
    inits();		   // Compute important helper params
  };

  EnumerateLabeledReconciliationModel::
  EnumerateLabeledReconciliationModel(const EnumerateLabeledReconciliationModel &M)
    : LabeledReconciledTreeModel(M),
      N_V(M.N_V),
      N_X(M.N_X)
  {
    inits();		   // Compute important helper params
  };

  EnumerateLabeledReconciliationModel::
  ~EnumerateLabeledReconciliationModel()
  {};

  EnumerateLabeledReconciliationModel&
  EnumerateLabeledReconciliationModel::operator=(const EnumerateLabeledReconciliationModel &M)
  {
    if (this != &M)
	{
	  LabeledReconciledTreeModel::operator=(M);
	  N_V = M.N_V;
	  N_X = M.N_X;
	}
    
    return *this;
  };
  
  //------------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------------
  unsigned 
  EnumerateLabeledReconciliationModel::getNumberOfReconciliations()
  {
    return N_X(S->getRootNode(), G->getRootNode());
  }
  
  unsigned 
  EnumerateLabeledReconciliationModel::computeGammaID()
  {
    return compute_u(S->getRootNode(),G->getRootNode());
  }
  
  void 
  EnumerateLabeledReconciliationModel::setGamma(unsigned unique)
  {
    gamma.reset();
    setGamma(S->getRootNode(), G->getRootNode(), unique);
    inits();
    return;
  };
  

  //-------------------------------------------------------------------
  //
  // I/O
  //
  //-------------------------------------------------------------------
  std::string 
  EnumerateLabeledReconciliationModel::print() const
  {
    std::ostringstream oss;
    oss << "enumerateReconciliationModel: Counts number of possible\n"
	<< "reconciliations between guest tree G and host tree S.\n"
	<< "It can also assign unique ID's to reconciliations and\n"
	<< "retrieve reconciliations given such a number; Thus it can/n"
	<< "be used to enumerate reconciliations for G and S. By \n"
	<< "inheriting from LabeledReconciledTreeModel it alos computes the\n"
	<< "probability of a reconciled tree (G, gamma), where gamma\n"
	<< "is a reonciliation of the guest tree G to a host tree S\n"
	<< indentString(ReconciliationModel::print());
    return oss.str();
  };
  
  std::string 
  EnumerateLabeledReconciliationModel::printh(Node* x) const
  {
    std::ostringstream oss;
    if(x->isLeaf() == false)
      {
	oss << printh(x->getLeftChild())
	    << printh(x->getRightChild());
      }
    oss <<  x->getNumber() << "    " << "\t";
    return oss.str();
  }
  
  std::string 
  EnumerateLabeledReconciliationModel::printu(Node* x, Node* u) const
  {
    std::ostringstream oss;
    oss << u->getNumber() << "\t" << printx(x,u) << "\n";
    if(u->isLeaf() == false)
      {
	oss << printu(x, u->getLeftChild());
	oss << printu(x, u->getRightChild());
      }      
    return oss.str();
  };
  
  std::string 
  EnumerateLabeledReconciliationModel::printx(Node* x, Node* u) const
  {
    std::ostringstream oss;
    if(x->isLeaf() == false)
      {
	oss << printx(x->getLeftChild(), u);
	oss << printx(x->getRightChild(), u);
      }
    oss <<  N_X(*x,*u) << " | " << N_V(*x,*u) << "\t";
    return oss.str();
  };      
  
  //------------------------------------------------------------------
  //
  // Implementation
  //
  //------------------------------------------------------------------
  void 
  EnumerateLabeledReconciliationModel::inits()    
  {
    LabeledReconciledTreeModel::inits();
    compute_N(S->getRootNode(), G->getRootNode());
  };
  
  void 
  EnumerateLabeledReconciliationModel::compute_N(Node* x, Node* u)
  {
    unsigned& i = N_V(x,u);
    unsigned& j = N_X(x,u);
    
    if(u->isLeaf())
      {
	i = j = 1;
	if(x->isLeaf() == false)
	  {
	    Node* y = x->getDominatingChild(sigma[u]);
	    compute_N(y,u);
	  }
	else
	  {
	    assert(sigma[u] == x);
	    return;
	  }
      }
    else
      {
	if(sigma[u] != x)
	  {
	    Node* y = x->getDominatingChild(sigma[u]);
	    compute_N(y,u);
	  }
	
	Node* v = u->getLeftChild();
	Node* w = u->getRightChild();
	compute_N(x,v);
	compute_N(x,w);
	
	if(*sigma[u] > *x)
	  {
	    i = j = 0;
	  }
	else
	  {
	    if(slice_L(x,u) == 1)
	      {
		Node* y = x->getDominatingChild(sigma[v]);
		Node* z = x->getDominatingChild(sigma[w]);
		if(sigma[u] == x)
		  {
		    i = N_X(y,v) * N_X(z,w);
		  }
		else
		  {
		    y = x->getDominatingChild(sigma[u]);
		    i = N_X(y,u);
		  }
	      }
	    else
	      {
		i = 0;
	      }	      
	    if(isomorphy[u])
	      {
		j = (N_X(x,v) * (N_X(x,w)+1))/2 + i;
	      }
	    else
	      {
		j = N_X(x,v) * N_X(x,w) + i;
	      }
	  }
      }
    return;
  };
    
  
  unsigned 
  EnumerateLabeledReconciliationModel::compute_u(Node* x,Node* u)
  {
    assert(x!=0);
    assert(u!=0);
    // assert(gamma.isInFullGamma(x,u));
    unsigned ret = 0;
    if(gamma.isInGamma(u,x))
      {
	if(sigma[u] == x)
	  {
	    if(u->isLeaf() == false)
	      {
		Node* v = u->getLeftChild();
		Node* w = u->getRightChild();
		Node* y = x->getDominatingChild(sigma[v]);
		Node* z = x->getDominatingChild(sigma[w]);
		ret = compute_u(y,v) * N_X(z,w) + compute_u(z,w);
	      }
	  }
	else
	  {
	    Node* y = x->getDominatingChild(sigma[u]);
	    ret = compute_u(y,u);
	  }
      }
    else
      {
	Node* v = u->getLeftChild();
	Node* w = u->getRightChild();
	if(isomorphy[u])
	  {
	    unsigned left = compute_u(x,v);
	    unsigned right = compute_u(x,w);
	    
	    if(right < left)
	      {
		unsigned tmp = right;
		right = left;
		left = tmp;
	      }
	    unsigned a = 0;
	    while(a < left)
	      {
		ret += (N_X(x,w) - a);
		a++;
	      }
	    ret += right - a  + N_V(x,u);
	    //test Isaac way of doing it
	    left = N_X(x,w) - left;;
	    right++;// = N_X(x,w) - right;;
	    unsigned tmpret = (N_X(x,w)*(N_X(x,w) - 1) - left * (left - 1)
			       + 2 * right) / 2 + N_V(x,u) - 1;
	    if(ret != tmpret)
	      {
		std::ostringstream oss;
		oss << "Isaac was wrong:" << "ret = " << ret << " and tmpret = " <<tmpret << "\n";  
		throw AnError(oss.str(),1);
		}
	  }
	else
	  {
	    ret = compute_u(x,v) * N_X(x,w) + compute_u(x,w) + N_V(x,u);
	  }
	}
    return ret;
  }
  
  void 
  EnumerateLabeledReconciliationModel::setGamma(Node* x,Node* u, unsigned unique)
  {
    assert(x!=0);
    assert(u!=0);
    
    if(unique < N_V(x,u))
      {
	if(sigma[u] == x)
	  {
	    if(x->isLeaf())
	      {
		assert(unique == 0);
		assert(sigma[u] == x);
	      }
	    else
	      {
		Node* v = u->getLeftChild();
		Node* w = u->getRightChild();
		Node* y = x->getDominatingChild(sigma[v]);
		Node* z = x->getDominatingChild(sigma[w]);
		
		setGamma(y,v, unique / N_X(z,w));
		setGamma(z,w, unique % N_X(z,w));
	      }
	  }
	else
	  {
	    Node* y = x->getDominatingChild(sigma[u]);
	    setGamma(y,u,unique);
	  }
	gamma.addToSet(x,u);
      }
    else
      {
	Node* v = u->getLeftChild();
	Node* w = u->getRightChild();
	unique -= N_V(x,u);
	
	if(isomorphy[u])
	  {
	    unsigned left = 0;
	    unsigned right = unique;
	    while(N_X(x,w) <= right)
	      {
		left ++;
		right -= (N_X(x,w) - left);
	      }
	    
	    // Testing Isaacs formula
	    unsigned left2 =(1.0 + std::sqrt(1.0 - 8 * (unique+1) + 
					     4 * N_X(x,w) * (N_X(x,w) + 1))) / 2;
	    unsigned right2 = (2 * (unique+1) - N_X(x,w) * (N_X(x,w) - 1) + 
			       left2 * (left2 - 1)) / 2;
	    
	    left2 = N_X(x,w) - left2;
	    right2--;
	    if(left != left2 || right != right2)
	      {
		std::ostringstream oss;
		  oss << "Isaac was wrong: left = " 
		      << left 
		      << ", right = " << right
		      << ", while left2 = " << left2 
		      << " and right2 = " << right2 << "\n";
		  throw AnError(oss.str(), 1);
	      }
	    setGamma(x,v, left);
	    setGamma(x,w, right);
	  }
	else
	  {	      
	    setGamma(x,v, unique / N_X(x,w));
	    setGamma(x,w, unique % N_X(x,w));
	  }
      }
    return;
  }
  
  

}//end namespace beep

