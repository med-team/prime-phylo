#include "PrimeOptionMap.hh"

#include "PrimeOption.hh"
#include "AnError.hh"

#include <algorithm>
#include <map>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <typeinfo>
//#include <curses.h>


unsigned beep::PrimeOptionMap::defIndent = 2;
unsigned beep::PrimeOptionMap::defTab = 20;
unsigned beep::PrimeOptionMap::maxLength = 80;
// unsigned dummy;
// WINDOW* w = initscr();
// int i;
// getmaxyx(w, maxLength, dummy);

namespace beep
{
  using namespace std;
  //---------------------------------------------------------------------
  //
  // Definitions for class PrimeOptionsMap
  //
  //---------------------------------------------------------------------
  // Construct/destruct/assign
  //-------------------------------
  PrimeOptionMap::PrimeOptionMap(string helperIds,
				 string unknownOptionErrorMessage)
    : helpIds(),
      usage(""),
      unknownOptionErrMsg(unknownOptionErrorMessage),
      options(),
      optionsById(),
      optionsInOrderOfIns()
  {
    // Store valid help IDs in set.
    string token;
    istringstream iss(helperIds);
    while(getline(iss, token, ','))
      {
	helpIds.insert(token);
      }
  }
  
  PrimeOptionMap::~PrimeOptionMap()
  {
    for(map<string, PrimeOption*>::iterator it = options.begin();
	it != options.end(); ++it)
      {
	PrimeOption* bo = (*it).second;
	delete bo;
      }
  }
  
  //  Interface
  //----------------------------------------------------------------

  // Add usage text
  //----------------------------------------------------------------
  void
  PrimeOptionMap::addUsageText(string progName, string parameters,
			       string usageText)
  {
    ostringstream oss;
    oss << "\n" 
	<< formatMessage("", "Usage:  " + progName + 
			 " [options] " + parameters) 
	<< "\n"
	<< formatMessage("", usageText);
    usage = oss.str();
  }

  string 
  PrimeOptionMap::getUsage() const
  {
    ostringstream oss;
    oss << usage << "\nOptions:\n";
    for(vector<PrimeOption*>::const_iterator i = optionsInOrderOfIns.begin();
	i != optionsInOrderOfIns.end(); i++)
      {
	oss << (*i)->getUsage();
      }
    return oss.str();
  }


  // Adds an option of various types.
  //----------------------------------------------------------------
  void 
  PrimeOptionMap::addBoolOption(std::string name, string id, 
				unsigned nParameters, 
				std::string defaultValues, 
				std::string validValues,
				std::string helpMessage)
  {
    throw AnError("BoolOption is currently not safe -- use IntOPtion",1);
    addOption(name, id, *new BoolOption(id, helpMessage, nParameters, 
					defaultValues, validValues));
  }

  void 
  PrimeOptionMap::addIntOption(std::string name, string id, 
			       unsigned nParameters,
			       std::string defaultValues,
			       std::string validValues,
			       std::string helpMessage)
  {
    addOption(name, id, *new IntOption(id, helpMessage, nParameters,
				   defaultValues, validValues));
  }
  
  void 
  PrimeOptionMap::addUnsignedOption(std::string name, string id, 
				    unsigned nParameters, 
				    std::string defaultValues,
				    std::string validValues,
				    std::string helpMessage)
  {
    addOption(name, id, *new UnsignedOption(id, helpMessage, nParameters, 
					defaultValues, validValues));
  }

  void 
  PrimeOptionMap::addRealOption(std::string name, string id,
				unsigned nParameters, 
				std::string defaultValues,
				std::string validValues,
				std::string helpMessage)
  {
    addOption(name, id, *new RealOption(id, helpMessage, nParameters, 
				    defaultValues, validValues));
  }

  void 
  PrimeOptionMap::addStringOption(std::string name, string id, 
				  unsigned nParameters, 
				  std::string defaultValues,
				  std::string validValues,
				  std::string helpMessage)
  {
    addOption(name, id, *new StringOption(id, helpMessage, nParameters, 
					  defaultValues, validValues));
  }
  
  void 
  PrimeOptionMap::addUserSubstMatrixOption(std::string name, string id, 
					   unsigned nParameters, 
					   std::string defaultValues,
					   std::string validValues,
					   std::string helpMessage)
  {
    addOption(name, id, 
	      *new UserSubstitutionMatrixOption(id, helpMessage, 
						nParameters, 
						defaultValues, 
						validValues));
  }


  // Returns the value of an option.
  //------------------------------------------------------------------------
  vector<bool> 
  PrimeOptionMap::getBool(std::string name)
  {
    PrimeOption& bo = getOption(name);
    bool tmp;
    if (bo.getType() != typeid2typestring(typeid(tmp).name()))
      { 
	throw AnError("Wrong option type for " + name + ", should be " 
		      + typeid2typestring(typeid(tmp).name())); 
      }
    return static_cast<BoolOption&>(bo).getParameters();
  }
  
  vector<int> 
  PrimeOptionMap::getInt(std::string name)
  {
    PrimeOption& bo = getOption(name);
    int tmp;
    if (bo.getType() != typeid2typestring(typeid(tmp).name()))
      { 
	throw AnError("Wrong option type for " + name + ", " 
		      + bo.getType() + ", should be " 
		      + typeid2typestring(typeid(tmp).name())); 
      }      
    return static_cast<IntOption&>(bo).getParameters();
  }
  
  vector<unsigned> 
  PrimeOptionMap::getUnsigned(std::string name)
  {
    PrimeOption& bo = getOption(name);
    if (bo.getType() != "unsigned int") // How portable is this?
      { 
	throw AnError("Wrong option type for " + name +
		      ", should be " + bo.getType()); 
      }
    return static_cast<UnsignedOption&>(bo).getParameters();
  }
  
  vector<Real> 
  PrimeOptionMap::getReal(std::string name)
  {
    PrimeOption& bo = getOption(name);
    if (bo.getType() != "double") // How portable is this?
      { 
	throw AnError("Wrong option type for " + name + ", should be double"); 
      }
    return static_cast<RealOption&>(bo).getParameters();
  }
  
  vector<std::string> 
  PrimeOptionMap::getString(std::string name)
  {
    PrimeOption& bo = getOption(name);
    if (bo.getType() != "std::string")// How portable is this?
      { 
	throw AnError("Wrong option type for " + name + ", should be string"); 
      }
    return static_cast<StringOption&>(bo).getParameters();
  }
  
  vector<UserSubstMatrixParams>
  PrimeOptionMap::getUserSubstitutionMatrix(std::string name)
  {
    PrimeOption& bo = getOption(name);
    if (bo.getType() != "SubstMatrix")
      { 
	throw AnError("Wrong option type for " + name + 
		      ", should be SubstMatrix."); 
      }
    return static_cast<UserSubstitutionMatrixOption&>(bo).getParameters();
  }

  // Parses options from user input as long as correct options 
  // starting with character '-' can be found. 
  //-----------------------------------------------------------
  bool 
  PrimeOptionMap::parseOptions(int& argIndex, int argc, char **argv)
  {
    if(argIndex >= argc)
      {
	return false;
      }
    while(argIndex < argc && argv[argIndex][0] == '-') 
      {

	string id = string(argv[argIndex++]).erase(0, 1);
	// Help option encountered, ignore by returning false.
	if(helpIds.find(id) != helpIds.end())	
	  { 
	    return false; 
	  }
	// Option not recognized.
	if(id == "" || optionsById.find(id) == optionsById.end())
	  {
	    throw AnError(unknownOptionErrMsg + " -" + id + '!', 1);
	  } 
	
	PrimeOption* bo = optionsById[id];
	ostringstream oss;
	if(bo->numberOfParameters() != MAXPARAMS)
	  {
	    for(unsigned i = 0; i < bo->numberOfParameters(); i++)
	      {
		capitalize(argv[argIndex]);
		oss << "  " << argv[argIndex++];
	      }
	  }
	else
	  {
	    capitalize(argv[argIndex]);
	    do
	      {
		string s = argv[argIndex];
		oss << s.substr(0,s.find(',')) << " ";
// 		oss << argv[argIndex++] << " ";
	      }
	    while(*--string(argv[argIndex++]).end() == ',');
	  }
// 	argIndex++;
	bo->setParameters(oss.str());
      }
    return true;
  }  
  
  ostream& operator<<(ostream& o, const PrimeOption &po)
  {
    ostringstream oss;
    oss << po.getUsage();
    return o << oss.str();    
  }

  ostream& operator<<(ostream& o, const PrimeOptionMap &pom)
  {
    ostringstream oss;
    oss << pom.getUsage();
    vector<PrimeOption*>::const_iterator it;
    for(it = pom.optionsInOrderOfIns.begin(); 
	it != pom.optionsInOrderOfIns.end(); ++it)
      {
	oss << *(*it);
      }
    oss << "\n";
    return o << oss.str();    
  }

  // Implementation
  //-----------------------
  void 
  PrimeOptionMap::addOption(string name, string id, PrimeOption& option)
  {
    options[name] = &option;
    optionsById[id] = &option;
    optionsInOrderOfIns.push_back(&option);
  }

  PrimeOption&
  PrimeOptionMap::getOption(std::string name)
  {
    if (options.find(name) == optionsById.end())
      { 
	throw AnError(unknownOptionErrMsg, 1); 
      }
    return *options[name];
  }

  PrimeOption& 
  PrimeOptionMap::PrimeOptionMap::getOptionById(string id)
  {
    if (optionsById.find(id) == optionsById.end())
      { 
	throw AnError(unknownOptionErrMsg, 1); 
      }
    return (*optionsById[id]);
  }
  

  bool 
  PrimeOptionMap::PrimeOptionMap::hasBeenParsed(string name)
  {
    return (options[name]->hasBeenParsed());
  }

  string 
  PrimeOptionMap::formatMessage(string opt, string expl)
  {
    unsigned indent = 0;
    unsigned tab = 0;
    if(opt != "")
      {
	indent = defIndent;
	tab = defTab;
      }
    std::ostringstream oss;
    oss << std::string(indent,' ') ;
    unsigned i = 0;
    unsigned j = 0;
    while(j < (unsigned)opt.size())
      {
	if(j+maxLength-indent > opt.size())
	  {
	    j = opt.size();
	    oss << opt.substr(i,j-i);
	  }
	else
	  {
	    j = opt.rfind(" ", j + maxLength - indent) + 1;
	    oss << opt.substr(i,j-i)
		<< "\n" << std::string(indent+4, ' ');
	  }
	i=j;
      }
    
    if(opt.size() > tab - indent)
      {
	oss << "\n"
	    << std::string(tab, ' ');
      }
    else
      oss << std::string(tab-indent-opt.size(), ' ');
    
    i = 0;
    j = 0;
    while(j < (unsigned)expl.size())
      {
	if(j+maxLength-tab > expl.size())
	  {
	    j = expl.size();
	    oss << expl.substr(i,j-i);// << std::string(tab, ' ');
	  }
	else
	  {
	    j = expl.rfind(" ", j + maxLength - tab) + 1;
	    oss << expl.substr(i,j-i) << "\n" << std::string(tab, ' ');
	  }
	i=j;
      }
    oss << "\n";
    return oss.str();
  }
  
  
} // end namespace beep.

