#ifndef _SIMPLEOBSERVER_HH
#define	_SIMPLEOBSERVER_HH

#include <iostream>
#include <fstream>
#include "AnError.hh"
#include "Probability.hh"
#include "IterationObserver.hh"




namespace beep {

  class MCMCModel;

  class SimpleObserver : public IterationObserver {
  public: 
    SimpleObserver(unsigned int iter_total ) : IterationObserver( iter_total )  { start_time = time(NULL); notRunYet = true;
 };
virtual    ~SimpleObserver(){};
    virtual bool afterEachStep(MCMCModel & model, unsigned int iterationsPerformed, bool stateWasChanged, std::string & stdoutStr, std::string & stderrStr );
    virtual void setOutputFile(const char *filename);//, char *header=NULL);
    virtual void setThinning(unsigned i);
    virtual void setPrintFactor(unsigned i);
    virtual bool setShowDiagnostics(bool yes_no);	

    Probability getLocalOptimum();
    std::string getBestState();

    std::string estimateTimeLeft(unsigned iteration, unsigned when_done);

    //-------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------

    /*
    friend std::ostream& operator<<(std::ostream &o, 
 			    const SimpleMCMC& A);
    virtual std::string print() const;
    */

  protected:
    unsigned iteration;	       // Output current state everytime this 
                               //counter is zero.
    unsigned thinning;	       // iteration is incremented modulo thinning 
                               //(i.e., iteration < thinning)
    unsigned print_factor;	   
                             
    std::ofstream os;	       // Stream to write output to.
    std::streambuf *cout_buf;  // Save old cout buffer here if we write to file
    bool localOptimumFound;    // Best likelihood value found in this iteration
    bool show_diagnostics;     // ...likelihood to cerr or not
    unsigned start_time;	// Keep track of how long we've beeen computing
    Probability localOptimum;  // Best likelihood value 
    std::string bestState;     // String representation for best state 
    bool notRunYet;            // Indicate if anything has been observed yet, i.e. if afterEachStep() has been run yet
  };
}

#endif	/* _SIMPLEOBSERVER_HH */


