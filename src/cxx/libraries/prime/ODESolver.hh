#ifndef ODESOLVER_HH
#define ODESOLVER_HH

#include <vector>

#include "AnError.hh"
#include "Beep.hh"

namespace beep
{

/**
 * Class for Runge-Kutta numerical solution of a system of ordinary differential
 * equations (ODEs) of the first order, y' = f(x,y) with initial value y_0 = y(x_0).
 * Uses coefficients due to Dormand-Prince of order 4 for y_1 and order 5 for error
 * estimator ŷ_1 (where order refers to number of coinciding terms in Taylor series
 * for estimate and exact solution). Uses automatic step size control.
 * 
 * The outline of this solver stems from "Solving Ordinary Differential Equations I" by
 * Hairer, Nörsett and Wanner, and the code is an adopted version of the author's old
 * Fortran code DOPRI5(). As such, it pretty much uses the same short variable names and
 * outline, but a slightly different invokation style.
 * 
 * The user creates a subclass of this class where the virtual evaluation function f(x,y)
 * is defined. Scalar tolerances are specified in the constructor. One may change various
 * default settings if required. Actual integration is made by calling dopri5().
 * If the initial step size is set to 0, the solver will make a qualified guess.
 * 
 * Additionally, one has the ability to define a callback function ("external solution
 * provider") solout which will be invoked after every accepted solver iteration.
 * This function can, if desired, alter the solution before next step. It may also call the
 * convenience function contd5() to get an interpolated estimate of the solution
 * between the old and current x-values, e.g. for fine-coarsed output.
 * However, such estimates are only available if a flag has been set to make
 * interpolation coefficients computed.
 * 
 * In essence, use like this:
 * <ul>
 * <li>Create subclass of ODESolver and pass in scalar
 *     tolerances etc. to superclass constructor.
 * <li>If required, change default settings.
 * <li>Override (i.e. implement) evaluation method fcn().
 * <li>If required, override external solution provider solout().
 * <li>...
 * <li>Solve one or multiple times by calling dopri5().
 * </ul>
 * 
 * @author Joel Sjöstrand.
 */
class ODESolver
{
	
public:

	/** Return values from solver function dopri5(). */
	enum SolverResult
	{
		SUCCESSFUL              =  1,  /*< Normal exit. */
		SUCCESSFUL_INTERRUPTED  =  2,  /*< External solution provider requested interrupt. */
		INCONSISTENT_INPUT      = -1,  /*< Aborted. Invalid input to solver. */ 
		INSUFFICIENT_NMAX       = -2,  /*< Aborted. Specified max no. of steps were insufficient. */ 
		TOO_SMALL_GEN_STEP_SIZE = -3,  /*< Aborted, Step size became to small. */
		PROBABLY_STIFF          = -4   /*< Aborted. Evaluation function was deemed stiff. */
	};
	
	/** Return values from external solution provider solout(). */
	enum ExtSolResult
	{
		INTERRUPT_SOLVER     = -1,  /*< External provider returned interrupt request. */
		NOT_INVOKED          =  0,  /*< External provider was not called. */
		SOLUTION_NOT_CHANGED =  1,  /*< External provider returned without changing solution. */
		SOLUTION_CHANGED     =  2   /*< External provider returned with altered solution. */
	};
	
	/**
	 * Constructor.
	 * @param rtol the relative tolerance.
	 * @param atol the absolute tolerance.
	 * @param hasSolout set to true if subclass overrides solout().
	 * @param doDense set to true if solout() calls contd5() to get interpolated values.
	 */
	ODESolver(Real rtol, Real atol, bool hasSolout=false, bool doDense=false);
	
	/**
	 * Destructor.
	 */
	virtual ~ODESolver();
	
	/**
	 * Solver. Numerically seeks the solution of an ODE y' = f(x,y) using Runge-Kutta with
	 * Dormand-Prince parameters given the initial values. Utilizes auto-step size control.
	 * Set the initial step size to zero to let the solver make a suggestion based on an Euler step.
	 * @param x the initial value of x. Updated during solving.
	 * @param xend the value of x where the solution is sought.
	 * @param y the initial value of y. Updated during solving, and containing the solution
	 * after successful return.
	 * @param h the initial step size. Udated during solving. Set to 0 for auto-estimation.
	 * @param rtol per-component relative tolerances. Set to override scalar value.
	 * @param atol per-component absolute tolerances. Set to override scalar value.
	 * @return a code stating solver's success or failure.
	 */
	SolverResult dopri5(
			Real& x,
			Real xend,
			std::vector<Real>& y,
			Real& h,
			const std::vector<Real>* rtol = NULL,
			const std::vector<Real>* atol = NULL
			);
	
	/**
	 * Returns various counters from last call to dopri5().
	 * @param nfcn number of function evaluations.
	 * @param nstep number of solver iterations (accepted or rejected steps).
	 * @param naccpt number of accepted steps.
	 * @param nrejct number of rejected steps (rejections in first step excluded).
	 */
	void getStatistics(unsigned& nfcn, unsigned& nstep, unsigned& naccpt, unsigned& nrejct) const;
	
	/**
	 * Gets the relative tolerance.
	 * @return the relative tolerance. 
	 */
	Real getRelativeTolerance() const;

	/**
	 * Gets the absolute tolerance.
	 * @return the absolute tolerance. 
	 */
	Real getAbsoluteTolerance() const;
	
	/**
	 * Sets the relative and absolute tolerances.
	 * @param rtol the relative tolerance.
	 * @param atol the absolute tolerance.
	 */
	void setTolerance(Real rtol, Real atol);
	
	/**
	 * Gets the external solution provider flag.
	 * @return true if solver invokes external solution provider
	 * after accepted solver steps.
	 */
	bool getHasSolout() const;
	
	/**
	 * Sets flag indicating if external solution provider should
	 * be invoked.
	 * @param hasSolout true to make solver invoke solout() after
	 *        accepted solver steps.
	 */
	void setHasSolout(bool hasSolout);
	
	/**
	 * Gets the dense output flag.
	 * @return true if calculating coefficients for dense output. 
	 */
	bool getDenseOuput() const;
	
	/**
	 * Sets the dense output flag. Setting to true only has an effect if
	 * hasSolout flag is true.
	 * @param doDense true to make solver compute dense output
	 *        coefficients.
	 */
	void setDenseOutput(bool doDense);
	
	/**
	 * Returns the maximum number of step the solver will make before aborting.
	 * @return the number of steps. Defaults to 100000.
	 */
	unsigned getMaxNoOfSteps() const;
	
	/**
	 * Sets the maximum number of step the solver is allowed to make before aborting.
	 * @param maxNoOfSteps the limit.
	 */
	void setMaxNoOfSteps(unsigned maxNoOfSteps);
	
	/**
	 * Returns the stiffness detection factor, i.e. every i-th iteration a test is made.
	 * Value 0 means no tests at all. Defaults to 1000.
	 * @return the factor.
	 */
	unsigned getStiffDetectFactor() const;
	
	/**
	 * Sets the stiffness detection factor. A sample test is made every i-th iteration.
	 * Set to 0 for no tests at all.
	 * @param factor the number of iterations between samples.
	 */
	void setStiffDetectFactor(unsigned factor);
	
	/**
	 * Returns the rounding unit: smallest number u so that 1.0 + u > 1.0.
	 * Defaults to 2.3e-16.
	 * @return the unit.
	 */
	Real getRoundingUnit() const;
	
	/**
	 * Sets the rounding unit. Must be in range (1e-35, 1).
	 * @param the unit.
	 */
	void setRoundingUnit(Real roundingUnit);
	
	/**
	 * Returns the safety factor for step size prediction. Defaults to 0.9.
	 * @return the factor.
	 */
	Real getSafetyFactor() const;
	
	/**
	 * Sets the safety factor for step size prediction. Must be in range (1e-4, 1).
	 * @param the factor.
	 */
	void setSafetyFactor(Real factor);
	
	/**
	 * Returns the parameters for step size selection. The new step size is chosen
	 * subject to param1 <= hnew/hold <= param2. Defaults to 0.2 and 10.0.
	 * @param the first size selection parameter.
	 * @param the second size selection parameter.
	 */
	void getStepSizeParams(Real& param1, Real& param2) const;

	/**
	 * Sets the parameters for step size selection.
	 * @param the first size selection parameter.
	 * @param the second size selection parameter.
	 */
	void setStepSizeParams(Real param1, Real param2);
	
	/**
	 * Returns the step size control stabilization parameter.
	 * Larger values (<= 0.1) make the step size control more stable.
	 * Defaults to 0.04.
	 * @return the stabilization parameter.
	 */
	Real getStepSizeStabilizationParam() const;

	/**
	 * Sets the step size control stabilization parameter. Must be in range [0, 0.2].
	 * @param beta the stabilization parameter.
	 */
	void setStepSizeStabilizationParam(Real beta);
	
	/**
	 * Returns the maximum step size. 0 means not specified and leads to xend - x.
	 */
	Real getMaxStepSize() const;

	/**
	 * Sets the maximum allowed step size. Sign does not matter. Set to 0 for
	 * unspecified, in which case xend - x is used.
	 */
	void setMaxStepSize(Real maxStepSize);
	
protected:
	
	/**
	 * The evaluation function y' = f(x,y).
	 * @param x the x value.
	 * @param y the y values.
	 * @param f the derivatives y'=f(x,y) the method computes.
	 */
	virtual void fcn(Real x, const std::vector<Real>& y, std::vector<Real>& f) = 0;
	
	/**
	 * External callback function which will be invoked after each (accepted) solver step.
	 * This function may change the solution, in which case it should make use of the
	 * corresponding return code. Additionally, it may call the method contd5() if it
	 * seeks an interpolated solution for a value in the range [xold,x] (albeit only if
	 * the hasDense flag has been set to true).
	 * Note: Make sure to set the flag hasSolout to true if overriding this method.
	 * @param no the current iteration number (accepted steps).
	 * @param xold the previous x-value of the solver.
	 * @param x the current x-value of the solver.
	 * @param y the current solution at x.
	 * @param icomp list of component indiced for which there are continuous output.
	 * @return a suitable return code.
	 */
	virtual ExtSolResult solout(unsigned no, Real xold, Real x, std::vector<Real>& y)
	{
		return NOT_INVOKED;
	};
	
	/**
	 * Used for e.g. continuous output in connection with an external solution provider.
	 * After a solver iteration has finished and the external provider is called(back),
	 * the latter may invoke this method to get an approximation of a component of the
	 * solution at a specified value in the range [xold,x]. This is only possible if there
	 * is dense output (see hasDense flag).
	 * @param i the index of the component of the solution.
	 * @param xCont the value in the current range [xold,x] for which the solution
	 *        component is sought.
	 * @return an interpolation of the i-th component of the solution at xCont.
	 */
	Real contd5(unsigned i, Real xCont);
	
	/**
	 * Works similarly to contd5(unsigned i, Real xCont), but returns
	 * all components.
	 * @param yIpl a vector where the interpolated solution will be stored.
	 * @param xCont the value in the current range [xold,x] for which the solution
	 *        is sought. 
	 */
	void contd5(std::vector<Real>& yIpl, Real xCont);
	
	/**
     * Flag indicating if external solution provider should be called.
     */
    bool m_hasSolout;
    
    /**
	 * Flag indicating if to compute dense output.
	 */
	 bool m_doDense;
    
	/**
	 * Storage for dense output, concat. for all 5 coefficients.
	 */
	std::vector<Real> m_cont;
	
private:
	
	/**
	 * Called by dopri5() before solving. Performs trivial initialization tasks.
	 */
	void initialize();
	
	/**
	 * Computes an initial step size guess using
	 * "explicit Euler" according to h = 0.01 * |y_0| / |f_0|.
	 * The increment for explicit Euler is small compared to the solution.
	 * @param x the initial x.
	 * @param y the initial y.
	 * @param posneg integration direction: -1 if x>xend, else 1.
	 * @param hmax the absolute value of the maximum step size.
	 * @param rtol per-component relative tolerances.
	 * @param atol per-component absolute tolerances.
	 * @return the suggested initial step size (with sign). 
	 */
	Real hinit(
			const Real& x,
			const std::vector<Real>& y,
			const int& posneg,
			const Real& hmax,
			const std::vector<Real>* rtol,
			const std::vector<Real>* atol
			);
	
public:
	
	/** Fifth-order Runge-Kutta method. */
	static const unsigned IORD = 5;
	
	/** Dormand-Prince parameters. */
	static const Real C2 =       1.0 /      5.0;
	static const Real C3 =       3.0 /     10.0;
	static const Real C4 =       4.0 /      5.0;
	static const Real C5 =       8.0 /      9.0;
	static const Real A21 =      1.0 /      5.0;
	static const Real A31 =      3.0 /     40.0;
	static const Real A32 =      9.0 /     40.0;
	static const Real A41 =     44.0 /     45.0;
	static const Real A42 =    -56.0 /     15.0;
	static const Real A43 =     32.0 /      9.0;
	static const Real A51 =  19372.0 /   6561.0;
    static const Real A52 = -25360.0 /   2187.0;
    static const Real A53 =  64448.0 /   6561.0;
    static const Real A54 =   -212.0 /    729.0;
    static const Real A61 =   9017.0 /   3168.0;
    static const Real A62 =   -355.0 /     33.0;
    static const Real A63 =  46732.0 /   5247.0;
    static const Real A64 =     49.0 /    176.0;
    static const Real A65 =  -5103.0 /  18656.0;
    static const Real A71 =     35.0 /    384.0;
    static const Real A73 =    500.0 /   1113.0;
    static const Real A74 =    125.0 /    192.0;
    static const Real A75 =  -2187.0 /   6784.0;
    static const Real A76 =     11.0 /     84.0;
    static const Real E1 =      71.0 /  57600.0;
    static const Real E3 =     -71.0 /  16695.0;
    static const Real E4 =      71.0 /   1920.0;
    static const Real E5 =  -17253.0 / 339200.0;
    static const Real E6 =      22.0 /    525.0;
    static const Real E7 =      -1.0 /     40.0;

    /** Dense-output parameters of Shampine (1986). */
    static const Real D1 =  -12715105075.0 /  11282082432.0;
    static const Real D3 =   87487479700.0 /  32700410799.0;
    static const Real D4 =  -10690763975.0 /   1880347072.0;
    static const Real D5 =  701980252875.0 / 199316789632.0;
    static const Real D6 =   -1453857185.0 /    822651844.0;
    static const Real D7 =      69997945.0 /     29380423.0;
    
private:

	Real m_rtol;            /**< Scalar relative tolerance. */
	Real m_atol;            /**< Scalar absolute tolerance. */
	unsigned m_nmax;        /**< Maximum number of allowed steps. */
	unsigned m_nstiff;      /**< Stiffness detection every i-th step. */
	Real m_uround;          /**< Rounding unit, smallest number satisfying 1 + m_uround > 1. */
	Real m_safe;            /**< Safety factor. */
	Real m_fac1;            /**< Step size selection parameter. */
	Real m_fac2;            /**< Step size selection parameter. */
	Real m_beta;            /**< Step size stabilization factor. */
	Real m_hmax;            /**< Maximum step size. Always positive. */
	
	unsigned m_nfcn;		/**< Number of function evaluations of last run. */
	unsigned m_nstep;		/**< Number of computed steps of last run. */
	unsigned m_naccpt;		/**< Number of accepted steps of last run. */
	unsigned m_nrejct;		/**< Number of rejected steps of last run (rejections in first step excluded). */
		
	unsigned m_n;               /**< Work var.: System size (no of comps.). */
	Real m_xold;				/**< Work var.: x-value of last iteration. */
	Real m_hout;				/**< Work var.: Step size of last iteration. */
	std::vector<Real> m_y1;		/**< Work var.: Main storage for integration. */
	std::vector<Real> m_k1;		/**< Work var.: Main storage for integration. */
	std::vector<Real> m_k2;		/**< Work var.: Main storage for integration. */
	std::vector<Real> m_k3;		/**< Work var.: Main storage for integration. */
	std::vector<Real> m_k4;		/**< Work var.: Main storage for integration. */
	std::vector<Real> m_k5;		/**< Work var.: Main storage for integration. */
	std::vector<Real> m_k6;		/**< Work var.: Main storage for integration. */
	std::vector<Real> m_ysti;	/**< Work var.: Main storage for integration. */
};

} // end namespace beep
	
#endif /*ODESOLVER_HH*/
