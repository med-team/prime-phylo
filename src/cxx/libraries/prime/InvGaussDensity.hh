#ifndef INVGAUSSDENSITY_HH
#define INVGAUSSDENSITY_HH

#include "Density2P_positive.hh"

namespace beep
{

  //----------------------------------------------------------------------
  //
  // Class InvGaussDensity 
  //! implements the inverse Gaussian density distribution.
  //!
  //! \f[
  //!  f(x) = 
  //!   \begin{cases}
  //!    \frac{e^{-\frac{(x-\alpha)^2}{2\beta x\alpha^2}}}
  //!         {\sqrt{2\pi\beta x^3}} 
  //!     & x>0\\   0 & \mbox{else},
  //!   \end{cases}
  //! \f]
  //! where \f$ \alpha = \mbox{mean} \f$ and 
  //! \f$ \beta = \frac{\mbox{variance}}{\alpha^3} \f$
  //!
  //! Note! Conventional notation often uses \f$ \mu = \alpha \f$ and 
  //! \f$ \lambda = 1/ \beta \f$.
  //!
  //! Invariants: variance > 0
  //!             mean     > 0
  //!
  //! Author: Martin Linder, copyright the MCMC-club, SBC
  //
  //----------------------------------------------------------------------

  class InvGaussDensity : public Density2P_positive
  {
  public:
    //----------------------------------------------------------------------
    //
    // Construct/Destruct
    //
    //----------------------------------------------------------------------
    InvGaussDensity(Real mean, Real variance, bool embedded = false);
    InvGaussDensity(const InvGaussDensity& df);
    ~InvGaussDensity();
    InvGaussDensity& operator=(const InvGaussDensity& df);
    
    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
    // Density-, distribution-, and sampling functions
    //----------------------------------------------------------------------
    Probability operator()(const Real& x) const;
    //! Not functional.  
    //! I have not found any formula for the inverse function of F(x) 
    // TODO: Find a formular/approximation for F^{-1}(p)/bens
    //------------------------------------------------------------------
    Real sampleValue(const Real& p) const;
    //! returns true if \f$ f(x)\neq 0 \Leftrightarrow  x > 0 \f$
    //  i.e., if x is in interval

    // Access parameters
    //---------------------------------------------------------------------- 
    Real getMean() const;
    Real getVariance() const;

    // Set Parameters
    //----------------------------------------------------------------------
    void setParameters(const Real& mean, const Real& variance);    
    void setMean(const Real& mean);    
    void setVariance(const Real& variance);
    
    //------------------------------------------------------------------
    //
    // I/O
    //
    //------------------------------------------------------------------

    virtual std::string print() const;

  private:
    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------
    static const double pi = 3.14159265358979; //!< math.constant \f$\pi\f$
    
    //The attribute c is a constant term, given the parameter 
    //values, in the log density function.
    //---------------------------------------------------------
    Real c; //!< \f$ c = \frac{1}{\sqrt{2\pi\beta}} \f$

  };

}//end namespace beep
#endif
