#ifndef CALCULATIONEXCEPTION_HH
#define CALCULATIONEXCEPTION_HH

#include <iostream>
#include <string>

static int n_exceptions = 0;


//! This class is used for very unlikely errors during calculations,
//! especially in ReconciliationTimeSampler. These exceptions are
//! thrown when we scenarios that will have probability zero. 
//! The class is not intended for error reporting, and also not used
//! for cases when we know there is a bug!
//!
//! To be able to diagnose for conditions that are bugs, we do some accounting
//! too.
//! \todo{Move this class inside namespace beep?
class CalculationException 
{
public:
  CalculationException() {};
  ~CalculationException() {};
  
  static void registerCall() { n_exceptions++;};
  
  static void statistics() {
#ifndef NDEBUG
    std::cout << "# Number of times CalculationException was used: " << n_exceptions << std::endl;
#endif
  }
};
#endif
