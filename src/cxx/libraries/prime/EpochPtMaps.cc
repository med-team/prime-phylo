#include <cassert>
#include <cmath>

#include "AnError.hh"
#include "EpochPtMaps.hh"

namespace beep
{

using namespace std;


template<typename T>
EpochPtMap<T>::EpochPtMap(const EpochTree& ES, const T& defaultVal) :
	m_ES(ES),
	m_offsets(),
	m_vals(),
	m_cache(),
	m_cacheIsValid(false)
{
	// Compute offsets.
	m_offsets.reserve(ES.getNoOfEpochs() + 1);
	m_offsets.push_back(0);
	for (EpochTree::const_iterator it = ES.begin(); it != ES.end(); ++it)
	{
		m_offsets.push_back(m_offsets.back() + (*it).getNoOfTimes());
	}
	
	// Create all default values.
	m_vals.reserve(m_offsets.back());
	for (EpochTree::const_iterator it = ES.begin(); it != ES.end(); ++it)
	{
		unsigned noOfTms = (*it).getNoOfTimes();
		unsigned noOfEdges = (*it).getNoOfEdges();
		for (unsigned j = 0; j < noOfTms; ++j)
		{
			m_vals.push_back(vector<T>(noOfEdges, defaultVal));
		}
	}
}


template<typename T>
EpochPtMap<T>::EpochPtMap(const EpochPtMap<T>& ptMap) :
	m_ES(ptMap.m_ES),
	m_offsets(ptMap.m_offsets),
	m_vals(ptMap.m_vals),
	m_cache(),
	m_cacheIsValid(false)
{
}


template<typename T>
EpochPtMap<T>::~EpochPtMap()
{
}


template<typename T>
EpochPtMap<T>& EpochPtMap<T>::operator=(const EpochPtMap<T>& ptMap)
{
	if (&m_ES != &(ptMap.m_ES))
		throw AnError("Cannot assign EpochPtMap=EpochPtMap when based on different tree instances.", 1);
	if (this != &ptMap)
	{
		m_offsets = ptMap.m_offsets;
		m_vals = ptMap.m_vals;
		m_cache.clear();
		m_cacheIsValid = false;
	}
	return *this;
}


template<typename T>
void EpochPtMap<T>::reset(const T& defaultVal)
{
	for (unsigned i = 0; i < m_vals.size(); ++i)
	{
		m_vals[i].assign(m_vals[i].size(), defaultVal);
	}
}


template<typename T>
void EpochPtMap<T>::cache()
{
	m_cache.assign(m_vals.begin(), m_vals.end());
	m_cacheIsValid = true;
}


template<typename T>
void EpochPtMap<T>::restoreCache()
{
	if (m_cacheIsValid)
	{
		m_vals.assign(m_cache.begin(), m_cache.end());
		m_cacheIsValid = false;
	}
}


template<typename T>
void EpochPtMap<T>::invalidateCache()
{
	m_cacheIsValid = false;
}


template<typename T>
string EpochPtMap<T>::print() const
{
	ostringstream oss;
	for (unsigned epi = m_ES.getNoOfEpochs(); epi > 0; --epi)
	{
		const EpochPtSet& ep = m_ES[epi-1];
		for (unsigned tm = ep.getNoOfTimes(); tm > 0; --tm)
		{
			oss << "# (" << epi-1 << '.' << tm-1 << "): ";
			const vector<T>& v = m_vals[m_offsets[epi-1] + tm-1];
			for (unsigned e = 0; e < v.size(); ++e)
			{
				oss << v[e] << ' ';
			}
			oss << endl;
		}
	}
	return oss.str();
}


// Excplicit specialization for class Probability.
template<>
string EpochPtMap<Probability>::print() const
{
	ostringstream oss;
	for (unsigned epi = m_ES.getNoOfEpochs(); epi > 0; --epi)
	{
		const EpochPtSet& ep = m_ES[epi-1];
		for (unsigned tm = ep.getNoOfTimes(); tm > 0; --tm)
		{
			oss << "# (" << epi-1 << '.' << tm-1 << "): ";
			const vector<Probability>& v = m_vals[m_offsets[epi-1] + tm-1];
			for (unsigned e = 0; e < v.size(); ++e)
			{
				oss << v[e].val() << ' ';
			}
			oss << endl;
		}
	}
	return oss.str();
}


///////////////////////// EpochPtPtMap ////////////////////////


template<typename T>
EpochPtPtMap<T>::EpochPtPtMap(const EpochTree& ES, const T& defaultVal) :
	m_ES(ES),
	m_offsets(),
	m_vals(1, 1),
	m_cache(1,1),
	m_cacheIsValid(false)
{
	// Compute offsets.
	m_offsets.reserve(ES.getNoOfEpochs() + 1);
	m_offsets.push_back(0);
	for (EpochTree::const_iterator it = ES.begin(); it != ES.end(); ++it)
	{
		m_offsets.push_back(m_offsets.back() + (*it).getNoOfTimes());
	}
	
	// Create and fill value matrix.
	m_vals = GenericMatrix< vector<T> >(m_offsets.back(), m_offsets.back());
	for (unsigned i = 0; i < ES.getNoOfEpochs(); ++i)
	{
		const EpochPtSet& iep = ES[i];
		unsigned wdi = iep.getNoOfEdges();
		for (unsigned s = 0; s < iep.getNoOfTimes(); ++s)
		{
			for (unsigned j = 0; j < ES.getNoOfEpochs(); ++j)
			{
				const EpochPtSet& jep = ES[j];
				unsigned wdj = jep.getNoOfEdges();
				for (unsigned t = 0; t < jep.getNoOfTimes(); ++t)
				{
					m_vals(m_offsets[i]+s, m_offsets[j]+t).assign(wdi*wdj, defaultVal);
				}
			}
		}
	}
}


template<typename T>
EpochPtPtMap<T>::EpochPtPtMap(const EpochPtPtMap<T>& ptPtMap) :
	m_ES(ptPtMap.m_ES),
	m_offsets(ptPtMap.m_offsets),
	m_vals(ptPtMap.m_vals),
	m_cache(GenericMatrix< vector<T> >(1, 1)),
	m_cacheIsValid(false)
{
}


template<typename T>
EpochPtPtMap<T>::~EpochPtPtMap()
{
}


template<typename T>
EpochPtPtMap<T>& EpochPtPtMap<T>::operator=(const EpochPtPtMap<T>& ptPtMap)
{
	if (&m_ES != &(ptPtMap.m_ES))
		throw AnError("Cannot assign EpochPtPtMap=EpochPtPtMap when based on different tree instances.", 1);
	if (this != &ptPtMap)
	{
		m_offsets = ptPtMap.m_offsets;
		m_vals = ptPtMap.m_vals;
		m_cache = GenericMatrix< vector<T> >(1, 1);
		m_cacheIsValid = false;
	}
	return *this;
}


template<typename T>
void EpochPtPtMap<T>::reset(const T& defaultVal)
{
	for (unsigned i = 0; i < m_vals.nrows(); ++i)
	{
		for (unsigned j = 0; j < m_vals.ncols(); ++j)
		{
			m_vals(i,j).assign(m_vals(i,j).size(), defaultVal);
		}
	}
}


template<typename T>
void EpochPtPtMap<T>::cache()
{
	m_cache = m_vals;
	m_cacheIsValid = true;
}


template<typename T>
void EpochPtPtMap<T>::restoreCache()
{
	if (m_cacheIsValid)
	{
		m_vals = m_cache;
		m_cacheIsValid = false;
	}
}


template<typename T>
void EpochPtPtMap<T>::invalidateCache()
{
	m_cacheIsValid = false;
}


template<typename T>
string EpochPtPtMap<T>::print() const
{
	ostringstream oss;
	
	for (unsigned i = 0; i < m_vals.nrows(); ++i)
	{
		unsigned e = 0;  // Epoch of i.
		while (i >= m_offsets[e+1]) { ++e; }
		
		for (unsigned j = 0; j < m_vals.ncols(); ++j)
		{
			if (m_vals(i,j).empty()) { continue; }
			unsigned f = 0;  // Epoch of j.
			while (j >= m_offsets[f+1]) { ++f; }	
		
			EpochTime s = EpochTime(e, i-m_offsets[e]);
			EpochTime t = EpochTime(f, j-m_offsets[f]);
			
			// For the moment, we print only for s>=t.
			if (s < t) { continue; }
			
			oss << "# (Epoch.time) to (epoch.time): "
				<< '(' << s.first << '.' << s.second << ") to "
				<< '(' << t.first << '.' << t.second << "):"
				<< endl;
			const vector<T>& v = m_vals(i,j);
			for (unsigned k = 0; k < m_ES[e].getNoOfEdges(); ++k)
			{
				oss << "# ";
				unsigned sz = m_ES[f].getNoOfEdges();
				for (unsigned l = 0; l < sz; ++l)
				{
					oss << v[k * sz + l] << ' ';
				}
				oss << endl;
			}
		}
	}
	return oss.str();
}


///////////////////////// Templates ////////////////////////

template class EpochPtMap<Real>;
template class EpochPtPtMap<Real>;
template class EpochPtMap<Probability>;
template class EpochPtPtMap<Probability>;

} // end namespace beep.
